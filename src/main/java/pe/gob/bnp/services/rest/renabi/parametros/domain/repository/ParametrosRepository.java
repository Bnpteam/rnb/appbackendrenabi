package pe.gob.bnp.services.rest.renabi.parametros.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseRepository;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaNombreBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaTipoBibliotecaHijoDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaTipoBibliotecaPadreDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaUbigeoDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.UbigeoDto;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.ListaMaestra;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.Maestra;

@Repository
public interface ParametrosRepository extends BaseRepository<Maestra>{
	public ListaMaestra listByTableType(String tipoTabla);
	public ListaTipoBibliotecaPadreDto listarTiposBibliotecaPadre();
	public ListaNombreBibliotecaDto listarNombresBiblioteca();
	public ListaTipoBibliotecaHijoDto listarTiposBibliotecaHijo();
	public ListaUbigeoDto listarUbigeos();
	public String getDescripcionLargaTablaMaestra(Long idMaestra);
	public List<UbigeoDto> listarDepartamentos();
	public List<UbigeoDto> listarProvincias(String coddep);
	public List<UbigeoDto> listarDistritos(String coddep, String codpro);
	
}