package pe.gob.bnp.services.rest.renabi.evento.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.Notification;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.evento.application.dto.EventoDto;
import pe.gob.bnp.services.rest.renabi.evento.application.dto.EventoEstadoDto;
import pe.gob.bnp.services.rest.renabi.evento.application.dto.mapper.EventoMapper;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.EventoDepartamento;
import pe.gob.bnp.services.rest.renabi.evento.domain.repository.EventoRepository;

@Service
public class EventoApplicationService {
	@Autowired
	EventoRepository eventoRepository;
	
	@Autowired
	EventoMapper eventoMapper;
	
	public List<EventoDto> obtenerNoticiasVigentes(String estado) {
		Notification notification = this.validation(estado);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		List<EventoDto> response = this.eventoMapper.mapperLista(this.eventoRepository.listarEventosVigentes(estado));
		return response;
	}
	
	public List<EventoDepartamento> obtenerEventosxDepartamento() {
		List<EventoDepartamento> response = this.eventoRepository.listarEventosxDepartamento();
		return response;
	}
		
	public ResponseTx cambiarEstado(EventoEstadoDto evento) {
		Notification notification = this.validation(evento);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}		
		ResponseTx response = this.eventoRepository.cambiarEstado(Util.getStringToLong(evento.getIdEvento()), evento.getIdEstado());
		return response;
	}	
	
	public Notification validation(EventoEstadoDto evento) {
		Notification notification = new Notification();
		if (evento == null) {
			notification.addError("No se encontraron datos del evento");
			return notification;
		}
		
		if (Util.isEmptyString(evento.getIdEstado())) {
			notification.addError("se debe enviar el id del nuevo estado");
		}

		if (Util.isEmptyString(evento.getIdEvento())) {
			notification.addError("se debe enviar el id del evento");
		}

		if (Util.getStringToLong(evento.getIdEvento())<=0L) {
			notification.addError("id de evento no válido");
		}
		
		return notification;
	}	
	
	public Notification validation(String estado) {
		Notification notification = new Notification();
		if (estado == null) {
			notification.addError("Debe ingresar al menos un filtro de búsqueda");
		}
		
		if (Util.isEmptyString(estado)) {
			notification.addError("Debe ingresar al menos un filtro de búsqueda");
		}

		return notification;
	}	
}
