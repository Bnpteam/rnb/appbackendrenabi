package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Tipo;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.TipoValor;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.Registrador;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.Responsable;

public class Biblioteca extends BaseEntity{
	private String numeroRNB;
	private String anioDeclaracion;
	private String nombre;
	private Registrador registrador;
	private Responsable responsable;
	private Entidad entidad;
	private Tipo ambitoTerritorial;
	private Tipo tipoFuncionamiento;
	private String direccion;
	private String correoElectronico;
	private int numeroBibliotecologos;
	private int numeroTotalPersonal;
	private String telefonoFijo;
	private String telefonoMovil;
	private Ubigeo ubigeo;
	private TipoBiblioteca tipoBiblioteca;
	private Tipo estadoBiblioteca;
	private Infraestructura infraestructura;
	private List<TipoValor> colecciones;
	private List<TipoValor> servicios;	
	private AtencionServicio atencionServicio;
	private Date fechaInscripcion;
	private Date fechaConformidad;
	private Date fechaObservacion;
	private int idUsuarioConformidad;
	private String usuarioConformidad;
	private String lectorTotalAnioAnterior;
	private String lectorTotal;
	private String lectorTotalConsultas;
	private String flagGestBibPlanTrabActual;
	private String flagGestBibReglamentos;
	private String flagGestBibEstadisticas;
	private String gestionBibEspecificar;
	private String flagAceptoTyC;
	private String flagAceptoDDJJ;
	
	public Biblioteca() {
		super();
		this.numeroRNB = Constants.EMPTY_STRING;
		this.anioDeclaracion = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
		this.entidad = new Entidad();
		this.ambitoTerritorial = new Tipo();
		this.tipoFuncionamiento = new Tipo();
		this.direccion = Constants.EMPTY_STRING;
		this.correoElectronico = Constants.EMPTY_STRING;
		this.numeroBibliotecologos = Constants.ZERO_INTEGER;
		this.numeroTotalPersonal = Constants.ZERO_INTEGER;
		this.telefonoFijo = Constants.EMPTY_STRING;
		this.telefonoMovil = Constants.EMPTY_STRING;
		this.registrador = new Registrador();
		this.responsable = new Responsable();
		this.ubigeo = new Ubigeo();
		this.tipoBiblioteca = new TipoBiblioteca();
		this.estadoBiblioteca = new Tipo();
		this.infraestructura = new Infraestructura();
		this.colecciones = new ArrayList<>();
		this.servicios = new ArrayList<>();	
		this.atencionServicio = new AtencionServicio();
		this.idUsuarioConformidad = Constants.ZERO_INTEGER;
		this.usuarioConformidad		= Constants.EMPTY_STRING;
		this.lectorTotalAnioAnterior 	= Constants.EMPTY_STRING;
		this.lectorTotal				= Constants.EMPTY_STRING;		
		this.lectorTotalConsultas		= Constants.EMPTY_STRING;	
		this.flagGestBibPlanTrabActual	= Constants.EMPTY_STRING;
		this.flagGestBibReglamentos		= Constants.EMPTY_STRING;
		this.flagGestBibEstadisticas	= Constants.EMPTY_STRING;
		this.gestionBibEspecificar		= Constants.EMPTY_STRING;
		this.flagAceptoTyC = Constants.EMPTY_STRING;
		this.flagAceptoDDJJ = Constants.EMPTY_STRING;
	}
	
	public String getNumeroRNB() {
		return numeroRNB;
	}
	public void setNumeroRNB(String numeroRNB) {
		this.numeroRNB = numeroRNB;
	}
	public String getAnioDeclaracion() {
		return anioDeclaracion;
	}
	public void setAnioDeclaracion(String anioDeclaracion) {
		this.anioDeclaracion = anioDeclaracion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Entidad getEntidad() {
		return entidad;
	}
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}
	public Tipo getAmbitoTerritorial() {
		return ambitoTerritorial;
	}
	public void setAmbitoTerritorial(Tipo ambitoTerritorial) {
		this.ambitoTerritorial = ambitoTerritorial;
	}
	public Tipo getTipoFuncionamiento() {
		return tipoFuncionamiento;
	}
	public void setTipoFuncionamiento(Tipo tipoFuncionamiento) {
		this.tipoFuncionamiento = tipoFuncionamiento;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public int getNumeroBibliotecologos() {
		return numeroBibliotecologos;
	}
	public void setNumeroBibliotecologos(int numeroBibliotecologos) {
		this.numeroBibliotecologos = numeroBibliotecologos;
	}
	public int getNumeroTotalPersonal() {
		return numeroTotalPersonal;
	}
	public void setNumeroTotalPersonal(int numeroTotalPersonal) {
		this.numeroTotalPersonal = numeroTotalPersonal;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public Registrador getRegistrador() {
		return registrador;
	}

	public void setRegistrador(Registrador registrador) {
		this.registrador = registrador;
	}

	public Responsable getResponsable() {
		return responsable;
	}

	public void setResponsable(Responsable responsable) {
		this.responsable = responsable;
	}

	public Ubigeo getUbigeo() {
		return ubigeo;
	}

	public void setUbigeo(Ubigeo ubigeo) {
		this.ubigeo = ubigeo;
	}

	public TipoBiblioteca getTipoBiblioteca() {
		return tipoBiblioteca;
	}

	public void setTipoBiblioteca(TipoBiblioteca tipoBiblioteca) {
		this.tipoBiblioteca = tipoBiblioteca;
	}

	public Tipo getEstadoBiblioteca() {
		return estadoBiblioteca;
	}

	public void setEstadoBiblioteca(Tipo estadoBiblioteca) {
		this.estadoBiblioteca = estadoBiblioteca;
	}

	public Infraestructura getInfraestructura() {
		return infraestructura;
	}

	public void setInfraestructura(Infraestructura infraestructura) {
		this.infraestructura = infraestructura;
	}

	public List<TipoValor> getColecciones() {
		return colecciones;
	}

	public void setColecciones(List<TipoValor> colecciones) {
		this.colecciones = colecciones;
	}

	public List<TipoValor> getServicios() {
		return servicios;
	}

	public void setServicios(List<TipoValor> servicios) {
		this.servicios = servicios;
	}

	public AtencionServicio getAtencionServicio() {
		return atencionServicio;
	}

	public void setAtencionServicio(AtencionServicio atencionServicio) {
		this.atencionServicio = atencionServicio;
	}

	public Date getFechaInscripcion() {
		return fechaInscripcion;
	}

	public void setFechaInscripcion(Date fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}

	public Date getFechaConformidad() {
		return fechaConformidad;
	}

	public void setFechaConformidad(Date fechaConformidad) {
		this.fechaConformidad = fechaConformidad;
	}

	public Date getFechaObservacion() {
		return fechaObservacion;
	}

	public void setFechaObservacion(Date fechaObservacion) {
		this.fechaObservacion = fechaObservacion;
	}

	public int getIdUsuarioConformidad() {
		return idUsuarioConformidad;
	}

	public void setIdUsuarioConformidad(int idUsuarioConformidad) {
		this.idUsuarioConformidad = idUsuarioConformidad;
	}

	public String getUsuarioConformidad() {
		return usuarioConformidad;
	}

	public void setUsuarioConformidad(String usuarioConformidad) {
		this.usuarioConformidad = usuarioConformidad;
	}

	public String getLectorTotalAnioAnterior() {
		return lectorTotalAnioAnterior;
	}

	public void setLectorTotalAnioAnterior(String lectorTotalAnioAnterior) {
		this.lectorTotalAnioAnterior = lectorTotalAnioAnterior;
	}

	public String getLectorTotal() {
		return lectorTotal;
	}

	public void setLectorTotal(String lectorTotal) {
		this.lectorTotal = lectorTotal;
	}

	public String getLectorTotalConsultas() {
		return lectorTotalConsultas;
	}

	public void setLectorTotalConsultas(String lectorTotalConsultas) {
		this.lectorTotalConsultas = lectorTotalConsultas;
	}

	public String getFlagGestBibPlanTrabActual() {
		return flagGestBibPlanTrabActual;
	}

	public void setFlagGestBibPlanTrabActual(String flagGestBibPlanTrabActual) {
		this.flagGestBibPlanTrabActual = flagGestBibPlanTrabActual;
	}

	public String getFlagGestBibReglamentos() {
		return flagGestBibReglamentos;
	}

	public void setFlagGestBibReglamentos(String flagGestBibReglamentos) {
		this.flagGestBibReglamentos = flagGestBibReglamentos;
	}

	public String getFlagGestBibEstadisticas() {
		return flagGestBibEstadisticas;
	}

	public void setFlagGestBibEstadisticas(String flagGestBibEstadisticas) {
		this.flagGestBibEstadisticas = flagGestBibEstadisticas;
	}

	public String getGestionBibEspecificar() {
		return gestionBibEspecificar;
	}

	public void setGestionBibEspecificar(String gestionBibEspecificar) {
		this.gestionBibEspecificar = gestionBibEspecificar;
	}

	public String getFlagAceptoTyC() {
		return flagAceptoTyC;
	}

	public void setFlagAceptoTyC(String flagAceptoTyC) {
		this.flagAceptoTyC = flagAceptoTyC;
	}

	public String getFlagAceptoDDJJ() {
		return flagAceptoDDJJ;
	}

	public void setFlagAceptoDDJJ(String flagAceptoDDJJ) {
		this.flagAceptoDDJJ = flagAceptoDDJJ;
	}

	
}
