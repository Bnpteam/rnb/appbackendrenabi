package pe.gob.bnp.services.rest.renabi.entidad.application.dto.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.EntidadDto;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.NuevaEntidadDto;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;

@Component
public class EntidadMapper {

	public NuevaEntidadDto mapperNuevo(Entidad objeto) {
		NuevaEntidadDto dto = new NuevaEntidadDto();
		dto.setCorreo(objeto.getCorreo());
		dto.setDireccion(objeto.getDireccion());
		dto.setFlagOrigen(objeto.getFlagOrigen());
		dto.setIdCreacionAuditoria(objeto.getIdUsuarioRegistro());
		dto.setIdEstado(objeto.getIdEstado());
		dto.setIdTipoGestion(objeto.getTipoGestion().getId());
		dto.setRazonSocial(objeto.getRazonSocial());
		dto.setRuc(objeto.getRuc());
		dto.setTelefonoFijo(objeto.getTelefonoFijo());
		dto.setTelefonoMovil(objeto.getTelefonoMovil());
		return dto;
	}
	
	public Entidad reverseMapperNuevo(NuevaEntidadDto dto) {
		Entidad objeto = new Entidad();
		objeto.setCorreo(dto.getCorreo());
		objeto.setDireccion(dto.getDireccion());
		objeto.setFlagOrigen(dto.getFlagOrigen());
		objeto.setIdEstado(dto.getIdEstado());
		objeto.setIdUsuarioRegistro(dto.getIdCreacionAuditoria());
		objeto.setRazonSocial(dto.getRazonSocial());
		objeto.setRuc(dto.getRuc());
		objeto.setTelefonoFijo(dto.getTelefonoFijo());
		objeto.setTelefonoMovil(dto.getTelefonoMovil());
		objeto.getTipoGestion().setId(dto.getIdTipoGestion());
		return objeto;
	}

	
	public EntidadDto mapper(Entidad objeto) {
		EntidadDto dto = new EntidadDto();
		dto.setCorreo(objeto.getCorreo());
		dto.setDireccion(objeto.getDireccion());
		dto.setEstado(objeto.getEstado());
		dto.setFechaCreacionAuditoria(Util.utilDateToString(objeto.getFechaRegistro()));
		dto.setFechaModificacionAuditoria(Util.utilDateToString(objeto.getFechaActualizacion()));
		dto.setFlagOrigen(objeto.getFlagOrigen());
		dto.setId(Util.getLongToString(objeto.getId()));
		dto.setIdCreacionAuditoria(objeto.getIdUsuarioRegistro());
		dto.setIdEstado(objeto.getIdEstado());
		dto.setIdModificacionAuditoria(objeto.getIdUsuarioActualizacion());
		dto.setIdTipoGestion(objeto.getTipoGestion().getId());
		dto.setRazonSocial(objeto.getRazonSocial());
		dto.setRuc(objeto.getRuc());
		dto.setTelefonoFijo(objeto.getTelefonoFijo());
		dto.setTelefonoMovil(objeto.getTelefonoMovil());
		dto.setTipoGestion(objeto.getTipoGestion().getNombre());
		return dto;
	}
	
	public Entidad reverseMapper(EntidadDto dto) {
		Entidad objeto = new Entidad();
		objeto.setCorreo(dto.getCorreo());
		objeto.setDireccion(dto.getDireccion());
		objeto.setEstado(dto.getEstado());
		objeto.setFechaActualizacion(Util.stringToUtilDate(dto.getFechaModificacionAuditoria()));
		objeto.setFechaRegistro(Util.stringToUtilDate(dto.getFechaCreacionAuditoria()));
		objeto.setFlagOrigen(dto.getFlagOrigen());
		objeto.setId(Util.getStringToLong(dto.getId()));
		objeto.setIdEstado(dto.getIdEstado());
		objeto.setIdUsuarioActualizacion(dto.getIdModificacionAuditoria());
		objeto.setIdUsuarioRegistro(dto.getIdCreacionAuditoria());
		objeto.setRazonSocial(dto.getRazonSocial());
		objeto.setRuc(dto.getRuc());
		objeto.setTelefonoFijo(dto.getTelefonoFijo());
		objeto.setTelefonoMovil(dto.getTelefonoMovil());
		objeto.getTipoGestion().setNombre(dto.getTipoGestion());
		objeto.getTipoGestion().setId(dto.getIdTipoGestion());
		return objeto;
	}

}
