package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="Biblioteca master con detalle de atención del servicio", description="Muestra información de una Biblioteca master, junto con la información de la última declaración verificada, y con detalle de atención del servicio de la última declaración verificada.")
public class BibliotecaMasterServicioDto {
	@ApiModelProperty(notes = "Id del master",required=true,value="Ej: 324")
	private String idMaster;

	@ApiModelProperty(notes = "Id de la última declaración verificada",required=true,value="Ej: 789")
	private String idDeclaración;

	@ApiModelProperty(notes = "Año de la declaracion",required=true,value="Ej: 2019")
	private String anioDeclaracion;

	@ApiModelProperty(notes = "Número único de biblioteca",required=true,value="Ej: SNB00003")
	private String numeroRNB;
	
	@ApiModelProperty(notes = "Nombre de la biblioteca",required=true,value="Ej: Biblioteca de Madrid")
	private String nombre;

	@ApiModelProperty(notes = "RUC de la entidad",required=true,value="Ej: 20451256874")
	private String rucEntidad;

	@ApiModelProperty(notes = "RUC de la entidad",required=true,value="Ej: Municipalidad de Chiclayo")
	private String razonSocialEntidad;
	
	@ApiModelProperty(notes = "Tipo de biblioteca",required=true,value="Ej: BIBLIOTECAS DE INSTITUCIONES EDUCATIVAS")
	private	String  tipoBibliotecaPadre;

	@ApiModelProperty(notes = "Sub Tipo de biblitoeca",required=true,value="Ej: IE INICIAL")
	private	String  tipoBibliotecaHijo;
	
	//ubigeo
	@ApiModelProperty(notes = "Departamento",required=true,value="Ej: LIMA")
	private String departamento;
	@ApiModelProperty(notes = "Provincia",required=true,value="Ej: LIMA")
	private String provincia;
	@ApiModelProperty(notes = "Distrito",required=true,value="Ej: SAN BORJA")
	private String distrito;
	
	@ApiModelProperty(notes = "Año de la declaracion",required=true,value="Ej: 2019")
	private String centroPoblado;
	@ApiModelProperty(notes = "Dirección de la biblioteca",required=true,value="Ej: Av. Arenales 3435")
	private String direccion;
	
	//Atencion del servicio
	@ApiModelProperty(notes = "Atención del servicio",required=true,value="Ej: Lista")
	private AtencionServicioDto atencionServicio;
	
	public BibliotecaMasterServicioDto(){
		this.atencionServicio = new  AtencionServicioDto();
	}
	

	public String getAnioDeclaracion() {
		return anioDeclaracion;
	}

	public void setAnioDeclaracion(String anioDeclaracion) {
		this.anioDeclaracion = anioDeclaracion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}



	public String getTipoBibliotecaPadre() {
		return tipoBibliotecaPadre;
	}

	public void setTipoBibliotecaPadre(String tipoBibliotecaPadre) {
		this.tipoBibliotecaPadre = tipoBibliotecaPadre;
	}

	public String getTipoBibliotecaHijo() {
		return tipoBibliotecaHijo;
	}

	public void setTipoBibliotecaHijo(String tipoBibliotecaHijo) {
		this.tipoBibliotecaHijo = tipoBibliotecaHijo;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}


	public String getIdMaster() {
		return idMaster;
	}


	public void setIdMaster(String idMaster) {
		this.idMaster = idMaster;
	}


	public String getIdDeclaración() {
		return idDeclaración;
	}


	public void setIdDeclaración(String idDeclaración) {
		this.idDeclaración = idDeclaración;
	}


	public String getRucEntidad() {
		return rucEntidad;
	}


	public void setRucEntidad(String rucEntidad) {
		this.rucEntidad = rucEntidad;
	}


	public String getRazonSocialEntidad() {
		return razonSocialEntidad;
	}


	public void setRazonSocialEntidad(String razonSocialEntidad) {
		this.razonSocialEntidad = razonSocialEntidad;
	}


	public String getCentroPoblado() {
		return centroPoblado;
	}


	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}


	public String getNumeroRNB() {
		return numeroRNB;
	}


	public void setNumeroRNB(String numeroRNB) {
		this.numeroRNB = numeroRNB;
	}


	public AtencionServicioDto getAtencionServicio() {
		return atencionServicio;
	}


	public void setAtencionServicio(AtencionServicioDto atencionServicio) {
		this.atencionServicio = atencionServicio;
	}

	
}
