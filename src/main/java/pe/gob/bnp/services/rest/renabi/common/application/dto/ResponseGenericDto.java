package pe.gob.bnp.services.rest.renabi.common.application.dto;

public class ResponseGenericDto<T> {
	private int httpStatus;
	private ResponseBaseDto response;
	private T payLoad;
	
	public int getHttpStatus() {
		return httpStatus;
	}
	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public T getPayLoad() {
		return payLoad;
	}
	public void setPayLoad(T payLoad) {
		this.payLoad = payLoad;
	}
	public ResponseBaseDto getResponse() {
		return response;
	}
	public void setResponse(ResponseBaseDto response) {
		this.response = response;
	}
	
}
