package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurno;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurnoDia;
import pe.gob.bnp.services.rest.renabi.common.Constants;

public class AtencionServicioTurnoDto{
	private String dia;
	private String valor;
	private List<AtencionServicioTurnoDiaDto> turnos;
	
	public AtencionServicioTurnoDto() {
		super();
		this.dia = Constants.EMPTY_STRING;
		this.turnos = new ArrayList<>();
		this.valor = Constants.EMPTY_STRING;
	}
	
	public AtencionServicioTurnoDto(AtencionServicioTurno objeto) {
		super();
		this.dia = objeto.getDia();
		this.valor = objeto.getValor();
		this.turnos = new ArrayList<>();
		for (AtencionServicioTurnoDia obj : objeto.getTurnos()) {
			this.turnos.add(new AtencionServicioTurnoDiaDto(obj));
		}
		
	}	
	
	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public List<AtencionServicioTurnoDiaDto> getTurnos() {
		return turnos;
	}
	public void setTurnos(List<AtencionServicioTurnoDiaDto> turnos) {
		this.turnos = turnos;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	
}
