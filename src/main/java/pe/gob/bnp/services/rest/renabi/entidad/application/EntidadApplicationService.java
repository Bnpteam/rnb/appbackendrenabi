package pe.gob.bnp.services.rest.renabi.entidad.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.Notification;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.NuevaEntidadDto;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.mapper.EntidadMapper;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;
import pe.gob.bnp.services.rest.renabi.entidad.domain.repository.EntidadRepository;

@Service
public class EntidadApplicationService {
	@Autowired
	EntidadRepository entidadRepository;
	
	@Autowired
	EntidadMapper entidadMapper;
	
	
	public ResponseTx save(NuevaEntidadDto dto) {
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		Entidad entidad = entidadMapper.reverseMapperNuevo(dto);
		return this.entidadRepository.persist(entidad);
	}
	
	public Notification validation(NuevaEntidadDto entidad) {
		Notification notification = new Notification();
		if (entidad == null) {
			notification.addError("No se encontraron datos de la entidad");
		}
		
		if (Util.isEmptyString(entidad.getRazonSocial())) {
			notification.addError("Se debe ingresar la razon social");
		}
		
		if (Util.isEmptyString(entidad.getDireccion())) {
			notification.addError("Se debe ingresar la dirección de la entidad");
		}		

		if (Util.isEmptyString(entidad.getRuc())) {
			notification.addError("Se debe ingresar el RUC");
		}
		
		if (Util.getString(entidad.getRuc()).trim().length()!=11) {
			notification.addError("RUC debe tener 11 dígitos");
		}		
		return notification;
	}

}
