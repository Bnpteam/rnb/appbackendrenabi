package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

public class BibliotecaId {
	private String id;
	private String idUsuario;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
}
