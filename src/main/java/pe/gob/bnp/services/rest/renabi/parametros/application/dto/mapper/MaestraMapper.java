package pe.gob.bnp.services.rest.renabi.parametros.application.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaMaestraDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.MaestraDto;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.ListaMaestra;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.Maestra;

@Component
public class MaestraMapper {

	public MaestraDto mapper(Maestra objeto) {
		MaestraDto dto = new MaestraDto(objeto);
		return dto;
	}
	
	public ListaMaestraDto mapper(ListaMaestra listaObjeto){
		ListaMaestraDto listaDto = new ListaMaestraDto();
		listaDto.getResponse().setCodigoRespuesta(listaObjeto.getCodigoResultado());
		listaDto.getResponse().setRespuesta(listaObjeto.getMensajeResultado());
		List<MaestraDto> lista = new ArrayList<>();
		for (int i = 0; i < listaObjeto.getLista().size(); i++) {
			MaestraDto dto = new MaestraDto(listaObjeto.getLista().get(i));
			lista.add(dto);
		}
		listaDto.setLista(lista);
		return listaDto;
	}
	
}
