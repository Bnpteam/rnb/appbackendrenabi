package pe.gob.bnp.services.rest.renabi.biblioteca.api.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.BibliotecaApplicationService;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaCabeceraDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaEmail;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaImagenEstadoDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaMaster;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaMasterServicioDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ImagenDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ListaEstadistica;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.NuevaBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ObservacionesBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.SubsanarBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.VerBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.mapper.BibliotecaMapper;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Biblioteca;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaId;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaMasterServicio;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaObservada;
import pe.gob.bnp.services.rest.renabi.common.api.controller.ResponseHandler;
import pe.gob.bnp.services.rest.renabi.common.application.EmailApplicationService;
import pe.gob.bnp.services.rest.renabi.common.application.EntityNotFoundResultException;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;

@RestController
@RequestMapping("/api/biblioteca")
@Api(value="/api/biblioteca", description="Servicio REST para Biblioteca - Desarrollado por OTIE/BNP")
@CrossOrigin(origins = "*")
public class BibliotecaController {
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	BibliotecaApplicationService bibliotecaApplicationService;

	@Autowired
	EmailApplicationService  emailApplicationService;	
	
	@Autowired
	BibliotecaMapper bibliotecaMapper;
	
    @Value("${alfresco.ws.uri}")
    private String alfrescoWsUri;
	
	
	@RequestMapping(method = RequestMethod.POST, path="/declaracion", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Registrar declaracion de Biblioteca", response= ResponseTx.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> regitrarDeclaracionBiblioteca(
			@ApiParam(value = "Estructura JSON de la biblioteca", required = true)
			@RequestBody NuevaBibliotecaDto dto) throws Exception {
		try {
			ResponseTx response = bibliotecaApplicationService.save(dto);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppMessageResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	@RequestMapping(method = RequestMethod.POST, path="/declaracionmanual", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Registrar declaracion manual de Biblioteca-Sin RUC", response= ResponseTx.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> regitrarDeclaracionManualBiblioteca(
			@ApiParam(value = "Estructura JSON de la biblioteca", required = true)
			@RequestBody NuevaBibliotecaDto dto) throws Exception {
		try {
			ResponseTx response = bibliotecaApplicationService.saveManual(dto);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppMessageResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/declaraciones",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar declaraciones de bibliotecas para uso interno", response= BibliotecaCabeceraDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> buscarDeclaracionesUsoInterno(
			@RequestParam(value = "nombre", defaultValue = "", required=false) String  nombre,
			@RequestParam(value = "razon_social", defaultValue = "", required=false) String  razonSocial,
			@RequestParam(value = "nro_rnb", defaultValue = "", required=false) String  numeroRNB,
			@RequestParam(value = "cod_dep", defaultValue = "", required=false) String  codDepartamento,
			@RequestParam(value = "cod_pro", defaultValue = "", required=false) String  codProvincia,
			@RequestParam(value = "cod_dis", defaultValue = "", required=false) String  codDistrito,
			@RequestParam(value = "id_estado", defaultValue = "", required=false) String  idEstado,
			@RequestParam(value = "fec_inscr_desde", defaultValue = "", required=false) String  fechaInscripcionDesde,
			@RequestParam(value = "fec_inscr_hasta", defaultValue = "", required=false) String  fechaInscripcionHasta,
			@RequestParam(value = "ruc", defaultValue = "", required=false) String  ruc,
			@RequestParam(value = "direccion", defaultValue = "", required=false) String  direccion){
		try {
			List<BibliotecaCabeceraDto> response = bibliotecaApplicationService.buscarBibliotecaParaVerificacion(
					nombre, razonSocial, numeroRNB, codDepartamento, codProvincia, 
					codDistrito, idEstado, fechaInscripcionDesde, fechaInscripcionHasta, ruc, direccion);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
	
	@RequestMapping(method = RequestMethod.GET, path="/declaracion/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Obtener declaración de biblioteca", response= VerBibliotecaDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> obtenerDeclaracionBiblioteca(
			@ApiParam(value = "id de la biblioteca", required = true)
			@PathVariable Long id){
		try {
			Biblioteca response = bibliotecaApplicationService.obtenerDeclaracionBiblioteca(id);
			if (response == null || response.getId()<=0L ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontró datos de la biblioteca.");
			}
			return this.responseHandler.getOkObjectResponse(this.bibliotecaMapper.mapper(response));
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontró datos de la biblioteca.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
	@RequestMapping(method = RequestMethod.PUT, path="/declaracion/confirmar", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Confirmar Biblioteca", response= ResponseTx.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> confirmarDeclaracionBiblioteca(
			@ApiParam(value = "Id de la biblioteca y el Id del verificardor", required = true)
			@RequestBody BibliotecaId dto) throws Exception {
		try {
			ResponseTx response = bibliotecaApplicationService.confirmarBiblioteca(dto);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT, path="/declaracion/validar", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Iniciar validacion de  Biblioteca", response= ResponseTx.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> iniciarValidacionDeclaracionBiblioteca(
			@ApiParam(value = "Id de la biblioteca y el Id del verificardor", required = true)
			@RequestBody BibliotecaId dto) throws Exception {
		try {
			ResponseTx response = bibliotecaApplicationService.validarBiblioteca(dto);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	@RequestMapping(method = RequestMethod.PUT, path="/declaracion/eliminar", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Eliminar Biblioteca", response= ResponseTx.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> eliminarDeclaracionBiblioteca(
			@ApiParam(value = "Id de la biblioteca y el Id del verificardor", required = true)
			@RequestBody BibliotecaId dto) throws Exception {
		try {
			ResponseTx response = bibliotecaApplicationService.eliminarBiblioteca(dto);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT, path="/declaracion/observar", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Observar Biblioteca", response= ResponseTx.class, responseContainer = "Set",httpMethod = "PUT")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> observarDeclaracionBiblioteca(
			@ApiParam(value = "Id de la biblioteca, el Id del verificardor, mensaje para el registrador", required = true)
			@RequestBody BibliotecaObservada dto) throws Exception {
		try {
			ResponseTx response = bibliotecaApplicationService.observarBiblioteca(dto);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
		
	
	//@RequestMapping(method = RequestMethod.POST, path="/archivos")
	//@PostMapping(value = "/archivos/nuevo")
	@RequestMapping(method = RequestMethod.POST, path="/declaracion/archivosanexos", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Anexar archivo a declaración de biblioteca", response= ResponseTx.class, responseContainer = "Set",httpMethod = "POST")
	//@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<Object> guardarImagenAnexaDeclaracion(
			@ApiParam(value = "Archivo", required = true)
			@RequestParam("file") MultipartFile file,
			@ApiParam(value = "id de la biblioteca", required = true)
			@RequestParam("id") String id,
			@ApiParam(value = "id del registrador", required = true)
			@RequestParam("idRegistrador") String idRegistrador
			) throws IOException {
		try {
			ResponseTx response = bibliotecaApplicationService.guardarArchivo(file, id,idRegistrador);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/declaracion/archivosanexos/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar imagenes anexas de la biblioteca", response= ImagenDto.class, responseContainer = "Set",httpMethod = "GET")
	public ResponseEntity<Object> obtenerImagenesAnexasDeclaracion(@PathVariable("id") long id){
		try {
			List<ImagenDto> response = bibliotecaApplicationService.obtenerImagenesAnexas(id);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path="/estadoImagenes", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Cambiar de estado a imagenes", response= ResponseTx.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> cambiarEstadoImagenes(
			@ApiParam(value = "Datos para actualizar estado imagenes", required = true)
			@RequestBody BibliotecaImagenEstadoDto dto) throws Exception {
		try {
			ResponseTx response = bibliotecaApplicationService.cambiarEstadoImagenes(dto);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppMessageResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Buscar Master de Bibliotecas", response= BibliotecaMaster.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> buscarMasterBibliotecas(
			@RequestParam(value = "nombre", defaultValue = "", required=false) String  nombre,
			@RequestParam(value = "nro_rnb", defaultValue = "", required=false) String  numeroRNB,
			@RequestParam(value = "cod_dep", defaultValue = "", required=false) String  codDepartamento,
			@RequestParam(value = "cod_pro", defaultValue = "", required=false) String  codProvincia,
			@RequestParam(value = "cod_dis", defaultValue = "", required=false) String  codDistrito,
			@RequestParam(value = "id_estado", defaultValue = "", required=false) String  idEstado,
			@RequestParam(value = "fec_inscr_desde", defaultValue = "", required=false) String  fechaInscripcionDesde,
			@RequestParam(value = "fec_inscr_hasta", defaultValue = "", required=false) String  fechaInscripcionHasta,
			@RequestParam(value = "ruc", defaultValue = "", required=false) String  ruc,
			@RequestParam(value = "direccion", defaultValue = "", required=false) String  direccion){
		try {
			List<BibliotecaMaster> response = bibliotecaApplicationService.buscarMasterBibliotecas(
					nombre, numeroRNB, codDepartamento, codProvincia, 
					codDistrito, fechaInscripcionDesde, fechaInscripcionHasta, ruc, direccion);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/master/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Obtener declaración de biblioteca", response= BibliotecaMasterServicioDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> obtenerMasterBiblioteca(
			@ApiParam(value = "id de la biblioteca master", required = true)
			@PathVariable Long id){
		try {
			BibliotecaMasterServicio response = bibliotecaApplicationService.obtenerMasterBiblioteca(id);
			if (response == null || response.getId()<=0L ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontró datos de la biblioteca.");
			}
			return this.responseHandler.getOkObjectResponse(this.bibliotecaMapper.mapper(response));
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontró datos de la biblioteca.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/estadisticas/{tipo}/{anio}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar estadisticas.", response= ListaEstadistica.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarEstadisticas(			
			@ApiParam(value = "Tipo de estadística. valores posibles: ESTADO, MES, DEPARTAMENTO, AMBITO_TERRITORIAL", required = true)
			@PathVariable String tipo,
			@ApiParam(value = "Año de inscripción. valores posibles: el año o el valor TODOS", required = false)
			@PathVariable String anio){
		try {
			ListaEstadistica response = bibliotecaApplicationService.obtenerEstadistica(anio, tipo);
			if (response == null || response.getLista().size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path="/declaracion/corregir", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Corregir declaracion de Biblioteca. Pasará de estado OBSERVADO a POR VALIDAR", response= ResponseTx.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> corregirDeclaracionBiblioteca(
			@ApiParam(value = "Estructura JSON de la biblioteca", required = true)
			@RequestBody SubsanarBibliotecaDto dto) throws Exception {
		try {
			ResponseTx response = bibliotecaApplicationService.subsanarDeclaracion(dto);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppMessageResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/declaracion/aprobada/email", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Envío de email para informar de aprobación de una declaración", response= String.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> enviarEmailDeclaracionAprobada(@RequestBody BibliotecaEmail datos){
		try {
			String resultado = this.emailApplicationService.sendHTMLEmailBibliotecaAprobada(datos);
			return new ResponseEntity<Object>(resultado, HttpStatus.OK);
		}catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	@RequestMapping(method = RequestMethod.POST, path = "/declaracion/observada/email", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Envío de email para informar de la observación de una declaración", response= String.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> enviarEmailDeclaracionObservada(@RequestBody BibliotecaEmail datos){
		try {
			String resultado = this.emailApplicationService.sendHTMLEmailBibliotecaObservada(datos);
			return new ResponseEntity<Object>(resultado, HttpStatus.OK);
		}catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}		
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/declaracion/rechazada/email", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Envío de email para informar del rechazo de una declaración", response= String.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> enviarEmailDeclaracionRechazada(@RequestBody BibliotecaEmail datos){
		try {
			String resultado = this.emailApplicationService.sendHTMLEmailBibliotecaRechazada(datos);
			return new ResponseEntity<Object>(resultado, HttpStatus.OK);
		}catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}		
	
	
	@RequestMapping(method = RequestMethod.GET, path="/declaracion/observaciones/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Obtener el listado histórico de las observaciones realizadas durante la validación de la declaración", response= ObservacionesBibliotecaDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> obtenerObservacionesBiblioteca(
			@ApiParam(value = "id de la biblioteca declarada", required = true)
			@PathVariable Long id){
		try {
			ObservacionesBibliotecaDto response = bibliotecaApplicationService.listarObservacionesBiblioteca(id);
			if (response == null  ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontró información.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontró datos de la biblioteca.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}		
		
}
