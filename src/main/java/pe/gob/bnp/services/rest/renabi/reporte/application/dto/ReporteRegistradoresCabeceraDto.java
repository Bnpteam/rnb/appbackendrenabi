package pe.gob.bnp.services.rest.renabi.reporte.application.dto;

public class ReporteRegistradoresCabeceraDto {
	
	private String id;
	private String rowRegistrador;	
	private String fechaRegistro;
	private String dni;
	private String nombres;	
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String correo;
	private String telefonoFijo;
	private String telefonoCelular;
	private String nroRegistros;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}	
	public String getRowRegistrador() {
		return rowRegistrador;
	}
	public void setRowRegistrador(String rowRegistrador) {
		this.rowRegistrador = rowRegistrador;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTelefonoFijo() {
		return telefonoFijo;
	}
	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}
	public String getTelefonoCelular() {
		return telefonoCelular;
	}
	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}
	public String getNroRegistros() {
		return nroRegistros;
	}
	public void setNroRegistros(String nroRegistros) {
		this.nroRegistros = nroRegistros;
	}
	

}
