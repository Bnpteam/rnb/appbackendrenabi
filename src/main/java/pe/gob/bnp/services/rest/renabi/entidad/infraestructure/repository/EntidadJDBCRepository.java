package pe.gob.bnp.services.rest.renabi.entidad.infraestructure.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseJDBCOperation;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;
import pe.gob.bnp.services.rest.renabi.entidad.domain.repository.EntidadRepository;

@Repository
public class EntidadJDBCRepository extends BaseJDBCOperation implements EntidadRepository {

	@Override
	public ResponseTx persist(Entidad entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_CREAR_ENTIDAD(?,?,?,?,?,?,?,?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_RAZON_SOCIAL", Util.getStringFormatted(entity.getRazonSocial()));
			cstm.setString("E_RUC", Util.getString(entity.getRuc()));
			cstm.setString("E_CORREO", Util.getStringFormatted(entity.getCorreo()));
			cstm.setString("E_DIRECCION", Util.getStringFormatted(entity.getDireccion()));
			cstm.setString("E_TELEFONO_FIJO", Util.getStringFormatted(entity.getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL", Util.getStringFormatted(entity.getTelefonoMovil()));
			cstm.setLong("E_TIPO_GESTION_ID", Util.getStringToLong(entity.getTipoGestion().getId()));
			cstm.setString("E_TIPO_GESTION", Util.getStringFormatted(entity.getTipoGestion().getNombre()));
			cstm.setString("E_FLAG_ORIGEN", Util.getString(entity.getFlagOrigen()));
			cstm.setString("E_ESTADO", Util.getString(entity.getIdEstado()));
			cstm.setLong("E_USUARIO_ID_REGISTRO", Util.getStringToLong(entity.getIdUsuarioRegistro()));
			cstm.registerOutParameter("S_ENTIDAD_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long id=  Util.getLongFromObject(cstm.getObject("S_ENTIDAD_ID"));
				response.setId(String.valueOf(id));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				if (response.getCodigoRespuesta().equals("0001")) {
					response.setRespuesta("Error: El RUC ya se encuentra registrado.");
				}else {
					response.setRespuesta("Error: no se pudo ejecutar la transacción.");
				}
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;		
	}

	@Override
	public Entidad read(Entidad entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Entidad> list(Entidad entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx update(Entidad entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx delete(Entidad entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx persistM(Entidad entity) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
