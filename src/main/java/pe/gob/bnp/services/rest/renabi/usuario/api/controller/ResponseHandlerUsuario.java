package pe.gob.bnp.services.rest.renabi.usuario.api.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import pe.gob.bnp.services.rest.renabi.common.application.dto.ResponseBaseDto;
import pe.gob.bnp.services.rest.renabi.common.application.dto.ResponseGenericDto;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.ResponseLoginPortalDto;

@Component
public class ResponseHandlerUsuario {
	
	public ResponseEntity<Object> getOkResponseGeneric(ResponseBaseDto responseBase, ResponseLoginPortalDto login) {
		ResponseGenericDto<ResponseLoginPortalDto> response= new ResponseGenericDto<ResponseLoginPortalDto>();
		response.setHttpStatus(HttpStatus.OK.value());
		response.setResponse(responseBase);
		response.setPayLoad(login);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}	
	
	public ResponseEntity<Object> getOkResponseGeneric(ResponseGenericDto<ResponseLoginPortalDto> response) {
		response.setHttpStatus(HttpStatus.OK.value());
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}		

}
