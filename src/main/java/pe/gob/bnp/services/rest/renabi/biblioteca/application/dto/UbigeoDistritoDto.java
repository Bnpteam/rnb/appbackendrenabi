package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class UbigeoDistritoDto {
	private String idDistrito;
	private String distrito;
	private String idProvincia;
	private String idDepartamento;
	
	public UbigeoDistritoDto() {
		super();
		this.idDistrito = Constants.EMPTY_STRING;
		this.distrito = Constants.EMPTY_STRING;
		this.idProvincia = Constants.EMPTY_STRING;
		this.idDepartamento = Constants.EMPTY_STRING;
	}

	public String getIdDistrito() {
		return idDistrito;
	}

	public void setIdDistrito(String idDistrito) {
		this.idDistrito = idDistrito;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public String getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(String idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	
	
	
}
