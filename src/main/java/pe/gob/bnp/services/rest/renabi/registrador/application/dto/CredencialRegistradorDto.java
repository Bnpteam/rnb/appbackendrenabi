package pe.gob.bnp.services.rest.renabi.registrador.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Credencial;

@ApiModel(value="credencial", description="Objeto con los datos para realizar la autenticación de un registrador")
public class CredencialRegistradorDto {
	@ApiModelProperty(notes = "DNI del registrador",required=true,value="58696924")	
	private String usuario;
	
	@ApiModelProperty(notes = "Contraseña que registró el registrador",required=true,value="12345")
	private String password;
	
	public CredencialRegistradorDto() {
		super();
		this.usuario = "";
		this.password = "";
	}

	public CredencialRegistradorDto(Credencial objeto) {
		super();
		this.usuario = objeto.getUsuario();
		this.password = objeto.getPassword();
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
}
