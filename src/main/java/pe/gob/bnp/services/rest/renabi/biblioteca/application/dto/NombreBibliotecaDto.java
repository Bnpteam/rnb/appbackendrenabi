package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class NombreBibliotecaDto {
	
	private String rowBiblioteca;	
	private String idBiblioteca;
	private String nombre;
	
	public NombreBibliotecaDto(String rowBiblioteca, String idBiblioteca, String nombre) {
		super();
		this.rowBiblioteca = rowBiblioteca;
		this.idBiblioteca = idBiblioteca;
		this.nombre = nombre;
	}
	
	public NombreBibliotecaDto() {
		super();
		this.rowBiblioteca = Constants.EMPTY_STRING;
		this.idBiblioteca = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
	}
	
	public String getRowBiblioteca() {
		return rowBiblioteca;
	}
	public void setRowBiblioteca(String rowBiblioteca) {
		this.rowBiblioteca = rowBiblioteca;
	}
	public String getIdBiblioteca() {
		return idBiblioteca;
	}
	public void setIdBiblioteca(String idBiblioteca) {
		this.idBiblioteca = idBiblioteca;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
