package pe.gob.bnp.services.rest.renabi.biblioteca.application;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaCabeceraDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaImagenEstadoDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaMaster;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.FiltroBibliotecaVerificacionDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ImagenDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ListaEstadistica;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.NuevaBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ObservacionesBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.SubsanarBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.mapper.BibliotecaMapper;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioMes;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurno;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurnoDia;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Biblioteca;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaId;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaMasterServicio;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaObservada;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.ImagenAnexa;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.repository.BibliotecaRepository;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.Notification;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.TipoValor;
import pe.gob.bnp.services.rest.renabi.common.util.alfresco.AlfrescoDocumentoAdjunto;
import pe.gob.bnp.services.rest.renabi.common.util.alfresco.FileAlfresco;
import pe.gob.bnp.services.rest.renabi.common.util.alfresco.ResultAlfresco;
import pe.gob.bnp.services.rest.renabi.common.util.openkm.FileOpenKM;
import pe.gob.bnp.services.rest.renabi.common.util.openkm.OpenKMDocumento;
import pe.gob.bnp.services.rest.renabi.common.util.openkm.ResultOpenKMDocumento;

@Service
public class BibliotecaApplicationService {
	@Autowired
	BibliotecaRepository bibliotecaRepository;
	
	@Autowired
	BibliotecaMapper bibliotecaMapper;
	

	@Value("${alfresco.repositorio.carpeta}")
	private String rutaAlfresco;
	
	@Value("${alfresco.wsdl}")
    private String alfrescoWsdl;
    
	@Value("${alfresco.endpoint}")
    private String alfrescoEndpoint;

	@Value("${alfresco.ws.uri}")
    private String alfrescoWsUri;

    @Value("${alfresco.service}")
    private String alfrescoService;

    @Value("${alfresco.repositorio.carpeta}")
    private String alfrescoRepositorioCarpeta;

    @Value("${alfresco.codigo}")
    private String alfrescoCodigo;

    @Value("${alfresco.usuario}")
    private String alfrescoUsuario;

    @Value("${alfresco.password}")
    private String alfrescoPassword;
    
    @Value("${alfresco.cantidad.general}")
    private String alfrescoCantidadGeneral;    
	
	
	public List<BibliotecaCabeceraDto> buscarBibliotecaParaVerificacion(
		String  nombre,		String razonSocial, String  numeroRNB,		String  codDepartamento,
		String  codProvincia,		String  codDistrito,		String  idEstado,
		String  fechaInscripcionDesde,		String  fechaInscripcionHasta, String ruc, String direccion){
		FiltroBibliotecaVerificacionDto dto = new FiltroBibliotecaVerificacionDto();
		dto.setNombre(nombre);
		dto.setRazonSocialEntidad(razonSocial);
		dto.setNumeroRNB(numeroRNB);
		dto.setCodDepartamento(codDepartamento);
		dto.setCodProvincia(codProvincia);
		dto.setCodDistrito(codDistrito);
		dto.setIdEstado(idEstado);
		dto.setFechaInscripcionDesde(fechaInscripcionDesde);
		dto.setFechaInscripcionHasta(fechaInscripcionHasta);
		dto.setRuc(ruc);
		dto.setDireccion(direccion);
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}		
		return this.bibliotecaRepository.listarBibliotecasPorVerificador(dto);
	}
	
	public Notification validation(FiltroBibliotecaVerificacionDto dto) {
		Notification notification = new Notification();
		if (dto == null) {
			notification.addError("No se encontró el filtro de búsqueda");
			return notification;
		}
		/*if (dto.getIdEstado() == null || dto.getIdEstado().equals("")) {
			notification.addError("El id del estado no debe estar vacío");
			return notification;
		}*/		

		return notification;
	}	
	
	public Biblioteca obtenerDeclaracionBiblioteca(Long id) {
		Notification notification = new Notification();
		notification = this.validation(id);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return this.bibliotecaRepository.obtenerBiblioteca(id);
	}

	//confirmar
	public ResponseTx confirmarBiblioteca(BibliotecaId bib) {
		Notification notification = new Notification();
		notification = this.validation(bib);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return this.setMessageResponseConfirmar(this.bibliotecaRepository.confirmarBiblioteca(bib));
	}
	
	public ResponseTx setMessageResponseConfirmar(ResponseTx response) {
		if (response.getCodigoRespuesta().equals("0000")) {
			response.setRespuesta("Se realizó el cambio de estado (confirmación)de forma exitosa");
		}
		if (response.getCodigoRespuesta().equals("0001")) {
			response.setRespuesta("Error: Estado actual de biblioteca no permite realizar la solicitud.");
		}			
		return response;		
	}	
	
	
	//eliminar
	public ResponseTx eliminarBiblioteca(BibliotecaId bib) {
		Notification notification = new Notification();
		notification = this.validation(bib);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return this.setMessageResponseEliminar(this.bibliotecaRepository.eliminarBiblioteca(bib));
	}
	
	
	public ResponseTx setMessageResponseEliminar(ResponseTx response) {
		if (response.getCodigoRespuesta().equals("0000")) {
			response.setRespuesta("Se realizó el cambio de estado (confirmación)de forma exitosa");
		}
		if (response.getCodigoRespuesta().equals("0001")) {
			response.setRespuesta("Error: Estado actual de biblioteca no permite realizar la solicitud.");
		}	
		if (response.getCodigoRespuesta().equals("0002")) {
			response.setRespuesta("Error: No se encontró la biblioteca para el id enviado.");
		}			
		return response;		
	}	

	

	
	// observar declaracion
	public ResponseTx observarBiblioteca(BibliotecaObservada bib) {
		Notification notification = new Notification();
		notification = this.validation(bib);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return this.setMessageResponseObservar(this.bibliotecaRepository.observarBiblioteca(bib));
	}		
	
	public ResponseTx setMessageResponseObservar(ResponseTx response) {
		if (response.getCodigoRespuesta().equals("0000")) {
			response.setRespuesta("Se realizó el cambio de estado (Observada) de forma exitosa");
		}
		if (response.getCodigoRespuesta().equals("0001")) {
			response.setRespuesta("Error: Estado actual de biblioteca no permite realizar la solicitud.");
		}	
		if (response.getCodigoRespuesta().equals("0002")) {
			response.setRespuesta("Error: No se encontró la biblioteca para el id enviado.");
		}			
		return response;		
	}	
	
	public Notification validation(BibliotecaObservada bib) {
		Notification notification = new Notification();
		if (bib.getId().equals("")) {
			notification.addError("El id de la biblioteca debe ser mayor a cero");
			return notification;
		}	
		if (bib.getIdUsuario().equals("")) {
			notification.addError("El id del usuario debe ser mayor a cero");
			return notification;
		}			
		if (Util.getString(bib.getMensaje()).equals("")) {
			notification.addError("El mensaje no debe estar vacío");
			return notification;
		}			
		return notification;
	}
	
	
	//validar declaracion
	public ResponseTx validarBiblioteca(BibliotecaId bib) {
		Notification notification = new Notification();
		notification = this.validation(bib);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return this.setMessageResponseInicioValidacion(this.bibliotecaRepository.iniciarValidacionBiblioteca(bib));
	}		
	
	public ResponseTx setMessageResponseInicioValidacion(ResponseTx response) {
		if (response.getCodigoRespuesta().equals("0000")) {
			response.setRespuesta("Se realizó el cambio de estado (Inicio de validación) de forma exitosa");
		}
		if (response.getCodigoRespuesta().equals("0001")) {
			response.setRespuesta("Error: Estado actual de biblioteca no permite realizar la solicitud.");
		}			
		return response;		
	}	
	
	
	public Notification validation(BibliotecaId bib) {
		Notification notification = new Notification();
		if (bib.getId().equals("")) {
			notification.addError("El id de la biblioteca debe ser mayor a cero");
			return notification;
		}	
		if (bib.getIdUsuario().equals("")) {
			notification.addError("El id del usuario debe ser mayor a cero");
			return notification;
		}			
		return notification;
	}		
	
	public Notification validation(Long id) {
		Notification notification = new Notification();
		if (id <=0L) {
			notification.addError("El id debe ser mayor a cero");
			return notification;
		}		
		return notification;
	}	

	
	public ResponseTx save(NuevaBibliotecaDto dto) {
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		Biblioteca biblioteca = bibliotecaMapper.reverseMapperNuevo(dto);
		ResponseTx response = this.bibliotecaRepository.persist(biblioteca);
		if (response!= null && response.getCodigoRespuesta()!=null && response.getCodigoRespuesta().equals("0000")){
			//atencion servicio
			ResponseTx resp = this.bibliotecaRepository.guardarAtencionServicio(Util.getStringToLong(response.getId()), biblioteca.getAtencionServicio(), Util.getStringToLong(biblioteca.getIdUsuarioRegistro()));
			if (resp!= null && resp.getCodigoRespuesta()!=null && resp.getCodigoRespuesta().equals("0000")){
				//atencion servicio mes
				for (AtencionServicioMes mes : biblioteca.getAtencionServicio().getMeses()) {
					this.bibliotecaRepository.guardarAtencionServicioMes(Util.getStringToLong(resp.getId()), mes,Util.getStringToLong(biblioteca.getIdUsuarioRegistro()) );
				}
				//atencion servicio turno
				for (AtencionServicioTurno dia : biblioteca.getAtencionServicio().getDias()) {
					ResponseTx respAtSerTurno = this.bibliotecaRepository.guardarAtencionServicioTurno(Util.getStringToLong(resp.getId()), dia,Util.getStringToLong(biblioteca.getIdUsuarioRegistro()) );
					//atencion servivio dia
					if (respAtSerTurno!= null && respAtSerTurno.getCodigoRespuesta()!=null && respAtSerTurno.getCodigoRespuesta().equals("0000")){
						for (AtencionServicioTurnoDia turno : dia.getTurnos()) {
							this.bibliotecaRepository.guardarAtencionServicioTurnoDia(Util.getStringToLong(respAtSerTurno.getId()), turno, Util.getStringToLong(biblioteca.getIdUsuarioRegistro()));
						}						
					}
				}				
				
			}
			
			
			//accesibilidad
			for (TipoValor tipoValor : biblioteca.getInfraestructura().getAccesibilidad()) {
				this.bibliotecaRepository.guardarDetalleBiblioteca(
						"ACCESIBILIDAD", 
						Util.getStringToLong(response.getId()), 
						Util.getStringToLong(biblioteca.getIdUsuarioRegistro()), 
						tipoValor);
			}
			//colecciones
			for (TipoValor tipoValor : biblioteca.getColecciones()) {
				this.bibliotecaRepository.guardarDetalleBiblioteca(
						"COLECCIONES", 
						Util.getStringToLong(response.getId()), 
						Util.getStringToLong(biblioteca.getIdUsuarioRegistro()), 
						tipoValor);
			}
			//servicios
			for (TipoValor tipoValor : biblioteca.getServicios()) {
				this.bibliotecaRepository.guardarDetalleBiblioteca(
						"SERVICIOS", 
						Util.getStringToLong(response.getId()), 
						Util.getStringToLong(biblioteca.getIdUsuarioRegistro()), 
						tipoValor);
			}
		}
		return this.setMessageResponseSave(response);
		
	}
	
	public ResponseTx saveManual(NuevaBibliotecaDto dto) {
		Notification notification = this.validationManual(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		Biblioteca biblioteca = bibliotecaMapper.reverseMapperNuevoManual(dto);
		ResponseTx response = this.bibliotecaRepository.persistM(biblioteca);
		if (response!= null && response.getCodigoRespuesta()!=null && response.getCodigoRespuesta().equals("0000")){
			//atencion servicio
			ResponseTx resp = this.bibliotecaRepository.guardarAtencionServicio(Util.getStringToLong(response.getId()), biblioteca.getAtencionServicio(), Util.getStringToLong(biblioteca.getIdUsuarioRegistro()));
			if (resp!= null && resp.getCodigoRespuesta()!=null && resp.getCodigoRespuesta().equals("0000")){
				//atencion servicio mes
				for (AtencionServicioMes mes : biblioteca.getAtencionServicio().getMeses()) {
					this.bibliotecaRepository.guardarAtencionServicioMes(Util.getStringToLong(resp.getId()), mes,Util.getStringToLong(biblioteca.getIdUsuarioRegistro()) );
				}
				//atencion servicio turno
				for (AtencionServicioTurno dia : biblioteca.getAtencionServicio().getDias()) {
					ResponseTx respAtSerTurno = this.bibliotecaRepository.guardarAtencionServicioTurno(Util.getStringToLong(resp.getId()), dia,Util.getStringToLong(biblioteca.getIdUsuarioRegistro()) );
					//atencion servivio dia
					if (respAtSerTurno!= null && respAtSerTurno.getCodigoRespuesta()!=null && respAtSerTurno.getCodigoRespuesta().equals("0000")){
						for (AtencionServicioTurnoDia turno : dia.getTurnos()) {
							this.bibliotecaRepository.guardarAtencionServicioTurnoDia(Util.getStringToLong(respAtSerTurno.getId()), turno, Util.getStringToLong(biblioteca.getIdUsuarioRegistro()));
						}						
					}
				}				
				
			}			
			
			//accesibilidad
			for (TipoValor tipoValor : biblioteca.getInfraestructura().getAccesibilidad()) {
				this.bibliotecaRepository.guardarDetalleBiblioteca(
						"ACCESIBILIDAD", 
						Util.getStringToLong(response.getId()), 
						Util.getStringToLong(biblioteca.getIdUsuarioRegistro()), 
						tipoValor);
			}
			//colecciones
			for (TipoValor tipoValor : biblioteca.getColecciones()) {
				this.bibliotecaRepository.guardarDetalleBiblioteca(
						"COLECCIONES", 
						Util.getStringToLong(response.getId()), 
						Util.getStringToLong(biblioteca.getIdUsuarioRegistro()), 
						tipoValor);
			}
			//servicios
			for (TipoValor tipoValor : biblioteca.getServicios()) {
				this.bibliotecaRepository.guardarDetalleBiblioteca(
						"SERVICIOS", 
						Util.getStringToLong(response.getId()), 
						Util.getStringToLong(biblioteca.getIdUsuarioRegistro()), 
						tipoValor);
			}
		}
		return this.setMessageResponseSave(response);
		
	}
	
	public ResponseTx setMessageResponseSave(ResponseTx response) {
		if (response.getCodigoRespuesta().equals("9999")) {
			response.setRespuesta("Error: No se ejecutó ninguna transacción.");
		}
		if (response.getCodigoRespuesta().equals("0001")) {
			response.setRespuesta("Error: No se pudo registrar la entidad.");
		}
		if (response.getCodigoRespuesta().equals("0002")) {
			response.setRespuesta("Error: No se pudo registrar el responsable.");
		}	
		if (response.getCodigoRespuesta().equals("0003")) {
			response.setRespuesta("Error: Nombre y/o Ubigeo de la biblioteca ya se encuentra registrado.");
		}					
		if (response.getCodigoRespuesta().equals("0004")) {
			response.setRespuesta("Error: Ambito territorial no encontrado.");
		}
		if (response.getCodigoRespuesta().equals("0005")) {
			response.setRespuesta("Error: Tipo de funcionamiento no encontrado.");
		}					
		if (response.getCodigoRespuesta().equals("0006")) {
			response.setRespuesta("Error: Tipo de estado de biblioteca no encontrado.");
		}					
		if (response.getCodigoRespuesta().equals("0007")) {
			response.setRespuesta("Error: Condición laboral no encontrada.");
		}					
		if (response.getCodigoRespuesta().equals("0008")) {
			response.setRespuesta("Error: Grado de instrucción no encontrada.");
		}					
		if (response.getCodigoRespuesta().equals("0009")) {
			response.setRespuesta("Error: Tipo de gestión no encontrada.");
		}					
		if (response.getCodigoRespuesta().equals("0010")) {
			response.setRespuesta("Error: Departamento no encontrado.");
		}					
		if (response.getCodigoRespuesta().equals("0011")) {
			response.setRespuesta("Error: Provincia no encontrada.");
		}					
		if (response.getCodigoRespuesta().equals("0012")) {
			response.setRespuesta("Error: Distrito no encontrado.");
		}					
		if (response.getCodigoRespuesta().equals("0013")) {
			response.setRespuesta("Error: Sub tipo de biblioteca no encontrado.");
		}					
		if (response.getCodigoRespuesta().equals("0014")) {
			response.setRespuesta("Error: Tipo de biblioteca no encontrado.");
		}	
		if (response.getCodigoRespuesta().equals("0015")) {
			response.setRespuesta("Error: Tipo de Local (Infraestructura) no encontrado.");
		}		
		if (response.getCodigoRespuesta().equals("0016")) {
			response.setRespuesta("Error: Datos de Registrador no encontrado.");
		}	
		if (response.getCodigoRespuesta().equals("0017")) {
			response.setRespuesta("Error: La biblioteca ya se encuentra registrada para el año de declaración.");
		}			
		if (response.getCodigoRespuesta().equals("0018")) {
			response.setRespuesta("Error: La biblioteca ya fue verificada anteriormente. Se debe usar la opción de actualización.");
		}
		if (response.getCodigoRespuesta().equals("0019")) {
			response.setRespuesta("Error: La biblioteca se encuentra en proceso de verificación para otro año de declaración. Debe finalizar dicho proceso y luego realizar la nueva declaración.");
		}			
		if (response.getCodigoRespuesta().equals("0020")) {
			response.setRespuesta("Error: No se puede realizar la actualización: Los datos clave (nombre, ubigeo) no coincide con el id de la biblioteca seleccionada.");
		}			
		if (response.getCodigoRespuesta().equals("0021")) {
			response.setRespuesta("Error: No se puede realizar la actualización: La declaración previa aún sigue en proceso de verificación.");
		}			
		if (response.getCodigoRespuesta().equals("0022")) {
			response.setRespuesta("Error: No se puede realizar la actualización: El id de biblioteca no pertenece a ninguna biblioteca registrada.");
		}	
		if (response.getCodigoRespuesta().equals("0023")) {
			response.setRespuesta("Error: Estado de biblioteca no válido.");
		}		
		return response;		
	}
	
	public Notification validation(NuevaBibliotecaDto dto) {
		Notification notification = new Notification();
		if (dto == null) {
			notification.addError("No se encontraron datos de la biblioteca");
			return notification;
		}
		if (dto.getEntidad() == null) {
			notification.addError("No se encontraron datos de la entidad");
			return notification;
		}		
		
		//Entidad
		if (Util.isEmptyString(dto.getEntidad().getRazonSocial())) {
			notification.addError("Se debe ingresar el nombre de la entidad");
		}
		if (Util.isEmptyString(dto.getEntidad().getRuc())) {
			notification.addError("Se debe ingresar el ruc de la entidad");
		}
		if (Util.getString(dto.getEntidad().getRuc()).length()!=11) {
			notification.addError("El ruc de la entidad debe tener 11 dígitos");
		}
		if (Util.isEmptyString(dto.getEntidad().getIdTipoGestion()) || Util.getStringToLong(dto.getEntidad().getIdTipoGestion())<=0L) {
			notification.addError("Se debe seleccionar el tipo de gestión de la entidad");
		}
		
		//Datos generales de la biblioteca
		if (Util.isEmptyString(dto.getNombre())) {
			notification.addError("Se debe ingresar el nombre de la biblioteca");
		}

		if (Util.isEmptyString(dto.getIdAmbitoTerritorial()) || Util.getStringToLong(dto.getIdAmbitoTerritorial())<=0L) {
			notification.addError("Se debe seleccionar el ámbito territorial de la biblioteca");
		}
		
		if (Util.isEmptyString(dto.getIdTipoFuncionamiento()) || Util.getStringToLong(dto.getIdTipoFuncionamiento())<=0L) {
			notification.addError("Se debe seleccionar el tipo de funcionamiento de la biblioteca");
		}		
		
		if (Util.isEmptyString(dto.getIdRegistrador()) || Util.getStringToLong(dto.getIdRegistrador())<=0L) {
			notification.addError("Id de registrador no válido");
		}		
		
		/*
		if (Util.isEmptyString(dto.getIdTipoBibliotecaHijo()) || Util.getStringToLong(dto.getIdTipoBibliotecaHijo())<=0L) {
			notification.addError("Id de subtipo de biblioteca no válido");
		}		*/

		if (Util.isEmptyString(dto.getIdTipoBibliotecaPadre()) || Util.getStringToLong(dto.getIdTipoBibliotecaPadre())<=0L) {
			notification.addError("Id de tipo de biblioteca no válido");
		}		

		if (Util.isEmptyString(dto.getCodDepartamento())) {
			notification.addError("Código de departamento no válido");
		}			

		if (Util.isEmptyString(dto.getCodProvincia())) {
			notification.addError("Código de provincia no válido");
		}			

		if (Util.isEmptyString(dto.getCodDistrito())) {
			notification.addError("Código de distrito no válido");
		}			
		
		if (Util.isEmptyString(dto.getDireccion()) ) {
			notification.addError("Se debe ingresar la dirección de la biblioteca");
		}			
		
		if (!Util.isEmptyString(dto.getDireccion()) && Util.getString(dto.getDireccion()).length()<=10 ) {
			notification.addError("Debe tener al menos 10 caracteres la dirección");
		}	
		
		if (Util.isEmptyString(dto.getFlagAceptoDDJJ()) || !Util.getString(dto.getFlagAceptoDDJJ()).equals("SI")) {
			notification.addError("Se debe aceptar la declaración jurada");
		}
		
		if (Util.isEmptyString(dto.getFlagAceptoTyC()) || !Util.getString(dto.getFlagAceptoTyC()).equals("SI")) {
			notification.addError("Se debe aceptar los términos y condiciones");
		}		
		
		if (dto.getResponsable() == null) {
			notification.addError("No se encontraron datos del responsable");
			return notification;
		}		

		if (Util.isEmptyString(dto.getResponsable().getNombres()) || Util.getString(dto.getResponsable().getNombres()).equals("")) {
			notification.addError("Se debe ingresar el nombre del responsable");
			return notification;
		}		

		if (Util.isEmptyString(dto.getResponsable().getApellidoPaterno()) || Util.getString(dto.getResponsable().getApellidoPaterno()).equals("")) {
			notification.addError("Se debe ingresar el apellido paterno del responsable");
			return notification;
		}		

		if (Util.isEmptyString(dto.getResponsable().getApellidoMaterno()) || Util.getString(dto.getResponsable().getApellidoMaterno()).equals("")) {
			notification.addError("Se debe ingresar el apellido materno del responsable");
			return notification;
		}		
		
		if (Util.isEmptyString(dto.getResponsable().getIdTipoDocumento()) || Util.getString(dto.getResponsable().getIdTipoDocumento()).equals("")) {
			notification.addError("Se debe ingresar el tipo de documento de identidad del responsable");
			return notification;
		}			

		if (Util.isEmptyString(dto.getResponsable().getNumeroDocumento()) || Util.getString(dto.getResponsable().getNumeroDocumento()).equals("")) {
			notification.addError("Se debe ingresar el número de documento de identidad del responsable");
			return notification;
		}			

		if (Util.isEmptyString(dto.getResponsable().getGenero()) || (!Util.getString(dto.getResponsable().getGenero()).equals("MASCULINO") && !Util.getString(dto.getResponsable().getGenero()).equals("FEMENINO"))) {
			notification.addError("Se debe ingresar el género: FEMENINO o MASCULINO");
			return notification;
		}	
		
		if (Util.isEmptyString(dto.getResponsable().getCorreo()) || Util.getString(dto.getResponsable().getCorreo()).equals("")) {
			notification.addError("Se debe ingresar el correo electrónico del responsable");
			return notification;
		}			
		
		if (Util.isEmptyString(dto.getResponsable().getIdGradoInstruccion()) || Util.getStringToLong(dto.getResponsable().getIdGradoInstruccion())<=0L ) {
			notification.addError("Se debe seleccionar el grado de instrucción del responsable");
		}			
		
		//if (Util.isEmptyString(dto.getResponsable().getProfesion()) || Util.getString(dto.getResponsable().getProfesion()).equals("")) {
		//	notification.addError("Se debe ingresar la profesión del responsable");
		//	return notification;
		//}
		
		if (Util.isEmptyString(dto.getResponsable().getIdCondicionLaboral()) || Util.getStringToLong(dto.getResponsable().getIdCondicionLaboral())<=0L ) {
			notification.addError("Se debe seleccionar la condición laboral del responsable");
		}	
		
		if (Util.isEmptyString(dto.getResponsable().getDireccion()) || Util.getString(dto.getResponsable().getDireccion()).equals("")) {
			notification.addError("Se debe ingresar la dirección del responsable");
			return notification;
		}		
		
		//infraestructura
		return notification;
	}
	
	public Notification validationManual(NuevaBibliotecaDto dto) {
		Notification notification = new Notification();
		if (dto == null) {
			notification.addError("No se encontraron datos de la biblioteca");
			return notification;
		}
		if (dto.getEntidad() == null) {
			notification.addError("No se encontraron datos de la entidad");
			return notification;
		}		
		
		//Entidad
		if (Util.isEmptyString(dto.getEntidad().getRazonSocial())) {
			notification.addError("Se debe ingresar el nombre de la entidad");
		}
	
		if (Util.isEmptyString(dto.getEntidad().getIdTipoGestion()) || Util.getStringToLong(dto.getEntidad().getIdTipoGestion())<=0L) {
			notification.addError("Se debe seleccionar el tipo de gestión de la entidad");
		}
		
		//Datos generales de la biblioteca
		if (Util.isEmptyString(dto.getNombre())) {
			notification.addError("Se debe ingresar el nombre de la biblioteca");
		}

		if (Util.isEmptyString(dto.getIdAmbitoTerritorial()) || Util.getStringToLong(dto.getIdAmbitoTerritorial())<=0L) {
			notification.addError("Se debe seleccionar el ámbito territorial de la biblioteca");
		}
		
		if (Util.isEmptyString(dto.getIdTipoFuncionamiento()) || Util.getStringToLong(dto.getIdTipoFuncionamiento())<=0L) {
			notification.addError("Se debe seleccionar el tipo de funcionamiento de la biblioteca");
		}		
		
		if (Util.isEmptyString(dto.getIdRegistrador()) || Util.getStringToLong(dto.getIdRegistrador())<=0L) {
			notification.addError("Id de registrador no válido");
		}		
		
		/*
		if (Util.isEmptyString(dto.getIdTipoBibliotecaHijo()) || Util.getStringToLong(dto.getIdTipoBibliotecaHijo())<=0L) {
			notification.addError("Id de subtipo de biblioteca no válido");
		}		*/

		if (Util.isEmptyString(dto.getIdTipoBibliotecaPadre()) || Util.getStringToLong(dto.getIdTipoBibliotecaPadre())<=0L) {
			notification.addError("Id de tipo de biblioteca no válido");
		}		

		if (Util.isEmptyString(dto.getCodDepartamento())) {
			notification.addError("Código de departamento no válido");
		}			

		if (Util.isEmptyString(dto.getCodProvincia())) {
			notification.addError("Código de provincia no válido");
		}			

		if (Util.isEmptyString(dto.getCodDistrito())) {
			notification.addError("Código de distrito no válido");
		}			
		
		if (Util.isEmptyString(dto.getDireccion()) ) {
			notification.addError("Se debe ingresar la dirección de la biblioteca");
		}			
		
		if (!Util.isEmptyString(dto.getDireccion()) && Util.getString(dto.getDireccion()).length()<=10 ) {
			notification.addError("Debe tener al menos 10 caracteres la dirección");
		}	
		
		if (Util.isEmptyString(dto.getFlagAceptoDDJJ()) || !Util.getString(dto.getFlagAceptoDDJJ()).equals("SI")) {
			notification.addError("Se debe aceptar la declaración jurada");
		}
		
		if (Util.isEmptyString(dto.getFlagAceptoTyC()) || !Util.getString(dto.getFlagAceptoTyC()).equals("SI")) {
			notification.addError("Se debe aceptar los términos y condiciones");
		}		
		
		if (dto.getResponsable() == null) {
			notification.addError("No se encontraron datos del responsable");
			return notification;
		}		

		if (Util.isEmptyString(dto.getResponsable().getNombres()) || Util.getString(dto.getResponsable().getNombres()).equals("")) {
			notification.addError("Se debe ingresar el nombre del responsable");
			return notification;
		}		

		if (Util.isEmptyString(dto.getResponsable().getApellidoPaterno()) || Util.getString(dto.getResponsable().getApellidoPaterno()).equals("")) {
			notification.addError("Se debe ingresar el apellido paterno del responsable");
			return notification;
		}		

		if (Util.isEmptyString(dto.getResponsable().getApellidoMaterno()) || Util.getString(dto.getResponsable().getApellidoMaterno()).equals("")) {
			notification.addError("Se debe ingresar el apellido materno del responsable");
			return notification;
		}		
		
		if (Util.isEmptyString(dto.getResponsable().getIdTipoDocumento()) || Util.getString(dto.getResponsable().getIdTipoDocumento()).equals("")) {
			notification.addError("Se debe ingresar el tipo de documento de identidad del responsable");
			return notification;
		}			

		if (Util.isEmptyString(dto.getResponsable().getNumeroDocumento()) || Util.getString(dto.getResponsable().getNumeroDocumento()).equals("")) {
			notification.addError("Se debe ingresar el número de documento de identidad del responsable");
			return notification;
		}			

		if (Util.isEmptyString(dto.getResponsable().getGenero()) || (!Util.getString(dto.getResponsable().getGenero()).equals("MASCULINO") && !Util.getString(dto.getResponsable().getGenero()).equals("FEMENINO"))) {
			notification.addError("Se debe ingresar el género: FEMENINO o MASCULINO");
			return notification;
		}	
		
		if (Util.isEmptyString(dto.getResponsable().getCorreo()) || Util.getString(dto.getResponsable().getCorreo()).equals("")) {
			notification.addError("Se debe ingresar el correo electrónico del responsable");
			return notification;
		}			
		
		if (Util.isEmptyString(dto.getResponsable().getIdGradoInstruccion()) || Util.getStringToLong(dto.getResponsable().getIdGradoInstruccion())<=0L ) {
			notification.addError("Se debe seleccionar el grado de instrucción del responsable");
		}			
		
		//if (Util.isEmptyString(dto.getResponsable().getProfesion()) || Util.getString(dto.getResponsable().getProfesion()).equals("")) {
		//	notification.addError("Se debe ingresar la profesión del responsable");
		//	return notification;
		//}
		
		if (Util.isEmptyString(dto.getResponsable().getIdCondicionLaboral()) || Util.getStringToLong(dto.getResponsable().getIdCondicionLaboral())<=0L ) {
			notification.addError("Se debe seleccionar la condición laboral del responsable");
		}	
		
		if (Util.isEmptyString(dto.getResponsable().getDireccion()) || Util.getString(dto.getResponsable().getDireccion()).equals("")) {
			notification.addError("Se debe ingresar la dirección del responsable");
			return notification;
		}		
		
		//infraestructura
		return notification;
	}
	
	//private static final String FILE_DIRECTORY = "D:/test";
	 
	public ResponseTx guardarArchivo(MultipartFile file, String idBiblioteca, String idRegistrador) throws IOException {
		//Path filePath = Paths.get(FILE_DIRECTORY + "/" + file.getOriginalFilename());
		byte[] contenido = file.getBytes();	
		ResponseTx responseFile = new ResponseTx();		
    	
    	ResultOpenKMDocumento resultOpenKMDocumento = new ResultOpenKMDocumento();
		OpenKMDocumento openKMDocumento = new OpenKMDocumento();
		FileOpenKM fileOpenKM = new FileOpenKM();    	
		Date fechaModificacion = new Date(); 
		
		//properties recargas
		int maxRecargas=3;
		int contadorRecargas=0;

		fileOpenKM.setFileName(Util.getUniqueValueUsingDateTime()+file.getOriginalFilename());
		fileOpenKM.setFechaDocumento(fechaModificacion);
		fileOpenKM.setRawFile(contenido);
		fileOpenKM.setExtension(file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1));
		fileOpenKM.setIdBiblioteca(idBiblioteca);
    	
    	
		//System.out.println("guardar archivo");		
		resultOpenKMDocumento = openKMDocumento.registrarContenido(fileOpenKM);
		
		if (resultOpenKMDocumento==null || !resultOpenKMDocumento.getCodigo().equals("00000")) {
			while(contadorRecargas<maxRecargas) {
				resultOpenKMDocumento = openKMDocumento.registrarContenido(fileOpenKM);
				if(resultOpenKMDocumento!=null && resultOpenKMDocumento.getCodigo().equals("00000")) {
					responseFile=this.bibliotecaRepository.guardarArchivoAdjunto(
							resultOpenKMDocumento, fileOpenKM.getFileName(), 
							fileOpenKM.getExtension(), Util.getStringToLong(idBiblioteca), 
							Util.getStringToLong(idRegistrador));			
					break;
				}
				contadorRecargas++;
			}
			
			
			
		}else {
			//ResponseTx responseFile = new ResponseTx();
			responseFile=this.bibliotecaRepository.guardarArchivoAdjunto(
					resultOpenKMDocumento, fileOpenKM.getFileName(), 
					fileOpenKM.getExtension(), Util.getStringToLong(idBiblioteca), 
					Util.getStringToLong(idRegistrador));			
		}	
			
		/*Notification notification = new Notification();
		notification = this.validation(resultOpenKMDocumento);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}*/
		if(resultOpenKMDocumento==null || !resultOpenKMDocumento.getCodigo().equals("00000")) {
			//System.out.println("error al obtener las imagenes ............");
			//notification.addError("error al obtener las imagenes ............");//resultOpenKMDocumento.getDescripcion()
			throw new IllegalArgumentException("error al registrar imagenes en el repositorio");
		}
		
		//System.out.println("inicio guardado bd");
		return responseFile;
	}	
	

	Notification validation(ResultAlfresco resultado) {
		Notification notification = new Notification();
		if (resultado== null || resultado.getCodigo()==null) {
			notification.addError("Archivo no se pudo guardar en el repositorio.");
			return notification;
		}
		if (!resultado.getCodigo().equals("00000")) {
			notification.addError(resultado.getDescripcion());
			return notification;
		}
		return notification;
	}
	
	Notification validation(ResultOpenKMDocumento resultado) {
		Notification notification = new Notification();
		if (resultado== null || resultado.getCodigo()==null) {
			//System.out.println("Archivo no se pudo guardar en el repositorio.");
			notification.addError("Archivo no se pudo guardar en el repositorio.");
			return notification;
		}
		if (!resultado.getCodigo().equals("00000")) {
			notification.addError(resultado.getDescripcion());
			//System.out.println("Archivo"+resultado.getDescripcion());
			return notification;
		}
		return notification;
	}
	
	public ResultAlfresco subirArchivoAlfresco(AlfrescoDocumentoAdjunto alfrescoAdjunto, FileAlfresco fileAlfresco, String idBiblioteca) {
		ResultAlfresco resultAlfresco = new ResultAlfresco();
		resultAlfresco = alfrescoAdjunto.subirArchivo(fileAlfresco, idBiblioteca);
		return resultAlfresco;
	}	
	
	
	
	public List<ImagenDto> obtenerImagenesAnexas(Long idBiblioteca){
		Notification notification = this.validation(idBiblioteca);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		List<ImagenAnexa> response = this.bibliotecaRepository.obtenerImagenesAnexas(idBiblioteca);
		
		List<ImagenDto> responseDto = new ArrayList<>();
		
		FileOpenKM fileOpenKM = new FileOpenKM();
    	ResultOpenKMDocumento resultOpenKMDocumento = new ResultOpenKMDocumento();
		OpenKMDocumento openKMDocumento = new OpenKMDocumento();
		
		//properties recargas
		List<ImagenAnexa> responseIncorrectas =new ArrayList<>();
		int maxRecargas=3;
		int contadorRecargas=0;		
		
		//obtener las imagenes alojadas en el repositorio
		for (ImagenAnexa imagenAnexa : response) {			
			
			fileOpenKM.setUiid(imagenAnexa.getUuid());
			resultOpenKMDocumento = openKMDocumento.consultarContenido(fileOpenKM);	
			
			if (resultOpenKMDocumento==null || !resultOpenKMDocumento.getCodigo().equals("00000")) {
				responseIncorrectas.add(imagenAnexa);
			}else {
				responseDto.add(obtenerImagenRepository(imagenAnexa,resultOpenKMDocumento));				
			}
			
		}
		
		//proceso de recargas
		while(!responseIncorrectas.isEmpty() && contadorRecargas<maxRecargas) {
			List<ImagenAnexa> responseIncorrectasTemporal =new ArrayList<>();
			responseIncorrectasTemporal.addAll(responseIncorrectas);
			for (ImagenAnexa imagenAnexa : responseIncorrectasTemporal) {			
				
				fileOpenKM.setUiid(imagenAnexa.getUuid());
				resultOpenKMDocumento = openKMDocumento.consultarContenido(fileOpenKM);	
				
				if (resultOpenKMDocumento!=null && resultOpenKMDocumento.getCodigo().equals("00000")) {
					responseDto.add(obtenerImagenRepository(imagenAnexa,resultOpenKMDocumento));					
					responseIncorrectas.remove(imagenAnexa);
				}			
			}
			contadorRecargas++;
		}
		
		if(!responseIncorrectas.isEmpty()) {
			//System.out.println("error al obtener las imagenes ............");
			notification.addError("error al obtener las imagenes del repositorio ............");//resultOpenKMDocumento.getDescripcion()
			throw new IllegalArgumentException(notification.errorMessage());
		}
		
		return responseDto;
	}
	
	
	private ImagenDto obtenerImagenRepository(ImagenAnexa imagenAnexa,ResultOpenKMDocumento resultOpenKMDocumento) {
		
		ImagenDto imagenDto = new ImagenDto();
		
		String[] baseFileName=imagenAnexa.getFileName().toLowerCase().split("_");
		String originalFileName=imagenAnexa.getFileName().toLowerCase();
		
		for(int i=0;i<baseFileName.length;i++) {
			if(baseFileName[i].contains(".")) {
				originalFileName=baseFileName[i];
			}
		}
		
		imagenDto.setId(imagenAnexa.getId());
		imagenDto.setFileExtension(imagenAnexa.getFileExtension().toLowerCase());
		imagenDto.setFileName(originalFileName);		
		imagenDto.setUuid(imagenAnexa.getUuid());
		String srcBase64 = Base64.getEncoder().encodeToString(resultOpenKMDocumento.getRawFile());
		imagenDto.setSrc("data:image/"+imagenAnexa.getFileExtension().toLowerCase()+";base64,"+ srcBase64);
		return imagenDto;
	}
		
	public ResponseTx cambiarEstadoImagenes(BibliotecaImagenEstadoDto bibliotecaImagen) {
		Notification notification = this.validation(bibliotecaImagen);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}		
		ResponseTx response = this.bibliotecaRepository.cambiarEstadoImagenes(Util.getStringToLong(bibliotecaImagen.getIdBibliotecaImagen()), bibliotecaImagen.getIdBibliotecaImagenEstado());
		return response;
	}
	
	public Notification validation(BibliotecaImagenEstadoDto bibliotecaImagen) {
		Notification notification = new Notification();
		if (bibliotecaImagen == null) {
			notification.addError("No se encontraron datos de la Imagen en Biblioteca");
			return notification;
		}
		
		if (Util.isEmptyString(bibliotecaImagen.getIdBibliotecaImagenEstado())) {
			notification.addError("se debe enviar el estado de la Imagen en Biblioteca");
		}

		if (Util.isEmptyString(bibliotecaImagen.getIdBibliotecaImagen())) {
			notification.addError("se debe enviar el id de la Biblioteca");
		}

		if (Util.getStringToLong(bibliotecaImagen.getIdBibliotecaImagen())<=0L) {
			notification.addError("id de Biblioteca no válido");
		}
		
		return notification;
	}	
	
	
	public List<BibliotecaMaster> buscarMasterBibliotecas(
			String  nombre,		String  numeroRNB,		String  codDepartamento,
			String  codProvincia,		String  codDistrito,	
			String  fechaInscripcionDesde,		String  fechaInscripcionHasta, String ruc, String direccion){
			FiltroBibliotecaVerificacionDto dto = new FiltroBibliotecaVerificacionDto();
			dto.setNombre(nombre);
			dto.setNumeroRNB(numeroRNB);
			dto.setCodDepartamento(codDepartamento);
			dto.setCodProvincia(codProvincia);
			dto.setCodDistrito(codDistrito);
			dto.setFechaInscripcionDesde(fechaInscripcionDesde);
			dto.setFechaInscripcionHasta(fechaInscripcionHasta);
			dto.setRuc(ruc);
			dto.setDireccion(direccion);
			Notification notification = this.validation(dto);
			if (notification.hasErrors()) {
				throw new IllegalArgumentException(notification.errorMessage());
			}		
			return this.bibliotecaRepository.listarMasterBibliotecas(dto);
	}
	
	public BibliotecaMasterServicio obtenerMasterBiblioteca(Long idMaster) {
		Notification notification = new Notification();
		notification = this.validation(idMaster);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return this.bibliotecaRepository.obtenerResumenBibliotecaMaster(idMaster);
	}	
	
	public ListaEstadistica obtenerEstadistica(String anio, String tipo) {
		ListaEstadistica resultado;
		Notification notification = this.validation(anio, tipo);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage()); 
		}
		resultado = this.bibliotecaRepository.obtenerEstadisticas(anio, tipo);
		return resultado;
	}
	
	Notification validation(String anio, String tipo) {
		Notification notification = new Notification();
		if (anio== null || anio.equals("")) {
			notification.addError("Debe ingresar el año o en caso sean todos los años colocar 'TODOS'.");
		}
		if (tipo== null || tipo.equals("")) {
			notification.addError("Tipo no debe ser vacío.");
		}
		return notification;
	}
	
	
	public ResponseTx subsanarDeclaracion(SubsanarBibliotecaDto dto) {
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		Biblioteca biblioteca = bibliotecaMapper.reverseMapperSubsanar(dto);
		ResponseTx response = this.bibliotecaRepository.subsanarBiblioteca(biblioteca);
		if (response!= null && response.getCodigoRespuesta()!=null && response.getCodigoRespuesta().equals("0000")){
			//atencion servicio
			ResponseTx resp = this.bibliotecaRepository.guardarAtencionServicio(Util.getStringToLong(response.getId()), biblioteca.getAtencionServicio(), Util.getStringToLong(biblioteca.getIdUsuarioRegistro()));
			if (resp!= null && resp.getCodigoRespuesta()!=null && resp.getCodigoRespuesta().equals("0000")){
				//atencion servicio mes
				for (AtencionServicioMes mes : biblioteca.getAtencionServicio().getMeses()) {
					this.bibliotecaRepository.guardarAtencionServicioMes(Util.getStringToLong(resp.getId()), mes,Util.getStringToLong(biblioteca.getIdUsuarioRegistro()) );
				}
				//atencion servicio turno
				for (AtencionServicioTurno dia : biblioteca.getAtencionServicio().getDias()) {
					ResponseTx respAtSerTurno = this.bibliotecaRepository.guardarAtencionServicioTurno(Util.getStringToLong(resp.getId()), dia,Util.getStringToLong(biblioteca.getIdUsuarioRegistro()) );
					//atencion servivio dia
					if (respAtSerTurno!= null && respAtSerTurno.getCodigoRespuesta()!=null && respAtSerTurno.getCodigoRespuesta().equals("0000")){
						for (AtencionServicioTurnoDia turno : dia.getTurnos()) {
							this.bibliotecaRepository.guardarAtencionServicioTurnoDia(Util.getStringToLong(respAtSerTurno.getId()), turno, Util.getStringToLong(biblioteca.getIdUsuarioRegistro()));
						}						
					}
				}				
				
			}
			
			
			//accesibilidad
			for (TipoValor tipoValor : biblioteca.getInfraestructura().getAccesibilidad()) {
				this.bibliotecaRepository.guardarDetalleBiblioteca(
						"ACCESIBILIDAD", 
						Util.getStringToLong(response.getId()), 
						Util.getStringToLong(biblioteca.getIdUsuarioRegistro()), 
						tipoValor);
			}
			//colecciones
			for (TipoValor tipoValor : biblioteca.getColecciones()) {
				this.bibliotecaRepository.guardarDetalleBiblioteca(
						"COLECCIONES", 
						Util.getStringToLong(response.getId()), 
						Util.getStringToLong(biblioteca.getIdUsuarioRegistro()), 
						tipoValor);
			}
			//servicios
			for (TipoValor tipoValor : biblioteca.getServicios()) {
				this.bibliotecaRepository.guardarDetalleBiblioteca(
						"SERVICIOS", 
						Util.getStringToLong(response.getId()), 
						Util.getStringToLong(biblioteca.getIdUsuarioRegistro()), 
						tipoValor);
			}
		}
		return this.setMessageResponseSubsanar(response);
		
	}

	public Notification validation(SubsanarBibliotecaDto dto) {
		Notification notification = new Notification();
		if (dto == null) {
			notification.addError("No se encontraron datos de la biblioteca");
			return notification;
		}
		if (dto.getEntidad() == null) {
			notification.addError("No se encontraron datos de la entidad");
			return notification;
		}		
		
		if (Util.isEmptyString(dto.getEntidad().getIdTipoGestion()) || Util.getStringToLong(dto.getEntidad().getIdTipoGestion())<=0L) {
			notification.addError("Se debe seleccionar el tipo de gestión de la entidad");
		}
		
		//Datos generales de la biblioteca
		if (Util.isEmptyString(dto.getNombre())) {
			notification.addError("Se debe ingresar el nombre de la biblioteca");
		}

		if (Util.isEmptyString(dto.getIdAmbitoTerritorial()) || Util.getStringToLong(dto.getIdAmbitoTerritorial())<=0L) {
			notification.addError("Se debe seleccionar el ámbito territorial de la biblioteca");
		}

		return notification;
	}	
	
	public ResponseTx setMessageResponseSubsanar(ResponseTx response) {
		if (response.getCodigoRespuesta().equals("9999")) {
			response.setRespuesta("Error: No se ejecutó ninguna transacción.");
		}
		if (response.getCodigoRespuesta().equals("0001")) {
			response.setRespuesta("Error: Id de la bibioteca no existe.");
		}
		if (response.getCodigoRespuesta().equals("0002")) {
			response.setRespuesta("Error: Id de la bibioteca no válido.");
		}	
		if (response.getCodigoRespuesta().equals("0003")) {
			response.setRespuesta("Error: Biblioteca no se encuentra en estado OBSERVADO.");
		}					
		if (response.getCodigoRespuesta().equals("0004")) {
			response.setRespuesta("Error: Biblioteca ya se encuentra declarada para el año de declaración.");
		}
		if (response.getCodigoRespuesta().equals("0005")) {
			response.setRespuesta("Error: Ámbito territorial no encontrado.");
		}			
		if (response.getCodigoRespuesta().equals("0006")) {
			response.setRespuesta("Error: Tipo de funcionamiento no encontrado.");
		}					
				
		if (response.getCodigoRespuesta().equals("0007")) {
			response.setRespuesta("Error: Condición laboral no encontrada.");
		}					
		if (response.getCodigoRespuesta().equals("0008")) {
			response.setRespuesta("Error: Grado de instrucción no encontrada.");
		}					
		if (response.getCodigoRespuesta().equals("0009")) {
			response.setRespuesta("Error: Tipo de gestión no encontrada.");
		}					
		if (response.getCodigoRespuesta().equals("0010")) {
			response.setRespuesta("Error: Departamento no encontrado.");
		}					
		if (response.getCodigoRespuesta().equals("0011")) {
			response.setRespuesta("Error: Provincia no encontrada.");
		}					
		if (response.getCodigoRespuesta().equals("0012")) {
			response.setRespuesta("Error: Distrito no encontrado.");
		}					
		if (response.getCodigoRespuesta().equals("0013")) {
			response.setRespuesta("Error: Sub tipo de biblioteca no encontrado.");
		}					
		if (response.getCodigoRespuesta().equals("0014")) {
			response.setRespuesta("Error: Tipo de biblioteca no encontrado.");
		}	
		if (response.getCodigoRespuesta().equals("0015")) {
			response.setRespuesta("Error: Tipo de Local (Infraestructura) no encontrado.");
		}		
		return response;		
	}	
	
	public ObservacionesBibliotecaDto listarObservacionesBiblioteca(Long idDeclaracion) {
		Notification notification = new Notification();
		notification = this.validation(idDeclaracion);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}		
		return this.bibliotecaRepository.obtenerObservacionesBiblioteca(idDeclaracion);
	}
		
	
}
