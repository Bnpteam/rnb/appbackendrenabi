package pe.gob.bnp.services.rest.renabi.common.infraestructure.repository;


import java.util.List;

import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;

public interface BaseRepository<T> {
	public ResponseTx persist(T entity);
	public ResponseTx persistM(T entity);
	public T read(T entity);
	public List<T> list(T entity);
	public ResponseTx update(T entity);
	public ResponseTx delete(T entity);
}
