package pe.gob.bnp.services.rest.renabi.common.application;

import javax.mail.internet.MimeUtility;

import org.apache.tomcat.util.buf.Utf8Encoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.json.UTF8DataInputJsonParser;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaEmail;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.MensajeTexto;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.CredencialRegistrador;



@Service
public class EmailApplicationService {
	//@Autowired
	private JavaMailSender emailSender;
	
	private MailContentBuilder mailContentBuilder;	
	
	@Value("${email.from}")
	String emailFrom;
	
	@Value("${email.titulo.registrador.cuenta.creacion}")
	String tituloCreacionCuentaRegistrador;	
	
	@Value("${email.titulo.registrador.cuenta.reenviocredencial}")
	String tituloRegistradorCuentaReenvioCredencial;	
	
	@Value("${email.titulo.biblioteca.inscripcion.aprobada}")
	String tituloBibliotecaInscripcionAprobada;	
	
	@Value("${email.titulo.biblioteca.inscripcion.observada}")
	String tituloBibliotecaInscripcionObservada;	
	
	@Value("${email.titulo.biblioteca.inscripcion.rechazada}")
	String tituloBibliotecaInscripcionRechazada;	
	
		
	
    @Autowired
    public EmailApplicationService(JavaMailSender emailSender, MailContentBuilder mailContentBuilder) {
        this.emailSender = emailSender;
        this.mailContentBuilder = mailContentBuilder;
    }
    
	public String sendTextMessageEmail(MensajeTexto mensaje) {
		String codigoResultado="";
		try {
			SimpleMailMessage message = new SimpleMailMessage();
			message.setFrom(emailFrom);
			message.setTo(mensaje.getTo());
			message.setSubject(mensaje.getSubject());
			message.setText(mensaje.getText());
			emailSender.send(message);
			codigoResultado = "0000";
		}catch(MailException e){
			codigoResultado = e.getMessage();
		}
		return codigoResultado;
	}
	
	public String sendHTMLEmailCreacionCuentaRegistrador(CredencialRegistrador credencial ) {
		String codigoResultado="";
	    try {
		    MimeMessagePreparator messagePreparator = mimeMessage -> {
	            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
	            messageHelper.setFrom(emailFrom);
	            messageHelper.setTo(credencial.getEmailDestino());
	            messageHelper.setSubject(tituloCreacionCuentaRegistrador);
	            String content = mailContentBuilder.buildTemplateCreacionCuenta(credencial.getDni(), credencial.getPassword());
	            messageHelper.setText(content, true);
		    };	    	
	    	emailSender.send(messagePreparator);
	    	codigoResultado = "0000";
	    }catch(MailException e) {
	    	codigoResultado = e.getMessage();
	    }
	    return codigoResultado;
	}
	
	
	public String sendHTMLEmailReenvioCredencialesCuentaRegistrador(CredencialRegistrador credencial ) {
		String codigoResultado="";
	    try {
		    MimeMessagePreparator messagePreparator = mimeMessage -> {
	            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
	            messageHelper.setFrom(emailFrom);
	            messageHelper.setTo(credencial.getEmailDestino());
	            messageHelper.setSubject(tituloRegistradorCuentaReenvioCredencial);
	            String content = mailContentBuilder.buildTemplateReenvioPassword(credencial.getDni(), credencial.getPassword());
	            messageHelper.setText(content, true);
		    };	    	
	    	emailSender.send(messagePreparator);
	    	codigoResultado = "0000";
	    }catch(MailException e) {
	    	codigoResultado = e.getMessage();
	    }
	    return codigoResultado;
	}	
	
	public String sendHTMLEmailBibliotecaAprobada(BibliotecaEmail datos ) {
		String codigoResultado=""; 
	    try {
		    MimeMessagePreparator messagePreparator = mimeMessage -> {
	            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
	            messageHelper.setFrom(emailFrom);
	            messageHelper.setTo(datos.getEmailDestino());
	            messageHelper.setSubject(tituloBibliotecaInscripcionAprobada);
	            String content = mailContentBuilder.buildTemplateBibliotecaAprobada(datos.getNombreBiblioteca(), datos.getAnio());
	            messageHelper.setText(content, true);
		    };	    	
	    	emailSender.send(messagePreparator);
	    	codigoResultado = "0000";
	    }catch(MailException e) {
	    	codigoResultado = e.getMessage();
	    }
	    return codigoResultado;
	}
	
	public String sendHTMLEmailBibliotecaObservada(BibliotecaEmail datos ) {
		String codigoResultado=""; 
	    try {
		    MimeMessagePreparator messagePreparator = mimeMessage -> {
	            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
	            messageHelper.setFrom(emailFrom);
	            messageHelper.setTo(datos.getEmailDestino());
	            messageHelper.setSubject(tituloBibliotecaInscripcionObservada);
	            String content = mailContentBuilder.buildTemplateBibliotecaObservada(datos.getNombreBiblioteca(), datos.getAnio());
	            messageHelper.setText(content, true);
		    };	    	
	    	emailSender.send(messagePreparator);
	    	codigoResultado = "0000";
	    }catch(MailException e) {
	    	codigoResultado = e.getMessage();
	    }
	    return codigoResultado;
	}		

	public String sendHTMLEmailBibliotecaRechazada(BibliotecaEmail datos ) {
		String codigoResultado=""; 
	    try {
		    MimeMessagePreparator messagePreparator = mimeMessage -> {
	            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
	            messageHelper.setFrom(emailFrom);
	            messageHelper.setTo(datos.getEmailDestino());
	            messageHelper.setSubject(tituloBibliotecaInscripcionRechazada);
	            String content = mailContentBuilder.buildTemplateBibliotecaObservada(datos.getNombreBiblioteca(), datos.getAnio());
	            messageHelper.setText(content, true);
		    };	    	
	    	emailSender.send(messagePreparator);
	    	codigoResultado = "0000";
	    }catch(MailException e) {
	    	codigoResultado = e.getMessage();
	    }
	    return codigoResultado;
	}	
}
