package pe.gob.bnp.services.rest.renabi.common;

public class Constants {
	public static final String EMPTY_STRING = "";
	public static final String ACTIVE_STATE="1";
	public static final String INACTIVE_STATE="0";
	public static final Long ZERO_LONG=0L;
	public static final Integer ZERO_INTEGER=0;
	public static final Long ID_AUDITORIA_NO_USUARIO = 9999L;
}
