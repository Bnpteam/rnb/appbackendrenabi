package pe.gob.bnp.services.rest.renabi.usuario.infraestructure.jdbc.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.application.dto.ResponseBaseDto;
import pe.gob.bnp.services.rest.renabi.common.application.dto.ResponseGenericDto;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Credencial;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseJDBCOperation;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.Maestra;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.AutenticacionTxDto;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.BibliotecaPortalDto;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.ResponseLoginPortalDto;
import pe.gob.bnp.services.rest.renabi.usuario.domain.repository.UsuarioRepository;

@Repository
public class UsuarioJDBCRepository extends BaseJDBCOperation implements UsuarioRepository {

	@Override
	public ResponseTx persist(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Maestra read(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Maestra> list(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx update(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx delete(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AutenticacionTxDto autenticarUsuario(Credencial credencial) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_PERSONA.SP_AUTENTICAR_TRABAJADOR(?,?,?,?,?) }";
        AutenticacionTxDto response = new AutenticacionTxDto();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_USUARIO", Util.getString(credencial.getUsuario()));
			cstm.setString("E_PASSWORD", Util.getString(credencial.getPassword()));
			cstm.registerOutParameter("S_USUARIO_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_NOMBRE_COMPLETO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				Long id=  Util.getLongFromObject(cstm.getObject("S_USUARIO_ID"));
				String nombres = Util.getString(cstm.getString("S_NOMBRE_COMPLETO"));
				response.setNombres(nombres);
				response.setId(Util.getLongToString(id));
				response.setRespuesta("Credenciales correctas.");
			}else {
				if (response.getCodigoRespuesta().equals("0001")) {
					response.setRespuesta("Error: El usuario/contraseña son incorrectos.");
				}else {
					response.setRespuesta("Error: no se pudo ejecutar la solicitud.");
				}
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseGenericDto<ResponseLoginPortalDto> autenticarUsuarioPortal(Credencial credencial) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_PERSONA.SP_LOGIN_PORTAL_WEB(?,?,?,?,?,?) }";
        ResponseBaseDto response = new ResponseBaseDto();
        ResponseLoginPortalDto payLoad = new ResponseLoginPortalDto();
        ResponseGenericDto<ResponseLoginPortalDto> responseGeneric= new ResponseGenericDto<ResponseLoginPortalDto>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_USUARIO", Util.getString(credencial.getUsuario()));
			cstm.setString("E_PASSWORD", Util.getString(credencial.getPassword()));
			cstm.registerOutParameter("S_REGISTRADOR_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_NOMBRE_COMPLETO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigo(codigoResultado);
			if (response.getCodigo().equals("0000")) {
				String id=  Util.getString(cstm.getString("S_REGISTRADOR_ID"));
				String nombres = Util.getString(cstm.getString("S_NOMBRE_COMPLETO"));
				payLoad.setNombreRegistrador(nombres);
				payLoad.setIdRegistrador(id);
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					BibliotecaPortalDto biblioteca = new BibliotecaPortalDto();
					biblioteca.setId(Util.getString(rs.getString("BIBLIOTECA_ID")));
					// biblioteca.setYear(Util.getString(rs.getString("ANIO_DECLARACION")));
					biblioteca.setNombre(Util.getString(rs.getString("NOMBRE")));
					payLoad.getBibliotecas().add(biblioteca);
				}					
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigo(String.valueOf(e.getErrorCode()));			
			response.setDescripcion(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigo(String.valueOf(e1.getErrorCode()));			
				response.setDescripcion(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		responseGeneric.setPayLoad(payLoad);
		responseGeneric.setResponse(response);
		return responseGeneric;	
	}

	@Override
	public ResponseTx persistM(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}
 
} 
