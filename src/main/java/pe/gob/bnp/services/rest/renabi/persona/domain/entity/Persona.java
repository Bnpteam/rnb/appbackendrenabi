package pe.gob.bnp.services.rest.renabi.persona.domain.entity;

import java.util.Date;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Tipo;

public class Persona extends BaseEntity{
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;
	private String genero;
	private Tipo tipoDocumentoIdentidad;
	private String numeroDocumentoIdentidad;
	private Date fechaNacimiento;
	private String nombreCompleto;
	private String documentoIdentidad;

	public Persona() {
		super();
		this.nombres = Constants.EMPTY_STRING;
		this.genero = Constants.EMPTY_STRING;
		this.apellidoPaterno = Constants.EMPTY_STRING;
		this.apellidoMaterno = Constants.EMPTY_STRING;
		this.tipoDocumentoIdentidad = new Tipo();
		this.numeroDocumentoIdentidad = Constants.EMPTY_STRING;
		this.fechaNacimiento = null;
		
		this.nombreCompleto = Constants.EMPTY_STRING;
		this.documentoIdentidad = Constants.EMPTY_STRING;

	}
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Tipo getTipoDocumentoIdentidad() {
		return tipoDocumentoIdentidad;
	}

	public void setTipoDocumentoIdentidad(Tipo tipoDocumentoIdentidad) {
		this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
	}

	public String getNumeroDocumentoIdentidad() {
		return numeroDocumentoIdentidad;
	}

	public void setNumeroDocumentoIdentidad(String numeroDocumentoIdentidad) {
		this.numeroDocumentoIdentidad = numeroDocumentoIdentidad;
	}

	public String getNombreCompleto() {
		return nombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}

	public String getDocumentoIdentidad() {
		return documentoIdentidad;
	}

	public void setDocumentoIdentidad(String documentoIdentidad) {
		this.documentoIdentidad = documentoIdentidad;
	}

	
	
}
