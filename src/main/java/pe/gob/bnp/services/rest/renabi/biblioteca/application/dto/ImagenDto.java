package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

public class ImagenDto {
	private String id;
	private String uuid;
	private String src;
	private String fileName;
	private String fileExtension;
	
	
	public ImagenDto() {
		super();
		this.id = "";
		this.uuid = "";
		this.src = "";
		this.fileName = "";
		this.fileExtension = "";
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileExtension() {
		return fileExtension;
	}
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}
	
	
}
