package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.application.dto.NuevoTipoValorDto;

public class NuevaInfraestructuraDto {
	private String idTipoLocal;
	private String areaM2;
	private String numeroAmbientes;
	private List<NuevoTipoValorDto> accesibilidad;
	
	public NuevaInfraestructuraDto() {
		super();
		this.idTipoLocal = Constants.EMPTY_STRING;
		this.areaM2 = Constants.EMPTY_STRING;
		this.numeroAmbientes = Constants.EMPTY_STRING;
		this.accesibilidad = new ArrayList<>();
	}

	public String getIdTipoLocal() {
		return idTipoLocal;
	}

	public void setIdTipoLocal(String idTipoLocal) {
		this.idTipoLocal = idTipoLocal;
	}

	public String getAreaM2() {
		return areaM2;
	}

	public void setAreaM2(String areaM2) {
		this.areaM2 = areaM2;
	}

	public String getNumeroAmbientes() {
		return numeroAmbientes;
	}

	public void setNumeroAmbientes(String numeroAmbientes) {
		this.numeroAmbientes = numeroAmbientes;
	}

	public List<NuevoTipoValorDto> getAccesibilidad() {
		return accesibilidad;
	}

	public void setAccesibilidad(List<NuevoTipoValorDto> accesibilidad) {
		this.accesibilidad = accesibilidad;
	}

}
