package pe.gob.bnp.services.rest.renabi.entidad.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;

@ApiModel(value="Entidad", description="Representa los datos de una entidad")
public class EntidadDto {
	@ApiModelProperty(notes = "Llave primario de la tabla TBL_ENTIDAD",required=true,value="Ej: 34")
	private String id;
	
	@ApiModelProperty(notes = "Nombre/razón social de la entidad",required=true,value="Ej: MUNICIPALIDAD DE AREQUIPA")
	private String razonSocial;
	
	@ApiModelProperty(notes = "RUC de la entidad",required=true,value="Ej: 20987654321")
	private String ruc;
	
	@ApiModelProperty(notes = "Correo de la entidad",required=true,value="Ej: correo@muniarequipa.gob.pe")
	private String correo;
	
	@ApiModelProperty(notes = "Dirección de la entidad",required=true,value="Ej: Av. Mario Vargas LLosa 134")
	private String direccion;
	
	@ApiModelProperty(notes = "Teléfono fijo de la entidad",required=false,value="Ej: 044870115")
	private String telefonoFijo;
	
	@ApiModelProperty(notes = "Teléfono móvil de la entidad",required=false,value="Ej: 987740113")
	private String telefonoMovil;
	
	@ApiModelProperty(notes = "Datos de la entidad obtenidos automáticamente de SUNAT o registrado manualmente por el registrador.",required=true,value="Valores: MANUAL/SUNAT Ej: SUNAT")
	private String flagOrigen;
	
	@ApiModelProperty(notes = "Tipo de gestión de la entidad",required=true,value="Ej: Privada")
	private String tipoGestion;
	
	@ApiModelProperty(notes = "Id del Tipo de gestión de la entidad",required=true,value="Ej: 56")
	private String idTipoGestion;
	
	@ApiModelProperty(notes = "Estado del registrador",required=false,value="Activo")
	private String estado;

	@ApiModelProperty(notes = "Id del estado del registrador",required=true,value="1")
	private String idEstado;
	
	@ApiModelProperty(notes = "Id del usuario de la aplicación que realiza el registro. 9999 por default.",required=true,value="9999")
	private String idCreacionAuditoria;

	@ApiModelProperty(notes = "Fecha de Registro. Auditoría. Se guarda la fecha de la base de datos por defecto.",required=false,value="16/07/2019")
	private String fechaCreacionAuditoria;

	@ApiModelProperty(notes = "Id del usuario de la aplicación que realiza la actualización. Auditoría. ",required=false,value="45")
	private String idModificacionAuditoria;

	@ApiModelProperty(notes = "Fecha de Actualización. Auditoría.",required=false,value="16/07/2019")
	private String fechaModificacionAuditoria;
	
	public EntidadDto() {
		super();
		this.id 					= Constants.EMPTY_STRING;
		this.razonSocial 			= Constants.EMPTY_STRING;
		this.ruc 					= Constants.EMPTY_STRING;
		this.correo 				= Constants.EMPTY_STRING;
		this.direccion 				= Constants.EMPTY_STRING;
		this.telefonoFijo 			= Constants.EMPTY_STRING;
		this.telefonoMovil 			= Constants.EMPTY_STRING;
		this.flagOrigen 			= Constants.EMPTY_STRING;
		this.tipoGestion 			= Constants.EMPTY_STRING;
		this.idTipoGestion 			= Constants.EMPTY_STRING;
		this.estado 				= Constants.EMPTY_STRING;
		this.idEstado 				= Constants.EMPTY_STRING;
		this.idCreacionAuditoria 	= Constants.EMPTY_STRING;
		this.fechaCreacionAuditoria = Constants.EMPTY_STRING;
		this.idModificacionAuditoria = Constants.EMPTY_STRING;
		this.fechaModificacionAuditoria = Constants.EMPTY_STRING;
	}


	
	public EntidadDto(Entidad objeto) {
		super();
		this.id = Util.getLongToString(objeto.getId());
		this.razonSocial = objeto.getRazonSocial();
		this.ruc = objeto.getRuc();
		this.correo = objeto.getCorreo();
		this.direccion = objeto.getDireccion();
		this.telefonoFijo = objeto.getTelefonoFijo();
		this.telefonoMovil = objeto.getTelefonoMovil();
		this.flagOrigen = objeto.getFlagOrigen();
		this.tipoGestion = objeto.getTipoGestion().getNombre();
		this.idTipoGestion = objeto.getTipoGestion().getId();
		this.estado = objeto.getEstado();
		this.idEstado = objeto.getIdEstado();
		this.idCreacionAuditoria = objeto.getIdUsuarioRegistro();
		this.fechaCreacionAuditoria = Util.utilDateToString(objeto.getFechaRegistro());
		this.idModificacionAuditoria = objeto.getIdUsuarioActualizacion();
		this.fechaModificacionAuditoria = Util.utilDateToString(objeto.getFechaActualizacion());
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getFlagOrigen() {
		return flagOrigen;
	}

	public void setFlagOrigen(String flagOrigen) {
		this.flagOrigen = flagOrigen;
	}

	public String getTipoGestion() {
		return tipoGestion;
	}

	public void setTipoGestion(String tipoGestion) {
		this.tipoGestion = tipoGestion;
	}

	public String getIdTipoGestion() {
		return idTipoGestion;
	}

	public void setIdTipoGestion(String idTipoGestion) {
		this.idTipoGestion = idTipoGestion;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getIdCreacionAuditoria() {
		return idCreacionAuditoria;
	}

	public void setIdCreacionAuditoria(String idCreacionAuditoria) {
		this.idCreacionAuditoria = idCreacionAuditoria;
	}

	public String getFechaCreacionAuditoria() {
		return fechaCreacionAuditoria;
	}

	public void setFechaCreacionAuditoria(String fechaCreacionAuditoria) {
		this.fechaCreacionAuditoria = fechaCreacionAuditoria;
	}

	public String getIdModificacionAuditoria() {
		return idModificacionAuditoria;
	}

	public void setIdModificacionAuditoria(String idModificacionAuditoria) {
		this.idModificacionAuditoria = idModificacionAuditoria;
	}

	public String getFechaModificacionAuditoria() {
		return fechaModificacionAuditoria;
	}

	public void setFechaModificacionAuditoria(String fechaModificacionAuditoria) {
		this.fechaModificacionAuditoria = fechaModificacionAuditoria;
	}	
	
	
}
