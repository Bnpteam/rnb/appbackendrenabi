package pe.gob.bnp.services.rest.renabi.parametros.domain.entity;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;

public class ListaMaestra extends BaseEntity{
	private List<Maestra> lista;

	public ListaMaestra() {
		super();
		this.lista = new ArrayList<>();
	}

	public List<Maestra> getLista() {
		return lista;
	}

	public void setLista(List<Maestra> lista) {
		this.lista = lista;
	}
	
}
