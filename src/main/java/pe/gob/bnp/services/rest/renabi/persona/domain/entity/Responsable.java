package pe.gob.bnp.services.rest.renabi.persona.domain.entity;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Tipo;

public class Responsable extends BaseEntity {
	private Persona persona;
	private String direccion;
	private String telefonoMovil;
	private String telefonoFijo;
	private String correo;	
	private Tipo gradoInstruccion;
	private String profesion;
	private Tipo condicionLaboral;
	
	public Responsable() {
		super();
		persona = new Persona();
		this.direccion = Constants.EMPTY_STRING;
		this.telefonoFijo= Constants.EMPTY_STRING;
		this.telefonoMovil= Constants.EMPTY_STRING;
		this.correo= Constants.EMPTY_STRING;		
		this.gradoInstruccion = new Tipo();
		this.profesion = Constants.EMPTY_STRING;
		this.condicionLaboral = new Tipo();		
	}
	
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public Tipo getGradoInstruccion() {
		return gradoInstruccion;
	}
	public void setGradoInstruccion(Tipo gradoInstruccion) {
		this.gradoInstruccion = gradoInstruccion;
	}
	public String getProfesion() {
		return profesion;
	}
	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}
	public Tipo getCondicionLaboral() {
		return condicionLaboral;
	}
	public void setCondicionLaboral(Tipo condicionLaboral) {
		this.condicionLaboral = condicionLaboral;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}
	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}
	public String getTelefonoFijo() {
		return telefonoFijo;
	}
	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	
}
