package pe.gob.bnp.services.rest.renabi.evento.application.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.evento.application.dto.EventoDto;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.Evento;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.EventoResumen;

@Component
public class EventoMapper {
	
	public List<EventoDto> mapperLista(List<Evento> lista) {
		List<EventoDto> response = new ArrayList<>();
		for (Evento evento : lista) {
			response.add(this.mapper(evento));
		}
		return response;
	}
	
	public EventoDto  mapper(Evento objeto) {
		EventoDto response = new EventoDto();
		response.setDetalleEvento(objeto.getDetalleEvento());
		response.setFechaFin(objeto.getFechaFin());
		response.setFechaInicio(objeto.getFechaInicio());
		response.setId(Util.getLongToString(objeto.getId()));
		response.setIdEstado(objeto.getIdEstado());
		response.setNombreBiblioteca(objeto.getNombreBiblioteca());
		response.setNombreResponsable(objeto.getNombreResponsable());
		response.setTipoEvento(objeto.getTipoEvento());
		response.setCantidad(objeto.getCantidad());
		response.setCorreo(objeto.getCorreo());
		response.setCosto(objeto.getCosto());
		response.setDepartamento(objeto.getDepartamento());
		response.setDireccion(objeto.getDireccion());
		response.setDistrito(objeto.getDistrito());
		response.setHoraFin(objeto.getHoraFin());
		response.setHoraInicio(objeto.getHoraInicio());
		response.setInscripcion(objeto.getInscripcion());
		response.setPaginaWeb(objeto.getPaginaWeb());
		response.setProvincia(objeto.getProvincia());
		response.setReferencia(objeto.getReferencia());
		response.setSubTipoBiblioteca(objeto.getSubTipoBiblioteca());
		response.setTelefono(objeto.getTelefono());
		response.setTipoBiblioteca(objeto.getTipoBiblioteca());
		return response;
	}
}
