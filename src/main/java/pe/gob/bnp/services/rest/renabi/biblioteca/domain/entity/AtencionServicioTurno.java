package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.AtencionServicioTurnoDiaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.AtencionServicioTurnoDto;
import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;

public class AtencionServicioTurno extends BaseEntity{
	private String dia;
	private String valor;
	private List<AtencionServicioTurnoDia> turnos;
	
	public AtencionServicioTurno() {
		super();
		this.dia = Constants.EMPTY_STRING;
		this.turnos = new ArrayList<>();
		this.valor = Constants.EMPTY_STRING;
	}

	public AtencionServicioTurno(AtencionServicioTurnoDto dto) {
		super();
		this.dia = dto.getDia();
		this.valor= dto.getValor();
		this.turnos = new ArrayList<>();
		for (AtencionServicioTurnoDiaDto atencionServicioTurnoDiaDto : dto.getTurnos()) {
			if (!validateExistTurno(atencionServicioTurnoDiaDto.getIdTurno())) {
				this.turnos.add(new AtencionServicioTurnoDia(atencionServicioTurnoDiaDto));
			}	
		}
	}
	
	
	private boolean validateExistTurno(String idTurno) {
		boolean result =false;
		for(int i=0;i<this.turnos.size();i++) {
			if(this.turnos.get(i).getTipoTurno().getId().equalsIgnoreCase(idTurno)) {
				//System.out.println("si se encontro repetido en el envio");
				result=true;
			}
		}
		return result;			
	}

	public String getDia() {
		return dia;
	}
	public void setDia(String dia) {
		this.dia = dia;
	}
	public List<AtencionServicioTurnoDia> getTurnos() {
		return turnos;
	}
	public void setTurnos(List<AtencionServicioTurnoDia> turnos) {
		this.turnos = turnos;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	
}
