package pe.gob.bnp.services.rest.renabi.common.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.TipoValor;

public class NuevoTipoValorDto {
	private String id;
	private String nombre;
	private String valor;
	
	public NuevoTipoValorDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
		this.valor = Constants.EMPTY_STRING;
	}
	
	public NuevoTipoValorDto(TipoValor valor) {
		super();
		this.id = valor.getId();
		this.nombre = valor.getNombre();
		this.valor = valor.getValor();
	}	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
		
}
