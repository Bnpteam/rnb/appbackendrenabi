package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Tipo;

public class AtencionServicio extends BaseEntity {
	private Tipo tipoAccesoPublico;
	private List<AtencionServicioMes> meses;
	private List<AtencionServicioTurno> dias;
	
	public AtencionServicio() {
		super();
		this.tipoAccesoPublico = new Tipo();
		this.meses = new ArrayList<>();
		this.dias = new ArrayList<>();
	}
	
	public void asignarTurnosYHorarios(List<AtencionServicioTurnoDia> turnosyHorarios) {
		for (int i = 0; i < this.getDias().size(); i++) {
			this.getDias().get(i).setTurnos(this.obtenerTurnoYHorario(this.getDias().get(i).getId(), turnosyHorarios));
		}
	}
	
	public List<AtencionServicioTurnoDia> obtenerTurnoYHorario(Long idAtencionTurno, List<AtencionServicioTurnoDia> turnosyHorarios) {
		List<AtencionServicioTurnoDia> respuesta = new ArrayList<>();
		for (AtencionServicioTurnoDia turnoyHorario : turnosyHorarios) {
			if (turnoyHorario.getIdAtencionTurno().compareTo(idAtencionTurno)==0) {
				respuesta.add(turnoyHorario);
			}
		}
		return respuesta;
	}
	
	public Tipo getTipoAccesoPublico() {
		return tipoAccesoPublico;
	}
	public void setTipoAccesoPublico(Tipo tipoAccesoPublico) {
		this.tipoAccesoPublico = tipoAccesoPublico;
	}
	public List<AtencionServicioMes> getMeses() {
		return meses;
	}
	public void setMeses(List<AtencionServicioMes> meses) {
		this.meses = meses;
	}
	public List<AtencionServicioTurno> getDias() {
		return dias;
	}
	public void setDias(List<AtencionServicioTurno> dias) {
		this.dias = dias;
	}
	
	
}
