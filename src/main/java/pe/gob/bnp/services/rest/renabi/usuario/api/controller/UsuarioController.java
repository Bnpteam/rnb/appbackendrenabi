package pe.gob.bnp.services.rest.renabi.usuario.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.services.rest.renabi.common.api.controller.ResponseHandler;
import pe.gob.bnp.services.rest.renabi.common.application.EntityNotFoundResultException;
import pe.gob.bnp.services.rest.renabi.common.application.dto.ResponseGenericDto;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Credencial;
import pe.gob.bnp.services.rest.renabi.usuario.application.UsuarioApplicationService;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.AutenticacionTxDto;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.ResponseLoginPortalDto;

@RestController
@RequestMapping("api/usuario")
@Api(value = "/api/usuario",description="Servicio REST para Trabajadores (verificadores por ej.) - Desarrollado por OTIE/BNP")
@CrossOrigin(origins = "*")
public class UsuarioController {
	@Autowired
	UsuarioApplicationService usuarioApplicationService;
	
	@Autowired
	ResponseHandler responseHandler;	
	
	@Autowired
	ResponseHandlerUsuario responseHandlerLocal;
	
	@RequestMapping(method = RequestMethod.POST, path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Login de usuario, debe estar habilitado en Active Directory", response= AutenticacionTxDto.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 400, message = "Solicitud contiene errores"),
        @ApiResponse(code = 404, message = "Error al momento de realizar consulta a Active Directory"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})	
	public ResponseEntity<Object> autenticarUsuario(
			@ApiParam(value = "objeto contiene el usuario y password ingresado por el usuario", required = true)
			@RequestBody  Credencial credencial){ 
		try {
			AutenticacionTxDto response = usuarioApplicationService.validarUsuario(credencial);
			if (response == null) {
				return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar la autenticación.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar la autenticación.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/loginPortal", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Login del Portal Web de Bibliotecas", response= AutenticacionTxDto.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 400, message = "Solicitud contiene errores"),
        @ApiResponse(code = 404, message = "Error al momento de realizar consulta a Active Directory"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})	
	public ResponseEntity<Object> autenticarUsuarioPortalWeb(
			@ApiParam(value = "objeto contiene el usuario y contraseña ingresada por el usuario", required = true)
			@RequestBody  Credencial credencial){ 
		try {
			ResponseGenericDto<ResponseLoginPortalDto> response = usuarioApplicationService.validarLoginUsuarioPortal(credencial);
			if (response == null) {
				return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar la autenticación.");
			}
			return this.responseHandlerLocal.getOkResponseGeneric(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("Error al momento de realizar la autenticación.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	

}
