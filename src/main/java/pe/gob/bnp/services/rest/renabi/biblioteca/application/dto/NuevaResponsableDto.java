package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="Responsable", description="Representa los datos de un responsable")
public class NuevaResponsableDto {
	@ApiModelProperty(notes = "Nombre del responsable ",required=true,value="Juan")
	private String nombres;

	@ApiModelProperty(notes = "Apellido paterno del responsable ",required=true,value="Perez")
	private String apellidoPaterno;

	@ApiModelProperty(notes = "Apellido materno del responsable ",required=false,value="Cardenas")
	private String apellidoMaterno;

	
	@ApiModelProperty(notes = "Id del tipo de documento de identidad del responsable ",required=true,value="DNI")
	private String idTipoDocumento;

	@ApiModelProperty(notes = "Número de documento de identidad del responsable ",required=true,value="45389001")
	private String numeroDocumento;

	@ApiModelProperty(notes = "Fecha de nacimiento del responsable ",required=true,value="25/01/1990")
	private String fechaNacimiento;
	
	@ApiModelProperty(notes = "Sexo del responsable ",required=true,value="Masculino")
	private String genero;

	@ApiModelProperty(notes = "Id del estado del responsable",required=true,value="1")
	private String idEstado;
	
	@ApiModelProperty(notes = "Teléfono móvil del responsable ",required=false,value="987654321")
	private String telefonoMovil;
	
	@ApiModelProperty(notes = "Teléfono fijo del responsable ",required=false,value="015632201")
	private String telefonoFijo;
	
	@ApiModelProperty(notes = "Correo del responsable ",required=false,value="juanperez@gmail.com")
	private String correo;	
	
	@ApiModelProperty(notes = "Id del grado de instrucción del responsable ",required=false,value="12")
	private String idGradoInstruccion;
	
	@ApiModelProperty(notes = "Profesión del responsable ",required=false,value="Administrador")
	private String profesion;
	
	@ApiModelProperty(notes = "Id de la condición laboral de responsable ",required=false,value="43")
	private String idCondicionLaboral;
	
	@ApiModelProperty(notes = "Dirección del responsable",required=true,value="Avenida del ejercito 3434 Miraflores - Lima - Perú")
	private String direccion;
	
	@ApiModelProperty(notes = "Id del usuario de la aplicación que realiza el registro. 9999 por default.",required=true,value="9999")
	private String idCreacionAuditoria;

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidoPaterno() {
		return apellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return apellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}

	public String getIdTipoDocumento() {
		return idTipoDocumento;
	}

	public void setIdTipoDocumento(String idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getIdGradoInstruccion() {
		return idGradoInstruccion;
	}

	public void setIdGradoInstruccion(String idGradoInstruccion) {
		this.idGradoInstruccion = idGradoInstruccion;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getIdCondicionLaboral() {
		return idCondicionLaboral;
	}

	public void setIdCondicionLaboral(String idCondicionLaboral) {
		this.idCondicionLaboral = idCondicionLaboral;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getIdCreacionAuditoria() {
		return idCreacionAuditoria;
	}

	public void setIdCreacionAuditoria(String idCreacionAuditoria) {
		this.idCreacionAuditoria = idCreacionAuditoria;
	}
	
	

}
