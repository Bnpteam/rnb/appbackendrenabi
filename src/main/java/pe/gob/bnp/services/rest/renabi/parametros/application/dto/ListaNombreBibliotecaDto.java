package pe.gob.bnp.services.rest.renabi.parametros.application.dto;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.NombreBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;

@ApiModel(value="ListaNombreBiblioteca", description="Objeto que representa la lista de registros de la tabla TBL_BIBLIOTECA")
public class ListaNombreBibliotecaDto {
	@ApiModelProperty(notes = "Resultado de la solicitud")
	private ResponseTx response;
	
	@ApiModelProperty(notes = "Lista de registros obtenidos", required=true)
	private List<NombreBibliotecaDto> lista;

	public ListaNombreBibliotecaDto() {
		super();
		this.response = new ResponseTx();
		this.lista = new ArrayList<>();
	}

	public ResponseTx getResponse() {
		return response;
	}

	public void setResponse(ResponseTx response) {
		this.response = response;
	}

	public List<NombreBibliotecaDto> getLista() {
		return lista;
	}

	public void setLista(List<NombreBibliotecaDto> lista) {
		this.lista = lista;
	}
	
	
}
