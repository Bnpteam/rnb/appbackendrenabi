package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class TipoBibliotecaPadreDto {
	private String idTipoBiblioteca;
	private String nombre;
	private String estado;
	
	public TipoBibliotecaPadreDto(String idTipoBiblioteca, String nombre, String estado) {
		super();
		this.idTipoBiblioteca = idTipoBiblioteca;
		this.nombre = nombre;
		this.estado = estado;
	}
	
	public TipoBibliotecaPadreDto() {
		super();
		this.idTipoBiblioteca = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
		this.estado = Constants.EMPTY_STRING;
	}	

	public String getIdTipoBiblioteca() {
		return idTipoBiblioteca;
	}

	public void setIdTipoBiblioteca(String idTipoBiblioteca) {
		this.idTipoBiblioteca = idTipoBiblioteca;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
