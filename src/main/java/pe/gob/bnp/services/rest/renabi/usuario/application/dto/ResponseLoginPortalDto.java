package pe.gob.bnp.services.rest.renabi.usuario.application.dto;

import java.util.ArrayList;
import java.util.List;

public class ResponseLoginPortalDto {
	private String idRegistrador;
	private String nombreRegistrador;
	private List<BibliotecaPortalDto> bibliotecas;
	
	
	
	public ResponseLoginPortalDto() {
		this.bibliotecas = new ArrayList<>();
	}
	public String getIdRegistrador() {
		return idRegistrador;
	}
	public void setIdRegistrador(String idRegistrador) {
		this.idRegistrador = idRegistrador;
	}
	public String getNombreRegistrador() {
		return nombreRegistrador;
	}
	public void setNombreRegistrador(String nombreRegistrador) {
		this.nombreRegistrador = nombreRegistrador;
	}
	public List<BibliotecaPortalDto> getBibliotecas() {
		return bibliotecas;
	}
	public void setBibliotecas(List<BibliotecaPortalDto> bibliotecas) {
		this.bibliotecas = bibliotecas;
	}

}
