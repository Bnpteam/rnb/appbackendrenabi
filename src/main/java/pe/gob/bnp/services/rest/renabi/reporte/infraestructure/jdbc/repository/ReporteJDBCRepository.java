package pe.gob.bnp.services.rest.renabi.reporte.infraestructure.jdbc.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.AtencionRptServicioTurnoDiaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.VerResponsableDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.VerRptBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.VerRptInfraestructuraDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioMes;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurno;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurnoDia;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Biblioteca;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.TipoValor;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseJDBCOperation;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.VerEntidadDto;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.Responsable;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteRegistradoresCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasRegistradasCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasVerificadasCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasxEstadoCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteGenericoFiltroDto;
import pe.gob.bnp.services.rest.renabi.reporte.domain.repository.ReporteRepository;

@Repository
public class ReporteJDBCRepository extends BaseJDBCOperation implements ReporteRepository{

	

	@Override
	public List<ReporteRegistradoresCabeceraDto> listarRegistradores(ReporteGenericoFiltroDto filtro) {
		// TODO Auto-generated method stub
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_REPORTES.SP_LIST_RPT_REGISTRADORES("
        		+ "?,?,?,?) }";
        List<ReporteRegistradoresCabeceraDto> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);			
			cstm.setDate("E_FECHA_DESDE", Util.stringToSQLDate(Util.getString(filtro.getFechaDesde())));
			cstm.setDate("E_FECHA_HASTA", Util.stringToSQLDate(Util.getString(filtro.getFechaHasta())));		
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ReporteRegistradoresCabeceraDto reporteReg = new ReporteRegistradoresCabeceraDto();
					reporteReg.setRowRegistrador(Util.getString(rs.getString("ROWREGISTRADOR")));
					reporteReg.setId(Util.getString(rs.getString("CODREGISTRADOR")));
					reporteReg.setFechaRegistro(Util.sqlDateToString(rs.getDate("FECHAREGISTRO")));
					reporteReg.setDni(Util.getString(rs.getString("DNI")));
					reporteReg.setNombres(Util.getString(rs.getString("NOMBRES")));
					reporteReg.setApellidoPaterno(Util.getString(rs.getString("APPATERNO")));
					reporteReg.setApellidoMaterno(Util.getString(rs.getString("APMATERNO")));
					reporteReg.setCorreo(Util.getString(rs.getString("CORREO")));
					reporteReg.setTelefonoFijo(Util.getString(rs.getString("TELFFIJO")));
					reporteReg.setTelefonoCelular(Util.getString(rs.getString("TELFCELULAR")));
					reporteReg.setNroRegistros(Util.getString(rs.getString("NROREGISTROS")));					
					response.add(reporteReg);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public List<ReporteBibliotecasxEstadoCabeceraDto> listarBibliotecasxEstado(ReporteGenericoFiltroDto filtro) {
		// TODO Auto-generated method stub
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_REPORTES.SP_LIST_RPT_BIBLIOTECA_ESTADO("
        		+ "?,?,?,?) }";
        List<ReporteBibliotecasxEstadoCabeceraDto> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);			
			cstm.setDate("E_FECHA_DESDE", Util.stringToSQLDate(Util.getString(filtro.getFechaDesde())));
			cstm.setDate("E_FECHA_HASTA", Util.stringToSQLDate(Util.getString(filtro.getFechaHasta())));		
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ReporteBibliotecasxEstadoCabeceraDto reporteBib = new ReporteBibliotecasxEstadoCabeceraDto();
					reporteBib.setRowBiblioteca(Util.getString(rs.getString("ROWBIBLIOTECA")));
					reporteBib.setId(Util.getString(rs.getString("CODBIBLIOTECA")));
					reporteBib.setEstado(Util.getString(rs.getString("ESTADO")));
					reporteBib.setFechaRegistro(Util.sqlDateToString(rs.getDate("FECHAREGISTRO")));
					reporteBib.setRazonSocial(Util.getString(rs.getString("RAZONSOCIAL")));
					reporteBib.setNombreBiblioteca(Util.getString(rs.getString("NOMBREBIBLIOTECA")));
					reporteBib.setTipoBiblioteca(Util.getString(rs.getString("TIPOBIBLIOTECA")));
					reporteBib.setSubTipoBiblioteca(Util.getString(rs.getString("SUBTIPOBIBLIOTECA")));					
					reporteBib.setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));
					reporteBib.setProvincia(Util.getString(rs.getString("PROVINCIA")));
					reporteBib.setDistrito(Util.getString(rs.getString("DISTRITO")));
					reporteBib.setCentroPoblado(Util.getString(rs.getString("CENTROPOBLADO")));
					reporteBib.setResponsableBiblioteca(Util.getString(rs.getString("RESPONSABLEBIBLIOTECA")));	
					reporteBib.setTelefonoFijo(Util.getString(rs.getString("TELEFONOFIJO")));
					reporteBib.setTelefonoCelular(Util.getString(rs.getString("TELEFONOCELULAR")));
					reporteBib.setCorreo(Util.getString(rs.getString("CORREO")));								
					response.add(reporteBib);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public List<ReporteBibliotecasVerificadasCabeceraDto> listarBibliotecasVerificadas(ReporteGenericoFiltroDto filtro) {
		// TODO Auto-generated method stub
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_REPORTES.SP_LIST_RPT_BIB_VERIFICADA("
        		+ "?,?,?,?) }";
        List<ReporteBibliotecasVerificadasCabeceraDto> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);			
			cstm.setDate("E_FECHA_DESDE", Util.stringToSQLDate(Util.getString(filtro.getFechaDesde())));
			cstm.setDate("E_FECHA_HASTA", Util.stringToSQLDate(Util.getString(filtro.getFechaHasta())));		
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ReporteBibliotecasVerificadasCabeceraDto reporteBib = new ReporteBibliotecasVerificadasCabeceraDto();
					reporteBib.setRowBiblioteca(Util.getString(rs.getString("ROWBIBLIOTECA")));
					reporteBib.setId(Util.getString(rs.getString("CODBIBLIOTECA")));
					reporteBib.setRazonSocial(Util.getString(rs.getString("RAZONSOCIAL")));
					reporteBib.setNombreBiblioteca(Util.getString(rs.getString("NOMBREBIBLIOTECA")));
					reporteBib.setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));
					reporteBib.setProvincia(Util.getString(rs.getString("PROVINCIA")));
					reporteBib.setDistrito(Util.getString(rs.getString("DISTRITO")));
					reporteBib.setCentroPoblado(Util.getString(rs.getString("CENTROPOBLADO")));
					reporteBib.setFechaVerificacion(Util.sqlDateToString(rs.getDate("FECHAVERIFICACION")));								
					response.add(reporteBib);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public List<ReporteBibliotecasRegistradasCabeceraDto> listarBibliotecasRegistradas(ReporteGenericoFiltroDto filtro) {
		// TODO Auto-generated method stub
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_REPORTES.SP_LIST_RPT_BIB_REGISTRADA("
        		+ "?,?,?,?) }";
        List<ReporteBibliotecasRegistradasCabeceraDto> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);			
			cstm.setDate("E_FECHA_DESDE", Util.stringToSQLDate(Util.getString(filtro.getFechaDesde())));
			cstm.setDate("E_FECHA_HASTA", Util.stringToSQLDate(Util.getString(filtro.getFechaHasta())));		
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ReporteBibliotecasRegistradasCabeceraDto reporteBib = new ReporteBibliotecasRegistradasCabeceraDto();
					reporteBib.setRowBiblioteca(Util.getString(rs.getString("ROWBIBLIOTECA")));
					reporteBib.setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));					
					reporteBib.setProvincia(Util.getString(rs.getString("PROVINCIA")));
					reporteBib.setDistrito(Util.getString(rs.getString("DISTRITO")));
					reporteBib.setCentroPoblado(Util.getString(rs.getString("CENTROPOBLADO")));
					reporteBib.setTipoBiblioteca(Util.getString(rs.getString("TIPOBIBLIOTECA")));
					reporteBib.setFechaRegistro(Util.sqlDateToString(rs.getDate("FECHAREGISTRO")));
					reporteBib.setNroLibros(Util.getString(rs.getString("NROLIBROS")));
					reporteBib.setPorValidar(Util.getString(rs.getString("PORVALIDAR")));
					reporteBib.setEnProceso(Util.getString(rs.getString("ENPROCESO")));
					reporteBib.setVerificado(Util.getString(rs.getString("VERFICADO")));
					reporteBib.setObservado(Util.getString(rs.getString("OBSERVADO")));
					reporteBib.setTotal(Util.getString(rs.getString("TOTAL")));
					response.add(reporteBib);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}
	
	@Override
	public VerRptBibliotecaDto listarBibliotecasxId(String idBiblioteca) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_REPORTES.SP_LIST_RPT_BIBLIOTECA_ID("
        		+ "?,?,?,?) }";
        VerRptBibliotecaDto biblioteca = new VerRptBibliotecaDto();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_IDBIBLIOTECA", idBiblioteca);
			
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_TURNO",   OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					biblioteca.setIdBiblioteca(rs.getString("IDBIBLIOTECA"));
					
					///ENTIDAD
					VerEntidadDto entidad = new VerEntidadDto();
					entidad.setId(rs.getString("IDENTIDAD"));
					entidad.setRazonSocial(Util.getString(rs.getString("RAZONSOCIALENTIDAD")));
					entidad.setRuc(Util.getString(rs.getString("RUCENTIDAD")));
					entidad.setCorreo(Util.getString(rs.getString("CORREOENTIDAD")));
					entidad.setDireccion(Util.getString(rs.getString("DIRECCIONENTIDAD")));
					entidad.setTelefonoFijo(Util.getString(rs.getString("TELEFONOFIJOENTIDAD")));
					entidad.setTelefonoMovil(Util.getString(rs.getString("TELEFONOMOVILENTIDAD")));
					entidad.setIdTipoGestion(Util.getString(rs.getString("TIPOGESTIONENTIDAD")));
					biblioteca.setEntidad(entidad);
					
					///BIBLIOTECA	
					biblioteca.setNombre(Util.getString(rs.getString("NOMBREBIBLIOTECA")));
					biblioteca.setAmbitoTerritorial(Util.getString(rs.getString("TIPOAMBITOBIBLIOTECA")));
					biblioteca.setTipoFuncionamiento(Util.getString(rs.getString("TIPOFUNCIONAMIENTOBIBLIOTECA")));
					biblioteca.setTipoBibliotecaPadre(Util.getString(rs.getString("TIPOBIBLIOTECA")));
					biblioteca.setTipoBibliotecaHijo(Util.getString(rs.getString("SUBTIPOBIBLIOTECA")));
					biblioteca.setNumeroBibliotecologos(Util.getString(rs.getString("TOTALBIBLIOTECOLOGOSBIBLIOTECA")));
					biblioteca.setNumeroTotalPersonal(Util.getString(rs.getString("CANTIDADPERSONALBIBLIOTECA")));
					biblioteca.setCorreoElectronico(Util.getString(rs.getString("CORREOBIBLIOTECA")));
					biblioteca.setTelefonoFijo(Util.getString(rs.getString("TELEFONOFIJOBIBLIOTECA")));
					biblioteca.setTelefonoMovil(Util.getString(rs.getString("TELEFONOMOVILBIBLIOTECA")));
					biblioteca.setUbigeo(Util.getString(rs.getString("UBIGEOBIBLIOTECA")));
					biblioteca.setCentroPoblado(Util.getString(rs.getString("CENTROPOBLADOBIBLIOTECA")));
					biblioteca.setDireccion(Util.getString(rs.getString("DIRECCIONBIBLIOTECA")));
					
					///RESPONSABLE
					VerResponsableDto responsable = new VerResponsableDto();
					responsable.setNumeroDocumento(Util.getString(rs.getString("DNIRESPONSABLE")));
					responsable.setNombres(Util.getString(rs.getString("NOMBRESRESPONSABLE")));
					responsable.setCorreo(Util.getString(rs.getString("CORREORESPONSABLE")));
					responsable.setTelefonoFijo(Util.getString(rs.getString("TELEFONOFIJORESPONSABLE")));
					responsable.setTelefonoMovil(Util.getString(rs.getString("TELEFONOMOVILRESPONSABLE")));
					responsable.setGenero(Util.getString(rs.getString("GENERORESPONSABLE")));
					System.out.println("date:"+Util.getUtilDate(rs.getDate("FECHANACIMIENTORESPONSABLE")));
					System.out.println("date1:"+Util.utilDateToString(Util.getUtilDate(rs.getDate("FECHANACIMIENTORESPONSABLE"))));
					
					responsable.setFechaNacimiento(Util.utilDateToString(Util.getUtilDate(rs.getDate("FECHANACIMIENTORESPONSABLE"))));
					
					responsable.setCondicionLaboral(Util.getString(rs.getString("CONDICIONLABORALRESPONSABLE")));
					responsable.setGradoInstruccion(Util.getString(rs.getString("GRADOINSTRUCCIONRESPONSABLE")));
					responsable.setProfesion(Util.getString(rs.getString("PROFESIONRESPONSABLE")));
					biblioteca.setResponsable(responsable);
					
					///INFRAESTRUCTURA Y EQUIPAMIENTO
					VerRptInfraestructuraDto infraestructura= new VerRptInfraestructuraDto();
					infraestructura.setTipoLocal(Util.getString(rs.getString("TIPOLOCALINFRAESTRUCTURA")));
					infraestructura.setAreaM2(Util.getString(rs.getString("AREAINFRAESTRUCTURA")));
					infraestructura.setNumeroAmbientes(Util.getString(rs.getString("NROAMBIENTESINFRAESTRUCTURA")));
					infraestructura.setNombreaccesibilidad(Util.getString(rs.getString("NOMACCESINFRAESTRUCTURA")));
					infraestructura.setValoraccesibilidad(Util.getString(rs.getString("VALORACCESINFRAESTRUCTURA")));
					biblioteca.setInfraestructura(infraestructura);
					
					///TIPO SERVICIO
					biblioteca.setNombreTipoServicio(Util.getString(rs.getString("NOMBRETIPOSERVICIO")));
					biblioteca.setValorTipoServicio(Util.getString(rs.getString("VALORTIPOSERVICIO")));
					
					///COLECCIONES
					biblioteca.setNombreColecciones(Util.getString(rs.getString("NOMBRECOLECCIONES")));
					biblioteca.setValorColecciones(Util.getString(rs.getString("VALORCOLECCIONES")));
					
					///ESTADISTICAS
					biblioteca.setAnioDeclaracion(Util.getString(rs.getString("ANIOANTERIORESTADISTICA")));
					biblioteca.setTotalLectores(Util.getString(rs.getString("NROUSUARIOSESTADISTICA")));
					biblioteca.setTotalConsultas(Util.getString(rs.getString("NROCONSULTASESTADISTICA")));
					
					///INFORMACION GESTION
					biblioteca.setPlanTrabActual(Util.getString(rs.getString("PLANTRABAJOANUALGESTION")));
					biblioteca.setReglamentos(Util.getString(rs.getString("REGLAMENTOGESTION")));
					biblioteca.setEstadisticas(Util.getString(rs.getString("ESTADISTICASGESTION")));
					biblioteca.setEspecificar(Util.getString(rs.getString("OTROESPECIFIQUEGESTION")));
					
					///ATENCION DEL SERVICIO
					biblioteca.setAccesoPublicoAtencionServicio(Util.getString(rs.getString("ACCESOPUBLICO")));
					biblioteca.setNombreMesesAtencionServicio(Util.getString(rs.getString("NOMBREMESES")));
					biblioteca.setValorMesesAtencionServicio(Util.getString(rs.getString("VALORMESES")));
					
					//biblioteca.getEstadoBiblioteca().setId(Util.getString(rs.getString("TIPO_ESTADO_BIBLIOTECA_ID")));
					//biblioteca.getEstadoBiblioteca().setNombre(Util.getString(rs.getString("TIPO_ESTADO_BIBLIOTECA")));
				
				}	
				this.closeResultSet(rs);			
				
				
				//atencion servicio turnos
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_TURNO");
				while (rs.next()) {
					AtencionRptServicioTurnoDiaDto dia = new AtencionRptServicioTurnoDiaDto();
					dia.setNombreTurno(Util.getString(rs.getString("TURNO")));
					dia.setHoraLunes(Util.getString(rs.getString("LUNES")));
					dia.setHoraMartes(Util.getString(rs.getString("MARTES")));
					dia.setHoraMiercoles(Util.getString(rs.getString("MIERCOLES")));
					dia.setHoraJueves(Util.getString(rs.getString("JUEVES")));
					dia.setHoraViernes(Util.getString(rs.getString("VIERNES")));
					dia.setHoraSabado(Util.getString(rs.getString("SABADO")));
					dia.setHoraDomingo(Util.getString(rs.getString("DOMINGO")));
					biblioteca.getTurnos().add(dia);
				}	
				this.closeResultSet(rs);				
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		//System.out.println("biblioteca nacimineto final:"+biblioteca.getResponsable().getPersona().getFechaNacimiento());
		return biblioteca;	
	}

	@Override
	public ResponseTx persist(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Biblioteca read(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Biblioteca> list(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx update(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx delete(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx persistM(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

}
