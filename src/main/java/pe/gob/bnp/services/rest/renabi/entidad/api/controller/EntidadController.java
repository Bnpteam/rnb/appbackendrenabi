package pe.gob.bnp.services.rest.renabi.entidad.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.services.rest.renabi.common.api.controller.ResponseHandler;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.entidad.application.EntidadApplicationService;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.NuevaEntidadDto;

@RestController
@RequestMapping("api/entidad/")
@Api(value = "/api/entidad/",description="Servicio REST para Entidades - Desarrollado por OTIE/BNP")
@CrossOrigin(origins = "*")
public class EntidadController {
	@Autowired
	EntidadApplicationService entidadApplicationService;
	
	@Autowired
	ResponseHandler responseHandler;
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Crear Entidad", response= ResponseTx.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> crearEntidad(
			@ApiParam(value = "Estructura JSON de la entidad", required = true)
			@RequestBody NuevaEntidadDto entidad) throws Exception {
		try {
			ResponseTx response = entidadApplicationService.save(entidad);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
}
