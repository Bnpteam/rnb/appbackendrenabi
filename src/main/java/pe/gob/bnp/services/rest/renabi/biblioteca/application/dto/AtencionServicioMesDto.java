package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioMes;
import pe.gob.bnp.services.rest.renabi.common.Constants;

public class AtencionServicioMesDto{
	private String idMes;
	private String mes;
	private String valor;

	public AtencionServicioMesDto() {
		super();
		this.mes = Constants.EMPTY_STRING;
		this.idMes = Constants.EMPTY_STRING;
		this.valor = Constants.EMPTY_STRING;
	}
	
	public AtencionServicioMesDto(AtencionServicioMes objeto) {
		super();
		this.mes = objeto.getMes().getNombre();
		this.idMes = objeto.getMes().getId();
		this.valor = objeto.getValor();
	}	

	public String getIdMes() {
		return idMes;
	}

	public void setIdMes(String idMes) {
		this.idMes = idMes;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	
	
}
