package pe.gob.bnp.services.rest.renabi.common.util.alfresco;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import ws.alfresco.servir.gob.pe.AlfrescoService;
import ws.alfresco.servir.gob.pe.AlfrescoService_Service;
import ws.alfresco.servir.gob.pe.ArchivoType;
import ws.alfresco.servir.gob.pe.AuditoriaType;
import ws.alfresco.servir.gob.pe.AutenticacionType;
import ws.alfresco.servir.gob.pe.ResponseAutenticacionType;
import ws.alfresco.servir.gob.pe.ResponseConsultarContenidoType;
import ws.alfresco.servir.gob.pe.ResponseDescargarArchivoType;
import ws.alfresco.servir.gob.pe.ResponseModificarContenidoType;
import ws.alfresco.servir.gob.pe.ResponseRegistrarContenidoType;
import ws.alfresco.servir.gob.pe.SeguridadType;
import ws.alfresco.servir.gob.pe.TokenType;

public class AlfrescoBase {

	private String 							sWsdl;
	private String 							endPoint;
	private String 							wsURI;
	
    private SeguridadType 					seguridad;
    private AuditoriaType 					auditoria;
    private AutenticacionType 				autenticacion;
    private	TokenType 						token;
    private	ResponseAutenticacionType 		responseAutenticacion;
	
    private	URL 							url;
    private	QName 							qws;

    private	AlfrescoService_Service 		service;
    private AlfrescoService 				port;
    
    private ResponseRegistrarContenidoType	responseRegistrarContenidoType;
    private ResponseModificarContenidoType	responseModificarContenidoType;
    
    private ResponseDescargarArchivoType	responseDescargarArchivoType;
	
    
    //@Value("${alfresco.wsdl}")
    private String alfrescoWsdl;
    
    //@Value("${alfresco.endpoint}")
    private String alfrescoEndpoint;

    //@Value("${alfresco.ws.uri}")
    private String alfrescoWsUri;

    //@Value("${alfresco.service}")
    private String alfrescoService;

    //@Value("${alfresco.repositorio.carpeta}")
    private String alfrescoRepositorioCarpeta;

    //@Value("${alfresco.codigo}")
    private String alfrescoCodigo;

    //@Value("${alfresco.usuario}")
    private String alfrescoUsuario;

    //@Value("${alfresco.password}")
    private String alfrescoPassword;
    
    //@Value("${alfresco.cantidad.general}")
    private String alfrescoCantidadGeneral;    
    
    public AlfrescoBase(){
    	this.init();
    }
    
	public AlfrescoBase(String alfrescoWsdl, String alfrescoEndpoint, String alfrescoWsUri, String alfrescoService,
			String alfrescoRepositorioCarpeta, String alfrescoCodigo, String alfrescoUsuario, String alfrescoPassword,
			String alfrescoCantidadGeneral) {
		this.alfrescoWsdl = alfrescoWsdl;
		this.alfrescoEndpoint = alfrescoEndpoint;
		this.alfrescoWsUri = alfrescoWsUri;
		this.alfrescoService = alfrescoService;
		this.alfrescoRepositorioCarpeta = alfrescoRepositorioCarpeta;
		this.alfrescoCodigo = alfrescoCodigo;
		this.alfrescoUsuario = alfrescoUsuario;
		this.alfrescoPassword = alfrescoPassword;
		this.alfrescoCantidadGeneral = alfrescoCantidadGeneral;
		this.init();
	}



	public void init(){
    	this.setSeguridad(new SeguridadType());
    	this.setAuditoria(new AuditoriaType());
    	this.setAutenticacion(new AutenticacionType());
    	this.setToken(new TokenType());
    	this.setResponseAutenticacion(new ResponseAutenticacionType());
    	
    	this.setResponseRegistrarContenidoType(new ResponseRegistrarContenidoType());

    	this.inicializarValores();
    }
    
    private void inicializarValores(){
    	this.setsWsdl(alfrescoWsdl);
    	this.setEndPoint(alfrescoEndpoint);
    	this.setWsURI(alfrescoWsUri);
    	
    	this.setQws(new QName(wsURI, alfrescoService));
    	
    	try {
			this.setUrl(new URL(this.getsWsdl()));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
    	
    	this.setService(new AlfrescoService_Service(this.getUrl(), this.getQws()));
    	this.setPort(this.getService().getAlfrescoServiceImplPort());
    	
    	((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPoint);
    	
    	this.getSeguridad().setCodAplicativo(alfrescoCodigo);
    	this.getAuditoria().setIpPc("127.0.0.1");
    	this.getAuditoria().setMacAddressPc("00:00:00:00:00");
    	
    	this.getAutenticacion().setUsuario(alfrescoUsuario);
    	this.getAutenticacion().setPassword(alfrescoPassword);
    	
    	this.setResponseAutenticacion(this.getPort().autenticacion(this.getSeguridad(), this.getAuditoria(), this.getAutenticacion()));
    }
    
	public String getString(String key) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		Properties properties = new Properties();
		try {
			properties.load(cl.getResourceAsStream("config.properties"));
			return properties.getProperty(key); 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "";
	}

	public FileAlfresco obtenerArchivoAlfresco(String uiid){
		
		FileAlfresco fileAlfresco = new FileAlfresco();
		ResponseConsultarContenidoType rcct = new ResponseConsultarContenidoType();
		
		ArchivoType archivoType = new ArchivoType();
		archivoType.setUiidRecurso(uiid);
		
		this.setResponseAutenticacion(this.getPort().autenticacion(this.getSeguridad(), this.getAuditoria(), this.getAutenticacion()));
		this.setToken(this.getResponseAutenticacion().getToken());
		
		rcct = this.getPort().consultarContenido(this.getToken(), this.getSeguridad(), this.getAuditoria(), archivoType);
		
		fileAlfresco.setRawFile(rcct.getArchivo().getDocumento());
		fileAlfresco.setFileName(rcct.getMetadata().getParameter2());
		
		//String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
		fileAlfresco.setExtension(fileAlfresco.getFileName().substring(fileAlfresco.getFileName().lastIndexOf(".") + 1));
		
		return fileAlfresco;
	}
	
	
	public String getsWsdl() {
		return sWsdl;
	}

	public void setsWsdl(String sWsdl) {
		this.sWsdl = sWsdl;
	}

	public String getEndPoint() {
		return endPoint;
	}

	public void setEndPoint(String endPoint) {
		this.endPoint = endPoint;
	}

	public String getWsURI() {
		return wsURI;
	}

	public void setWsURI(String wsURI) {
		this.wsURI = wsURI;
	}

	public SeguridadType getSeguridad() {
		return seguridad;
	}

	public void setSeguridad(SeguridadType seguridad) {
		this.seguridad = seguridad;
	}

	public AuditoriaType getAuditoria() {
		return auditoria;
	}

	public void setAuditoria(AuditoriaType auditoria) {
		this.auditoria = auditoria;
	}

	public AutenticacionType getAutenticacion() {
		return autenticacion;
	}

	public void setAutenticacion(AutenticacionType autenticacion) {
		this.autenticacion = autenticacion;
	}

	public TokenType getToken() {
		return token;
	}

	public void setToken(TokenType token) {
		this.token = token;
	}

	public ResponseAutenticacionType getResponseAutenticacion() {
		return responseAutenticacion;
	}

	public void setResponseAutenticacion(
			ResponseAutenticacionType responseAutenticacion) {
		this.responseAutenticacion = responseAutenticacion;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public QName getQws() {
		return qws;
	}

	public void setQws(QName qws) {
		this.qws = qws;
	}

	public AlfrescoService_Service getService() {
		return service;
	}

	public void setService(AlfrescoService_Service service) {
		this.service = service;
	}

	public AlfrescoService getPort() {
		return port;
	}

	public void setPort(AlfrescoService port) {
		this.port = port;
	}

	public ResponseRegistrarContenidoType getResponseRegistrarContenidoType() {
		return responseRegistrarContenidoType;
	}

	public void setResponseRegistrarContenidoType(
			ResponseRegistrarContenidoType responseRegistrarContenidoType) {
		this.responseRegistrarContenidoType = responseRegistrarContenidoType;
	}

	public ResponseModificarContenidoType getResponseModificarContenidoType() {
		return responseModificarContenidoType;
	}

	public void setResponseModificarContenidoType(
			ResponseModificarContenidoType responseModificarContenidoType) {
		this.responseModificarContenidoType = responseModificarContenidoType;
	}

	public ResponseDescargarArchivoType getResponseDescargarArchivoType() {
		return responseDescargarArchivoType;
	}

	public void setResponseDescargarArchivoType(
			ResponseDescargarArchivoType responseDescargarArchivoType) {
		this.responseDescargarArchivoType = responseDescargarArchivoType;
	}

	public String getAlfrescoWsdl() {
		return alfrescoWsdl;
	}

	public void setAlfrescoWsdl(String alfrescoWsdl) {
		this.alfrescoWsdl = alfrescoWsdl;
	}

	public String getAlfrescoEndpoint() {
		return alfrescoEndpoint;
	}

	public void setAlfrescoEndpoint(String alfrescoEndpoint) {
		this.alfrescoEndpoint = alfrescoEndpoint;
	}

	public String getAlfrescoWsUri() {
		return alfrescoWsUri;
	}

	public void setAlfrescoWsUri(String alfrescoWsUri) {
		this.alfrescoWsUri = alfrescoWsUri;
	}

	public String getAlfrescoService() {
		return alfrescoService;
	}

	public void setAlfrescoService(String alfrescoService) {
		this.alfrescoService = alfrescoService;
	}

	public String getAlfrescoRepositorioCarpeta() {
		return alfrescoRepositorioCarpeta;
	}

	public void setAlfrescoRepositorioCarpeta(String alfrescoRepositorioCarpeta) {
		this.alfrescoRepositorioCarpeta = alfrescoRepositorioCarpeta;
	}

	public String getAlfrescoCodigo() {
		return alfrescoCodigo;
	}

	public void setAlfrescoCodigo(String alfrescoCodigo) {
		this.alfrescoCodigo = alfrescoCodigo;
	}

	public String getAlfrescoUsuario() {
		return alfrescoUsuario;
	}

	public void setAlfrescoUsuario(String alfrescoUsuario) {
		this.alfrescoUsuario = alfrescoUsuario;
	}

	public String getAlfrescoPassword() {
		return alfrescoPassword;
	}

	public void setAlfrescoPassword(String alfrescoPassword) {
		this.alfrescoPassword = alfrescoPassword;
	}

	public String getAlfrescoCantidadGeneral() {
		return alfrescoCantidadGeneral;
	}

	public void setAlfrescoCantidadGeneral(String alfrescoCantidadGeneral) {
		this.alfrescoCantidadGeneral = alfrescoCantidadGeneral;
	}
	
	
	
}

