package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.application.dto.NuevoTipoValorDto;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.SubsanarEntidadDto;

public class SubsanarBibliotecaDto {
	//Datos generales
	private String id;
	private String nombre;
	private String correoElectronico;
	private String telefonoFijo;
	private String telefonoMovil;
	private String numeroBibliotecologos;
	private String numeroTotalPersonal;
	private String totalLectoresAnioAnterior;
	private String totalLectores;
	private String totalConsultas;
	private String planTrabActual;
	private String reglamentos;
	private String estadisticas;
	private String especificar;
	private String idUsuarioAuditoria;

	//Ambito Territorial
	private String idAmbitoTerritorial;

	//Tipo de Funcionamiento
	private String idTipoFuncionamiento;

	//Entidad
	private SubsanarEntidadDto entidad;
	
	//Responsable
	private SubsanarResponsableDto responsable;

	//Tipo biblioteca
	private	String idTipoBibliotecaPadre;
	private String idTipoBibliotecaHijo;
	
	
	//ubigeo
	private String codDepartamento;
	private String codProvincia;
	private String codDistrito;
	private String centroPoblado;
	private String direccion;
	
	//Infraestructura
	private NuevaInfraestructuraDto infraestructura;	
	private List<NuevoTipoValorDto> colecciones;
	private List<NuevoTipoValorDto> servicios;
	
	//Atencion del servicio
	private AtencionServicioDto atencionServicio;
	
	public SubsanarBibliotecaDto(){
		this.infraestructura = new  NuevaInfraestructuraDto();
		this.entidad = new SubsanarEntidadDto();
		this.responsable = new SubsanarResponsableDto();
		this.infraestructura = new NuevaInfraestructuraDto();
		this.colecciones = new ArrayList<>();
		this.servicios = new ArrayList<>();
		this.atencionServicio = new  AtencionServicioDto();
		this.idUsuarioAuditoria = Constants.EMPTY_STRING;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getNumeroBibliotecologos() {
		return numeroBibliotecologos;
	}

	public void setNumeroBibliotecologos(String numeroBibliotecologos) {
		this.numeroBibliotecologos = numeroBibliotecologos;
	}

	public String getNumeroTotalPersonal() {
		return numeroTotalPersonal;
	}

	public void setNumeroTotalPersonal(String numeroTotalPersonal) {
		this.numeroTotalPersonal = numeroTotalPersonal;
	}


	public String getTotalLectoresAnioAnterior() {
		return totalLectoresAnioAnterior;
	}

	public void setTotalLectoresAnioAnterior(String totalLectoresAnioAnterior) {
		this.totalLectoresAnioAnterior = totalLectoresAnioAnterior;
	}

	public String getTotalLectores() {
		return totalLectores;
	}

	public void setTotalLectores(String totalLectores) {
		this.totalLectores = totalLectores;
	}

	public String getTotalConsultas() {
		return totalConsultas;
	}

	public void setTotalConsultas(String totalConsultas) {
		this.totalConsultas = totalConsultas;
	}

	public String getPlanTrabActual() {
		return planTrabActual;
	}

	public void setPlanTrabActual(String planTrabActual) {
		this.planTrabActual = planTrabActual;
	}

	public String getReglamentos() {
		return reglamentos;
	}

	public void setReglamentos(String reglamentos) {
		this.reglamentos = reglamentos;
	}

	public String getEstadisticas() {
		return estadisticas;
	}

	public void setEstadisticas(String estadisticas) {
		this.estadisticas = estadisticas;
	}

	public String getEspecificar() {
		return especificar;
	}

	public void setEspecificar(String especificar) {
		this.especificar = especificar;
	}

	public String getIdAmbitoTerritorial() {
		return idAmbitoTerritorial;
	}

	public void setIdAmbitoTerritorial(String idAmbitoTerritorial) {
		this.idAmbitoTerritorial = idAmbitoTerritorial;
	}

	public String getIdTipoFuncionamiento() {
		return idTipoFuncionamiento;
	}

	public void setIdTipoFuncionamiento(String idTipoFuncionamiento) {
		this.idTipoFuncionamiento = idTipoFuncionamiento;
	}

	
	public SubsanarEntidadDto getEntidad() {
		return entidad;
	}

	public void setEntidad(SubsanarEntidadDto entidad) {
		this.entidad = entidad;
	}

	public SubsanarResponsableDto getResponsable() {
		return responsable;
	}

	public void setResponsable(SubsanarResponsableDto responsable) {
		this.responsable = responsable;
	}

	public String getIdTipoBibliotecaPadre() {
		return idTipoBibliotecaPadre;
	}

	public void setIdTipoBibliotecaPadre(String idTipoBibliotecaPadre) {
		this.idTipoBibliotecaPadre = idTipoBibliotecaPadre;
	}

	public String getIdTipoBibliotecaHijo() {
		return idTipoBibliotecaHijo;
	}

	public void setIdTipoBibliotecaHijo(String idTipoBibliotecaHijo) {
		this.idTipoBibliotecaHijo = idTipoBibliotecaHijo;
	}

	public String getCodDepartamento() {
		return codDepartamento;
	}

	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}

	public String getCodProvincia() {
		return codProvincia;
	}

	public void setCodProvincia(String codProvincia) {
		this.codProvincia = codProvincia;
	}

	public String getCodDistrito() {
		return codDistrito;
	}

	public void setCodDistrito(String codDistrito) {
		this.codDistrito = codDistrito;
	}

	public String getCentroPoblado() {
		return centroPoblado;
	}

	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public NuevaInfraestructuraDto getInfraestructura() {
		return infraestructura;
	}

	public void setInfraestructura(NuevaInfraestructuraDto infraestructura) {
		this.infraestructura = infraestructura;
	}

	public List<NuevoTipoValorDto> getColecciones() {
		return colecciones;
	}

	public void setColecciones(List<NuevoTipoValorDto> colecciones) {
		this.colecciones = colecciones;
	}

	public List<NuevoTipoValorDto> getServicios() {
		return servicios;
	}

	public void setServicios(List<NuevoTipoValorDto> servicios) {
		this.servicios = servicios;
	}

	public AtencionServicioDto getAtencionServicio() {
		return atencionServicio;
	}

	public void setAtencionServicio(AtencionServicioDto atencionServicio) {
		this.atencionServicio = atencionServicio;
	}

	public String getIdUsuarioAuditoria() {
		return idUsuarioAuditoria;
	}

	public void setIdUsuarioAuditoria(String idUsuarioAuditoria) {
		this.idUsuarioAuditoria = idUsuarioAuditoria;
	}

	

}
