package pe.gob.bnp.services.rest.renabi.evento.domain.entity;

import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;

public class EventoResumen extends BaseEntity{

	private String tipoEvento;
	private String nombreBiblioteca;
	private String nombreResponsable;
	private String detalleEvento;
	private String fechaInicio;
	private String fechaFin;
	
	
	public String getTipoEvento() {
		return tipoEvento;
	}
	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	public String getNombreBiblioteca() {
		return nombreBiblioteca;
	}
	public void setNombreBiblioteca(String nombreBiblioteca) {
		this.nombreBiblioteca = nombreBiblioteca;
	}
	public String getNombreResponsable() {
		return nombreResponsable;
	}
	public void setNombreResponsable(String nombreResponsable) {
		this.nombreResponsable = nombreResponsable;
	}
	public String getDetalleEvento() {
		return detalleEvento;
	}
	public void setDetalleEvento(String detalleEvento) {
		this.detalleEvento = detalleEvento;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	
}
