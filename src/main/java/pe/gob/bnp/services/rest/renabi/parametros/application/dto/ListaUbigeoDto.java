package pe.gob.bnp.services.rest.renabi.parametros.application.dto;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;

@ApiModel(value="Lista de Ubigeos", description="Objeto que representa la lista de ubigeos de reniec")
public class ListaUbigeoDto {
	@ApiModelProperty(notes = "Resultado de la solicitud")
	private ResponseTx response;
	
	@ApiModelProperty(notes = "Lista de registros obtenidos", required=true)
	private List<UbigeoDto> lista;
	
	public ListaUbigeoDto() {
		super();
		this.response = new ResponseTx();
		this.lista = new ArrayList<>();
	}
	
	public ResponseTx getResponse() {
		return response;
	}
	public void setResponse(ResponseTx response) {
		this.response = response;
	}

	public List<UbigeoDto> getLista() {
		return lista;
	}

	public void setLista(List<UbigeoDto> lista) {
		this.lista = lista;
	}

}
