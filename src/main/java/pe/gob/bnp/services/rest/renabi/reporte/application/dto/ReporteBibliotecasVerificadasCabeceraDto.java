package pe.gob.bnp.services.rest.renabi.reporte.application.dto;

public class ReporteBibliotecasVerificadasCabeceraDto {
	
	private String id;
	private String rowBiblioteca;	
	private String razonSocial;
	private String nombreBiblioteca;	
	private String departamento;
	private String provincia;
	private String distrito;
	private String centroPoblado;
	private String fechaVerificacion;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRowBiblioteca() {
		return rowBiblioteca;
	}
	public void setRowBiblioteca(String rowBiblioteca) {
		this.rowBiblioteca = rowBiblioteca;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getNombreBiblioteca() {
		return nombreBiblioteca;
	}
	public void setNombreBiblioteca(String nombreBiblioteca) {
		this.nombreBiblioteca = nombreBiblioteca;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getCentroPoblado() {
		return centroPoblado;
	}
	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}
	public String getFechaVerificacion() {
		return fechaVerificacion;
	}
	public void setFechaVerificacion(String fechaVerificacion) {
		this.fechaVerificacion = fechaVerificacion;
	}

}
