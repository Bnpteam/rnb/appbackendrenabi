package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.AtencionServicioDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.AtencionServicioMesDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.AtencionServicioTurnoDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaMasterServicioDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.NuevaBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.SubsanarBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.VerBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioMes;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurno;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Biblioteca;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaMasterServicio;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.dto.NuevoTipoValorDto;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.TipoValor;

@Component
public class BibliotecaMapper {
	public NuevaBibliotecaDto mapperNuevo(Biblioteca objeto) {
		NuevaBibliotecaDto dto = new NuevaBibliotecaDto(objeto);
		return dto;
	}
	public VerBibliotecaDto mapper(Biblioteca objeto){
		VerBibliotecaDto dto = new VerBibliotecaDto();
		dto.setId(Util.getLongToString(objeto.getId()));
		dto.setNombre(Util.getString(objeto.getNombre()));
		dto.setAmbitoTerritorial(objeto.getAmbitoTerritorial().getNombre());
		dto.setAnioDeclaracion(objeto.getAnioDeclaracion());
		dto.setCentroPoblado(objeto.getUbigeo().getCentroPoblado());
		dto.setCodDepartamento(objeto.getUbigeo().getCodDepartamento());
		dto.setCodDistrito(objeto.getUbigeo().getCodDistrito());
		dto.setCodProvincia(objeto.getUbigeo().getCodProvincia());
		dto.setCorreoElectronico(objeto.getCorreoElectronico());
		dto.setDepartamento(objeto.getUbigeo().getDepartamento());
		dto.setDireccion(objeto.getUbigeo().getDireccion());
		dto.setDistrito(objeto.getUbigeo().getDistrito());
		dto.setDocIdentidadRegistrador(objeto.getRegistrador().getPersona().getDocumentoIdentidad());
		dto.setNombreRegistrador(objeto.getRegistrador().getPersona().getNombreCompleto());
		dto.setIdRegistrador(Util.getLongToString(objeto.getRegistrador().getId()));
		dto.setEstadoBiblioteca(objeto.getEstadoBiblioteca().getNombre());
		dto.setIdAmbitoTerritorial(objeto.getAmbitoTerritorial().getId());
		dto.setIdEstadoBiblioteca(objeto.getEstadoBiblioteca().getId());
		dto.setIdTipoBibliotecaHijo(Util.getIntToString(objeto.getTipoBiblioteca().getIdTipoBiblioteca()));
		dto.setIdTipoBibliotecaPadre(Util.getIntToString(objeto.getTipoBiblioteca().getIdTipoBibliotecaPadre()));
		dto.setIdTipoFuncionamiento(Util.getString(objeto.getTipoFuncionamiento().getId()));
		dto.setNumeroBibliotecologos(Util.getIntToString(objeto.getNumeroBibliotecologos()));
		dto.setNumeroTotalPersonal(Util.getIntToString(objeto.getNumeroTotalPersonal()));
		dto.setProvincia(Util.getString(objeto.getUbigeo().getProvincia()));
		dto.setTelefonoFijo(Util.getString(objeto.getTelefonoFijo()));
		dto.setTelefonoMovil(Util.getString(objeto.getTelefonoMovil()));
		dto.setTipoBibliotecaHijo(Util.getString(objeto.getTipoBiblioteca().getNombre()));
		dto.setTipoBibliotecaPadre(Util.getString(objeto.getTipoBiblioteca().getNombrePadre()));
		dto.setTipoFuncionamiento(Util.getString(objeto.getTipoFuncionamiento().getNombre()));
		dto.setTotalLectores(objeto.getLectorTotal());
		dto.setTotalConsultas(objeto.getLectorTotalConsultas());
		dto.setTotalLectoresAnioAnterior(objeto.getLectorTotalAnioAnterior());
		dto.setPlanTrabActual(objeto.getFlagGestBibPlanTrabActual());
		dto.setReglamentos(objeto.getFlagGestBibReglamentos());
		dto.setEstadisticas(objeto.getFlagGestBibEstadisticas());
		dto.setEspecificar(objeto.getGestionBibEspecificar());

		//responsable
		dto.getResponsable().setId(Util.getLongToString(objeto.getResponsable().getId()));
		dto.getResponsable().setApellidoMaterno(objeto.getResponsable().getPersona().getApellidoMaterno());
		dto.getResponsable().setApellidoPaterno(objeto.getResponsable().getPersona().getApellidoPaterno());
		dto.getResponsable().setCondicionLaboral(objeto.getResponsable().getCondicionLaboral().getNombre());
		dto.getResponsable().setCorreo(objeto.getResponsable().getCorreo());
		dto.getResponsable().setDireccion(objeto.getResponsable().getDireccion());
		//dto.getResponsable().setFechaNacimiento(Util.utilDateToString(objeto.getResponsable().getPersona().getFechaActualizacion()));
		dto.getResponsable().setFechaNacimiento(Util.utilDateToString(objeto.getResponsable().getPersona().getFechaNacimiento()));
		dto.getResponsable().setGenero(objeto.getResponsable().getPersona().getGenero());
		dto.getResponsable().setGradoInstruccion(objeto.getResponsable().getGradoInstruccion().getNombre());
		dto.getResponsable().setIdCondicionLaboral(objeto.getResponsable().getCondicionLaboral().getId());
		dto.getResponsable().setIdEstado(objeto.getResponsable().getIdEstado());
		dto.getResponsable().setIdGradoInstruccion(objeto.getResponsable().getGradoInstruccion().getId());
		dto.getResponsable().setIdTipoDocumento(objeto.getResponsable().getPersona().getTipoDocumentoIdentidad().getNombre());
		dto.getResponsable().setNombres(objeto.getResponsable().getPersona().getNombres());
		dto.getResponsable().setNumeroDocumento(objeto.getResponsable().getPersona().getNumeroDocumentoIdentidad());
		dto.getResponsable().setProfesion(objeto.getResponsable().getProfesion());
		dto.getResponsable().setTelefonoFijo(objeto.getResponsable().getTelefonoFijo());
		dto.getResponsable().setTelefonoMovil(objeto.getResponsable().getTelefonoMovil());

		//entidad
		dto.getEntidad().setCorreo(objeto.getEntidad().getCorreo());
		dto.getEntidad().setDireccion(objeto.getEntidad().getDireccion());		
		dto.getEntidad().setFlagOrigen(objeto.getEntidad().getFlagOrigen());
		dto.getEntidad().setId(Util.getLongToString(objeto.getId()));		
		dto.getEntidad().setIdTipoGestion(objeto.getEntidad().getTipoGestion().getId());		
		dto.getEntidad().setRazonSocial(objeto.getEntidad().getRazonSocial());
		dto.getEntidad().setRuc(objeto.getEntidad().getRuc());		
		dto.getEntidad().setTelefonoFijo(objeto.getEntidad().getTelefonoFijo());		
		dto.getEntidad().setTelefonoMovil(objeto.getEntidad().getTelefonoMovil());		
		dto.getEntidad().setTipoGestion(objeto.getEntidad().getTipoGestion().getNombre());
		dto.getEntidad().setIdEstado(objeto.getEntidad().getIdEstado());

		
		//servicios
		for (TipoValor servicio : objeto.getServicios()) {
			dto.getServicios().add(new NuevoTipoValorDto(servicio));
		}
		
		//colecciones
		for (TipoValor coleccion : objeto.getColecciones()) {
			dto.getColecciones().add(new NuevoTipoValorDto(coleccion));
		}
		
		//infraestructura
		for (TipoValor accesabilidad : objeto.getInfraestructura().getAccesibilidad()) {
			dto.getInfraestructura().getAccesibilidad().add(new NuevoTipoValorDto(accesabilidad));
		}
		dto.getInfraestructura().setAreaM2(objeto.getInfraestructura().getAreaM2());
		dto.getInfraestructura().setIdTipoLocal(objeto.getInfraestructura().getTipoLocal().getId());
		dto.getInfraestructura().setNumeroAmbientes(objeto.getInfraestructura().getNumeroAmbientes());
		dto.getInfraestructura().setTipoLocal(objeto.getInfraestructura().getTipoLocal().getNombre());
		
		//atenncion servicio
		dto.setAtencionServicio(new AtencionServicioDto(objeto.getAtencionServicio()));
		return dto;
	}	
	
	public Biblioteca reverseMapperNuevo(NuevaBibliotecaDto dto) {
		Biblioteca objeto = new Biblioteca();
		objeto.setId(Util.getStringToLong(dto.getId()));
		objeto.getAmbitoTerritorial().setId(dto.getIdAmbitoTerritorial());;
		objeto.setAnioDeclaracion(dto.getAnioDeclaracion());
		objeto.setCorreoElectronico(dto.getCorreoElectronico());
		objeto.setDireccion(dto.getDireccion());
		objeto.getEntidad().setCorreo(dto.getEntidad().getCorreo());
		objeto.getEntidad().setDireccion(dto.getEntidad().getDireccion());
		objeto.getEntidad().setFlagOrigen(dto.getEntidad().getFlagOrigen());
		objeto.getEntidad().setIdEstado(dto.getEntidad().getIdEstado());
		objeto.getEntidad().setIdUsuarioRegistro(dto.getEntidad().getIdCreacionAuditoria());
		objeto.getEntidad().setRazonSocial(dto.getEntidad().getRazonSocial());
		objeto.getEntidad().setRuc(dto.getEntidad().getRuc());
		objeto.getEntidad().setTelefonoFijo(dto.getEntidad().getTelefonoFijo());
		objeto.getEntidad().setTelefonoMovil(dto.getEntidad().getTelefonoMovil());
		objeto.getEntidad().getTipoGestion().setId(dto.getEntidad().getIdTipoGestion());
		objeto.getEstadoBiblioteca().setId(dto.getIdEstadoBiblioteca());
		objeto.setIdUsuarioRegistro(dto.getIdCreacionAuditoria());
		objeto.setIpRegistro(dto.getIpCreacionAuditoria());
		objeto.setNombre(dto.getNombre());
		objeto.setNumeroBibliotecologos(Util.getStringToInt(dto.getNumeroBibliotecologos()));
		objeto.setNumeroTotalPersonal(Util.getStringToInt(dto.getNumeroTotalPersonal()));
		objeto.getRegistrador().setId(Util.getStringToLong(dto.getIdRegistrador()));
		objeto.getResponsable().getCondicionLaboral().setId(dto.getResponsable().getIdCondicionLaboral());
		objeto.getResponsable().setCorreo(dto.getResponsable().getCorreo());
		objeto.getResponsable().setDireccion(dto.getResponsable().getDireccion());
		objeto.getResponsable().setIdEstado(dto.getResponsable().getIdEstado());
		objeto.getResponsable().getGradoInstruccion().setId(dto.getResponsable().getIdGradoInstruccion());
		objeto.getResponsable().setIdUsuarioRegistro(dto.getIdCreacionAuditoria());
		objeto.getResponsable().setProfesion(dto.getResponsable().getProfesion());
		objeto.getResponsable().setTelefonoFijo(dto.getResponsable().getTelefonoFijo());
		objeto.getResponsable().setTelefonoMovil(dto.getResponsable().getTelefonoMovil());
		objeto.getResponsable().getPersona().setApellidoMaterno(dto.getResponsable().getApellidoMaterno());
		objeto.getResponsable().getPersona().setApellidoPaterno(dto.getResponsable().getApellidoPaterno());
		objeto.getResponsable().getPersona().setFechaNacimiento(Util.stringToUtilDate(dto.getResponsable().getFechaNacimiento()));
		objeto.getResponsable().getPersona().setGenero(dto.getResponsable().getGenero());
		objeto.getResponsable().getPersona().setIdEstado(dto.getResponsable().getIdEstado());
		objeto.getResponsable().getPersona().setNombres(dto.getResponsable().getNombres());
		objeto.getResponsable().getPersona().setNumeroDocumentoIdentidad(dto.getResponsable().getNumeroDocumento());
		objeto.getResponsable().getPersona().getTipoDocumentoIdentidad().setNombre(dto.getResponsable().getIdTipoDocumento());
		objeto.getTipoBiblioteca().setIdTipoBiblioteca(Util.getStringToInt(dto.getIdTipoBibliotecaHijo()));
		objeto.getTipoBiblioteca().setIdTipoBibliotecaPadre(Util.getStringToInt(dto.getIdTipoBibliotecaPadre()));
		objeto.getUbigeo().setCentroPoblado(dto.getCentroPoblado());
		objeto.getUbigeo().setCodDepartamento(dto.getCodDepartamento());
		objeto.getUbigeo().setCodDistrito(dto.getCodDistrito());
		objeto.getUbigeo().setCodProvincia(dto.getCodProvincia());
		objeto.getUbigeo().setDireccion(dto.getDireccion());
		objeto.getTipoFuncionamiento().setId(dto.getIdTipoFuncionamiento());
		objeto.setTelefonoFijo(dto.getTelefonoFijo());
		objeto.setTelefonoMovil(dto.getTelefonoMovil());
		objeto.getInfraestructura().setAreaM2(dto.getInfraestructura().getAreaM2());
		objeto.getInfraestructura().setNumeroAmbientes(dto.getInfraestructura().getNumeroAmbientes());
		objeto.getInfraestructura().getTipoLocal().setId(dto.getInfraestructura().getIdTipoLocal());
		
		objeto.setFlagGestBibEstadisticas(Util.getString(dto.getEstadisticas()));
		objeto.setFlagGestBibPlanTrabActual(Util.getString(dto.getPlanTrabActual()));
		objeto.setFlagGestBibReglamentos(Util.getString(dto.getReglamentos()));
		objeto.setGestionBibEspecificar(Util.getString(dto.getEspecificar()));
		objeto.setLectorTotal(Util.getString(dto.getTotalLectores()));
		objeto.setLectorTotalAnioAnterior(Util.getString(dto.getTotalLectoresAnioAnterior()));
		objeto.setLectorTotalConsultas(Util.getString(dto.getTotalConsultas()));
		
		//atencion del servicio
		objeto.getAtencionServicio().getTipoAccesoPublico().setId(dto.getAtencionServicio().getIdAccesoPublico()); 
		objeto.getAtencionServicio().getTipoAccesoPublico().setNombre(dto.getAtencionServicio().getNombreAccesoPublico());
		//meses
		for (AtencionServicioMesDto atencionServicioMesDto : dto.getAtencionServicio().getMeses()) {
			objeto.getAtencionServicio().getMeses().add(new AtencionServicioMes(atencionServicioMesDto));
		}
		//dias
		for (AtencionServicioTurnoDto atencionServicioTurnoDto : dto.getAtencionServicio().getDias()) {
			objeto.getAtencionServicio().getDias().add(new AtencionServicioTurno(atencionServicioTurnoDto));
		} 

		//infraestructura accesibilidad 
		for (NuevoTipoValorDto tipoValorDto : dto.getInfraestructura().getAccesibilidad() ) {
			TipoValor tipoValor = new TipoValor(tipoValorDto);
			objeto.getInfraestructura().getAccesibilidad().add(tipoValor);
		}
		
		//colecciones 
		for (NuevoTipoValorDto tipoValorDto : dto.getColecciones() ) {
			TipoValor tipoValor = new TipoValor(tipoValorDto);
			objeto.getColecciones().add(tipoValor);
		}

		//servicios
		for (NuevoTipoValorDto tipoValorDto : dto.getServicios() ) {
			TipoValor tipoValor = new TipoValor(tipoValorDto);
			objeto.getServicios().add(tipoValor);
		}
		objeto.setFlagAceptoDDJJ(dto.getFlagAceptoDDJJ());
		objeto.setFlagAceptoTyC(dto.getFlagAceptoTyC());
		
		return objeto;
	}
	
	public Biblioteca reverseMapperNuevoManual(NuevaBibliotecaDto dto) {
		Biblioteca objeto = new Biblioteca();
		objeto.setId(Util.getStringToLong(dto.getId()));
		objeto.getAmbitoTerritorial().setId(dto.getIdAmbitoTerritorial());;
		objeto.setAnioDeclaracion(dto.getAnioDeclaracion());
		objeto.setCorreoElectronico(dto.getCorreoElectronico());
		objeto.setDireccion(dto.getDireccion());
		objeto.getEntidad().setCorreo(dto.getEntidad().getCorreo());
		objeto.getEntidad().setDireccion(dto.getEntidad().getDireccion());
		objeto.getEntidad().setFlagOrigen(dto.getEntidad().getFlagOrigen());
		objeto.getEntidad().setIdEstado(dto.getEntidad().getIdEstado());
		objeto.getEntidad().setIdUsuarioRegistro(dto.getEntidad().getIdCreacionAuditoria());
		objeto.getEntidad().setRazonSocial(dto.getEntidad().getRazonSocial());
		//objeto.getEntidad().setRuc(dto.getEntidad().getRuc());
		objeto.getEntidad().setTelefonoFijo(dto.getEntidad().getTelefonoFijo());
		objeto.getEntidad().setTelefonoMovil(dto.getEntidad().getTelefonoMovil());
		objeto.getEntidad().getTipoGestion().setId(dto.getEntidad().getIdTipoGestion());
		objeto.getEstadoBiblioteca().setId(dto.getIdEstadoBiblioteca());
		objeto.setIdUsuarioRegistro(dto.getIdCreacionAuditoria());
		objeto.setIpRegistro(dto.getIpCreacionAuditoria());
		objeto.setNombre(dto.getNombre());
		objeto.setNumeroBibliotecologos(Util.getStringToInt(dto.getNumeroBibliotecologos()));
		objeto.setNumeroTotalPersonal(Util.getStringToInt(dto.getNumeroTotalPersonal()));
		objeto.getRegistrador().setId(Util.getStringToLong(dto.getIdRegistrador()));
		objeto.getResponsable().getCondicionLaboral().setId(dto.getResponsable().getIdCondicionLaboral());
		objeto.getResponsable().setCorreo(dto.getResponsable().getCorreo());
		objeto.getResponsable().setDireccion(dto.getResponsable().getDireccion());
		objeto.getResponsable().setIdEstado(dto.getResponsable().getIdEstado());
		objeto.getResponsable().getGradoInstruccion().setId(dto.getResponsable().getIdGradoInstruccion());
		objeto.getResponsable().setIdUsuarioRegistro(dto.getIdCreacionAuditoria());
		objeto.getResponsable().setProfesion(dto.getResponsable().getProfesion());
		objeto.getResponsable().setTelefonoFijo(dto.getResponsable().getTelefonoFijo());
		objeto.getResponsable().setTelefonoMovil(dto.getResponsable().getTelefonoMovil());
		objeto.getResponsable().getPersona().setApellidoMaterno(dto.getResponsable().getApellidoMaterno());
		objeto.getResponsable().getPersona().setApellidoPaterno(dto.getResponsable().getApellidoPaterno());
		objeto.getResponsable().getPersona().setFechaNacimiento(Util.stringToUtilDate(dto.getResponsable().getFechaNacimiento()));
		objeto.getResponsable().getPersona().setGenero(dto.getResponsable().getGenero());
		objeto.getResponsable().getPersona().setIdEstado(dto.getResponsable().getIdEstado());
		objeto.getResponsable().getPersona().setNombres(dto.getResponsable().getNombres());
		objeto.getResponsable().getPersona().setNumeroDocumentoIdentidad(dto.getResponsable().getNumeroDocumento());
		objeto.getResponsable().getPersona().getTipoDocumentoIdentidad().setNombre(dto.getResponsable().getIdTipoDocumento());
		objeto.getTipoBiblioteca().setIdTipoBiblioteca(Util.getStringToInt(dto.getIdTipoBibliotecaHijo()));
		objeto.getTipoBiblioteca().setIdTipoBibliotecaPadre(Util.getStringToInt(dto.getIdTipoBibliotecaPadre()));
		objeto.getUbigeo().setCentroPoblado(dto.getCentroPoblado());
		objeto.getUbigeo().setCodDepartamento(dto.getCodDepartamento());
		objeto.getUbigeo().setCodDistrito(dto.getCodDistrito());
		objeto.getUbigeo().setCodProvincia(dto.getCodProvincia());
		objeto.getUbigeo().setDireccion(dto.getDireccion());
		objeto.getTipoFuncionamiento().setId(dto.getIdTipoFuncionamiento());
		objeto.setTelefonoFijo(dto.getTelefonoFijo());
		objeto.setTelefonoMovil(dto.getTelefonoMovil());
		objeto.getInfraestructura().setAreaM2(dto.getInfraestructura().getAreaM2());
		objeto.getInfraestructura().setNumeroAmbientes(dto.getInfraestructura().getNumeroAmbientes());
		objeto.getInfraestructura().getTipoLocal().setId(dto.getInfraestructura().getIdTipoLocal());
		
		objeto.setFlagGestBibEstadisticas(Util.getString(dto.getEstadisticas()));
		objeto.setFlagGestBibPlanTrabActual(Util.getString(dto.getPlanTrabActual()));
		objeto.setFlagGestBibReglamentos(Util.getString(dto.getReglamentos()));
		objeto.setGestionBibEspecificar(Util.getString(dto.getEspecificar()));
		objeto.setLectorTotal(Util.getString(dto.getTotalLectores()));
		objeto.setLectorTotalAnioAnterior(Util.getString(dto.getTotalLectoresAnioAnterior()));
		objeto.setLectorTotalConsultas(Util.getString(dto.getTotalConsultas()));
		
		//atencion del servicio
		objeto.getAtencionServicio().getTipoAccesoPublico().setId(dto.getAtencionServicio().getIdAccesoPublico()); 
		objeto.getAtencionServicio().getTipoAccesoPublico().setNombre(dto.getAtencionServicio().getNombreAccesoPublico());
		//meses
		for (AtencionServicioMesDto atencionServicioMesDto : dto.getAtencionServicio().getMeses()) {
			objeto.getAtencionServicio().getMeses().add(new AtencionServicioMes(atencionServicioMesDto));
		}
		//dias
		for (AtencionServicioTurnoDto atencionServicioTurnoDto : dto.getAtencionServicio().getDias()) {
			objeto.getAtencionServicio().getDias().add(new AtencionServicioTurno(atencionServicioTurnoDto));
		} 

		//infraestructura accesibilidad 
		for (NuevoTipoValorDto tipoValorDto : dto.getInfraestructura().getAccesibilidad() ) {
			TipoValor tipoValor = new TipoValor(tipoValorDto);
			objeto.getInfraestructura().getAccesibilidad().add(tipoValor);
		}
		
		//colecciones 
		for (NuevoTipoValorDto tipoValorDto : dto.getColecciones() ) {
			TipoValor tipoValor = new TipoValor(tipoValorDto);
			objeto.getColecciones().add(tipoValor);
		}

		//servicios
		for (NuevoTipoValorDto tipoValorDto : dto.getServicios() ) {
			TipoValor tipoValor = new TipoValor(tipoValorDto);
			objeto.getServicios().add(tipoValor);
		}
		objeto.setFlagAceptoDDJJ(dto.getFlagAceptoDDJJ());
		objeto.setFlagAceptoTyC(dto.getFlagAceptoTyC());
		
		return objeto;
	}
	
	public Biblioteca reverseMapperSubsanar(SubsanarBibliotecaDto dto) {
		Biblioteca objeto = new Biblioteca();
		objeto.setId(Util.getStringToLong(dto.getId()));
		objeto.getAmbitoTerritorial().setId(dto.getIdAmbitoTerritorial());;
		objeto.setCorreoElectronico(dto.getCorreoElectronico());
		objeto.setDireccion(dto.getDireccion());
		objeto.getEntidad().setCorreo(dto.getEntidad().getCorreo());
		objeto.getEntidad().setDireccion(dto.getEntidad().getDireccion());
		objeto.getEntidad().setTelefonoFijo(dto.getEntidad().getTelefonoFijo());
		objeto.getEntidad().setTelefonoMovil(dto.getEntidad().getTelefonoMovil());
		objeto.getEntidad().getTipoGestion().setId(dto.getEntidad().getIdTipoGestion());
		objeto.setNombre(dto.getNombre());
		objeto.setNumeroBibliotecologos(Util.getStringToInt(dto.getNumeroBibliotecologos()));
		objeto.setNumeroTotalPersonal(Util.getStringToInt(dto.getNumeroTotalPersonal()));
		objeto.getResponsable().getCondicionLaboral().setId(dto.getResponsable().getIdCondicionLaboral());
		objeto.getResponsable().setCorreo(dto.getResponsable().getCorreo());
		objeto.getResponsable().setDireccion(dto.getResponsable().getDireccion());
		objeto.getResponsable().getGradoInstruccion().setId(dto.getResponsable().getIdGradoInstruccion());
		objeto.getResponsable().setProfesion(dto.getResponsable().getProfesion());
		objeto.getResponsable().setTelefonoFijo(dto.getResponsable().getTelefonoFijo());
		objeto.getResponsable().setTelefonoMovil(dto.getResponsable().getTelefonoMovil());
		
		//actualizacion Responsable ini
		objeto.getResponsable().setIdEstado(dto.getResponsable().getIdEstado());		
		objeto.getResponsable().getPersona().setApellidoMaterno(dto.getResponsable().getApellidoMaterno());
		objeto.getResponsable().getPersona().setApellidoPaterno(dto.getResponsable().getApellidoPaterno());
		//System.out.println("fecha recuperada al observar:"+dto.getResponsable().getFechaNacimiento());
		//System.out.println("fecha recuperada al observar:"+Util.stringToUtilDateGeneric(dto.getResponsable().getFechaNacimiento()));
		objeto.getResponsable().getPersona().setFechaNacimiento(Util.stringToUtilDateGeneric(dto.getResponsable().getFechaNacimiento()));
		
		objeto.getResponsable().getPersona().setGenero(dto.getResponsable().getGenero());
		objeto.getResponsable().getPersona().setIdEstado(dto.getResponsable().getIdEstado());
		objeto.getResponsable().getPersona().setNombres(dto.getResponsable().getNombres());
		//System.out.println("nrodocumento recuperada al observar:"+dto.getResponsable().getNumeroDocumento());
		objeto.getResponsable().getPersona().setNumeroDocumentoIdentidad(dto.getResponsable().getNumeroDocumento());
		objeto.getResponsable().getPersona().getTipoDocumentoIdentidad().setNombre(dto.getResponsable().getIdTipoDocumento());
		//actualizacion Responsable fin
		
		
		objeto.getTipoBiblioteca().setIdTipoBiblioteca(Util.getStringToInt(dto.getIdTipoBibliotecaHijo()));
		objeto.getTipoBiblioteca().setIdTipoBibliotecaPadre(Util.getStringToInt(dto.getIdTipoBibliotecaPadre()));
		objeto.getUbigeo().setCentroPoblado(dto.getCentroPoblado());
		objeto.getUbigeo().setCodDepartamento(dto.getCodDepartamento());
		objeto.getUbigeo().setCodDistrito(dto.getCodDistrito());
		objeto.getUbigeo().setCodProvincia(dto.getCodProvincia());
		objeto.getUbigeo().setDireccion(dto.getDireccion());
		objeto.getTipoFuncionamiento().setId(dto.getIdTipoFuncionamiento());
		objeto.setTelefonoFijo(dto.getTelefonoFijo());
		objeto.setTelefonoMovil(dto.getTelefonoMovil());
		objeto.getInfraestructura().setAreaM2(dto.getInfraestructura().getAreaM2());
		objeto.getInfraestructura().setNumeroAmbientes(dto.getInfraestructura().getNumeroAmbientes());
		objeto.getInfraestructura().getTipoLocal().setId(dto.getInfraestructura().getIdTipoLocal());
		objeto.setIdUsuarioActualizacion(dto.getIdUsuarioAuditoria());
		
		objeto.setFlagGestBibEstadisticas(Util.getString(dto.getEstadisticas()));
		objeto.setFlagGestBibPlanTrabActual(Util.getString(dto.getPlanTrabActual()));
		objeto.setFlagGestBibReglamentos(Util.getString(dto.getReglamentos()));
		objeto.setGestionBibEspecificar(Util.getString(dto.getEspecificar()));
		objeto.setLectorTotal(Util.getString(dto.getTotalLectores()));
		objeto.setLectorTotalAnioAnterior(Util.getString(dto.getTotalLectoresAnioAnterior()));
		objeto.setLectorTotalConsultas(Util.getString(dto.getTotalConsultas()));
		
		//atencion del servicio
		objeto.getAtencionServicio().getTipoAccesoPublico().setId(dto.getAtencionServicio().getIdAccesoPublico()); 
		objeto.getAtencionServicio().getTipoAccesoPublico().setNombre(dto.getAtencionServicio().getNombreAccesoPublico());
		//meses
		for (AtencionServicioMesDto atencionServicioMesDto : dto.getAtencionServicio().getMeses()) {
			objeto.getAtencionServicio().getMeses().add(new AtencionServicioMes(atencionServicioMesDto));
		}
		//dias
		for (AtencionServicioTurnoDto atencionServicioTurnoDto : dto.getAtencionServicio().getDias()) {
			objeto.getAtencionServicio().getDias().add(new AtencionServicioTurno(atencionServicioTurnoDto));
		} 

		//infraestructura accesibilidad 
		for (NuevoTipoValorDto tipoValorDto : dto.getInfraestructura().getAccesibilidad() ) {
			TipoValor tipoValor = new TipoValor(tipoValorDto);
			objeto.getInfraestructura().getAccesibilidad().add(tipoValor);
		}
		
		//colecciones 
		for (NuevoTipoValorDto tipoValorDto : dto.getColecciones() ) {
			TipoValor tipoValor = new TipoValor(tipoValorDto);
			objeto.getColecciones().add(tipoValor);
		}

		//servicios
		for (NuevoTipoValorDto tipoValorDto : dto.getServicios() ) {
			TipoValor tipoValor = new TipoValor(tipoValorDto);
			objeto.getServicios().add(tipoValor);
		}
		
		return objeto;
	}	
	
	public BibliotecaMasterServicioDto mapper(BibliotecaMasterServicio objeto){
		BibliotecaMasterServicioDto dto = new BibliotecaMasterServicioDto();
		dto.setIdMaster(Util.getLongToString(objeto.getId()));
		dto.setIdDeclaración(Util.getLongToString(objeto.getIdDeclaracion()));
		dto.setNombre(Util.getString(objeto.getNombre()));
		dto.setAnioDeclaracion(objeto.getAnioDeclaracion());
		dto.setCentroPoblado(objeto.getUbigeo().getCentroPoblado());
		dto.setDepartamento(objeto.getUbigeo().getDepartamento());
		dto.setProvincia(Util.getString(objeto.getUbigeo().getProvincia()));
		dto.setDistrito(objeto.getUbigeo().getDistrito());
		dto.setDireccion(objeto.getUbigeo().getDireccion());
		dto.setTipoBibliotecaHijo(Util.getString(objeto.getTipoBiblioteca().getNombre()));
		dto.setTipoBibliotecaPadre(Util.getString(objeto.getTipoBiblioteca().getNombrePadre()));
		dto.setRazonSocialEntidad(objeto.getEntidad().getRazonSocial());
		dto.setRucEntidad(objeto.getEntidad().getRuc());
		dto.setNumeroRNB(objeto.getNumeroRNB());

		//atenncion servicio
		dto.setAtencionServicio(new AtencionServicioDto(objeto.getAtencionServicio()));
		return dto;
	}	
		
	
}
