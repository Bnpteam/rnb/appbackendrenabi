package pe.gob.bnp.services.rest.renabi.common.infraestructure.repository;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class BaseJDBCOperation implements BaseOperation {
	private static final Logger LOGGER = Logger.getLogger(BaseJDBCOperation.class);
	
	public void closeSqlConnections(ResultSet rs, CallableStatement cstm) {
		closeResultSet(rs);
		closeCallableStatement(cstm);
	}
	
	@Override
	public void closeResultSet(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException exception) {
            LOGGER.error("SQLException " + exception.getMessage(), exception);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
    }

	@Override
	public void closePreparedStatement(PreparedStatement preparedStatement) {
        try {
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException exception) {
            LOGGER.error(exception.getMessage(), exception);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
	}

	@Override
	public void closeStatement(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException exception) {
            LOGGER.error(exception.getMessage(), exception);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
	}

	@Override
	public void closeCallableStatement(CallableStatement callableStatement) {
        try {
            if (callableStatement != null) {
                callableStatement.close();
            }
        } catch (SQLException exception) {
            LOGGER.error(exception.getMessage(), exception);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
    }

	@Override
	public Connection getConnection() {
        Connection connection = null;
        try {
            connection = SQLConnection.getConnectionJDBC();
        } catch (SQLException exception) {
            LOGGER.error(exception.getMessage(), exception);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
        return connection;
    }

	@Override
	public void closeConnection(Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException exception) {
            LOGGER.error(exception.getMessage(), exception);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
    }

	@Override
	public void commitTransaction(Connection connection) {
        try {
            if (connection != null) {
                connection.commit();
            }
        } catch (SQLException exception) {
            LOGGER.error(exception.getMessage(), exception);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
	}

	@Override
	public void rollbackTransaction(Connection connection) {
        try {
            if (connection != null) {
                connection.rollback();
            }
        } catch (SQLException exception) {
            LOGGER.error(exception.getMessage(), exception);
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
	}

}
