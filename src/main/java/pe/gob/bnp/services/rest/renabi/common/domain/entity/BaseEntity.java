package pe.gob.bnp.services.rest.renabi.common.domain.entity;

import java.util.Date;

public class BaseEntity {
	protected Long id;
	protected String 	idEstado;
	protected String 	estado;
	//transaction
	protected String 	codigoResultado;
	protected String 	mensajeResultado;
	//audit
	protected Date 		fechaRegistro;
	protected String	idUsuarioRegistro;
	protected Date 		fechaActualizacion;
	protected String  	idUsuarioActualizacion;
	protected String	ipRegistro;
	protected String 	ipActualizacion;
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCodigoResultado() {
		return codigoResultado;
	}
	public void setCodigoResultado(String codigoResultado) {
		this.codigoResultado = codigoResultado;
	}
	public String getMensajeResultado() {
		return mensajeResultado;
	}
	public void setMensajeResultado(String mensajeResultado) {
		this.mensajeResultado = mensajeResultado;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getIdUsuarioRegistro() {
		return idUsuarioRegistro;
	}
	public void setIdUsuarioRegistro(String idUsuarioRegistro) {
		this.idUsuarioRegistro = idUsuarioRegistro;
	}
	public Date getFechaActualizacion() {
		return fechaActualizacion;
	}
	public void setFechaActualizacion(Date fechaActualizacion) {
		this.fechaActualizacion = fechaActualizacion;
	}
	public String getIdUsuarioActualizacion() {
		return idUsuarioActualizacion;
	}
	public void setIdUsuarioActualizacion(String idUsuarioActualizacion) {
		this.idUsuarioActualizacion = idUsuarioActualizacion;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIpRegistro() {
		return ipRegistro;
	}
	public void setIpRegistro(String ipRegistro) {
		this.ipRegistro = ipRegistro;
	}
	public String getIpActualizacion() {
		return ipActualizacion;
	}
	public void setIpActualizacion(String ipActualizacion) {
		this.ipActualizacion = ipActualizacion;
	}

	

}
