package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.application.dto.NuevoTipoValorDto;

public class VerInfraestructuraDto {
	private String idTipoLocal;
	private String tipoLocal;
	private String areaM2;
	private String numeroAmbientes;
	private List<NuevoTipoValorDto> accesibilidad;
//	private List<NuevoTipoValorDto> colecciones;
//	private List<NuevoTipoValorDto> servicios;
	
	public VerInfraestructuraDto() {
		super();
		this.idTipoLocal = Constants.EMPTY_STRING;
		this.areaM2 = Constants.EMPTY_STRING;
		this.numeroAmbientes = Constants.EMPTY_STRING;
		this.accesibilidad = new ArrayList<>();
//		this.colecciones = new ArrayList<>();
//		this.servicios = new ArrayList<>();		
		this.tipoLocal = Constants.EMPTY_STRING;
	}

	public String getIdTipoLocal() {
		return idTipoLocal;
	}

	public void setIdTipoLocal(String idTipoLocal) {
		this.idTipoLocal = idTipoLocal;
	}

	public String getAreaM2() {
		return areaM2;
	}

	public void setAreaM2(String areaM2) {
		this.areaM2 = areaM2;
	}

	public String getNumeroAmbientes() {
		return numeroAmbientes;
	}

	public void setNumeroAmbientes(String numeroAmbientes) {
		this.numeroAmbientes = numeroAmbientes;
	}

	public List<NuevoTipoValorDto> getAccesibilidad() {
		return accesibilidad;
	}

	public void setAccesibilidad(List<NuevoTipoValorDto> accesibilidad) {
		this.accesibilidad = accesibilidad;
	}

	public String getTipoLocal() {
		return tipoLocal;
	}

	public void setTipoLocal(String tipoLocal) {
		this.tipoLocal = tipoLocal;
	}

	
}
