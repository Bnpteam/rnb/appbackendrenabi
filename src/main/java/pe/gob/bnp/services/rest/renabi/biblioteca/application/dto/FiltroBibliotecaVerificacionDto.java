package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

public class FiltroBibliotecaVerificacionDto {
	private String codDepartamento;
	private String codProvincia;
	private String codDistrito;
	private String nombre;
	private String razonSocialEntidad;
	private String numeroRNB;
	private String fechaInscripcionDesde;
	private String fechaInscripcionHasta;
	private String idEstado;
	private String ruc;
	private String direccion;
	
	public String getRazonSocialEntidad() {
		return razonSocialEntidad;
	}
	public void setRazonSocialEntidad(String razonSocialEntidad) {
		this.razonSocialEntidad = razonSocialEntidad;
	}
	public String getRuc() {
		return ruc;
	}
	public void setRuc(String ruc) {
		this.ruc = ruc;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getCodDepartamento() {
		return codDepartamento;
	}
	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}
	public String getCodProvincia() {
		return codProvincia;
	}
	public void setCodProvincia(String codProvincia) {
		this.codProvincia = codProvincia;
	}
	public String getCodDistrito() {
		return codDistrito;
	}
	public void setCodDistrito(String codDistrito) {
		this.codDistrito = codDistrito;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNumeroRNB() {
		return numeroRNB;
	}
	public void setNumeroRNB(String numeroRNB) {
		this.numeroRNB = numeroRNB;
	}
	public String getFechaInscripcionDesde() {
		return fechaInscripcionDesde;
	}
	public void setFechaInscripcionDesde(String fechaInscripcionDesde) {
		this.fechaInscripcionDesde = fechaInscripcionDesde;
	}
	public String getFechaInscripcionHasta() {
		return fechaInscripcionHasta;
	}
	public void setFechaInscripcionHasta(String fechaInscripcionHasta) {
		this.fechaInscripcionHasta = fechaInscripcionHasta;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}

}
