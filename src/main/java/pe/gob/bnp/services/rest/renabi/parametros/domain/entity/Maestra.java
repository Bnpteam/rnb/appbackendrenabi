package pe.gob.bnp.services.rest.renabi.parametros.domain.entity;

import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;

public class Maestra extends BaseEntity {
	private String tipoTabla;
	private String descripcionCorta;
	private String descripcionLarga;
	
	public String getTipoTabla() {
		return tipoTabla;
	}
	public void setTipoTabla(String tipoTabla) {
		this.tipoTabla = tipoTabla;
	}
	public String getDescripcionCorta() {
		return descripcionCorta;
	}
	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}
	public String getDescripcionLarga() {
		return descripcionLarga;
	}
	public void setDescripcionLarga(String descripcionLarga) {
		this.descripcionLarga = descripcionLarga;
	}
	
}
