package pe.gob.bnp.services.rest.renabi.persona.domain.entity;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;

public class Registrador extends BaseEntity {
	private Persona persona;
	private String usuario;
	private String password;
	private String direccion;	
	private String telefonoMovil;
	private String telefonoFijo;
	private String correo;		
	
	public Registrador() {
		super();
		this.persona = new Persona();
		this.usuario = Constants.EMPTY_STRING;
		this.password = Constants.EMPTY_STRING;
		this.direccion= Constants.EMPTY_STRING;
		this.telefonoMovil= Constants.EMPTY_STRING;
		this.telefonoFijo= Constants.EMPTY_STRING;
		this.correo= Constants.EMPTY_STRING;		
	}
	
	public Registrador(Persona persona, String usuario, String password, String direccion, String telefonoMovil, String telefonoFijo, String correo) {
		super();
		this.persona = persona;
		this.usuario = usuario;
		this.password = password;
		this.direccion = direccion;
		this.telefonoMovil = telefonoMovil;
		this.telefonoFijo = telefonoFijo;
		this.correo = correo;
	}
	
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	
	
}
