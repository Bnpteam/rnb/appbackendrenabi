package pe.gob.bnp.services.rest.renabi.usuario.application;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.services.rest.renabi.common.application.Notification;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.application.dto.ResponseBaseDto;
import pe.gob.bnp.services.rest.renabi.common.application.dto.ResponseGenericDto;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Credencial;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.AutenticacionTxDto;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.ResponseLoginPortalDto;
import pe.gob.bnp.services.rest.renabi.usuario.domain.repository.UsuarioRepository;

@Service
public class UsuarioApplicationService {
	@Autowired
	UsuarioRepository usuarioRepository;
	
	public ResponseGenericDto<ResponseLoginPortalDto> validarLoginUsuarioPortal(Credencial credencial) throws Exception {
		Notification notification = this.validation(credencial);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		ResponseGenericDto<ResponseLoginPortalDto> response= this.usuarioRepository.autenticarUsuarioPortal(credencial);
		ResponseBaseDto responseUpgraded = this.validarRptaLoginUsuarioPortal(response.getResponse());
		response.setResponse(responseUpgraded);
		return response;	
	}
	
	ResponseBaseDto validarRptaLoginUsuarioPortal(ResponseBaseDto response){
		if (response.getCodigo().equals("0000")) {
			response.setDescripcion("Autenticación exitosa");
		}
		if (response.getCodigo().equals("0001")) {
			response.setDescripcion("Usuario y/o Contraseña incorrecta(s)");
		}

		if (response.getCodigo().equals("0002")) {
			response.setDescripcion("Su cuenta se encuentra deshabilitada.");
		}
		if (response.getCodigo().equals("0003")) {
			response.setDescripcion("No cuenta con bibliotecas. Debe tener al menos una biblioteca verificada para poder ingresar.");
		}
		if (response.getCodigo().equals("0004")) {
			response.setDescripcion("Para ingresar al portal debe primero crear una cuenta de usuario en el RNB.");
		}
		return response;
	}
	
	public AutenticacionTxDto validarUsuario(Credencial credencial) throws Exception {
		Notification notification = this.validation(credencial);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		ResponseTx responseAD= this.validarUsuarioenActiveDirectory(credencial);
		
		Notification notificationAD = this.validation(responseAD);
		if (notificationAD.hasErrors()) {
			throw new IllegalArgumentException(notificationAD.errorMessage());
		}	
		
		AutenticacionTxDto response= this.usuarioRepository.autenticarUsuario(credencial);
		return response;	
	}
	
	
	private Notification validation(ResponseTx response) {
		Notification notification = new Notification();
		if (response == null) {
			notification.addError("ResponseTx es nulo");
			return notification;
		}

		if (response.getCodigoRespuesta()== null || !response.getCodigoRespuesta().equals("0000")) {
			notification.addError(response.getRespuesta());
			return notification;
		}
		return notification;
	}
	
	private Notification validation(Credencial credencial) {
		Notification notification = new Notification();
		if (credencial == null) {
			notification.addError("Request Body inválido.");
			return notification;
		}

		if (credencial.getUsuario() == null || credencial.getUsuario().equals("")) {
			notification.addError("Escriba un nombre de usuario y una contraseña.");
			return notification;
		}
		if (credencial.getPassword() == null || credencial.getPassword().equals("")) {
			notification.addError("Escriba un nombre de usuario y una contraseña.");
			return notification;
		}
		return notification;
	}	
	
	
	@SuppressWarnings({ "unchecked", "unused" })
	public ResponseTx validarUsuarioenActiveDirectory(Credencial credencial){//aqui viene la integración con Active Directory
		
		ResponseTx response = null;

		@SuppressWarnings("rawtypes")
		Hashtable credenciales = new Hashtable(11);
		DirContext contexto = null;
		try {
			credenciales.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
			credenciales.put(Context.SECURITY_AUTHENTICATION,"simple");
			credenciales.put(Context.SECURITY_PRINCIPAL, credencial.getUsuario()+"@bnp.gob.pe");
			credenciales.put(Context.SECURITY_CREDENTIALS, credencial.getPassword());
			credenciales.put(Context.PROVIDER_URL, "ldap://172.16.88.203:389");
			
			contexto = new InitialDirContext(credenciales);
			response = new ResponseTx();
			if (contexto!=null) {
				response.setCodigoRespuesta("0000");
				response.setRespuesta("El usuario y contraseña son correctos.");
			}else {
				response.setCodigoRespuesta("0001");
				response.setRespuesta("El nombre de usuario o contraseña no es correcto.");
			}
		} catch (NamingException e) {
			response = new ResponseTx();
			this.obtenerMotivoRechazoEnAD(response,new String(e.toString()));
			System.out.println("Error en e.getMessage: "+e.getMessage());
			/*e.printStackTrace();*/
		} finally {
			if (contexto!=null) {
				try {
					contexto.close();
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
		}
		return response;
	}	
	
	public void obtenerMotivoRechazoEnAD(ResponseTx response, String cadena) {
		int pos = cadena.indexOf("data ");
		String descripcionRechazo = "";
		if (pos>0) {
			String campo1= cadena.substring(pos+5,pos+5+3).concat("");
			if (campo1.equals("52e")){
				//System.out.println("Valor 52e SI es igual a "+campo1);
				descripcionRechazo = "El nombre de usuario o contraseña no es correcto.";
			}	
			if (campo1.equals("775")){
				//System.out.println("Valor 52e SI es igual a "+campo1);
				descripcionRechazo = "La cuenta a que se hace referencia está bloqueada y no se puede utilizar.";
			}			
			if (campo1.equals("525​")){
				descripcionRechazo = "Usuario no existe.";
			}

			if (campo1.equals("530​") || campo1.equals("531​")){
				descripcionRechazo = "Intento de logueo no permitido.";
			}			
			if (campo1.equals("532​")){
				descripcionRechazo = "Contraseña expirada.";
			}	
			if (campo1.equals("533​")){
				descripcionRechazo = "Cuenta deshabilitada.";
			}			
			if (campo1.equals("701​")){
				descripcionRechazo = "Cuenta expirada.";
			}
			if (campo1.equals("773​")){
				descripcionRechazo = "Debe resetear su contraseña.";
			}				
		
			response.setCodigoRespuesta(campo1);
			response.setRespuesta(descripcionRechazo);
		}else {
			response.setCodigoRespuesta("999");
			response.setRespuesta("No se pudo obtener él código de rechazo");
		}
	}	
}
