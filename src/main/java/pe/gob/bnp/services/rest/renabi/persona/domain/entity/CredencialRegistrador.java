package pe.gob.bnp.services.rest.renabi.persona.domain.entity;

public class CredencialRegistrador {
	private String emailDestino;
	/*private String titulo;*/
	private String dni;
	private String password;
	
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmailDestino() {
		return emailDestino;
	}
	public void setEmailDestino(String emailDestino) {
		this.emailDestino = emailDestino;
	}

	
}
