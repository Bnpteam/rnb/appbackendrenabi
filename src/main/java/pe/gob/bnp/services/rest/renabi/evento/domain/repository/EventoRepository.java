package pe.gob.bnp.services.rest.renabi.evento.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseRepository;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.Evento;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.EventoDepartamento;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.EventoResumen;

@Repository
public interface EventoRepository extends BaseRepository<EventoResumen>{
	public List<Evento> listarEventosVigentes(String estado);
	public ResponseTx cambiarEstado(Long id, String idEstado);
	public List<EventoDepartamento> listarEventosxDepartamento();
		
}
