package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.AtencionServicioMesDto;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Tipo;

public class AtencionServicioMes extends BaseEntity{
	private Tipo mes;
	private String valor;

	public AtencionServicioMes() {
		super();
		this.mes = new Tipo();
	}
	
	public AtencionServicioMes(AtencionServicioMesDto dto) {
		super();
		this.mes = new Tipo();
		this.mes.setId(dto.getIdMes());
		this.mes.setNombre(dto.getMes());
		this.valor = dto.getValor();
	}	

	public Tipo getMes() {
		return mes;
	}

	public void setMes(Tipo mes) {
		this.mes = mes;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
