package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

public class BibliotecaMaster {
	private String idMaster;
	private String nombre;
	private String numeroRNB;
	//private String fechaInscripcion;
	private String anioDeclaracion;
	//private String estado;
	//private String nombreResponsable;
	//private String docIdentidadResponsable;
	//private String nombreRegistrador;
	//private String docIdentidadRegistrador;
	private String razonSocialEntidad;
	private String rucEntidad;
	private String departamento;
	private String provincia;
	private String distrito;
	private String centroPoblado;
	private String direccion;
	private String idDeclaracion;

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNumeroRNB() {
		return numeroRNB;
	}
	public void setNumeroRNB(String numeroRNB) {
		this.numeroRNB = numeroRNB;
	}

	public String getAnioDeclaracion() {
		return anioDeclaracion;
	}
	public void setAnioDeclaracion(String anioDeclaracion) {
		this.anioDeclaracion = anioDeclaracion;
	}
	public String getRazonSocialEntidad() {
		return razonSocialEntidad;
	}
	public void setRazonSocialEntidad(String razonSocialEntidad) {
		this.razonSocialEntidad = razonSocialEntidad;
	}
	public String getRucEntidad() {
		return rucEntidad;
	}
	public void setRucEntidad(String rucEntidad) {
		this.rucEntidad = rucEntidad;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getCentroPoblado() {
		return centroPoblado;
	}
	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getIdMaster() {
		return idMaster;
	}
	public void setIdMaster(String idMaster) {
		this.idMaster = idMaster;
	}
	public String getIdDeclaracion() {
		return idDeclaracion;
	}
	public void setIdDeclaracion(String idDeclaracion) {
		this.idDeclaracion = idDeclaracion;
	}
	
	
	
}
