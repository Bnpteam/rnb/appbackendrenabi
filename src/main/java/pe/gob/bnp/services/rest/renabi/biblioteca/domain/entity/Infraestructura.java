package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Tipo;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.TipoValor;

public class Infraestructura extends BaseEntity {
	private Tipo tipoLocal;
	private String areaM2;
	private String numeroAmbientes;
	private List<TipoValor> accesibilidad;
	
	public Infraestructura() {
		super();
		this.tipoLocal = new Tipo();
		this.areaM2 = Constants.EMPTY_STRING;
		this.numeroAmbientes = Constants.EMPTY_STRING;
		this.accesibilidad = new ArrayList<>();
	}

	public Tipo getTipoLocal() {
		return tipoLocal;
	}

	public void setTipoLocal(Tipo tipoLocal) {
		this.tipoLocal = tipoLocal;
	}

	public String getAreaM2() {
		return areaM2;
	}

	public void setAreaM2(String areaM2) {
		this.areaM2 = areaM2;
	}

	public String getNumeroAmbientes() {
		return numeroAmbientes;
	}

	public void setNumeroAmbientes(String numeroAmbientes) {
		this.numeroAmbientes = numeroAmbientes;
	}

	public List<TipoValor> getAccesibilidad() {
		return accesibilidad;
	}

	public void setAccesibilidad(List<TipoValor> accesibilidad) {
		this.accesibilidad = accesibilidad;
	}
	
}
