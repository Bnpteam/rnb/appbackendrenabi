package pe.gob.bnp.services.rest.renabi.reporte.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.VerRptBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Biblioteca;
import pe.gob.bnp.services.rest.renabi.common.application.Notification;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteRegistradoresCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasRegistradasCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasVerificadasCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasxEstadoCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteGenericoFiltroDto;
import pe.gob.bnp.services.rest.renabi.reporte.domain.repository.ReporteRepository;

@Service
public class ReporteApplicationService {
	
	@Autowired
	ReporteRepository reporteRepository;
	
	public List<ReporteRegistradoresCabeceraDto> buscarRegistradores(String fechaDesde, String fechaHasta){
		
			ReporteGenericoFiltroDto dto = new ReporteGenericoFiltroDto();			
			dto.setFechaDesde(fechaDesde);
			dto.setFechaHasta(fechaHasta);			
			Notification notification = this.validation(dto);
			if (notification.hasErrors()) {
				throw new IllegalArgumentException(notification.errorMessage());
			}		
			return this.reporteRepository.listarRegistradores(dto);
	}
	
	public List<ReporteBibliotecasxEstadoCabeceraDto> buscarBibliotecasxEstado(String fechaDesde, String fechaHasta){
		
		ReporteGenericoFiltroDto dto = new ReporteGenericoFiltroDto();			
		dto.setFechaDesde(fechaDesde);
		dto.setFechaHasta(fechaHasta);			
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}		
		return this.reporteRepository.listarBibliotecasxEstado(dto);
	}
	
	public List<ReporteBibliotecasVerificadasCabeceraDto> buscarBibliotecasVerificadas(String fechaDesde, String fechaHasta){
		
		ReporteGenericoFiltroDto dto = new ReporteGenericoFiltroDto();			
		dto.setFechaDesde(fechaDesde);
		dto.setFechaHasta(fechaHasta);			
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}		
		return this.reporteRepository.listarBibliotecasVerificadas(dto);
	}
	

	public List<ReporteBibliotecasRegistradasCabeceraDto> buscarBibliotecasRegistradas(String fechaDesde, String fechaHasta){
		
		ReporteGenericoFiltroDto dto = new ReporteGenericoFiltroDto();			
		dto.setFechaDesde(fechaDesde);
		dto.setFechaHasta(fechaHasta);			
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}		
		return this.reporteRepository.listarBibliotecasRegistradas(dto);
	}
	
	public VerRptBibliotecaDto buscarBibliotecasxId(String bibliotecaId){
		Notification notification = new Notification();
		notification = this.validation(bibliotecaId);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		return this.reporteRepository.listarBibliotecasxId(bibliotecaId);
	}

	
	public Notification validation(ReporteGenericoFiltroDto dto) {
		Notification notification = new Notification();
		if (dto == null) {
			notification.addError("No se encontró el filtro de búsqueda");
			return notification;
		}
		
		return notification;
	}
	
	public Notification validation(String dto) {
		Notification notification = new Notification();
		if (dto == null) {
			notification.addError("No se encontró el filtro de búsqueda");
			return notification;
		}
		
		return notification;
	}		


}
