package pe.gob.bnp.services.rest.renabi.registrador.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.Registrador;

@ApiModel(value="Registrador", description="Objeto para la clase Registrador")
public class RegistradorDto {
	@ApiModelProperty(notes = "id de la tabla tbl_registrador",name="registrador_id",required=true,value="100")
	private String id;
	
	@ApiModelProperty(notes = "id de la tabla tbl_persona",name="persona_id",required=true,value="45")
	private String idPersona;
	
	@ApiModelProperty(notes = "Nombre del registrador ",required=true,value="Juan")
	private String nombres;

	@ApiModelProperty(notes = "Apellido paterno del registrador ",required=true,value="Perez")
	private String apellidoPaterno;

	@ApiModelProperty(notes = "Apellido materno del registrador ",required=false,value="Cardenas")
	private String apellidoMaterno;

	
	@ApiModelProperty(notes = "Id del tipo de documento de identidad del registrador ",required=true,value="DNI")
	private String idTipoDocumento;

	@ApiModelProperty(notes = "Tipo de documento de identidad del registrador ",required=true,value="DNI")
	private String tipoDocumento;

	@ApiModelProperty(notes = "Número de documento de identidad del registrador ",required=true,value="45389001")
	private String numeroDocumento;

	@ApiModelProperty(notes = "Fecha de nacimiento del registrador ",required=true,value="25/01/1990")
	private String fechaNacimiento;
	
	
	@ApiModelProperty(notes = "Sexo del registrador ",required=true,value="Masculino")
	private String genero;
	
	@ApiModelProperty(notes = "Teléfono móvil del registrador ",required=false,value="987654321")
	private String telefonoMovil;
	
	@ApiModelProperty(notes = "Teléfono fijo del registrador ",required=false,value="015632201")
	private String telefonoFijo;
	
	@ApiModelProperty(notes = "Correo del registrador ",required=false,value="juanperez@gmail.com")
	private String correo;	
	
	@ApiModelProperty(notes = "nombre usuario que usará el registrador para ingresar al sistema",required=true,value="45389001")
	private String usuario;


	@ApiModelProperty(notes = "clave que usará el registrador para ingresar al sistema",required=true,value="12424266")
	private String password;
	
	/*private String idGradoInstruccion;
	private String gradoInstruccion;
	private String profesion;
	private String condicionLaboral;
	private String idCondicionLaboral;*/
	@ApiModelProperty(notes = "Estado del registrador",required=false,value="Activo")
	private String estado;

	@ApiModelProperty(notes = "Id del estado del registrador",required=true,value="1")
	private String idEstado;
	
	@ApiModelProperty(notes = "Dirección del registrador",required=true,value="Avenida del ejercito 3434 Miraflores - Lima - Perú")
	private String direccion;
	
	@ApiModelProperty(notes = "Id del usuario de la aplicación que realiza el registro. 9999 por default.",required=true,value="9999")
	private String idCreacionAuditoria;

	@ApiModelProperty(notes = "Fecha de Registro. Auditoría. Se guarda la fecha de la base de datos por defecto.",required=false,value="16/07/2019")
	private String fechaCreacionAuditoria;

	@ApiModelProperty(notes = "Id del usuario de la aplicación que realiza la actualización. Auditoría. ",required=false,value="45")
	private String idModificacionAuditoria;

	@ApiModelProperty(notes = "Fecha de Actualización. Auditoría.",required=false,value="16/07/2019")
	private String fechaModificacionAuditoria;
	
	public RegistradorDto() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.idPersona = Constants.EMPTY_STRING;
		this.nombres = Constants.EMPTY_STRING;
		this.apellidoPaterno = Constants.EMPTY_STRING;
		this.apellidoMaterno = Constants.EMPTY_STRING;
		this.tipoDocumento = Constants.EMPTY_STRING;
		this.numeroDocumento = Constants.EMPTY_STRING;
		this.fechaNacimiento = Constants.EMPTY_STRING;
		this.genero = Constants.EMPTY_STRING;
		this.usuario = Constants.EMPTY_STRING;
		this.password = Constants.EMPTY_STRING;
		this.direccion = Constants.EMPTY_STRING;
		this.idTipoDocumento = Constants.EMPTY_STRING;
		
		this.estado = Constants.EMPTY_STRING;
		this.idEstado = Constants.ACTIVE_STATE;
		this.idCreacionAuditoria = Constants.EMPTY_STRING;
		this.fechaCreacionAuditoria = Constants.EMPTY_STRING;
		this.idModificacionAuditoria = Constants.EMPTY_STRING;
		this.fechaModificacionAuditoria = Constants.EMPTY_STRING;
		this.telefonoFijo = Constants.EMPTY_STRING;
		this.telefonoMovil = Constants.EMPTY_STRING;
		this.correo = Constants.EMPTY_STRING;
	}
	
	
	public RegistradorDto(Registrador objeto) {
		super();
		this.id = Util.getLongToString(objeto.getId());
		this.idPersona =  Util.getLongToString(objeto.getPersona().getId());
		this.nombres = objeto.getPersona().getNombres();
		this.apellidoPaterno = objeto.getPersona().getApellidoPaterno();
		this.apellidoMaterno = objeto.getPersona().getApellidoMaterno();
		this.tipoDocumento = objeto.getPersona().getTipoDocumentoIdentidad().getId();
		this.numeroDocumento = objeto.getPersona().getTipoDocumentoIdentidad().getNombre();
		this.fechaNacimiento = Util.utilDateToString(objeto.getPersona().getFechaNacimiento());
		this.genero = objeto.getPersona().getGenero();
		this.usuario = objeto.getUsuario();
		this.password = objeto.getPassword();
		this.estado = objeto.getEstado();
		this.idCreacionAuditoria = objeto.getIdUsuarioRegistro();
		this.fechaCreacionAuditoria = Util.utilDateToString(objeto.getFechaRegistro());
		this.idModificacionAuditoria = objeto.getIdUsuarioActualizacion();
		this.fechaModificacionAuditoria = Util.utilDateToString(objeto.getFechaActualizacion());
		this.telefonoFijo = Util.getString(objeto.getTelefonoFijo());
		this.telefonoMovil = Util.getString(objeto.getTelefonoMovil());
		this.correo = Util.getString(objeto.getCorreo());
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getIdCreacionAuditoria() {
		return idCreacionAuditoria;
	}
	public void setIdCreacionAuditoria(String idCreacionAuditoria) {
		this.idCreacionAuditoria = idCreacionAuditoria;
	}
	public String getFechaCreacionAuditoria() {
		return fechaCreacionAuditoria;
	}
	public void setFechaCreacionAuditoria(String fechaCreacionAuditoria) {
		this.fechaCreacionAuditoria = fechaCreacionAuditoria;
	}
	public String getIdModificacionAuditoria() {
		return idModificacionAuditoria;
	}
	public void setIdModificacionAuditoria(String idModificacionAuditoria) {
		this.idModificacionAuditoria = idModificacionAuditoria;
	}
	public String getFechaModificacionAuditoria() {
		return fechaModificacionAuditoria;
	}
	public void setFechaModificacionAuditoria(String fechaModificacionAuditoria) {
		this.fechaModificacionAuditoria = fechaModificacionAuditoria;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getIdEstado() {
		return idEstado;
	}


	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getIdTipoDocumento() {
		return idTipoDocumento;
	}


	public void setIdTipoDocumento(String idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}


	public String getIdPersona() {
		return idPersona;
	}


	public void setIdPersona(String idPersona) {
		this.idPersona = idPersona;
	}


	public String getTelefonoMovil() {
		return telefonoMovil;
	}


	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}


	public String getTelefonoFijo() {
		return telefonoFijo;
	}


	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}


	public String getCorreo() {
		return correo;
	}


	public void setCorreo(String correo) {
		this.correo = correo;
	}

	
	
}
