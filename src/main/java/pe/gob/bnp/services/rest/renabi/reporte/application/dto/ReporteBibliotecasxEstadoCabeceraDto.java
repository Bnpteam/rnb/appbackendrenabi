package pe.gob.bnp.services.rest.renabi.reporte.application.dto;

public class ReporteBibliotecasxEstadoCabeceraDto {
	
	private String id;
	private String rowBiblioteca;	
	private String estado;
	private String fechaRegistro;	
	private String razonSocial;
	private String nombreBiblioteca;	
	private String tipoBiblioteca;
	private String subTipoBiblioteca;
	private String departamento;
	private String provincia;
	private String distrito;
	private String centroPoblado;
	private String responsableBiblioteca;
	private String telefonoFijo;	
	private String telefonoCelular;
	private String correo;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRowBiblioteca() {
		return rowBiblioteca;
	}
	public void setRowBiblioteca(String rowBiblioteca) {
		this.rowBiblioteca = rowBiblioteca;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getRazonSocial() {
		return razonSocial;
	}
	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}
	public String getNombreBiblioteca() {
		return nombreBiblioteca;
	}
	public void setNombreBiblioteca(String nombreBiblioteca) {
		this.nombreBiblioteca = nombreBiblioteca;
	}
	public String getTipoBiblioteca() {
		return tipoBiblioteca;
	}
	public void setTipoBiblioteca(String tipoBiblioteca) {
		this.tipoBiblioteca = tipoBiblioteca;
	}
	public String getSubTipoBiblioteca() {
		return subTipoBiblioteca;
	}
	public void setSubTipoBiblioteca(String subTipoBiblioteca) {
		this.subTipoBiblioteca = subTipoBiblioteca;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getCentroPoblado() {
		return centroPoblado;
	}
	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}
	public String getResponsableBiblioteca() {
		return responsableBiblioteca;
	}
	public void setResponsableBiblioteca(String responsableBiblioteca) {
		this.responsableBiblioteca = responsableBiblioteca;
	}
	public String getTelefonoFijo() {
		return telefonoFijo;
	}
	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}
	public String getTelefonoCelular() {
		return telefonoCelular;
	}
	public void setTelefonoCelular(String telefonoCelular) {
		this.telefonoCelular = telefonoCelular;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}

}
