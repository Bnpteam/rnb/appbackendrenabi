package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;

public class BibliotecaMasterServicio extends BaseEntity {
	private String numeroRNB;
	private String anioDeclaracion;
	private String nombre;
	private Entidad entidad;
	private String direccion;
	private Ubigeo ubigeo;
	private TipoBiblioteca tipoBiblioteca;
	private AtencionServicio atencionServicio;
	private Long idDeclaracion;
	
	
	
	public BibliotecaMasterServicio() {
		super();
		this.numeroRNB = Constants.EMPTY_STRING;
		this.anioDeclaracion = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
		this.entidad = new Entidad();
		this.direccion = Constants.EMPTY_STRING;
		this.ubigeo = new Ubigeo();
		this.tipoBiblioteca = new TipoBiblioteca();
		this.atencionServicio = new AtencionServicio();
		this.idDeclaracion = Constants.ZERO_LONG;
	}
	
	public String getNumeroRNB() {
		return numeroRNB;
	}
	public void setNumeroRNB(String numeroRNB) {
		this.numeroRNB = numeroRNB;
	}
	public String getAnioDeclaracion() {
		return anioDeclaracion;
	}
	public void setAnioDeclaracion(String anioDeclaracion) {
		this.anioDeclaracion = anioDeclaracion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Entidad getEntidad() {
		return entidad;
	}
	public void setEntidad(Entidad entidad) {
		this.entidad = entidad;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Ubigeo getUbigeo() {
		return ubigeo;
	}
	public void setUbigeo(Ubigeo ubigeo) {
		this.ubigeo = ubigeo;
	}
	public TipoBiblioteca getTipoBiblioteca() {
		return tipoBiblioteca;
	}
	public void setTipoBiblioteca(TipoBiblioteca tipoBiblioteca) {
		this.tipoBiblioteca = tipoBiblioteca;
	}
	public AtencionServicio getAtencionServicio() {
		return atencionServicio;
	}
	public void setAtencionServicio(AtencionServicio atencionServicio) {
		this.atencionServicio = atencionServicio;
	}
	public Long getIdDeclaracion() {
		return idDeclaracion;
	}
	public void setIdDeclaracion(Long idDeclaracion) {
		this.idDeclaracion = idDeclaracion;
	}
	

	
	
}
