package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class Ubigeo {
	private String codDepartamento;
	private String departamento;
	private String codProvincia;
	private String provincia;
	private String codDistrito;
	private String distrito;
	private String centroPoblado;
	private String direccion;
	
	public Ubigeo() {
		super();
		this.codDepartamento = Constants.EMPTY_STRING;
		this.departamento = Constants.EMPTY_STRING;
		this.codProvincia = Constants.EMPTY_STRING;
		this.provincia = Constants.EMPTY_STRING;
		this.codDistrito = Constants.EMPTY_STRING;
		this.distrito = Constants.EMPTY_STRING;
		this.centroPoblado = Constants.EMPTY_STRING;
		this.direccion = Constants.EMPTY_STRING;
	}
	

	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}


	public String getCodDepartamento() {
		return codDepartamento;
	}


	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}


	public String getCodProvincia() {
		return codProvincia;
	}


	public void setCodProvincia(String codProvincia) {
		this.codProvincia = codProvincia;
	}


	public String getCodDistrito() {
		return codDistrito;
	}


	public void setCodDistrito(String codDistrito) {
		this.codDistrito = codDistrito;
	}


	public String getCentroPoblado() {
		return centroPoblado;
	}


	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
	
	
}
