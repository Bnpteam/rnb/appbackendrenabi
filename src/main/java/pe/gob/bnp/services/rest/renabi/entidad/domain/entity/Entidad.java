package pe.gob.bnp.services.rest.renabi.entidad.domain.entity;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Tipo;

public class Entidad extends BaseEntity {
	private String razonSocial;
	private String ruc;
	private String correo;
	private String direccion;
	private String telefonoFijo;
	private String telefonoMovil;
	private String flagOrigen;
	private Tipo tipoGestion;
	
	public Entidad() {
		super();
		this.razonSocial 	= Constants.EMPTY_STRING;
		this.ruc 			= Constants.EMPTY_STRING;
		this.correo 		= Constants.EMPTY_STRING;
		this.direccion 		= Constants.EMPTY_STRING;
		this.telefonoFijo 	= Constants.EMPTY_STRING;
		this.telefonoMovil 	= Constants.EMPTY_STRING;
		this.flagOrigen 	= Constants.EMPTY_STRING;
		this.tipoGestion = new Tipo();
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getFlagOrigen() {
		return flagOrigen;
	}

	public void setFlagOrigen(String flagOrigen) {
		this.flagOrigen = flagOrigen;
	}

	public Tipo getTipoGestion() {
		return tipoGestion;
	}

	public void setTipoGestion(Tipo tipoGestion) {
		this.tipoGestion = tipoGestion;
	}
	
	
}
