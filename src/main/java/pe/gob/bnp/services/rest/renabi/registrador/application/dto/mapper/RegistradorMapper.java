package pe.gob.bnp.services.rest.renabi.registrador.application.dto.mapper;

import org.springframework.stereotype.Component;

import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.Registrador;
import pe.gob.bnp.services.rest.renabi.registrador.application.dto.RegistradorDto;

@Component
public class RegistradorMapper {
	
	public RegistradorDto mapper(Registrador objeto) {
		RegistradorDto dto = new RegistradorDto(objeto);
		return dto;
	}
	
	public Registrador reverseMapper(RegistradorDto dto) {
		Registrador objeto = new Registrador();
		objeto.setEstado(dto.getEstado());
		objeto.setId(Util.getStringToLong(dto.getId()));
		objeto.setIdEstado(dto.getIdEstado());
		objeto.setFechaActualizacion(Util.stringToUtilDate(dto.getFechaModificacionAuditoria()));
		objeto.setFechaRegistro(Util.stringToUtilDate(dto.getFechaCreacionAuditoria()));
		objeto.setIdUsuarioActualizacion(dto.getIdModificacionAuditoria());
		objeto.setIdUsuarioRegistro(dto.getIdCreacionAuditoria());
		objeto.setPassword(dto.getPassword());
		objeto.getPersona().setApellidoMaterno(dto.getApellidoMaterno());
		objeto.getPersona().setApellidoPaterno(dto.getApellidoPaterno());
		objeto.setDireccion(dto.getDireccion());
		objeto.getPersona().getTipoDocumentoIdentidad().setId(dto.getIdTipoDocumento());
		objeto.getPersona().getTipoDocumentoIdentidad().setNombre(dto.getTipoDocumento());
		objeto.getPersona().setNumeroDocumentoIdentidad(dto.getNumeroDocumento());
		objeto.getPersona().setEstado(dto.getEstado());
		objeto.getPersona().setIdEstado(dto.getIdEstado());
		objeto.getPersona().setFechaNacimiento(Util.stringToUtilDate(dto.getFechaNacimiento()));
		objeto.getPersona().setGenero(dto.getGenero());
		objeto.getPersona().setId(Util.getStringToLong(dto.getIdPersona()));
		objeto.getPersona().setNombres(dto.getNombres());
		objeto.setTelefonoFijo(dto.getTelefonoFijo());
		objeto.setTelefonoMovil(dto.getTelefonoMovil());
		objeto.setCorreo(dto.getCorreo());
		return objeto;
	}

}
