package pe.gob.bnp.services.rest.renabi.registrador.application;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaCabeceraDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.FiltroBibliotecaVerificacionDto;
import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.Notification;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Credencial;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.CredencialMapper;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.Registrador;
import pe.gob.bnp.services.rest.renabi.registrador.application.dto.CredencialRegistradorDto;
import pe.gob.bnp.services.rest.renabi.registrador.application.dto.RegistradorDto;
import pe.gob.bnp.services.rest.renabi.registrador.application.dto.mapper.RegistradorMapper;
import pe.gob.bnp.services.rest.renabi.registrador.domain.entity.Credenciales;
import pe.gob.bnp.services.rest.renabi.registrador.domain.repository.RegistradorRepository;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.AutenticacionTxDto;

@Service
public class RegistradorApplicationService {
	@Autowired
	RegistradorRepository registradorRepository;
	
	@Autowired
	RegistradorMapper registradorMapper;
	
	@Autowired
	CredencialMapper credencialMapper;
	
	public ResponseTx save(RegistradorDto dto) {
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		Registrador registrador = registradorMapper.reverseMapper(dto);
		return this.registradorRepository.persist(registrador);
	}
	
	public AutenticacionTxDto autenticar(CredencialRegistradorDto dto) {
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}
		Credencial credencial= credencialMapper.reverseMapperRegistrador(dto);
		return this.registradorRepository.autenticarUsuario(credencial);
	}	
	
	public Notification validation(CredencialRegistradorDto registrador) {
		Notification notification = new Notification();
		if (registrador == null) {
			notification.addError("No se encontraron datos de la credencial");
		}
		
		if (Util.isEmptyString(registrador.getUsuario())) {
			notification.addError("Se debe ingresar el usuario");
		}

		if (Util.isEmptyString(registrador.getPassword())) {
			notification.addError("Se debe ingresar la contraseña");
		}

		return notification;
	}	
	
	public Notification validation(RegistradorDto registrador) {
		Notification notification = new Notification();
		if (registrador == null) {
			notification.addError("No se encontraron datos del registrador");
		}
		
		if (Util.isEmptyString(registrador.getNombres())) {
			notification.addError("Se debe ingresar el nombre del registrador");
		}

		if (Util.isEmptyString(registrador.getApellidoPaterno())) {
			notification.addError("Se debe ingresar el apellido paterno del registrador");
		}

		if (Util.isEmptyString(registrador.getTipoDocumento())) {
			notification.addError("Se debe ingresar el tipo de documento de identidad del registrador");
		}
		if (Util.isEmptyString(registrador.getNumeroDocumento())) {
			notification.addError("Se debe ingresar el número de documento de identidad del registrador");
		}

		if (Util.getString(registrador.getNumeroDocumento()).length()!=8) {
			notification.addError("El número de documento de identidad debe tener 8 dígitos");
		}
		
		if (Util.isEmptyString(registrador.getFechaNacimiento())) {
			notification.addError("Se debe ingresar la fecha de nacimiento del registrador");
		}
		
		if (Util.isEmptyString(registrador.getGenero())) {
			notification.addError("Se debe ingresar el género del registrador");
		}
		
		if (Util.isEmptyString(registrador.getCorreo())) {
			notification.addError("Se debe ingresar el correo electrónico del registrador");
		}
		
		return notification;
	}
	
	public List<BibliotecaCabeceraDto> obtenerBibliotecas(Long idRegistrador){
			Notification notification = this.validation(idRegistrador);
			if (notification.hasErrors()) {
				throw new IllegalArgumentException(notification.errorMessage());
			}		
			return this.registradorRepository.listarBibliotecas(idRegistrador);
	}
		
	public Notification validation(Long idRegistrador) {
		Notification notification = new Notification();
		if (idRegistrador <= Constants.ZERO_LONG) {
			notification.addError("Id de registrador debe ser mayor a cero");
			return notification;
		}

		return notification;
	}		
	
	public List<BibliotecaCabeceraDto> obtenerBibliotecaVerificadas(
		String  nombre,		String  numeroRNB,		String  codDepartamento,
		String  codProvincia,		String  codDistrito,		
		String  fechaInscripcionDesde,		String  fechaInscripcionHasta, String ruc, String direccion){
		FiltroBibliotecaVerificacionDto dto = new FiltroBibliotecaVerificacionDto();
		dto.setNombre(nombre);
		dto.setNumeroRNB(numeroRNB);
		dto.setCodDepartamento(codDepartamento);
		dto.setCodProvincia(codProvincia);
		dto.setCodDistrito(codDistrito);
		dto.setFechaInscripcionDesde(fechaInscripcionDesde);
		dto.setFechaInscripcionHasta(fechaInscripcionHasta);
		dto.setRuc(ruc);
		dto.setDireccion(direccion);
		Notification notification = this.validation(dto);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}		
		return this.registradorRepository.listarBibliotecasVerificadas(dto);
	}

	public Notification validation(FiltroBibliotecaVerificacionDto dto) {
		Notification notification = new Notification();
		if (dto == null) {
			notification.addError("No se encontró el filtro de búsqueda");
			return notification;
		}
		return notification;
	}	
	
	
	public Credenciales obtenerCredencialRegistrador(String correo) {
		Notification notification = this.validation(correo);
		if (notification.hasErrors()) {
			throw new IllegalArgumentException(notification.errorMessage());
		}			
		return this.registradorRepository.obtenerCredencialRegistrador(Util.getStringFormatted(correo.toUpperCase()));
	}
	
	public Notification validation(String correo) {
		Notification notification = new Notification();
		if (correo == null || Util.isEmptyString(correo)) {
			notification.addError("Debe ingresar el correo");
			return notification;
		}
		return notification;
	}	

}
