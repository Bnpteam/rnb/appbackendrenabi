package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.VerEntidadDto;

@ApiModel(value="Ver Reporte Biblioteca", description="Representa los datos de una bibioteca en formato para un reporte")
public class VerRptBibliotecaDto {
	//Datos generales
		@ApiModelProperty(notes = "Id de la biblioteca",required=true,value="Ej: 324")
		private String idBiblioteca;

		@ApiModelProperty(notes = "Año de la declaracion",required=true,value="Ej: 2019")
		private String anioDeclaracion;
		
		@ApiModelProperty(notes = "Nombre de la biblioteca",required=true,value="Ej: Biblioteca de Madrid")
		private String nombre;

		@ApiModelProperty(notes = "Correo electrónico de la biblioteca",required=true,value="Ej: correo@gmail.com")
		private String correoElectronico;

		@ApiModelProperty(notes = "Teléfono fijo",required=true,value="Ej: 06578711")
		private String telefonoFijo;

		@ApiModelProperty(notes = "Teléfono móvil",required=true,value="Ej: 95601112")
		private String telefonoMovil;

		@ApiModelProperty(notes = "Número de bibliotecólogos",required=true,value="Ej: 34")
		private String numeroBibliotecologos;

		@ApiModelProperty(notes = "Número total de personal",required=true,value="Ej: 45")
		private String numeroTotalPersonal;

		@ApiModelProperty(notes = "Id del estado",required=true,value="Ej: 1")
		private String idEstadoBiblioteca;

		@ApiModelProperty(notes = "Estado",required=true,value="Ej: POR VALIDAR")
		private String estadoBiblioteca;

		//Ambito Territorial
		@ApiModelProperty(notes = "Id del ambito territorial",required=true,value="Ej: 24")
		private String idAmbitoTerritorial;
		@ApiModelProperty(notes = "Ámbito territorial",required=true,value="Ej: URBANO")
		private String ambitoTerritorial;

		//Tipo de Funcionamiento
		@ApiModelProperty(notes = "Id del tipo de funcionamiento",required=true,value="Ej: 4")
		private String idTipoFuncionamiento;

		@ApiModelProperty(notes = "Tipo de funcionamiento",required=true,value="Ej: ACTIVA")
		private String tipoFuncionamiento;

		//Registrador
		@ApiModelProperty(notes = "Id del Registrador",required=true,value="Ej: 90")
		private String idRegistrador;

		@ApiModelProperty(notes = "Nombre completo del Registrador",required=true,value="Ej: Juan Pérez Cárdenas")
		private String nombreRegistrador;

		@ApiModelProperty(notes = "Documento de identidad del Registrador",required=true,value="Ej: DNI-12013457")
		private String docIdentidadRegistrador;
		
		@ApiModelProperty(notes = "Usuarios en el año anterior",required=true,value="Ej: 949")
		private String totalLectoresAnioAnterior;

		@ApiModelProperty(notes = "Número de usuarios/asistentes",required=true,value="Ej: 456")
		private String totalLectores;

		@ApiModelProperty(notes = "Número de consultas",required=true,value="Ej: 98")
		private String totalConsultas;

		@ApiModelProperty(notes = "Gestión de la biblioteca - Plan de trabajo anual",required=true,value="Ej: SI")
		private String planTrabActual;

		@ApiModelProperty(notes = "Gestión de la biblioteca - Reglamentos",required=true,value="Ej: SI")
		private String reglamentos;

		@ApiModelProperty(notes = "Gestión de la biblioteca - Estadísticas",required=true,value="Ej: NO")
		private String estadisticas;

		@ApiModelProperty(notes = "Gestión de la biblioteca - Especificar",required=true,value="Ej: ............")
		private String especificar;	
		
		//Entidad
		@ApiModelProperty(notes = "Entidad",required=true,value="Ej: clase Entidad")
		private VerEntidadDto entidad;
		
		//Responsable
		@ApiModelProperty(notes = "Responsable",required=true,value="Ej: clase Responsable")
		private VerResponsableDto responsable;

		//Tipo biblioteca
		@ApiModelProperty(notes = "Id del tipo de biblitoeca",required=true,value="Ej: 45")
		private	String idTipoBibliotecaPadre;

		@ApiModelProperty(notes = "Tipo de biblitoeca",required=true,value="Ej: BIBLIOTECAS DE INSTITUCIONES EDUCATIVAS")
		private	String  tipoBibliotecaPadre;

		@ApiModelProperty(notes = "Id del subtipo de biblitoeca",required=true,value="Ej: 56")
		private String idTipoBibliotecaHijo;

		@ApiModelProperty(notes = "Sub Tipo de biblitoeca",required=true,value="Ej: IE INICIAL")
		private	String  tipoBibliotecaHijo;
		
		
		//ubigeo
		@ApiModelProperty(notes = "Ubigeo",required=true,value="LIMA-LIMA-LINCE")
		private String ubigeo;
		
		@ApiModelProperty(notes = "Año de la declaracion",required=true,value="Ej: 2019")
		private String centroPoblado;
		
		@ApiModelProperty(notes = "Dirección de la biblioteca",required=true,value="Ej: Av. Arenales 3435")
		private String direccion;
		
		//Infraestructura
		@ApiModelProperty(notes = "Infraestructura",required=true,value="Ej: clase Infraestructura")
		private VerRptInfraestructuraDto infraestructura;	
		
		@ApiModelProperty(notes = "Colecciones Nombres",required=true,value="Ej: Lista")
		private String nombreColecciones;
		
		@ApiModelProperty(notes = "Colecciones Valores",required=true,value="Ej: Lista")
		private String valorColecciones;
		
		@ApiModelProperty(notes = "Servicios Nombres",required=true,value="Ej: Lista")
		private String nombreTipoServicio;
		
		@ApiModelProperty(notes = "Servicios Valores",required=true,value="Ej: Lista")
		private String valorTipoServicio;
		
		//Atencion del servicio
		@ApiModelProperty(notes = "Acceso Publico del Servicio",required=true,value="Ej: Lista")
		private String accesoPublicoAtencionServicio;		

		@ApiModelProperty(notes = "Nombres Meses del Servicio",required=true,value="Ej: Lista")
		private String nombreMesesAtencionServicio;	

		@ApiModelProperty(notes = "Valores Meses del Servicio",required=true,value="Ej: Lista")
		private String valorMesesAtencionServicio;	
		
		@ApiModelProperty(notes = "Atención del servicio",required=true,value="Ej: Lista")
		private List<AtencionRptServicioTurnoDiaDto> turnos;	


		public VerRptBibliotecaDto(){
			this.entidad = new VerEntidadDto();
			this.responsable = new VerResponsableDto();
			this.infraestructura = new VerRptInfraestructuraDto();
			this.turnos = new ArrayList<>();
		}
		

		public String getAnioDeclaracion() {
			return anioDeclaracion;
		}

		public void setAnioDeclaracion(String anioDeclaracion) {
			this.anioDeclaracion = anioDeclaracion;
		}

		public String getNombre() {
			return nombre;
		}

		public void setNombre(String nombre) {
			this.nombre = nombre;
		}

		public String getIdAmbitoTerritorial() {
			return idAmbitoTerritorial;
		}

		public void setIdAmbitoTerritorial(String idAmbitoTerritorial) {
			this.idAmbitoTerritorial = idAmbitoTerritorial;
		}

		public String getIdTipoFuncionamiento() {
			return idTipoFuncionamiento;
		}

		public void setIdTipoFuncionamiento(String idTipoFuncionamiento) {
			this.idTipoFuncionamiento = idTipoFuncionamiento;
		}

		public String getDireccion() {
			return direccion;
		}

		public void setDireccion(String direccion) {
			this.direccion = direccion;
		}

		public String getCorreoElectronico() {
			return correoElectronico;
		}

		public void setCorreoElectronico(String correoElectronico) {
			this.correoElectronico = correoElectronico;
		}

		public String getNumeroBibliotecologos() {
			return numeroBibliotecologos;
		}

		public void setNumeroBibliotecologos(String numeroBibliotecologos) {
			this.numeroBibliotecologos = numeroBibliotecologos;
		}

		public String getNumeroTotalPersonal() {
			return numeroTotalPersonal;
		}

		public void setNumeroTotalPersonal(String numeroTotalPersonal) {
			this.numeroTotalPersonal = numeroTotalPersonal;
		}

		public String getTelefonoFijo() {
			return telefonoFijo;
		}

		public void setTelefonoFijo(String telefonoFijo) {
			this.telefonoFijo = telefonoFijo;
		}

		public String getTelefonoMovil() {
			return telefonoMovil;
		}

		public void setTelefonoMovil(String telefonoMovil) {
			this.telefonoMovil = telefonoMovil;
		}

		public String getIdRegistrador() {
			return idRegistrador;
		}

		public void setIdRegistrador(String idRegistrador) {
			this.idRegistrador = idRegistrador;
		}

		public String getCentroPoblado() {
			return centroPoblado;
		}

		public void setCentroPoblado(String centroPoblado) {
			this.centroPoblado = centroPoblado;
		}

		public String getIdTipoBibliotecaPadre() {
			return idTipoBibliotecaPadre;
		}

		public void setIdTipoBibliotecaPadre(String idTipoBibliotecaPadre) {
			this.idTipoBibliotecaPadre = idTipoBibliotecaPadre;
		}

		public String getIdTipoBibliotecaHijo() {
			return idTipoBibliotecaHijo;
		}

		public void setIdTipoBibliotecaHijo(String idTipoBibliotecaHijo) {
			this.idTipoBibliotecaHijo = idTipoBibliotecaHijo;
		}

		public String getIdEstadoBiblioteca() {
			return idEstadoBiblioteca;
		}

		public void setIdEstadoBiblioteca(String idEstadoBiblioteca) {
			this.idEstadoBiblioteca = idEstadoBiblioteca;
		}

		public String getUbigeo() {
			return ubigeo;
		}

		public void setUbigeo(String ubigeo) {
			this.ubigeo = ubigeo;
		}		

		public String getNombreColecciones() {
			return nombreColecciones;
		}


		public void setNombreColecciones(String nombreColecciones) {
			this.nombreColecciones = nombreColecciones;
		}
		
		public String getValorColecciones() {
			return valorColecciones;
		}


		public void setValorColecciones(String valorColecciones) {
			this.valorColecciones = valorColecciones;
		}
		
		public String getNombreTipoServicio() {
			return nombreTipoServicio;
		}

		public void setNombreTipoServicio(String nombreTipoServicio) {
			this.nombreTipoServicio = nombreTipoServicio;
		}
		
		public String getValorTipoServicio() {
			return valorTipoServicio;
		}

		public void setValorTipoServicio(String valorTipoServicio) {
			this.valorTipoServicio = valorTipoServicio;
		}

		public String getIdBiblioteca() {
			return idBiblioteca;
		}

		public void setIdBiblioteca(String id) {
			this.idBiblioteca = id;
		}

		public String getEstadoBiblioteca() {
			return estadoBiblioteca;
		}

		public void setEstadoBiblioteca(String estadoBiblioteca) {
			this.estadoBiblioteca = estadoBiblioteca;
		}

		public String getAmbitoTerritorial() {
			return ambitoTerritorial;
		}

		public void setAmbitoTerritorial(String ambitoTerritorial) {
			this.ambitoTerritorial = ambitoTerritorial;
		}

		public String getTipoFuncionamiento() {
			return tipoFuncionamiento;
		}

		public void setTipoFuncionamiento(String tipoFuncionamiento) {
			this.tipoFuncionamiento = tipoFuncionamiento;
		}

		public String getNombreRegistrador() {
			return nombreRegistrador;
		}

		public void setNombreRegistrador(String nombreRegistrador) {
			this.nombreRegistrador = nombreRegistrador;
		}

		public String getDocIdentidadRegistrador() {
			return docIdentidadRegistrador;
		}

		public void setDocIdentidadRegistrador(String docIdentidadRegistrador) {
			this.docIdentidadRegistrador = docIdentidadRegistrador;
		}

		public VerEntidadDto getEntidad() {
			return entidad;
		}

		public void setEntidad(VerEntidadDto entidad) {
			this.entidad = entidad;
		}

		public VerResponsableDto getResponsable() {
			return responsable;
		}

		public void setResponsable(VerResponsableDto responsable) {
			this.responsable = responsable;
		}

		public String getTipoBibliotecaPadre() {
			return tipoBibliotecaPadre;
		}

		public void setTipoBibliotecaPadre(String tipoBibliotecaPadre) {
			this.tipoBibliotecaPadre = tipoBibliotecaPadre;
		}

		public String getTipoBibliotecaHijo() {
			return tipoBibliotecaHijo;
		}

		public void setTipoBibliotecaHijo(String tipoBibliotecaHijo) {
			this.tipoBibliotecaHijo = tipoBibliotecaHijo;
		}
		
		public VerRptInfraestructuraDto getInfraestructura() {
			return infraestructura;
		}

		public void setInfraestructura(VerRptInfraestructuraDto infraestructura) {
			this.infraestructura = infraestructura;
		}

		public String getTotalLectoresAnioAnterior() {
			return totalLectoresAnioAnterior;
		}

		public void setTotalLectoresAnioAnterior(String totalLectoresAnioAnterior) {
			this.totalLectoresAnioAnterior = totalLectoresAnioAnterior;
		}

		public String getTotalLectores() {
			return totalLectores;
		}

		public void setTotalLectores(String totalLectores) {
			this.totalLectores = totalLectores;
		}

		public String getTotalConsultas() {
			return totalConsultas;
		}

		public void setTotalConsultas(String totalConsultas) {
			this.totalConsultas = totalConsultas;
		}

		public String getPlanTrabActual() {
			return planTrabActual;
		}

		public void setPlanTrabActual(String planTrabActual) {
			this.planTrabActual = planTrabActual;
		}

		public String getReglamentos() {
			return reglamentos;
		}

		public void setReglamentos(String reglamentos) {
			this.reglamentos = reglamentos;
		}

		public String getEstadisticas() {
			return estadisticas;
		}

		public void setEstadisticas(String estadisticas) {
			this.estadisticas = estadisticas;
		}

		public String getEspecificar() {
			return especificar;
		}

		public void setEspecificar(String especificar) {
			this.especificar = especificar;
		}
		
		public String getAccesoPublicoAtencionServicio() {
			return accesoPublicoAtencionServicio;
		}


		public void setAccesoPublicoAtencionServicio(String accesoPublicoAtencionServicio) {
			this.accesoPublicoAtencionServicio = accesoPublicoAtencionServicio;
		}
		
		public String getNombreMesesAtencionServicio() {
			return nombreMesesAtencionServicio;
		}


		public void setNombreMesesAtencionServicio(String nombreMesesAtencionServicio) {
			this.nombreMesesAtencionServicio = nombreMesesAtencionServicio;
		}	
		
		public String getValorMesesAtencionServicio() {
			return valorMesesAtencionServicio;
		}

		public void setValorMesesAtencionServicio(String valorMesesAtencionServicio) {
			this.valorMesesAtencionServicio = valorMesesAtencionServicio;
		}
		
		public List<AtencionRptServicioTurnoDiaDto> getTurnos() {
			return turnos;
		}

		public void setTurnos(List<AtencionRptServicioTurnoDiaDto> turnos) {
			this.turnos = turnos;
		}
}
