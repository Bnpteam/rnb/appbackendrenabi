package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class TipoBiblioteca {
	private	int idTipoBiblioteca;
	private String nombre;
	private int idTipoBibliotecaPadre;
	private String nombrePadre;
	private String estado;
	
	
	public TipoBiblioteca() {
		super();
		this.idTipoBiblioteca = Constants.ZERO_INTEGER;
		this.nombre = Constants.EMPTY_STRING;
		this.idTipoBibliotecaPadre =Constants.ZERO_INTEGER;
		this.estado = Constants.EMPTY_STRING;
		this.nombrePadre = Constants.EMPTY_STRING;
	}
	
	public int getIdTipoBiblioteca() {
		return idTipoBiblioteca;
	}
	public void setIdTipoBiblioteca(int idTipoBiblioteca) {
		this.idTipoBiblioteca = idTipoBiblioteca;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getIdTipoBibliotecaPadre() {
		return idTipoBibliotecaPadre;
	}
	public void setIdTipoBibliotecaPadre(int idTipoBibliotecaPadre) {
		this.idTipoBibliotecaPadre = idTipoBibliotecaPadre;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNombrePadre() {
		return nombrePadre;
	}

	public void setNombrePadre(String nombrePadre) {
		this.nombrePadre = nombrePadre;
	}
	
	
}
