package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class TipoBibliotecaHijoDto {
	private String idTipoBiblioteca;
	private String nombre;
	private String idTipoBibliotecaPadre;
	private String estado;
	
	public TipoBibliotecaHijoDto() {
		super();
		this.idTipoBiblioteca = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
		this.idTipoBibliotecaPadre = Constants.EMPTY_STRING;
		this.estado = Constants.EMPTY_STRING;
	}
	
	public String getIdTipoBiblioteca() {
		return idTipoBiblioteca;
	}
	public void setIdTipoBiblioteca(String idTipoBiblioteca) {
		this.idTipoBiblioteca = idTipoBiblioteca;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getIdTipoBibliotecaPadre() {
		return idTipoBibliotecaPadre;
	}
	public void setIdTipoBibliotecaPadre(String idTipoBibliotecaPadre) {
		this.idTipoBibliotecaPadre = idTipoBibliotecaPadre;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
