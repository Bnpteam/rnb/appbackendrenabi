package pe.gob.bnp.services.rest.renabi.evento.domain.entity;

public class EventoDepartamento {
	private String idDepartamento;
	private String departamento;
	private String cantidadEventos;
	
	public String getIdDepartamento() {
		return idDepartamento;
	}
	public void setIdDepartamento(String idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getCantidadEventos() {
		return cantidadEventos;
	}
	public void setCantidadEventos(String cantidadEventos) {
		this.cantidadEventos = cantidadEventos;
	}

	
}
