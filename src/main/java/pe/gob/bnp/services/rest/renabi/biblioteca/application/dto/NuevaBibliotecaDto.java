package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Biblioteca;
import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.dto.NuevoTipoValorDto;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.NuevaEntidadDto;

public class NuevaBibliotecaDto {
	//Datos generales
	private String id;
	private String anioDeclaracion;
	private String nombre;
	private String correoElectronico;
	private String telefonoFijo;
	private String telefonoMovil;
	private String numeroBibliotecologos;
	private String numeroTotalPersonal;
	private String idEstadoBiblioteca;
	private String idCreacionAuditoria;
	private String ipCreacionAuditoria;
	private String totalLectoresAnioAnterior;
	private String totalLectores;
	private String totalConsultas;
	private String planTrabActual;
	private String reglamentos;
	private String estadisticas;
	private String especificar;	

	//Ambito Territorial
	private String idAmbitoTerritorial;

	//Tipo de Funcionamiento
	private String idTipoFuncionamiento;

	//Registrador
	private String idRegistrador;
	
	//Entidad
	private NuevaEntidadDto entidad;
	
	//Responsable
	private NuevaResponsableDto responsable;

	//Tipo biblioteca
	private	String idTipoBibliotecaPadre;
	private String idTipoBibliotecaHijo;
	
	
	//ubigeo
	private String codDepartamento;
	private String codProvincia;
	private String codDistrito;
	private String centroPoblado;
	private String direccion;
	
	//Infraestructura
	private NuevaInfraestructuraDto infraestructura;	
	private List<NuevoTipoValorDto> colecciones;
	private List<NuevoTipoValorDto> servicios;
	
	//Atencion del servicio
	private AtencionServicioDto atencionServicio;
	
	private String flagAceptoTyC;
	private String flagAceptoDDJJ;	
	
	public NuevaBibliotecaDto(){
		this.infraestructura = new  NuevaInfraestructuraDto();
		this.entidad = new NuevaEntidadDto();
		this.responsable = new NuevaResponsableDto();
		this.infraestructura = new NuevaInfraestructuraDto();
		this.colecciones = new ArrayList<>();
		this.servicios = new ArrayList<>();
		this.atencionServicio = new  AtencionServicioDto();
		this.flagAceptoTyC = Constants.EMPTY_STRING;
		this.flagAceptoDDJJ = Constants.EMPTY_STRING;		
	}
	
	//fllc completar
	public NuevaBibliotecaDto(Biblioteca objeto) {
		super();
		this.anioDeclaracion = objeto.getAnioDeclaracion();
		this.nombre = objeto.getNombre();
		this.entidad = new NuevaEntidadDto(objeto.getEntidad());
		this.idAmbitoTerritorial = objeto.getAmbitoTerritorial().getId();
		this.idTipoFuncionamiento = objeto.getTipoFuncionamiento().getId();
		this.direccion = objeto.getDireccion();
		this.correoElectronico = objeto.getCorreoElectronico();
		this.numeroBibliotecologos = Util.getIntToString(objeto.getNumeroBibliotecologos());
		this.numeroTotalPersonal = Util.getIntToString(objeto.getNumeroTotalPersonal());
		this.telefonoFijo = objeto.getTelefonoFijo();
		this.telefonoMovil = objeto.getTelefonoMovil();
		
		this.idTipoBibliotecaPadre = Util.getIntToString(objeto.getTipoBiblioteca().getIdTipoBibliotecaPadre());
		this.idTipoBibliotecaHijo = Util.getIntToString(objeto.getTipoBiblioteca().getIdTipoBiblioteca());
		this.idEstadoBiblioteca = objeto.getEstadoBiblioteca().getId();
		this.idCreacionAuditoria = objeto.getIdUsuarioRegistro();
		this.ipCreacionAuditoria = objeto.getIpRegistro();
		this.responsable = new NuevaResponsableDto();
		this.infraestructura = new  NuevaInfraestructuraDto();
		
		//this.codDepartamento = obs fllc
	}

	public String getAnioDeclaracion() {
		return anioDeclaracion;
	}

	public void setAnioDeclaracion(String anioDeclaracion) {
		this.anioDeclaracion = anioDeclaracion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public NuevaEntidadDto getEntidad() {
		return entidad;
	}

	public void setEntidad(NuevaEntidadDto entidad) {
		this.entidad = entidad;
	}

	public String getIdAmbitoTerritorial() {
		return idAmbitoTerritorial;
	}

	public void setIdAmbitoTerritorial(String idAmbitoTerritorial) {
		this.idAmbitoTerritorial = idAmbitoTerritorial;
	}

	public String getIdTipoFuncionamiento() {
		return idTipoFuncionamiento;
	}

	public void setIdTipoFuncionamiento(String idTipoFuncionamiento) {
		this.idTipoFuncionamiento = idTipoFuncionamiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getNumeroBibliotecologos() {
		return numeroBibliotecologos;
	}

	public void setNumeroBibliotecologos(String numeroBibliotecologos) {
		this.numeroBibliotecologos = numeroBibliotecologos;
	}

	public String getNumeroTotalPersonal() {
		return numeroTotalPersonal;
	}

	public void setNumeroTotalPersonal(String numeroTotalPersonal) {
		this.numeroTotalPersonal = numeroTotalPersonal;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getIdRegistrador() {
		return idRegistrador;
	}

	public void setIdRegistrador(String idRegistrador) {
		this.idRegistrador = idRegistrador;
	}

	public String getCentroPoblado() {
		return centroPoblado;
	}

	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}

	public String getIdTipoBibliotecaPadre() {
		return idTipoBibliotecaPadre;
	}

	public void setIdTipoBibliotecaPadre(String idTipoBibliotecaPadre) {
		this.idTipoBibliotecaPadre = idTipoBibliotecaPadre;
	}

	public String getIdTipoBibliotecaHijo() {
		return idTipoBibliotecaHijo;
	}

	public void setIdTipoBibliotecaHijo(String idTipoBibliotecaHijo) {
		this.idTipoBibliotecaHijo = idTipoBibliotecaHijo;
	}

	public String getIdEstadoBiblioteca() {
		return idEstadoBiblioteca;
	}

	public void setIdEstadoBiblioteca(String idEstadoBiblioteca) {
		this.idEstadoBiblioteca = idEstadoBiblioteca;
	}

	public String getIdCreacionAuditoria() {
		return idCreacionAuditoria;
	}

	public void setIdCreacionAuditoria(String idCreacionAuditoria) {
		this.idCreacionAuditoria = idCreacionAuditoria;
	}

	public String getIpCreacionAuditoria() {
		return ipCreacionAuditoria;
	}

	public void setIpCreacionAuditoria(String ipCreacionAuditoria) {
		this.ipCreacionAuditoria = ipCreacionAuditoria;
	}

	public NuevaResponsableDto getResponsable() {
		return responsable;
	}

	public void setResponsable(NuevaResponsableDto responsable) {
		this.responsable = responsable;
	}

	public String getCodDepartamento() {
		return codDepartamento;
	}

	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}

	public String getCodProvincia() {
		return codProvincia;
	}

	public void setCodProvincia(String codProvincia) {
		this.codProvincia = codProvincia;
	}

	public String getCodDistrito() {
		return codDistrito;
	}

	public void setCodDistrito(String codDistrito) {
		this.codDistrito = codDistrito;
	}


	public NuevaInfraestructuraDto getInfraestructura() {
		return infraestructura;
	}


	public void setInfraestructura(NuevaInfraestructuraDto infraestructura) {
		this.infraestructura = infraestructura;
	}


	public List<NuevoTipoValorDto> getColecciones() {
		return colecciones;
	}


	public void setColecciones(List<NuevoTipoValorDto> colecciones) {
		this.colecciones = colecciones;
	}


	public List<NuevoTipoValorDto> getServicios() {
		return servicios;
	}


	public void setServicios(List<NuevoTipoValorDto> servicios) {
		this.servicios = servicios;
	}


	public AtencionServicioDto getAtencionServicio() {
		return atencionServicio;
	}


	public void setAtencionServicio(AtencionServicioDto atencionServicio) {
		this.atencionServicio = atencionServicio;
	}

	public String getTotalLectoresAnioAnterior() {
		return totalLectoresAnioAnterior;
	}

	public void setTotalLectoresAnioAnterior(String totalLectoresAnioAnterior) {
		this.totalLectoresAnioAnterior = totalLectoresAnioAnterior;
	}

	public String getTotalLectores() {
		return totalLectores;
	}

	public void setTotalLectores(String totalLectores) {
		this.totalLectores = totalLectores;
	}

	public String getTotalConsultas() {
		return totalConsultas;
	}

	public void setTotalConsultas(String totalConsultas) {
		this.totalConsultas = totalConsultas;
	}

	public String getPlanTrabActual() {
		return planTrabActual;
	}

	public void setPlanTrabActual(String planTrabActual) {
		this.planTrabActual = planTrabActual;
	}

	public String getReglamentos() {
		return reglamentos;
	}

	public void setReglamentos(String reglamentos) {
		this.reglamentos = reglamentos;
	}

	public String getEstadisticas() {
		return estadisticas;
	}

	public void setEstadisticas(String estadisticas) {
		this.estadisticas = estadisticas;
	}

	public String getEspecificar() {
		return especificar;
	}

	public void setEspecificar(String especificar) {
		this.especificar = especificar;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFlagAceptoTyC() {
		return flagAceptoTyC;
	}

	public void setFlagAceptoTyC(String flagAceptoTyC) {
		this.flagAceptoTyC = flagAceptoTyC;
	}

	public String getFlagAceptoDDJJ() {
		return flagAceptoDDJJ;
	}

	public void setFlagAceptoDDJJ(String flagAceptoDDJJ) {
		this.flagAceptoDDJJ = flagAceptoDDJJ;
	}


}
