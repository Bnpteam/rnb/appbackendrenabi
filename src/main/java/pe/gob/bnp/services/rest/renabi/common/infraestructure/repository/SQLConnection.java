package pe.gob.bnp.services.rest.renabi.common.infraestructure.repository;


import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SQLConnection {

    private static final Logger LOGGER = Logger.getLogger(Connection.class);
    private static InitialContext initialContext;
    private static HashMap<String, DataSource> cache = new HashMap<String, DataSource>();
    
	private static String ipDbSIGA;
	
	private static String usuarioDb;    

	private static String passwordDb;    	

	private static String eschemaDb;    
	
    static {
        try {
            initialContext = new InitialContext();
        } catch (NamingException exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
    }

	public static String getIpDbSIGA() {
		return ipDbSIGA;
	}

	
	
	@Value("${sinabi.db.ip}")
	public  void setIpDbSIGA(String ip) {
		ipDbSIGA = ip;
	}

	public static String getUsuarioDb() {
		return usuarioDb;
	}


	@Value("${sinabi.db.usuario}")
	public  void setUsuarioDb( String usuario) {
		usuarioDb = usuario;
	}

	public static String getPasswordDb() {
		return passwordDb;
	}


	@Value("${sinabi.db.password}")
	public  void setPasswordDb( String password) {
		passwordDb = password;
	}

	public static String getEschemaDb() {
		return eschemaDb;
	}


	@Value("${sinabi.db.schema}")
	public  void setEschemaDb(String schema) {
		eschemaDb = schema;
	}
    
    public static Connection getConnectionJDBC() throws Exception {
        //String connectionURL = "jdbc:oracle:thin:@"+urlDbSIGA+";user="+usuarioDbSIGA+";password="+passwordDbSIGA+";";
    	String connectionURL = "jdbc:oracle:thin:@"+ipDbSIGA+":1521:"+eschemaDb;  //DEV
    	//String connectionURL = "jdbc:oracle:thin:@172.16.88.154:1521:DBDEV";  //el firme
        Connection connection = null;
        try {
            //connection = DriverManager.getConnection(connectionURL,"RNB","dev_rnbsd23x");
            connection = DriverManager.getConnection(connectionURL,usuarioDb,passwordDb);
            connection.setAutoCommit(false);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return connection;
    }

    public static Connection getConnectionJNDI(String jndi) throws Exception {
        Connection connection = getDataSource(jndi).getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

    private static DataSource getDataSource(String jndi) throws Exception {
        DataSource dataSource = null;
        try {
            dataSource = (DataSource) cache.get(jndi);
            if (dataSource == null) {
                dataSource = (DataSource) initialContext.lookup(jndi);
                cache.put(jndi, dataSource);
            }
        } catch (Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
        }
        return dataSource;
    }


}
