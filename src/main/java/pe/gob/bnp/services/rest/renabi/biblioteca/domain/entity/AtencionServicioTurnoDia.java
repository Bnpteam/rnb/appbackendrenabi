package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.AtencionServicioTurnoDiaDto;
import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.BaseEntity;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Tipo;

public class AtencionServicioTurnoDia extends BaseEntity{
	private Tipo tipoTurno;
	private String horaInicio;
	private String horaFin;
	private Long idAtencionTurno;
	
	public AtencionServicioTurnoDia() {
		super();
		this.tipoTurno = new Tipo();
		this.horaInicio = Constants.EMPTY_STRING;
		this.horaFin = Constants.EMPTY_STRING;
		this.idAtencionTurno = Constants.ZERO_LONG;
	}
	
	public AtencionServicioTurnoDia(AtencionServicioTurnoDiaDto dto) {
		super();
		this.tipoTurno = new Tipo();
		this.tipoTurno.setId(dto.getIdTurno());
		this.tipoTurno.setNombre(dto.getNombreTurno());
		this.horaInicio = dto.getHoraInicio();
		this.horaFin = dto.getHoraFin();
	}
	
	public Tipo getTipoTurno() {
		return tipoTurno;
	}
	public void setTipoTurno(Tipo tipoTurno) {
		this.tipoTurno = tipoTurno;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}

	public Long getIdAtencionTurno() {
		return idAtencionTurno;
	}

	public void setIdAtencionTurno(Long idAtencionTurno) {
		this.idAtencionTurno = idAtencionTurno;
	}
	

	
}
