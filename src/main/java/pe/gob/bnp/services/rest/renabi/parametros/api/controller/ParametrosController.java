package pe.gob.bnp.services.rest.renabi.parametros.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.services.rest.renabi.common.api.controller.ResponseHandler;
import pe.gob.bnp.services.rest.renabi.common.application.EntityNotFoundResultException;
import pe.gob.bnp.services.rest.renabi.parametros.application.ParametrosApplicationService;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaMaestraDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaNombreBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaTipoBibliotecaHijoDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaTipoBibliotecaPadreDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaUbigeoDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.UbigeoDto;

@RestController
@RequestMapping("api/parametros/")
@Api(value = "/api/parametros/",description="Servicio REST para manejo de parámetros generales - Desarrollado por OTIE/BNP")
@CrossOrigin(origins = "*")
public class ParametrosController {
	@Autowired
	ParametrosApplicationService parametrosApplicationService;
	
	@Autowired
	ResponseHandler responseHandler;
	
	
	@RequestMapping(method = RequestMethod.GET, path="/{tipoTabla}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar tabla Maestra", response= ListaMaestraDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarTablaMaestra(
			@ApiParam(value = "nombre del tipo de tabla a extraer", required = true)
			@PathVariable String tipoTabla ){
		try {
			ListaMaestraDto response = parametrosApplicationService.listar(tipoTabla);
			if (response == null || response.getLista().size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/tiposBibliotecaPadre", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar tabla Maestra", response= ListaTipoBibliotecaPadreDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarTiposBibliotecaPadre(){
		try {
			ListaTipoBibliotecaPadreDto response = parametrosApplicationService.listarTiposBibliotecaPadre();
			if (response == null || response.getLista().size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/tiposBibliotecaHijo", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar tabla Maestra", response= ListaTipoBibliotecaHijoDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarTiposBibliotecaHijo(){
		try {
			ListaTipoBibliotecaHijoDto response = parametrosApplicationService.listarTiposBibliotecaHijo();
			if (response == null || response.getLista().size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/ubigeos", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar tabla de ubigeos", response= ListaUbigeoDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeos(){
		try {
			ListaUbigeoDto response = parametrosApplicationService.listarUbigeos();
			if (response == null || response.getLista().size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
		
	
	@RequestMapping(method = RequestMethod.GET, path="/maestra/nombrelargo/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Obtiene la descripción larga de un registro de la tabla maestra", response= String.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarTablaMaestraNombre(
			@ApiParam(value = "Id de la tabla maestra", required = true)
			@PathVariable Long id){
		try {
			String response = parametrosApplicationService.getDescripcionLargaTablaMaestra(id);
			if (response == null ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontró el registro.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontró el registro.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
	
	@RequestMapping(method = RequestMethod.GET, path="/ubigeo/departamentos", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar tabla de ubigeo de departamentos", response= UbigeoDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeoDepartamentos(){
		try {
			List<UbigeoDto> response = parametrosApplicationService.listarDepartamentos();
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/ubigeo/provincias/{coddep}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar tabla de ubigeo de provincias", response= UbigeoDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeoProvincias(			
			@ApiParam(value = "Código de departamento", required = true)
			@PathVariable String coddep){
		try {
			List<UbigeoDto> response = parametrosApplicationService.listarProvincias(coddep);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
			
	
	@RequestMapping(method = RequestMethod.GET, path="/ubigeo/distritos/{coddep}/{codpro}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar tabla de ubigeo de distritos", response= UbigeoDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarUbigeoDistritos(			
			@ApiParam(value = "Código de departamento", required = true)
			@PathVariable String coddep,
			@ApiParam(value = "Código de provincia", required = true)
			@PathVariable String codpro){
		try {
			List<UbigeoDto> response = parametrosApplicationService.listarDistritos(coddep, codpro);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/nombresBiblioteca", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Listar nombres Biblioteca", response= ListaNombreBibliotecaDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> listarNombresBiblioteca(){
		try {
			ListaNombreBibliotecaDto response = parametrosApplicationService.listarNombresBiblioteca();
			if (response == null || response.getLista().size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
}
