package pe.gob.bnp.services.rest.renabi.parametros.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.Maestra;

@ApiModel(value="Maestra", description="Objeto que represta un registra de la tabla TBL_MAESTRA")
public class MaestraDto {
	@ApiModelProperty(notes = "id de la tabla tbl_maestra",required=true,value="10")
	private String id;
	
	@ApiModelProperty(notes = "descripción corta",value="DNI")
	private String descripcionCorta;
	
	@ApiModelProperty(notes = "descripción larga",value="Documento Nacional de Identidad")
	private String descripcionLarga;
	
	public MaestraDto() {
	}
	
	public MaestraDto(Maestra objeto) {
		super();
		this.id = Util.getLongToString(objeto.getId());
		this.descripcionCorta = objeto.getDescripcionCorta();
		this.descripcionLarga = objeto.getDescripcionLarga();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDescripcionCorta() {
		return descripcionCorta;
	}
	public void setDescripcionCorta(String descripcionCorta) {
		this.descripcionCorta = descripcionCorta;
	}
	public String getDescripcionLarga() {
		return descripcionLarga;
	}
	public void setDescripcionLarga(String descripcionLarga) {
		this.descripcionLarga = descripcionLarga;
	}
	
	
}
