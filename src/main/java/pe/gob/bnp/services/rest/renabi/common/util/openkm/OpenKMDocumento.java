package pe.gob.bnp.services.rest.renabi.common.util.openkm;

import java.rmi.RemoteException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import pe.gob.bnp.services.rest.renabi.common.util.openkm.FileOpenKM;
import pe.gob.bnp.services.rest.renabi.common.util.openkm.OpenKMBase;
import pe.gob.bnp.services.rest.renabi.common.util.openkm.OpenKMDocumento;
import pe.gob.bnp.services.rest.renabi.common.util.openkm.ResultOpenKMDocumento;
import ws.openkm.bnp.gob.pe.ArchivoType;
import ws.openkm.bnp.gob.pe.MetadataType;

public class OpenKMDocumento extends OpenKMBase{
    
	private static final Log logger = LogFactory.getLog(OpenKMDocumento.class);
 
	public ResultOpenKMDocumento registrarContenido(FileOpenKM fileOpenKM){
//		logger.warn("registrarContenido:init");
		ResultOpenKMDocumento resultOpenKMDocumento = new ResultOpenKMDocumento();
		
		MetadataType metadataType = new MetadataType();
		metadataType.setParameter1("okg:documento");
		metadataType.setParameter2(this.getNombreArchivoOPENKM(fileOpenKM.getFileName()));
//		metadataType.setParameter3(fileOpenKM.getFechaUpload()+".000");
		
		ArchivoType archivoType = new ArchivoType();
		archivoType.setDocumento(fileOpenKM.getRawFile());
		archivoType.setFormato(fileOpenKM.getExtension());		
		archivoType.setRutaRepositorio(this.getRutaRepositorio(fileOpenKM));
		archivoType.setNombreDocumento(this.getNombreArchivoOPENKM(fileOpenKM.getFileName()));
				
		try {
//			logger.warn("registrarContenido:open");
			super.setResponseRegistrarContenidoType(super.getOkm().registrarContenido(null, super.getSeguridadType(), super.getAuditoriaType(), metadataType, archivoType));
			resultOpenKMDocumento.setUiid(super.getResponseRegistrarContenidoType().getArchivo().getUiidRecurso());
			resultOpenKMDocumento.setDescripcion(super.getResponseRegistrarContenidoType().getMensaje().getDescripMensaje());
			resultOpenKMDocumento.setCodigo(super.getResponseRegistrarContenidoType().getMensaje().getCodigoMensaje());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			resultOpenKMDocumento=null;
			//System.out.println("error al guardar archivo:"+e);
//			logger.error("registrarContenido:error "+fileOpenKM.getFileName(), e);
		}
//		logger.warn("registrarContenido:out");	
		return resultOpenKMDocumento;
	}
	
	public ResultOpenKMDocumento modificarContenido(FileOpenKM fileOpenKM){
//		logger.warn("modificarContenido:init");
		ResultOpenKMDocumento resultOpenKMDocumento = new ResultOpenKMDocumento();
		
		MetadataType metadataType = new MetadataType();
		metadataType.setParameter1("okg:documento");
		metadataType.setParameter2(this.getNombreArchivoOPENKM(fileOpenKM.getFileName()));	
		metadataType.setParameter3(fileOpenKM.getFechaUpload()+".000");
		metadataType.setParameter4(fileOpenKM.isPrincipal()?fileOpenKM.getTipoDoc():"ANEXOS");
		metadataType.setParameter5(fileOpenKM.isPrincipal()?"true":"false");
		metadataType.setParameter6(fileOpenKM.getExtension());
		metadataType.setParameter7(fileOpenKM.getNumeroExpediente());
		
		ArchivoType archivoType = new ArchivoType();
		archivoType.setDocumento(fileOpenKM.getRawFile());
		archivoType.setUiidRecurso(fileOpenKM.getUiid());
				
		try {
//			logger.warn("modificarContenido:open");
			super.setResponseModificarContenidoType(super.getOkm().modificarContenido(null, super.getSeguridadType(), super.getAuditoriaType(), metadataType, archivoType));
			resultOpenKMDocumento.setCodigo(super.getResponseModificarContenidoType().getMensaje().getCodigoMensaje());
			resultOpenKMDocumento.setDescripcion(super.getResponseModificarContenidoType().getMensaje().getDescripMensaje());	
			resultOpenKMDocumento.setUiid(fileOpenKM.getUiid());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			resultOpenKMDocumento=null;
//			logger.error("modificarContenido:error "+fileOpenKM.getFileName()+" "+fileOpenKM.getUiid(), e);			
		}
//		logger.warn("modificarContenido:out");	
		return resultOpenKMDocumento;
	}
	
	public ResultOpenKMDocumento consultarContenido(FileOpenKM fileOpenKM){
//		logger.warn("consultarContenido:init");
		ResultOpenKMDocumento resultOpenKMDocumento = new ResultOpenKMDocumento();
						
		ArchivoType archivoType = new ArchivoType();
		archivoType.setUiidRecurso(fileOpenKM.getUiid());
				
		try {
//			logger.warn("consultarContenido:open");
			super.setResponseConsultarContenidoType(super.getOkm().consultarContenido(null, super.getSeguridadType(), super.getAuditoriaType(), archivoType));
			resultOpenKMDocumento.setRawFile(super.getResponseConsultarContenidoType().getArchivo().getDocumento());
			resultOpenKMDocumento.setDescripcion(super.getResponseConsultarContenidoType().getMensaje().getDescripMensaje());
			resultOpenKMDocumento.setCodigo(super.getResponseConsultarContenidoType().getMensaje().getCodigoMensaje());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			resultOpenKMDocumento=null;
//			logger.error("consultarContenido:error"+" "+fileOpenKM.getUiid(), e);
		}
//		logger.warn("consultarContenido:out");	
		return resultOpenKMDocumento;
	}
	
	public String getNombreArchivoOPENKM(String nameFile){
		return (nameFile.replace("/","_"));	
	}
	
	public String getRutaRepositorio(FileOpenKM fileOpenKM){
		
		StringBuilder pathAlfrescoFull = new StringBuilder();	
		pathAlfrescoFull.append("/RENABI/ARCHIVOS");		
 		pathAlfrescoFull.append("/"+fileOpenKM.getAnnioFechaDocumento());
		pathAlfrescoFull.append("/"+fileOpenKM.getMesFechaDocumento());
		pathAlfrescoFull.append("/"+fileOpenKM.getDiaFechaDocumento());
		pathAlfrescoFull.append("/"+fileOpenKM.getIdBiblioteca());
  
		return pathAlfrescoFull.toString();

	}
	
}
