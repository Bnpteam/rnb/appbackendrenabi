package pe.gob.bnp.services.rest.renabi.common.util.openkm;

import javax.xml.rpc.ServiceException;

import ws.openkm.bnp.gob.pe.ArchivoType;
import ws.openkm.bnp.gob.pe.AuditoriaType;
import ws.openkm.bnp.gob.pe.MetadataType;
import ws.openkm.bnp.gob.pe.OpenkmService_PortType;
import ws.openkm.bnp.gob.pe.OpenkmService_Service;
import ws.openkm.bnp.gob.pe.OpenkmService_ServiceLocator;
import ws.openkm.bnp.gob.pe.ResponseConsultarContenidoType;
import ws.openkm.bnp.gob.pe.ResponseModificarContenidoType;
import ws.openkm.bnp.gob.pe.ResponseRegistrarContenidoType;
import ws.openkm.bnp.gob.pe.SeguridadType;
import ws.openkm.bnp.gob.pe.TokenType;

public class OpenKMBase {
	private OpenkmService_Service okmService;
	private OpenkmService_PortType okm;
	private MetadataType metadataType;
	private TokenType tokenType;
	private SeguridadType seguridadType;
	private AuditoriaType auditoriaType;
	private ArchivoType archivoType;
	
	private ResponseRegistrarContenidoType responseRegistrarContenidoType;
	private ResponseConsultarContenidoType responseConsultarContenidoType;
	private ResponseModificarContenidoType responseModificarContenidoType;
	
	public OpenKMBase(){
	this.init();
}

public void init(){
	
	this.setMetadataType(new MetadataType());
	this.setTokenType(new TokenType());
	this.setSeguridadType(new SeguridadType());
	this.setAuditoriaType(new AuditoriaType());	
	this.setArchivoType(new ArchivoType());
	
	this.setResponseRegistrarContenidoType(new ResponseRegistrarContenidoType());
	this.setResponseConsultarContenidoType(new ResponseConsultarContenidoType());
	this.setResponseModificarContenidoType(new ResponseModificarContenidoType());
		
	this.inicializarValores();
}

private void inicializarValores(){
	
	this.getSeguridadType().setCodAplicativo("1");		
	this.getAuditoriaType().setIpPc("172.16.88.113");
	this.getAuditoriaType().setMacAddressPc("1");
	
	try {
		this.setOkmService(new OpenkmService_ServiceLocator());
		this.setOkm(this.getOkmService().getOpenkmServiceImplPort());
		
	} catch (ServiceException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}

public OpenkmService_Service getOkmService() {
	return okmService;
}

public void setOkmService(OpenkmService_Service okmService) {
	this.okmService = okmService;
}

public OpenkmService_PortType getOkm() {
	return okm;
}

public void setOkm(OpenkmService_PortType okm) {
	this.okm = okm;
}

public MetadataType getMetadataType() {
	return metadataType;
}

public void setMetadataType(MetadataType metadataType) {
	this.metadataType = metadataType;
}

public TokenType getTokenType() {
	return tokenType;
}

public void setTokenType(TokenType tokenType) {
	this.tokenType = tokenType;
}

public SeguridadType getSeguridadType() {
	return seguridadType;
}

public void setSeguridadType(SeguridadType seguridadType) {
	this.seguridadType = seguridadType;
}

public AuditoriaType getAuditoriaType() {
	return auditoriaType;
}

public void setAuditoriaType(AuditoriaType auditoriaType) {
	this.auditoriaType = auditoriaType;
}

public ArchivoType getArchivoType() {
	return archivoType;
}

public void setArchivoType(ArchivoType archivoType) {
	this.archivoType = archivoType;
}

public ResponseRegistrarContenidoType getResponseRegistrarContenidoType() {
	return responseRegistrarContenidoType;
}

public void setResponseRegistrarContenidoType(ResponseRegistrarContenidoType responseRegistrarContenidoType) {
	this.responseRegistrarContenidoType = responseRegistrarContenidoType;
}

public ResponseConsultarContenidoType getResponseConsultarContenidoType() {
	return responseConsultarContenidoType;
}

public void setResponseConsultarContenidoType(ResponseConsultarContenidoType responseConsultarContenidoType) {
	this.responseConsultarContenidoType = responseConsultarContenidoType;
}

public ResponseModificarContenidoType getResponseModificarContenidoType() {
	return responseModificarContenidoType;
}

public void setResponseModificarContenidoType(ResponseModificarContenidoType responseModificarContenidoType) {
	this.responseModificarContenidoType = responseModificarContenidoType;
}
}
