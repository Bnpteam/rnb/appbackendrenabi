package pe.gob.bnp.services.rest.renabi.parametros.application;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.gob.bnp.services.rest.renabi.common.application.Notification;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaMaestraDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaNombreBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaTipoBibliotecaHijoDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaTipoBibliotecaPadreDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaUbigeoDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.UbigeoDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.mapper.MaestraMapper;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.ListaMaestra;
import pe.gob.bnp.services.rest.renabi.parametros.domain.repository.ParametrosRepository;

@Service
public class ParametrosApplicationService {
	@Autowired
	ParametrosRepository parametrosRepository;
	
	@Autowired
	MaestraMapper maestraMapper;
	
	public ListaUbigeoDto listarUbigeos() {
		return this.parametrosRepository.listarUbigeos();	
	}
	
	public List<UbigeoDto> listarDepartamentos() {
		return this.parametrosRepository.listarDepartamentos();	
	}
	
	public List<UbigeoDto> listarProvincias(String coddep) {
		return this.parametrosRepository.listarProvincias(coddep);	
	}

	public List<UbigeoDto> listarDistritos(String coddep, String codpro) {
		return this.parametrosRepository.listarDistritos(coddep, codpro );	
	}
	
	public ListaTipoBibliotecaHijoDto listarTiposBibliotecaHijo() {
		return this.parametrosRepository.listarTiposBibliotecaHijo();	
	}

	
	public ListaTipoBibliotecaPadreDto listarTiposBibliotecaPadre() {
		return this.parametrosRepository.listarTiposBibliotecaPadre();
	}
	
	public ListaNombreBibliotecaDto listarNombresBiblioteca() {
		return this.parametrosRepository.listarNombresBiblioteca();
	}
	
	public ListaMaestraDto listar(String tipoTabla) {
		Notification notification = new Notification();
		notification = validation(tipoTabla);
		if (notification.hasErrors()) { 
			throw new IllegalArgumentException(notification.errorMessage());
		}
		ListaMaestra lista = this.parametrosRepository.listByTableType(tipoTabla);
		return maestraMapper.mapper(lista);
	}
	
	public Notification validation(String tipoTabla) {
		Notification notification = new Notification();
		if (tipoTabla == null || tipoTabla.trim().equals("")) {
			notification.addError("Debe ingresar el tipo de tabla");
		}
		
		return notification;
	}	
	
	public Notification validation(Long id) {
		Notification notification = new Notification();
		if (id == null || id <=0L) {
			notification.addError("Id debe ser mayor a cero");
		}
		
		return notification;
	}	
	
	public String getDescripcionLargaTablaMaestra(Long idMaestra) {
		Notification notification = validation(idMaestra);
		if (notification.hasErrors()) { 
			throw new IllegalArgumentException(notification.errorMessage());
		}
		String response = this.parametrosRepository.getDescripcionLargaTablaMaestra(idMaestra);
		return response;
	}	
	

}
