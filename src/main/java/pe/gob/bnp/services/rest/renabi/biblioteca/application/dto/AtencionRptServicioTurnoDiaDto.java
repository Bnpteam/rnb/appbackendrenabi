package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class AtencionRptServicioTurnoDiaDto {
	
	private String nombreTurno;
	private String horaLunes;
	private String horaMartes;
	private String horaMiercoles;
	private String horaJueves;
	private String horaViernes;
	private String horaSabado;
	private String horaDomingo;
	

	public AtencionRptServicioTurnoDiaDto() {
		super();
		this.nombreTurno = Constants.EMPTY_STRING;
		this.horaLunes = Constants.EMPTY_STRING;
		this.horaMartes = Constants.EMPTY_STRING;
		this.horaMiercoles = Constants.EMPTY_STRING;
		this.horaJueves = Constants.EMPTY_STRING;
		this.horaViernes = Constants.EMPTY_STRING;
		this.horaSabado = Constants.EMPTY_STRING;
		this.horaDomingo = Constants.EMPTY_STRING;
	}	
	
	public String getNombreTurno() {
		return nombreTurno;
	}
	public void setNombreTurno(String nombreTurno) {
		this.nombreTurno = nombreTurno;
	}
	
	public String getHoraLunes() {
		return horaLunes;
	}

	public void setHoraLunes(String horaLunes) {
		this.horaLunes = horaLunes;
	}

	public String getHoraMartes() {
		return horaMartes;
	}

	public void setHoraMartes(String horaMartes) {
		this.horaMartes = horaMartes;
	}

	public String getHoraMiercoles() {
		return horaMiercoles;
	}

	public void setHoraMiercoles(String horaMiercoles) {
		this.horaMiercoles = horaMiercoles;
	}

	public String getHoraJueves() {
		return horaJueves;
	}

	public void setHoraJueves(String horaJueves) {
		this.horaJueves = horaJueves;
	}

	public String getHoraViernes() {
		return horaViernes;
	}

	public void setHoraViernes(String horaViernes) {
		this.horaViernes = horaViernes;
	}

	public String getHoraSabado() {
		return horaSabado;
	}

	public void setHoraSabado(String horaSabado) {
		this.horaSabado = horaSabado;
	}

	public String getHoraDomingo() {
		return horaDomingo;
	}

	public void setHoraDomingo(String horaDomingo) {
		this.horaDomingo = horaDomingo;
	}

}
