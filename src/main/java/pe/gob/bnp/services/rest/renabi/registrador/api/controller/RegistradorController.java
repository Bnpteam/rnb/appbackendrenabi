package pe.gob.bnp.services.rest.renabi.registrador.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaCabeceraDto;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.api.controller.ResponseHandler;
import pe.gob.bnp.services.rest.renabi.common.application.EmailApplicationService;
import pe.gob.bnp.services.rest.renabi.common.application.EntityNotFoundResultException;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.CredencialRegistrador;
import pe.gob.bnp.services.rest.renabi.registrador.application.RegistradorApplicationService;
import pe.gob.bnp.services.rest.renabi.registrador.application.dto.CredencialRegistradorDto;
import pe.gob.bnp.services.rest.renabi.registrador.application.dto.RegistradorDto;
import pe.gob.bnp.services.rest.renabi.registrador.domain.entity.Credenciales;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.AutenticacionTxDto;

@RestController
@RequestMapping("api/registrador/")
@Api(value = "/api/registrador/",description="Servicio REST para Registrardor - Desarrollado por OTIE/BNP")
@CrossOrigin(origins = "*")
public class RegistradorController {
	@Autowired
	RegistradorApplicationService registradorApplicationService;
	
	@Autowired
	EmailApplicationService  emailApplicationService;	
	
	@Autowired
	ResponseHandler responseHandler;
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Crear Registrador", response= ResponseTx.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> crearRegistrador(
			@ApiParam(value = "Estructura JSON del registrador", required = true)
			@RequestBody RegistradorDto registrador) throws Exception {
		try {
			ResponseTx response = registradorApplicationService.save(registrador);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE,  produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Autenticar Registrador", response= AutenticacionTxDto.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> autenticarRegistrador(
			@ApiParam(value = "Credenciales del registrador", required = true)
			@RequestBody CredencialRegistradorDto credencial) throws Exception {
		try {
			AutenticacionTxDto response = registradorApplicationService.autenticar(credencial);
			return this.responseHandler.getOkObjectResponse(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/declaraciones/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> buscarBibliotecasDeRegistrador(@PathVariable("id") long id){
		try {
			List<BibliotecaCabeceraDto> response = registradorApplicationService.obtenerBibliotecas(id);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/declaraciones/verificadas",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> buscarDeclaracionesDeRegistrador(
			@RequestParam(value = "nombre", defaultValue = "", required=false) String  nombre,
			@RequestParam(value = "nro_rnb", defaultValue = "", required=false) String  numeroRNB,
			@RequestParam(value = "cod_dep", defaultValue = "", required=false) String  codDepartamento,
			@RequestParam(value = "cod_pro", defaultValue = "", required=false) String  codProvincia,
			@RequestParam(value = "cod_dis", defaultValue = "", required=false) String  codDistrito,
			@RequestParam(value = "fec_inscr_desde", defaultValue = "", required=false) String  fechaInscripcionDesde,
			@RequestParam(value = "fec_inscr_hasta", defaultValue = "", required=false) String  fechaInscripcionHasta,
			@RequestParam(value = "ruc", defaultValue = "", required=false) String  ruc,
			@RequestParam(value = "direccion", defaultValue = "", required=false) String  direccion){
		try {
			List<BibliotecaCabeceraDto> response = registradorApplicationService.obtenerBibliotecaVerificadas(nombre, numeroRNB, codDepartamento, codProvincia, 
					codDistrito, fechaInscripcionDesde, fechaInscripcionHasta, ruc, direccion);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/email/creacioncuenta", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Envío de email para creacion de cuenta de un regitrador", response= String.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})	
	public ResponseEntity<Object> enviarEmailCreacionCuentaRegistrador(@RequestBody CredencialRegistrador credencial){
		try {
			String resultado = this.emailApplicationService.sendHTMLEmailCreacionCuentaRegistrador(credencial);
			return new ResponseEntity<Object>(resultado, HttpStatus.OK);
		}catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}		
	
	
	@RequestMapping(method = RequestMethod.POST, path = "/email/reenviocredencial", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Envío de email para creacion de cuenta de un regitrador", response= String.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Operación exitosa"),
        @ApiResponse(code = 500, message = "Error al momento de ejecutar la petición"),
	})	
	public ResponseEntity<Object> enviarEmailReenvioCredenciales(@RequestBody CredencialRegistrador credencial){
		try {
			String resultado = this.emailApplicationService.sendHTMLEmailReenvioCredencialesCuentaRegistrador(credencial);
			return new ResponseEntity<Object>(resultado, HttpStatus.OK);
		}catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	
	@RequestMapping(method = RequestMethod.GET, path="/recuperacioncuenta/{correo}",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> buscarBibliotecasDeRegistrador(@PathVariable("correo") String correo){
		try {
			Credenciales response = registradorApplicationService.obtenerCredencialRegistrador(correo);
			if (response == null || response.getCodigoRespuesta()==null || !response.getCodigoRespuesta().equals("0000")) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron credenciales para el correo ingresado.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	
		

}
