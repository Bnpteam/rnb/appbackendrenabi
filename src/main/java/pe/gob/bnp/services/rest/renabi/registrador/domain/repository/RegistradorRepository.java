package pe.gob.bnp.services.rest.renabi.registrador.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaCabeceraDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.FiltroBibliotecaVerificacionDto;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Credencial;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseRepository;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.Registrador;
import pe.gob.bnp.services.rest.renabi.registrador.domain.entity.Credenciales;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.AutenticacionTxDto;

@Repository
public interface RegistradorRepository  extends BaseRepository<Registrador>{
	public AutenticacionTxDto autenticarUsuario(Credencial credencial);
	public List<BibliotecaCabeceraDto> listarBibliotecas(Long id);
	public List<BibliotecaCabeceraDto> listarBibliotecasVerificadas(FiltroBibliotecaVerificacionDto filtro);
	public Credenciales obtenerCredencialRegistrador (String correo);
	
}
