package pe.gob.bnp.services.rest.renabi.parametros.infraestructure.jdbc.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.NombreBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.TipoBibliotecaHijoDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.TipoBibliotecaPadreDto;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseJDBCOperation;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaNombreBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaTipoBibliotecaHijoDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaTipoBibliotecaPadreDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.ListaUbigeoDto;
import pe.gob.bnp.services.rest.renabi.parametros.application.dto.UbigeoDto;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.ListaMaestra;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.Maestra;
import pe.gob.bnp.services.rest.renabi.parametros.domain.repository.ParametrosRepository;

@Repository
public class ParametrosJDBCRepository extends BaseJDBCOperation implements ParametrosRepository{

	@Override
	public ResponseTx persist(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Maestra read(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Maestra> list(Maestra entity) {
		List<Maestra> response = new ArrayList<>();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_LISTA_X_TIPO(?,?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_TIPO_TABLA", Util.getString(entity.getTipoTabla()));
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (codigoResultado.equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					Maestra maestra= new Maestra();
					maestra.setDescripcionCorta(Util.getString(rs.getString("DESCRIPCION_CORTA")));
					maestra.setDescripcionLarga(Util.getString(rs.getString("DESCRIPCION_LARGA")));
					maestra.setId(rs.getLong("MAESTRA_ID"));
					response.add(maestra);
				}					
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTx update(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx delete(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListaMaestra listByTableType(String tipoTabla) {
		ListaMaestra response = new ListaMaestra();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_LISTA_X_TIPO(?,?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_TIPO_TABLA", Util.getString(tipoTabla));
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoResultado(codigoResultado);
			if (codigoResultado.equals("0000")) {
				response.setMensajeResultado("Transacción exitosa");
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					Maestra maestra= new Maestra();
					maestra.setDescripcionCorta(Util.getString(rs.getString("DESCRIPCION_CORTA")));
					maestra.setDescripcionLarga(Util.getString(rs.getString("DESCRIPCION_LARGA")));
					maestra.setId(rs.getLong("MAESTRA_ID"));
					response.getLista().add(maestra);
				}					
			}else {
				response.setMensajeResultado("Error al momento de obtener la información de la base de datos.");
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ListaTipoBibliotecaPadreDto listarTiposBibliotecaPadre() {
		ListaTipoBibliotecaPadreDto response = new ListaTipoBibliotecaPadreDto();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_TIPOS_BIB(?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.getResponse().setCodigoRespuesta(codigoResultado);
			if (codigoResultado.equals("0000")) {
				response.getResponse().setRespuesta("Transacción exitosa");
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					TipoBibliotecaPadreDto obj= new TipoBibliotecaPadreDto();
					obj.setNombre(Util.getString(rs.getString("NOMBRE")));
					obj.setIdTipoBiblioteca(Util.getString(rs.getString("TIPO_BIBLIOTECA_ID")));
					obj.setEstado(Util.getString(rs.getString("ESTADO")));
					response.getLista().add(obj);
				}					
			}else {
				response.getResponse().setRespuesta("Error al momento de obtener la información de la base de datos.");
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ListaTipoBibliotecaHijoDto listarTiposBibliotecaHijo() {
		ListaTipoBibliotecaHijoDto response = new ListaTipoBibliotecaHijoDto();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_SUB_TIPOS_BIB(?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.getResponse().setCodigoRespuesta(codigoResultado);
			if (codigoResultado.equals("0000")) {
				response.getResponse().setRespuesta("Transacción exitosa");
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					TipoBibliotecaHijoDto obj= new TipoBibliotecaHijoDto();
					obj.setNombre(Util.getString(rs.getString("NOMBRE")));
					obj.setIdTipoBiblioteca(Util.getString(rs.getString("TIPO_BIBLIOTECA_ID")));
					obj.setEstado(Util.getString(rs.getString("ESTADO")));
					obj.setIdTipoBibliotecaPadre(Util.getString(rs.getString("TIPO_BIBLIOTECA_PADRE_ID")));
					response.getLista().add(obj);
				}					
			}else {
				response.getResponse().setRespuesta("Error al momento de obtener la información de la base de datos.");
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ListaUbigeoDto listarUbigeos() {
		ListaUbigeoDto response = new ListaUbigeoDto();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_UBIGEOS(?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.getResponse().setCodigoRespuesta(codigoResultado);
			if (codigoResultado.equals("0000")) {
				response.getResponse().setRespuesta("Transacción exitosa");
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					UbigeoDto obj= new UbigeoDto();
					obj.setNombre(Util.getString(rs.getString("DESCRIPCION")));
					obj.setCodDepartamento(Util.getString(rs.getString("CODDEP")));
					obj.setCodProvincia(Util.getString(rs.getString("CODPRO")));
					obj.setCodDistrito(Util.getString(rs.getString("CODDIS")));
					response.getLista().add(obj);
				}					
			}else {
				response.getResponse().setRespuesta("Error al momento de obtener la información de la base de datos.");
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public String getDescripcionLargaTablaMaestra(Long idMaestra) {
		String response = "";
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_DESCR_ITEM_MAESTRA(?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_MAESTRA_ID", idMaestra);
			cstm.registerOutParameter("S_DESC_LARGA", OracleTypes.VARCHAR);
			cstm.execute();
			
			response = (String) cstm.getObject("S_DESC_LARGA");
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public List<UbigeoDto> listarDepartamentos() {
		List<UbigeoDto> response = new ArrayList<>();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_UBIGEOS_DPTO(?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (codigoResultado.equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					UbigeoDto obj= new UbigeoDto();
					obj.setNombre(Util.getString(rs.getString("DESCRIPCION")));
					obj.setCodDepartamento(Util.getString(rs.getString("CODDEP")));
					obj.setCodProvincia(Util.getString(rs.getString("CODPRO")));
					obj.setCodDistrito(Util.getString(rs.getString("CODDIS")));
					response.add(obj);
				}					
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public List<UbigeoDto> listarProvincias(String coddep) {
		List<UbigeoDto> response = new ArrayList<>();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_UBIGEOS_PROV(?,?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_CODDEPARTAMENTO", Util.getString(coddep));
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (codigoResultado.equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					UbigeoDto obj= new UbigeoDto();
					obj.setNombre(Util.getString(rs.getString("DESCRIPCION")));
					obj.setCodDepartamento(Util.getString(rs.getString("CODDEP")));
					obj.setCodProvincia(Util.getString(rs.getString("CODPRO")));
					obj.setCodDistrito(Util.getString(rs.getString("CODDIS")));
					response.add(obj);
				}					
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public List<UbigeoDto> listarDistritos(String coddep, String codpro) {
		List<UbigeoDto> response = new ArrayList<>();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_UBIGEOS_DIS(?,?,?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_CODDEPARTAMENTO", Util.getString(coddep));
			cstm.setString("E_CODPROVINCIA", Util.getString(codpro));
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (codigoResultado.equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					UbigeoDto obj= new UbigeoDto();
					obj.setNombre(Util.getString(rs.getString("DESCRIPCION")));
					obj.setCodDepartamento(Util.getString(rs.getString("CODDEP")));
					obj.setCodProvincia(Util.getString(rs.getString("CODPRO")));
					obj.setCodDistrito(Util.getString(rs.getString("CODDIS")));
					response.add(obj);
				}					
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ListaNombreBibliotecaDto listarNombresBiblioteca() {
		ListaNombreBibliotecaDto response = new ListaNombreBibliotecaDto();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_MAESTRAS.SP_GET_NOMBRES_BIB(?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.getResponse().setCodigoRespuesta(codigoResultado);
			if (codigoResultado.equals("0000")) {
				response.getResponse().setRespuesta("Transacción exitosa");
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					NombreBibliotecaDto obj= new NombreBibliotecaDto();
					obj.setRowBiblioteca(Util.getString(rs.getString("ROWBIBLIOTECA")));
					obj.setIdBiblioteca(Util.getString(rs.getString("IDBIBLIOTECA")));
					obj.setNombre(Util.getString(rs.getString("NOMBREBIBLIOTECA")));
					response.getLista().add(obj);
				}					
			}else {
				response.getResponse().setRespuesta("Error al momento de obtener la información de la base de datos.");
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTx persistM(Maestra entity) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
