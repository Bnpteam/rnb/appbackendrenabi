package pe.gob.bnp.services.rest.renabi.usuario.domain.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.services.rest.renabi.common.application.dto.ResponseGenericDto;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Credencial;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseRepository;
import pe.gob.bnp.services.rest.renabi.parametros.domain.entity.Maestra;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.AutenticacionTxDto;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.ResponseLoginPortalDto;

@Repository
public interface UsuarioRepository  extends BaseRepository<Maestra> {
	public AutenticacionTxDto autenticarUsuario(Credencial credencial);
	public ResponseGenericDto<ResponseLoginPortalDto> autenticarUsuarioPortal(Credencial credencial);
}
