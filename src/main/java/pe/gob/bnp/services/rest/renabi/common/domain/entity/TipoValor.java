package pe.gob.bnp.services.rest.renabi.common.domain.entity;

import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.application.dto.NuevoTipoValorDto;

public class TipoValor {
		private String idPadre;
		private String id;
		private String nombre;
		private String valor;
		private String estado;
		
		public TipoValor() {
			super();
			this.idPadre = Constants.EMPTY_STRING;
			this.id = Constants.EMPTY_STRING;
			this.nombre = Constants.EMPTY_STRING;
			this.valor = Constants.EMPTY_STRING;
			this.estado = Constants.EMPTY_STRING;
		}
		
		public TipoValor(NuevoTipoValorDto dto) {
			this.id = dto.getId();
			this.nombre = dto.getNombre();
			this.valor = dto.getValor();
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getValor() {
			return valor;
		}
		public void setValor(String valor) {
			this.valor = valor;
		}

		public String getIdPadre() {
			return idPadre;
		}

		public void setIdPadre(String idPadre) {
			this.idPadre = idPadre;
		}

		public String getEstado() {
			return estado;
		}

		public void setEstado(String estado) {
			this.estado = estado;
		}

		
		
}
