package pe.gob.bnp.services.rest.renabi.biblioteca.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaCabeceraDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaMaster;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.FiltroBibliotecaVerificacionDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ListaEstadistica;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ObservacionesBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicio;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioMes;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurno;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurnoDia;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Biblioteca;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaId;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaMasterServicio;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaObservada;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.ImagenAnexa;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.TipoValor;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseRepository;
import pe.gob.bnp.services.rest.renabi.common.util.alfresco.ResultAlfresco;
import pe.gob.bnp.services.rest.renabi.common.util.openkm.ResultOpenKMDocumento;

@Repository
public interface BibliotecaRepository extends BaseRepository<Biblioteca>{
	public List<BibliotecaCabeceraDto> listarBibliotecasPorVerificador(FiltroBibliotecaVerificacionDto filtro);
	public ResponseTx guardarDetalleBiblioteca(String tabla, Long idPadre, Long idUsuarioRegistro, TipoValor tipoValor);
	public ResponseTx guardarAtencionServicio(Long idBiblioteca, AtencionServicio atencionServicio,Long idUsuarioRegistro);
	public ResponseTx guardarAtencionServicioTurno(Long idAtencionServicio, AtencionServicioTurno atencionServicioTurno,Long idUsuarioRegistro);
	public ResponseTx guardarAtencionServicioMes(Long idAtencionServicio, AtencionServicioMes atencionServicioMes,Long idUsuarioRegistro);
	public ResponseTx guardarAtencionServicioTurnoDia(Long idAtencionTurno, AtencionServicioTurnoDia atencionServicioTurnoDia,Long idUsuarioRegistro);
	public Biblioteca obtenerBiblioteca(Long id);
	public ResponseTx confirmarBiblioteca(BibliotecaId biblioteca);
	public ResponseTx corregirBiblioteca(BibliotecaId biblioteca);
	public ResponseTx observarBiblioteca(BibliotecaObservada biblioteca);
	public ResponseTx iniciarValidacionBiblioteca(BibliotecaId biblioteca);
	public ResponseTx eliminarBiblioteca(BibliotecaId biblioteca);
	//public BibliotecaDescarga obtenerBibliotecaDescarga(Long id);
	//public ResponseTx guardarArchivoAdjunto(ResultAlfresco resultAlfresco, String fileName, String fileExtension, Long idBiblioteca, Long idRegistrador);
	public ResponseTx guardarArchivoAdjunto(ResultOpenKMDocumento resultAlfresco, String fileName, String fileExtension, Long idBiblioteca, Long idRegistrador);
	public List<ImagenAnexa> obtenerImagenesAnexas(Long idBiblioteca);
	public ResponseTx cambiarEstadoImagenes(Long idBiblioteca, String idEstado);
	public List<BibliotecaMaster> listarMasterBibliotecas(FiltroBibliotecaVerificacionDto filtro);
	public BibliotecaMasterServicio obtenerResumenBibliotecaMaster(Long idMaster);
	
	public ListaEstadistica obtenerEstadisticas(String anio, String tipo);
	public ResponseTx subsanarBiblioteca(Biblioteca entity);
	public ObservacionesBibliotecaDto obtenerObservacionesBiblioteca(Long idDeclaracion);
}
	