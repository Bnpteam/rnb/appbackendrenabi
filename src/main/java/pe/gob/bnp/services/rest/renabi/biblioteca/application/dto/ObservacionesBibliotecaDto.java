package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.ObservacionBiblioteca;

public class ObservacionesBibliotecaDto {
	private String codigoRespuesta;
	private List<ObservacionBiblioteca> observaciones;

	public ObservacionesBibliotecaDto() {
		super();
		this.codigoRespuesta = "";
		this.observaciones = new ArrayList<>();
	}
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}
	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}
	public List<ObservacionBiblioteca> getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(List<ObservacionBiblioteca> observaciones) {
		this.observaciones = observaciones;
	}
	
	
	
}
// 