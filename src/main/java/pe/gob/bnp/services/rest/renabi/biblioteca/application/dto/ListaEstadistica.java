package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Estadistica;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;

@ApiModel(value="Lista de items estadísticos", description="Lista de items estadísticos")
public class ListaEstadistica {
	@ApiModelProperty(notes = "Resultado de la solicitud")
	private ResponseTx response;
	
	@ApiModelProperty(notes = "Lista de registros obtenidos", required=true)
	private List<Estadistica> lista;
	
	public ListaEstadistica() {
		super();
		this.response = new ResponseTx();
		this.lista = new ArrayList<>();
	}
	
	public ResponseTx getResponse() {
		return response;
	}
	public void setResponse(ResponseTx response) {
		this.response = response;
	}

	public List<Estadistica> getLista() {
		return lista;
	}

	public void setLista(List<Estadistica> lista) {
		this.lista = lista;
	}


}
