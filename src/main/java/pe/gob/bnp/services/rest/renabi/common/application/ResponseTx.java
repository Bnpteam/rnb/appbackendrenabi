package pe.gob.bnp.services.rest.renabi.common.application;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.common.Constants;

@ApiModel(value="Response", description="Respuesta de la transacción")
public class ResponseTx {
	@ApiModelProperty(notes = "id de la persistencia realizada",required=false,value="100")
	private String id;
	
	@ApiModelProperty(notes = "código de respuesta",required=true,value="0000")
	private String codigoRespuesta;
	
	@ApiModelProperty(notes = "descripción de la respuesta",required=true,value="Transacción realizada de forma exitosa.")
	private String respuesta;
	
	public ResponseTx() {
		super();
		this.id = Constants.EMPTY_STRING;
		this.codigoRespuesta = Constants.EMPTY_STRING;
		this.respuesta = Constants.EMPTY_STRING;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	
	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	
}
