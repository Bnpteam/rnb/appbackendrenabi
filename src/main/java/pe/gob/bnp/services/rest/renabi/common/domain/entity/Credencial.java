package pe.gob.bnp.services.rest.renabi.common.domain.entity;

public class Credencial {
	private String usuario;
	private String password;

	
	public Credencial() {
		super();
		this.usuario = "";
		this.password = "";
	}
	
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	
}
