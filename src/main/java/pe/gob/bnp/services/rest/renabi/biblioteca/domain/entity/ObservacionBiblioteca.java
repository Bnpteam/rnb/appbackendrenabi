package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

public class ObservacionBiblioteca {
	private String idDeclaracion;
	private String fechaObservacion;
	private String estadoPrevioBiblioteca;
	private String estadoActualBiblioteca;
	private String observacion;
	private String estadoObservacion;
	
	public String getIdDeclaracion() {
		return idDeclaracion;
	}
	public void setIdDeclaracion(String idDeclaracion) {
		this.idDeclaracion = idDeclaracion;
	}
	public String getFechaObservacion() {
		return fechaObservacion;
	}
	public void setFechaObservacion(String fechaObservacion) {
		this.fechaObservacion = fechaObservacion;
	}
	public String getEstadoPrevioBiblioteca() {
		return estadoPrevioBiblioteca;
	}
	public void setEstadoPrevioBiblioteca(String estadoPrevioBiblioteca) {
		this.estadoPrevioBiblioteca = estadoPrevioBiblioteca;
	}
	public String getEstadoActualBiblioteca() {
		return estadoActualBiblioteca;
	}
	public void setEstadoActualBiblioteca(String estadoActualBiblioteca) {
		this.estadoActualBiblioteca = estadoActualBiblioteca;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	public String getEstadoObservacion() {
		return estadoObservacion;
	}
	public void setEstadoObservacion(String estadoObservacion) {
		this.estadoObservacion = estadoObservacion;
	}
	

	
}
