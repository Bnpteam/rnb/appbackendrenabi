package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class VerRptInfraestructuraDto {
	
	private String idTipoLocal;
	private String tipoLocal;
	private String areaM2;
	private String numeroAmbientes;
	private String nombreaccesibilidad;
	private String valoraccesibilidad;		


	public VerRptInfraestructuraDto() {
		super();
		this.idTipoLocal = Constants.EMPTY_STRING;
		this.areaM2 = Constants.EMPTY_STRING;
		this.numeroAmbientes = Constants.EMPTY_STRING;		
		this.tipoLocal = Constants.EMPTY_STRING;
		this.nombreaccesibilidad =  Constants.EMPTY_STRING;
		this.valoraccesibilidad =  Constants.EMPTY_STRING;
	}

	
	public String getIdTipoLocal() {
		return idTipoLocal;
	}

	public void setIdTipoLocal(String idTipoLocal) {
		this.idTipoLocal = idTipoLocal;
	}

	public String getTipoLocal() {
		return tipoLocal;
	}

	public void setTipoLocal(String tipoLocal) {
		this.tipoLocal = tipoLocal;
	}

	public String getAreaM2() {
		return areaM2;
	}

	public void setAreaM2(String areaM2) {
		this.areaM2 = areaM2;
	}

	public String getNumeroAmbientes() {
		return numeroAmbientes;
	}

	public void setNumeroAmbientes(String numeroAmbientes) {
		this.numeroAmbientes = numeroAmbientes;
	}

	public String getNombreaccesibilidad() {
		return nombreaccesibilidad;
	}

	public void setNombreaccesibilidad(String nombreaccesibilidad) {
		this.nombreaccesibilidad = nombreaccesibilidad;
	}

	public String getValoraccesibilidad() {
		return valoraccesibilidad;
	}

	public void setValoraccesibilidad(String valoraccesibilidad) {
		this.valoraccesibilidad = valoraccesibilidad;
	}
}
