package pe.gob.bnp.services.rest.renabi.biblioteca.infraestructure.jdbc.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaCabeceraDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaMaster;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.FiltroBibliotecaVerificacionDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ListaEstadistica;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.ObservacionesBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicio;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioMes;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurno;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurnoDia;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Biblioteca;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaId;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaMasterServicio;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.BibliotecaObservada;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Estadistica;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.ImagenAnexa;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.ObservacionBiblioteca;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.repository.BibliotecaRepository;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.TipoValor;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseJDBCOperation;
import pe.gob.bnp.services.rest.renabi.common.util.alfresco.ResultAlfresco;
import pe.gob.bnp.services.rest.renabi.common.util.openkm.ResultOpenKMDocumento;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.Responsable;

@Repository
public class BibliotecaJDBCRepository extends BaseJDBCOperation implements BibliotecaRepository {

	@Override
	public ResponseTx persist(Biblioteca entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_CREAR_BIBLIOTECA("
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", entity.getId());
			cstm.setString("E_ANIO_DECLARACION", Util.getStringFormatted(entity.getAnioDeclaracion()));
			cstm.setLong("E_REGISTRADOR_ID", entity.getRegistrador().getId());
			cstm.setString("E_NOMBRE", Util.getStringFormatted(entity.getNombre()));
			cstm.setLong("E_TIPO_BIBLIOTECA_ID", entity.getTipoBiblioteca().getIdTipoBibliotecaPadre());
			cstm.setLong("E_SUB_TIPO_BIBLIOTECA_ID", entity.getTipoBiblioteca().getIdTipoBiblioteca());
			cstm.setLong("E_TIPO_FUNCIONAMIENTO_ID", Util.getStringToLong(entity.getTipoFuncionamiento().getId()));
			cstm.setLong("E_TIPO_AMBITO_ID", Util.getStringToLong(entity.getAmbitoTerritorial().getId()));
			cstm.setString("E_CODDEPARTAMENTO", Util.getString(entity.getUbigeo().getCodDepartamento()));
			cstm.setString("E_CODPROVINCIA", Util.getString(entity.getUbigeo().getCodProvincia()));
			cstm.setString("E_CODDISTRITO", Util.getString(entity.getUbigeo().getCodDistrito()));
			cstm.setString("E_CENTROPOBLADO", Util.getStringFormatted(entity.getUbigeo().getCentroPoblado()));
			cstm.setString("E_DIRECCION", Util.getStringFormatted(entity.getUbigeo().getDireccion()));
			//cstm.setString("E_CORREO", Util.getStringFormatted(entity.getCorreoElectronico()));
			cstm.setString("E_CORREO", Util.cleanValue(entity.getCorreoElectronico()));
			cstm.setString("E_TELEFONO_FIJO", Util.getString(entity.getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL", Util.getString(entity.getTelefonoMovil()));
			cstm.setInt("E_TOTAL_BIBLIOTECOLOGOS", entity.getNumeroBibliotecologos());
			cstm.setInt("E_TOTAL_PERSONAL", entity.getNumeroTotalPersonal());
			cstm.setInt("E_TIPO_ESTADO_BIBLIOTECA_ID", Util.getStringToInt(entity.getEstadoBiblioteca().getId()));
			cstm.setString("E_IP_REGISTRO", Util.getString(entity.getIpRegistro()));
			cstm.setString("E_USU_TOTAL_ANIO_ANTERIOR", Util.getString(entity.getLectorTotalAnioAnterior()));
			cstm.setString("E_USU_TOTAL", Util.getString(entity.getLectorTotal()));
			cstm.setString("E_USU_TOTAL_CONSULTAS", Util.getString(entity.getLectorTotalConsultas()));
			cstm.setString("E_GESBIB_PLAN_TRABAJO_ANUAL", Util.getString(entity.getFlagGestBibPlanTrabActual()));
			cstm.setString("E_GESBIB_REGLAMENTOS", Util.getString(entity.getFlagGestBibReglamentos()));
			cstm.setString("E_GESBIB_ESTADISTICAS", Util.getString(entity.getFlagGestBibEstadisticas()));
			cstm.setString("E_GESBIB_ESPECIFICAR", Util.getStringFormatted(entity.getGestionBibEspecificar()));
			//entidad
			cstm.setString("E_RAZON_SOCIAL_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getRazonSocial()));
			cstm.setString("E_RUC_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getRuc()));
			//cstm.setString("E_CORREO_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getCorreo()));
			cstm.setString("E_CORREO_ENTIDAD", Util.cleanValue(entity.getEntidad().getCorreo()));
			cstm.setString("E_DIRECCION_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getDireccion()));
			cstm.setString("E_TELEFONO_FIJO_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getTelefonoMovil()));
			cstm.setLong("E_TIPO_GESTION_ID_ENTIDAD", Util.getStringToLong(entity.getEntidad().getTipoGestion().getId()));
			cstm.setString("E_FLAG_ORIGEN_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getFlagOrigen()));
			cstm.setString("E_ESTADO_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getIdEstado()));
			
			//responsable
			cstm.setString("E_NOMBRE_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getNombres()));
			cstm.setString("E_APE_PATERNO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getApellidoPaterno()));
			cstm.setString("E_APE_MATERNO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getApellidoMaterno()));
			cstm.setString("E_TIPO_DOCUMENTO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getTipoDocumentoIdentidad().getNombre()));
			cstm.setString("E_NUMERO_DOCUMENTO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getNumeroDocumentoIdentidad()));
			cstm.setString("E_GENERO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getGenero()));
			//System.out.println("fecha nacimiento format:"+  Util.getSQLDate(entity.getResponsable().getPersona().getFechaNacimiento()));
			cstm.setDate("E_FEC_NAC_RESPONSABLE", Util.getSQLDate(entity.getResponsable().getPersona().getFechaNacimiento()));
			cstm.setString("E_ESTADO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getIdEstado()));
			cstm.setLong("E_TIPO_GRADO_INSTRUCCION_ID", Util.getStringToLong(entity.getResponsable().getGradoInstruccion().getId()));
			cstm.setString("E_PROFESION_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getProfesion()));
			cstm.setLong("E_TIPO_CONDICION_LABORAL_ID", Util.getStringToLong(entity.getResponsable().getCondicionLaboral().getId()));
			cstm.setString("E_DIRECCION_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getDireccion()));
			cstm.setString("E_TELEFONO_FIJO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getTelefonoMovil()));
			//cstm.setString("E_CORREO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getCorreo()));
			cstm.setString("E_CORREO_RESPONSABLE", Util.cleanValue(entity.getResponsable().getCorreo()));
			cstm.setLong("E_TIPO_LOCAL_ID", Util.getStringToLong(entity.getInfraestructura().getTipoLocal().getId()));
			cstm.setString("E_AREA_M2", Util.getString(entity.getInfraestructura().getAreaM2()));
			cstm.setString("E_NUMERO_AMBIENTES", Util.getString(entity.getInfraestructura().getNumeroAmbientes()));
			cstm.setString("E_FLAG_ACEPTO_TC", Util.getStringFormatted(entity.getFlagAceptoTyC()));
			cstm.setString("E_FLAG_ACEPTO_DDJJ", Util.getStringFormatted(entity.getFlagAceptoDDJJ()));
			
			cstm.setLong("E_USUARIO_ID_REGISTRO", Util.getStringToLong(entity.getIdUsuarioRegistro()));
			cstm.registerOutParameter("S_BIBLIOTECA_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long id=  Util.getLongFromObject(cstm.getObject("S_BIBLIOTECA_ID"));
				response.setId(String.valueOf(id));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
//				if (response.getCodigoRespuesta().equals("9999")) {
//					response.setRespuesta("Error: No se ejecutó ninguna transacción.");
//				}
//				if (response.getCodigoRespuesta().equals("0001")) {
//					response.setRespuesta("Error: No se pudo registrar la entidad.");
//				}
//				if (response.getCodigoRespuesta().equals("0002")) {
//					response.setRespuesta("Error: No se pudo registrar el responsable.");
//				}	
//				if (response.getCodigoRespuesta().equals("0003")) {
//					response.setRespuesta("Error: Nombre de la biblioteca ya se encuentra registrado.");
//				}					
//				if (response.getCodigoRespuesta().equals("0004")) {
//					response.setRespuesta("Error: Ambito territorial no encontrado.");
//				}
//				if (response.getCodigoRespuesta().equals("0005")) {
//					response.setRespuesta("Error: Tipo de funcionamiento no encontrado.");
//				}					
//				if (response.getCodigoRespuesta().equals("0006")) {
//					response.setRespuesta("Error: Tipo de estado de biblioteca no encontrado.");
//				}					
//				if (response.getCodigoRespuesta().equals("0007")) {
//					response.setRespuesta("Error: Condición laboral no encontrada.");
//				}					
//				if (response.getCodigoRespuesta().equals("0008")) {
//					response.setRespuesta("Error: Grado de instrucción no encontrada.");
//				}					
//				if (response.getCodigoRespuesta().equals("0009")) {
//					response.setRespuesta("Error: Tipo de gestión no encontrada.");
//				}					
//				if (response.getCodigoRespuesta().equals("0010")) {
//					response.setRespuesta("Error: Departamento no encontrado.");
//				}					
//				if (response.getCodigoRespuesta().equals("0011")) {
//					response.setRespuesta("Error: Provincia no encontrada.");
//				}					
//				if (response.getCodigoRespuesta().equals("0012")) {
//					response.setRespuesta("Error: Distrito no encontrado.");
//				}					
//				if (response.getCodigoRespuesta().equals("0013")) {
//					response.setRespuesta("Error: Sub tipo de biblioteca no encontrado.");
//				}					
//				if (response.getCodigoRespuesta().equals("0014")) {
//					response.setRespuesta("Error: Tipo de biblioteca no encontrado.");
//				}	
//				if (response.getCodigoRespuesta().equals("0015")) {
//					response.setRespuesta("Error: Tipo de Local (Infraestructura) no encontrado.");
//				}					
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;		
	}
	
	@Override
	public ResponseTx persistM(Biblioteca entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_CREAR_BIBLIOTECA_MANUAL("
        		+ "?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", entity.getId());
			cstm.setString("E_ANIO_DECLARACION", Util.getStringFormatted(entity.getAnioDeclaracion()));
			cstm.setLong("E_REGISTRADOR_ID", entity.getRegistrador().getId());
			cstm.setString("E_NOMBRE", Util.getStringFormatted(entity.getNombre()));
			cstm.setLong("E_TIPO_BIBLIOTECA_ID", entity.getTipoBiblioteca().getIdTipoBibliotecaPadre());
			cstm.setLong("E_SUB_TIPO_BIBLIOTECA_ID", entity.getTipoBiblioteca().getIdTipoBiblioteca());
			cstm.setLong("E_TIPO_FUNCIONAMIENTO_ID", Util.getStringToLong(entity.getTipoFuncionamiento().getId()));
			cstm.setLong("E_TIPO_AMBITO_ID", Util.getStringToLong(entity.getAmbitoTerritorial().getId()));
			cstm.setString("E_CODDEPARTAMENTO", Util.getString(entity.getUbigeo().getCodDepartamento()));
			cstm.setString("E_CODPROVINCIA", Util.getString(entity.getUbigeo().getCodProvincia()));
			cstm.setString("E_CODDISTRITO", Util.getString(entity.getUbigeo().getCodDistrito()));
			cstm.setString("E_CENTROPOBLADO", Util.getStringFormatted(entity.getUbigeo().getCentroPoblado()));
			cstm.setString("E_DIRECCION", Util.getStringFormatted(entity.getUbigeo().getDireccion()));
			//cstm.setString("E_CORREO", Util.getStringFormatted(entity.getCorreoElectronico()));
			cstm.setString("E_CORREO", Util.cleanValue(entity.getCorreoElectronico()));
			cstm.setString("E_TELEFONO_FIJO", Util.getString(entity.getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL", Util.getString(entity.getTelefonoMovil()));
			cstm.setInt("E_TOTAL_BIBLIOTECOLOGOS", entity.getNumeroBibliotecologos());
			cstm.setInt("E_TOTAL_PERSONAL", entity.getNumeroTotalPersonal());
			cstm.setInt("E_TIPO_ESTADO_BIBLIOTECA_ID", Util.getStringToInt(entity.getEstadoBiblioteca().getId()));
			cstm.setString("E_IP_REGISTRO", Util.getString(entity.getIpRegistro()));
			cstm.setString("E_USU_TOTAL_ANIO_ANTERIOR", Util.getString(entity.getLectorTotalAnioAnterior()));
			cstm.setString("E_USU_TOTAL", Util.getString(entity.getLectorTotal()));
			cstm.setString("E_USU_TOTAL_CONSULTAS", Util.getString(entity.getLectorTotalConsultas()));
			cstm.setString("E_GESBIB_PLAN_TRABAJO_ANUAL", Util.getString(entity.getFlagGestBibPlanTrabActual()));
			cstm.setString("E_GESBIB_REGLAMENTOS", Util.getString(entity.getFlagGestBibReglamentos()));
			cstm.setString("E_GESBIB_ESTADISTICAS", Util.getString(entity.getFlagGestBibEstadisticas()));
			cstm.setString("E_GESBIB_ESPECIFICAR", Util.getStringFormatted(entity.getGestionBibEspecificar()));
			//entidad
			cstm.setString("E_RAZON_SOCIAL_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getRazonSocial()));
			//cstm.setString("E_RUC_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getRuc()));
			//cstm.setString("E_CORREO_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getCorreo()));
			cstm.setString("E_CORREO_ENTIDAD", Util.cleanValue(entity.getEntidad().getCorreo()));
			cstm.setString("E_DIRECCION_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getDireccion()));
			cstm.setString("E_TELEFONO_FIJO_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getTelefonoMovil()));
			cstm.setLong("E_TIPO_GESTION_ID_ENTIDAD", Util.getStringToLong(entity.getEntidad().getTipoGestion().getId()));
			cstm.setString("E_FLAG_ORIGEN_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getFlagOrigen()));
			cstm.setString("E_ESTADO_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getIdEstado()));
			
			//responsable
			cstm.setString("E_NOMBRE_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getNombres()));
			cstm.setString("E_APE_PATERNO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getApellidoPaterno()));
			cstm.setString("E_APE_MATERNO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getApellidoMaterno()));
			cstm.setString("E_TIPO_DOCUMENTO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getTipoDocumentoIdentidad().getNombre()));
			cstm.setString("E_NUMERO_DOCUMENTO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getNumeroDocumentoIdentidad()));
			cstm.setString("E_GENERO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getGenero()));
			//System.out.println("fecha nacimiento format:"+  Util.getSQLDate(entity.getResponsable().getPersona().getFechaNacimiento()));
			cstm.setDate("E_FEC_NAC_RESPONSABLE", Util.getSQLDate(entity.getResponsable().getPersona().getFechaNacimiento()));
			cstm.setString("E_ESTADO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getIdEstado()));
			cstm.setLong("E_TIPO_GRADO_INSTRUCCION_ID", Util.getStringToLong(entity.getResponsable().getGradoInstruccion().getId()));
			cstm.setString("E_PROFESION_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getProfesion()));
			cstm.setLong("E_TIPO_CONDICION_LABORAL_ID", Util.getStringToLong(entity.getResponsable().getCondicionLaboral().getId()));
			cstm.setString("E_DIRECCION_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getDireccion()));
			cstm.setString("E_TELEFONO_FIJO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getTelefonoMovil()));
			//cstm.setString("E_CORREO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getCorreo()));
			cstm.setString("E_CORREO_RESPONSABLE", Util.cleanValue(entity.getResponsable().getCorreo()));
			cstm.setLong("E_TIPO_LOCAL_ID", Util.getStringToLong(entity.getInfraestructura().getTipoLocal().getId()));
			cstm.setString("E_AREA_M2", Util.getString(entity.getInfraestructura().getAreaM2()));
			cstm.setString("E_NUMERO_AMBIENTES", Util.getString(entity.getInfraestructura().getNumeroAmbientes()));
			cstm.setString("E_FLAG_ACEPTO_TC", Util.getStringFormatted(entity.getFlagAceptoTyC()));
			cstm.setString("E_FLAG_ACEPTO_DDJJ", Util.getStringFormatted(entity.getFlagAceptoDDJJ()));
			
			cstm.setLong("E_USUARIO_ID_REGISTRO", Util.getStringToLong(entity.getIdUsuarioRegistro()));
			cstm.registerOutParameter("S_BIBLIOTECA_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long id=  Util.getLongFromObject(cstm.getObject("S_BIBLIOTECA_ID"));
				response.setId(String.valueOf(id));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;		
	}


	@Override
	public Biblioteca read(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Biblioteca> list(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx update(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx delete(Biblioteca entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BibliotecaCabeceraDto> listarBibliotecasPorVerificador(FiltroBibliotecaVerificacionDto entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_LIST_BIB_VERIFICADOR("
        		+ "?,?,?,?,?,?,?,?,?,?,?,?,?) }";
        List<BibliotecaCabeceraDto> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_CODDEPARTAMENTO", Util.getString(entity.getCodDepartamento()));
			cstm.setString("E_CODPROVINCIA", Util.getString(entity.getCodProvincia()));
			cstm.setString("E_CODDISTRITO", Util.getString(entity.getCodDistrito()));
			cstm.setString("E_NOMBRE", Util.getStringFormatted(entity.getNombre()));
			cstm.setString("E_RAZON_SOCIAL_ENTIDAD", Util.getStringFormatted(entity.getRazonSocialEntidad()));
			cstm.setString("E_NUMERO_RNB", Util.getStringFormatted(entity.getNumeroRNB()));
			cstm.setDate("E_FECHA_INSCR_DESDE", Util.stringToSQLDate(Util.getString(entity.getFechaInscripcionDesde())));
			cstm.setDate("E_FECHA_INSCR_HASTA", Util.stringToSQLDate(Util.getString(entity.getFechaInscripcionHasta())));
			cstm.setString("E_DIRECCION", Util.getStringFormatted(entity.getDireccion()));
			cstm.setString("E_RUC", Util.getString(entity.getRuc()));
			cstm.setString("E_ID_ESTADO", Util.getString(entity.getIdEstado()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					BibliotecaCabeceraDto biblioteca = new BibliotecaCabeceraDto();
					biblioteca.setId(Util.getString(rs.getString("BIBLIOTECA_ID")));
					biblioteca.setFechaInscripcion(Util.sqlDateToString(rs.getDate("FECHA_INSCRIPCION")));
					biblioteca.setNombre(Util.getString(rs.getString("NOMBRE_BIBLIOTECA")));
					biblioteca.setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));
					biblioteca.setProvincia(Util.getString(rs.getString("PROVINCIA")));
					biblioteca.setDistrito(Util.getString(rs.getString("DISTRITO")));
					biblioteca.setNumeroRNB(Util.getString(rs.getString("NUMERO_RNB")));
					biblioteca.setAnioDeclaracion(Util.getString(rs.getString("ANIO_DECLARACION")));
					biblioteca.setEstado(Util.getString(rs.getString("TIPO_ESTADO_BIBLIOTECA")));
					biblioteca.setRazonSocialEntidad(Util.getString(rs.getString("RAZON_SOCIAL_ENTIDAD")));
					biblioteca.setRucEntidad(Util.getString(rs.getString("RUC_ENTIDAD")));
					biblioteca.setNombreResponsable(Util.getString(rs.getString("NOMBRE_RESPONSABLE")));
					biblioteca.setDocIdentidadResponsable(Util.getString(rs.getString("DOC_IDENT_RESPONSABLE")));
					biblioteca.setNombreRegistrador(Util.getString(rs.getString("NOMBRE_REGISTRADOR")));
					biblioteca.setDocIdentidadRegistrador(Util.getString(rs.getString("DOC_IDENT_REGISTRADOR")));
					biblioteca.setDireccion(Util.getString(rs.getString("DIRECCION")));
					biblioteca.setCentroPoblado(Util.getString(rs.getString("CENTRO_POBLADO")));
					biblioteca.setTelefonoFijoResponsable(Util.getString(rs.getString("TELEFONO_FIJO")));
					biblioteca.setTelefonoMovilResponsable(Util.getString(rs.getString("TELEFONO_MOVIL")));
					biblioteca.setCorreoResponsable(Util.getString(rs.getString("CORREO")));
					response.add(biblioteca);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTx guardarDetalleBiblioteca(String tabla, Long idPadre, Long idUsuarioRegistro,  TipoValor entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_INS_TIPO_VALOR("
        		+ "?,?,?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_TABLA", Util.getStringFormatted(tabla));
			cstm.setLong("E_ID_PADRE", idPadre);
			cstm.setLong("E_ID", Util.getStringToLong(entity.getId()));
			cstm.setString("E_NOMBRE", Util.getStringFormatted(entity.getNombre()));
			cstm.setString("E_VALOR", Util.getString(entity.getValor()));
			cstm.setLong("E_USUARIO_ID_REGISTRO", idUsuarioRegistro);
			cstm.registerOutParameter("S_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long id=  Util.getLongFromObject(cstm.getObject("S_ID"));
				response.setId(String.valueOf(id));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo insertar el registro de la tabla "+tabla);
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTx guardarAtencionServicio(Long idBiblioteca, AtencionServicio entity,Long idUsuarioRegistro) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_INS_ATEN_SERV("
        		+ "?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", idBiblioteca);
			cstm.setLong("E_TIPO_ACCESO_PUBLICO_ID", Util.getStringToLong(entity.getTipoAccesoPublico().getId()));
			cstm.setString("E_TIPO_ACCESO_PUBLICO", Util.getString(entity.getTipoAccesoPublico().getNombre()));
			cstm.setLong("E_USUARIO_ID_REGISTRO", idUsuarioRegistro);
			cstm.registerOutParameter("S_ATENCION_SERVICIO_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long id=  Util.getLongFromObject(cstm.getObject("S_ATENCION_SERVICIO_ID"));
				response.setId(String.valueOf(id));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo insertar el registro de la tabla atencion servicio");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTx guardarAtencionServicioMes(Long idAtencionServicio, AtencionServicioMes entity,Long idUsuarioRegistro) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_INS_ATEN_MES("
        		+ "?,?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_ATENCION_SERVICIO_ID", idAtencionServicio);
			cstm.setLong("E_TIPO_MES_ID", Util.getStringToLong(entity.getMes().getId()));
			cstm.setString("E_TIPO_MES", Util.getStringFormatted(entity.getMes().getNombre()));
			cstm.setString("E_VALOR", Util.getString(entity.getValor()));
			cstm.setLong("E_USUARIO_ID_REGISTRO", idUsuarioRegistro);
			cstm.registerOutParameter("S_ATENCION_MES_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long id=  Util.getLongFromObject(cstm.getObject("S_ATENCION_MES_ID"));
				response.setId(String.valueOf(id));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo insertar el registro de la tabla atencion mes");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTx guardarAtencionServicioTurno(Long idAtencionServicio,
			AtencionServicioTurno entity,Long idUsuarioRegistro) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_INS_ATEN_TURN("
        		+ "?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_ATENCION_SERVICIO_ID", idAtencionServicio);
			cstm.setString("E_DIA", Util.getStringFormatted(entity.getDia()));
			cstm.setString("E_VALOR", Util.getStringFormatted(entity.getValor()));
			cstm.setLong("E_USUARIO_ID_REGISTRO", idUsuarioRegistro);
			cstm.registerOutParameter("S_ATENCION_TURNO_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long id=  Util.getLongFromObject(cstm.getObject("S_ATENCION_TURNO_ID"));
				response.setId(String.valueOf(id));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo insertar el registro de la tabla atencion turno");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTx guardarAtencionServicioTurnoDia(Long idAtencionTurno,
			AtencionServicioTurnoDia entity,Long idUsuarioRegistro) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_INS_ATEN_TURN_DIA("
        		+ "?,?,?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_ATENCION_TURNO_ID", idAtencionTurno);
			cstm.setLong("E_TIPO_TURNO_ID", Util.getStringToLong(entity.getTipoTurno().getId()));
			cstm.setString("E_TIPO_TURNO", Util.getStringFormatted(entity.getTipoTurno().getNombre()));
			cstm.setString("E_HORA_INICIO", Util.getStringFormatted(entity.getHoraInicio()));
			cstm.setString("E_HORA_FIN", Util.getStringFormatted(entity.getHoraFin()));
			cstm.setLong("E_USUARIO_ID_REGISTRO", idUsuarioRegistro);
			cstm.registerOutParameter("S_TURNO_DIA_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long id=  Util.getLongFromObject(cstm.getObject("S_TURNO_DIA_ID"));
				response.setId(String.valueOf(id));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo insertar el registro de la tabla atencion turno dia");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public Biblioteca obtenerBiblioteca(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_GET_BIBLIOTECA("
        		+ "?,?,?,?,?,?,?,?,?,?,?,?,?) }";
        Biblioteca biblioteca = new Biblioteca();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ENTIDAD",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_RESPONSABLE",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_INFRAESTRUCTURA",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ACCESIBILIDAD",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_COLECCIONES",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_SERVICIOS",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ATENSERV",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ATENSERVMES",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ATENSERVTURNO",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ATENSERVTURNODIA",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					biblioteca.setId(rs.getLong("BIBLIOTECA_ID"));
					biblioteca.setFechaInscripcion(rs.getDate("FECHA_INSCRIPCION"));
					biblioteca.setNumeroRNB(Util.getString(rs.getString("NUMERO_RNB")));
					biblioteca.setAnioDeclaracion(Util.getString(rs.getString("ANIO_DECLARACION")));
					biblioteca.getRegistrador().setId(rs.getLong("REGISTRADOR_ID"));
					biblioteca.getRegistrador().getPersona().setNombreCompleto(Util.getString(rs.getString("REGISTRADOR_NOMBRE")));
					biblioteca.getRegistrador().getPersona().setDocumentoIdentidad(Util.getString(rs.getString("REGISTRADOR_DOCIDEN")));
					biblioteca.setNombre(Util.getString(rs.getString("NOMBRE")));
					biblioteca.getTipoBiblioteca().setIdTipoBibliotecaPadre(rs.getInt("TIPO_BIBLIOTECA_ID"));
					biblioteca.getTipoBiblioteca().setNombrePadre(rs.getString("TIPO_BIBLIOTECA"));
					biblioteca.getTipoBiblioteca().setIdTipoBiblioteca(rs.getInt("SUB_TIPO_BIBLIOTECA_ID"));
					biblioteca.getTipoBiblioteca().setNombre(rs.getString("SUB_TIPO_BIBLIOTECA"));
					biblioteca.getAmbitoTerritorial().setId(Util.getLongToString(rs.getLong("TIPO_AMBITO_ID")));
					biblioteca.getAmbitoTerritorial().setNombre(Util.getString(rs.getString("TIPO_AMBITO")));
					biblioteca.getUbigeo().setCentroPoblado(Util.getString(rs.getString("CENTRO_POBLADO")));
					biblioteca.getUbigeo().setCodDepartamento(Util.getString(rs.getString("CODDEPARTAMENTO")));
					biblioteca.getUbigeo().setCodDistrito(Util.getString(rs.getString("CODDISTRITO")));
					biblioteca.getUbigeo().setCodProvincia(Util.getString(rs.getString("CODPROVINCIA")));
					biblioteca.getUbigeo().setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));
					biblioteca.getUbigeo().setDireccion(Util.getString(rs.getString("DIRECCION")));
					biblioteca.getUbigeo().setDistrito(Util.getString(rs.getString("DISTRITO")));
					biblioteca.getUbigeo().setProvincia(Util.getString(rs.getString("PROVINCIA")));
					biblioteca.getTipoFuncionamiento().setId(Util.getString(rs.getString("TIPO_FUNCIONAMIENTO_ID")));
					biblioteca.getTipoFuncionamiento().setNombre(Util.getString(rs.getString("TIPO_FUNCIONAMIENTO")));
					biblioteca.setCorreoElectronico(Util.getString(rs.getString("CORREO")));
					biblioteca.setTelefonoFijo(Util.getString(rs.getString("TELEFONO_FIJO")));
					biblioteca.setTelefonoMovil(Util.getString(rs.getString("TELEFONO_MOVIL")));
					biblioteca.setNumeroBibliotecologos(rs.getInt("TOTAL_BIBLIOTECOLOGOS"));
					biblioteca.setNumeroTotalPersonal(rs.getInt("TOTAL_PERSONAL"));
					biblioteca.getEstadoBiblioteca().setId(Util.getString(rs.getString("TIPO_ESTADO_BIBLIOTECA_ID")));
					biblioteca.getEstadoBiblioteca().setNombre(Util.getString(rs.getString("TIPO_ESTADO_BIBLIOTECA")));
					biblioteca.setFechaConformidad(rs.getDate("FECHA_CONFORMIDAD"));
					biblioteca.setIdUsuarioConformidad(rs.getInt("ID_USUARIO_CONFORMIDAD"));
					biblioteca.setUsuarioConformidad(Util.getString(rs.getString("USUARIO_CONFORMIDAD")));
					biblioteca.setLectorTotal(rs.getString("USU_TOTAL"));
					biblioteca.setLectorTotalAnioAnterior(rs.getString("USU_TOTAL_ANIO_ANTERIOR"));
					biblioteca.setLectorTotalConsultas(rs.getString("USU_TOTAL_CONSULTAS"));
					biblioteca.setFlagGestBibPlanTrabActual(rs.getString("GESBIB_PLAN_TRABAJO_ANUAL"));
					biblioteca.setFlagGestBibReglamentos(rs.getString("GESBIB_REGLAMENTOS"));
					biblioteca.setFlagGestBibEstadisticas(rs.getString("GESBIB_ESTADISTICAS"));
					biblioteca.setGestionBibEspecificar(rs.getString("GESBIB_ESPECIFICAR"));
				}	
				this.closeResultSet(rs);
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ENTIDAD");
				while (rs.next()) {
					Entidad entidad = new Entidad();
					entidad.setId(rs.getLong("ENT_BIB_ID"));
					entidad.setRazonSocial(Util.getString(rs.getString("RAZON_SOCIAL")));
					entidad.setRuc(Util.getString(rs.getString("RUC")));
					entidad.setCorreo(Util.getString(rs.getString("CORREO")));
					entidad.setDireccion(Util.getString(rs.getString("DIRECCION")));
					entidad.setTelefonoFijo(Util.getString(rs.getString("TELEFONO_FIJO")));
					entidad.setTelefonoMovil(Util.getString(rs.getString("TELEFONO_MOVIL")));
					entidad.setFlagOrigen(Util.getString(rs.getString("FLAG_ORIGEN")));
					entidad.getTipoGestion().setId(Util.getString(rs.getString("TIPO_GESTION_ID")));
					entidad.getTipoGestion().setNombre(Util.getString(rs.getString("TIPO_GESTION")));
					entidad.setIdEstado(Util.getString(rs.getString("ESTADO")));
					biblioteca.setEntidad(entidad);
				}	
				this.closeResultSet(rs);		
				
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_RESPONSABLE");
				while (rs.next()) {
					Responsable responsable = new Responsable();
					responsable.setId(rs.getLong("RESPONSABLE_ID"));
					responsable.getPersona().setNombres(Util.getString(rs.getString("NOMBRES")));
					responsable.getPersona().setApellidoPaterno(Util.getString(rs.getString("APELLIDO_PATERNO")));
					responsable.getPersona().setApellidoMaterno(Util.getString(rs.getString("APELLIDO_MATERNO")));
					responsable.getPersona().getTipoDocumentoIdentidad().setNombre(Util.getString(rs.getString("TIPO_DOCUMENTO")));
					responsable.getPersona().setNumeroDocumentoIdentidad(Util.getString(rs.getString("NUMERO_DOCUMENTO")));
					//System.out.println("verificando fecha naciminento:"+ rs.getDate("FECHA_NACIMIENTO"));
					//System.out.println("verificando fecha naciminento conversion :"+ Util.getUtilDate(rs.getDate("FECHA_NACIMIENTO")));
					responsable.getPersona().setFechaNacimiento(Util.getUtilDate(rs.getDate("FECHA_NACIMIENTO")));
					responsable.getPersona().setGenero(Util.getString(rs.getString("GENERO")));
					responsable.setIdEstado(Util.getString(rs.getString("ESTADO")));
					responsable.setTelefonoFijo(Util.getString(rs.getString("TELEFONO_FIJO")));
					responsable.setTelefonoMovil(Util.getString(rs.getString("TELEFONO_MOVIL")));
					responsable.setCorreo(Util.getString(rs.getString("CORREO")));
					responsable.getGradoInstruccion().setId(Util.getLongToString(rs.getLong("TIPO_GRADO_INSTRUCCION_ID")));
					responsable.getGradoInstruccion().setNombre(Util.getString(rs.getString("TIPO_GRADO_INSTRUCCION")));
					responsable.setProfesion(Util.getString(rs.getString("PROFESION")));
					responsable.getCondicionLaboral().setId(Util.getLongToString(rs.getLong("TIPO_CONDICION_LABORAL_ID")));
					responsable.getCondicionLaboral().setNombre(Util.getString(rs.getString("TIPO_CONDICION_LABORAL")));
					responsable.setDireccion(Util.getString(rs.getString("DIRECCION")));
					biblioteca.setResponsable(responsable);
				}
				this.closeResultSet(rs);		
				
				//infraestructura
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_INFRAESTRUCTURA");
				while (rs.next()) {
					biblioteca.getInfraestructura().getTipoLocal().setId(Util.getLongToString(rs.getLong("TIPO_LOCAL_ID")));
					biblioteca.getInfraestructura().getTipoLocal().setNombre(Util.getString(rs.getString("TIPO_LOCAL")));
					biblioteca.getInfraestructura().setAreaM2(Util.getString(rs.getString("AREA_M2")));
					biblioteca.getInfraestructura().setNumeroAmbientes(Util.getString(rs.getString("NUMERO_AMBIENTES")));
					biblioteca.getInfraestructura().setIdEstado(Util.getString(rs.getString("ESTADO")));
					biblioteca.getInfraestructura().setId(rs.getLong("INFRAESTRUCTURA_ID"));
				}	
				this.closeResultSet(rs);
				
				//accesibilidad
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ACCESIBILIDAD");
				List<TipoValor> accesibilidad = new ArrayList<>();
				while (rs.next()) {
					TipoValor valor = new TipoValor();
					valor.setIdPadre(rs.getString("ACCESIBILIDAD_ID"));
					valor.setId(rs.getString("TIPO_ACCESIBILIDAD_ID"));
					valor.setNombre(rs.getString("TIPO_ACCESIBILIDAD"));
					valor.setValor(rs.getString("VALOR"));
					valor.setEstado(rs.getString("ESTADO"));
					accesibilidad.add(valor);
				}
				biblioteca.getInfraestructura().setAccesibilidad(accesibilidad);
				this.closeResultSet(rs);
				
				//colecciones
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_COLECCIONES");
				while (rs.next()) {
					TipoValor valor = new TipoValor();
					valor.setIdPadre(Util.getLongToString(rs.getLong("COLECCIONES_ID")));
					valor.setId(Util.getLongToString(rs.getLong("TIPO_COLECCIONES_ID")));
					valor.setNombre(Util.getString(rs.getString("TIPO_COLECCIONES")));
					valor.setValor(Util.getString(rs.getString("TOTAL")));
					valor.setEstado(Util.getString(rs.getString("ESTADO")));
					biblioteca.getColecciones().add(valor);
				}
				this.closeResultSet(rs);
				
				//servicios
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_SERVICIOS");
				while (rs.next()) {
					TipoValor valor = new TipoValor();
					valor.setIdPadre(Util.getLongToString(rs.getLong("SERVICIOS_ID")));
					valor.setId(Util.getLongToString(rs.getLong("TIPO_SERVICIO_ID")));
					valor.setNombre(Util.getString(rs.getString("TIPO_SERVICIO")));
					valor.setValor(Util.getString(rs.getString("VALOR")));
					valor.setEstado(Util.getString(rs.getString("ESTADO")));
					biblioteca.getServicios().add(valor);
				}
				this.closeResultSet(rs);
				
				//atencion servicio
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ATENSERV");
				while (rs.next()) {
					biblioteca.getAtencionServicio().setId(rs.getLong("ATENCION_SERVICIO_ID"));
					biblioteca.getAtencionServicio().getTipoAccesoPublico().setId(Util.getLongToString(rs.getLong("TIPO_ACCESO_PUBLICO_ID")));
					biblioteca.getAtencionServicio().getTipoAccesoPublico().setNombre(Util.getString(rs.getString("TIPO_ACCESO_PUBLICO")));
					biblioteca.getAtencionServicio().setIdEstado(Util.getString(rs.getString("ESTADO")));
				}	
				this.closeResultSet(rs);
				
				//atencion servicio mes
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ATENSERVMES");
				while (rs.next()) {
					AtencionServicioMes mes = new AtencionServicioMes();
					mes.setId(rs.getLong("ATENCION_MES_ID"));
					mes.getMes().setId(Util.getLongToString(rs.getLong("TIPO_MES_ID")));
					mes.getMes().setNombre(Util.getString(rs.getString("TIPO_MES")));
					mes.setValor(Util.getString(rs.getString("VALOR")));
					mes.setIdEstado(Util.getString(rs.getString("ESTADO")));
					biblioteca.getAtencionServicio().getMeses().add(mes);
				}	
				this.closeResultSet(rs);				

				//atencion servicio turnos
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ATENSERVTURNO");
				while (rs.next()) {
					AtencionServicioTurno dia = new AtencionServicioTurno();
					dia.setId(rs.getLong("ATENCION_TURNO_ID"));
					dia.setDia(Util.getString(rs.getString("DIA")));
					dia.setValor(Util.getString(rs.getString("VALOR")));
					dia.setIdEstado(Util.getString(rs.getString("ESTADO")));
					biblioteca.getAtencionServicio().getDias().add(dia);
				}	
				this.closeResultSet(rs);				
				
				//atencion servicio dia
				List<AtencionServicioTurnoDia> lista = new ArrayList<>();
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ATENSERVTURNODIA");
				while (rs.next()) {
					AtencionServicioTurnoDia dia = new AtencionServicioTurnoDia();
					dia.setId(rs.getLong("TURNO_DIA_ID"));
					dia.setIdAtencionTurno(rs.getLong("ATENCION_TURNO_ID"));
					dia.getTipoTurno().setId(Util.getString(rs.getString("TIPO_TURNO_ID")));
					dia.getTipoTurno().setNombre(Util.getString(rs.getString("TIPO_TURNO")));
					dia.setHoraInicio(Util.getString(rs.getString("HORA_INICIO")));
					dia.setHoraFin(Util.getString(rs.getString("HORA_FIN")));
					dia.setIdEstado(Util.getString(rs.getString("ESTADO")));
					lista.add(dia);
				}	
				biblioteca.getAtencionServicio().asignarTurnosYHorarios(lista);
				this.closeResultSet(rs);
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		//System.out.println("biblioteca nacimineto final:"+biblioteca.getResponsable().getPersona().getFechaNacimiento());
		return biblioteca;	
	}

	@Override
	public ResponseTx confirmarBiblioteca(BibliotecaId biblioteca) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_CONFIRMAR_BIBLIOTECA("
        		+ "?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", Util.getStringToLong(biblioteca.getId()));
			cstm.setLong("E_VERIFICADOR_ID", Util.getStringToLong(biblioteca.getIdUsuario()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo realizar la solicitud ");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}


	@Override
	public ResponseTx iniciarValidacionBiblioteca(BibliotecaId biblioteca) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_INICIAR_VERIF_BIBLIOTECA("
        		+ "?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", Util.getStringToLong(biblioteca.getId()));
			cstm.setLong("E_VERIFICADOR_ID", Util.getStringToLong(biblioteca.getIdUsuario()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo realizar la solicitud ");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTx eliminarBiblioteca(BibliotecaId biblioteca) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_RECHAZAR_BIBLIOTECA("
        		+ "?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", Util.getStringToLong(biblioteca.getId()));
			cstm.setLong("E_VERIFICADOR_ID", Util.getStringToLong(biblioteca.getIdUsuario()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo realizar la solicitud ");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTx observarBiblioteca(BibliotecaObservada biblioteca) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_OBSERVAR_BIBLIOTECA("
        		+ "?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", Util.getStringToLong(biblioteca.getId()));
			cstm.setLong("E_VERIFICADOR_ID", Util.getStringToLong(biblioteca.getIdUsuario()));
			cstm.setString("E_MENSAJE", Util.getStringFormatted(biblioteca.getMensaje()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo realizar la solicitud ");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	//public ResponseTx guardarArchivoAdjunto(ResultAlfresco resultAlfresco, String fileName, String fileExtension, Long idBiblioteca, Long idRegistrador) {
	public ResponseTx guardarArchivoAdjunto(ResultOpenKMDocumento resultOpenKM, String fileName, String fileExtension, Long idBiblioteca, Long idRegistrador) {
	Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_INS_ARCHIVO_ADJUNTO("
        		+ "?,?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", idBiblioteca);
			cstm.setString("E_FILE_NAME", Util.getStringFormatted(fileName));
			cstm.setString("E_FILE_EXTENSION", Util.getStringFormatted(fileExtension));
			cstm.setString("E_UUID", Util.getString(resultOpenKM.getUiid()));
			cstm.setLong("E_USUARIO_ID_REGISTRO", idRegistrador);
			cstm.registerOutParameter("S_MULTIMEDIA_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long id=  Util.getLongFromObject(cstm.getObject("S_MULTIMEDIA_ID"));
				response.setId(String.valueOf(id));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setId("");
				response.setRespuesta("Error: No se pudo realizar la solicitud ");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public List<ImagenAnexa> obtenerImagenesAnexas(Long idBiblioteca) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_GET_IMAGENES_ANEXAS("
        		+ "?,?,?) }";
        List<ImagenAnexa> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", idBiblioteca);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ImagenAnexa imagen = new ImagenAnexa();
					imagen.setId(Util.getLongToString(rs.getLong("MULTIMEDIA_ID")));
					imagen.setUuid(Util.getString(rs.getString("UUID")));
					imagen.setFileName(Util.getString(rs.getString("FILE_NAME")));
					imagen.setFileExtension(Util.getString(rs.getString("FILE_EXTENSION")));
					response.add(imagen);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}
	
	@Override
	public ResponseTx cambiarEstadoImagenes(Long idBiblioteca, String idEstado) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_UPDATE_ESTADO_IMAGENES("
        		+ "?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", idBiblioteca);
			cstm.setString("E_ESTADO", idEstado);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(Util.getString(codigoResultado));
			if (response.getCodigoRespuesta().equals("0000")) {
				response.setRespuesta("ActualizaciÃ³n exitosa.");
			}else {
				response.setRespuesta("Error al momento de actualizar estado de imagen.");
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta("9998");
			response.setCodigoRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				response.setCodigoRespuesta("9999");
				response.setCodigoRespuesta(e1.getMessage());
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public List<BibliotecaMaster> listarMasterBibliotecas(FiltroBibliotecaVerificacionDto entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_LIST_MASTER_BIBLIOTECAS("
        		+ "?,?,?,?,?,?,?,?,?,?,?) }";
        List<BibliotecaMaster> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try { 
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_CODDEPARTAMENTO", Util.getString(entity.getCodDepartamento()));
			cstm.setString("E_CODPROVINCIA", Util.getString(entity.getCodProvincia()));
			cstm.setString("E_CODDISTRITO", Util.getString(entity.getCodDistrito()));
			cstm.setString("E_NOMBRE", Util.getStringFormatted(entity.getNombre()));
			cstm.setString("E_NUMERO_RNB", Util.getStringFormatted(entity.getNumeroRNB()));
			cstm.setDate("E_FECHA_INSCR_DESDE", Util.stringToSQLDate(Util.getString(entity.getFechaInscripcionDesde())));
			cstm.setDate("E_FECHA_INSCR_HASTA", Util.stringToSQLDate(Util.getString(entity.getFechaInscripcionHasta())));
			cstm.setString("E_DIRECCION", Util.getStringFormatted(entity.getDireccion()));
			cstm.setString("E_RUC", Util.getString(entity.getRuc()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					BibliotecaMaster biblioteca = new BibliotecaMaster();
					biblioteca.setIdDeclaracion(Util.getString(rs.getString("BIBLIOTECA_ID")));
					biblioteca.setIdMaster(Util.getString(rs.getString("BIBLIOTECA_MASTER_ID")));
					biblioteca.setNumeroRNB(Util.getString(rs.getString("NUMERO_RNB")));
					biblioteca.setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));
					biblioteca.setProvincia(Util.getString(rs.getString("PROVINCIA")));
					biblioteca.setDistrito(Util.getString(rs.getString("DISTRITO")));
					biblioteca.setAnioDeclaracion(Util.getString(rs.getString("ULT_ANIO_DECLARACION")));
					biblioteca.setCentroPoblado(Util.getString(rs.getString("CENTRO_POBLADO")));
					biblioteca.setDireccion(Util.getString(rs.getString("DIRECCION")));
					biblioteca.setRazonSocialEntidad(Util.getString(rs.getString("RAZON_SOCIAL_ENTIDAD")));
					biblioteca.setRucEntidad(Util.getString(rs.getString("RUC_ENTIDAD")));
					biblioteca.setNombre(Util.getString(rs.getString("NOMBRE_BIBLIOTECA")));
					response.add(biblioteca);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public BibliotecaMasterServicio obtenerResumenBibliotecaMaster(Long idMaster) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_GET_MASTER_BIBLIOTECA("
        		+ "?,?,?,?,?,?,?) }";
        BibliotecaMasterServicio biblioteca = new BibliotecaMasterServicio();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_MASTER_ID", idMaster);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR_GENERAL",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ATENSERV",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ATENSERVMES",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ATENSERVTURNO",OracleTypes.CURSOR);
			cstm.registerOutParameter("S_C_CURSOR_ATENSERVTURNODIA",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_GENERAL");
				while (rs.next()) {
					biblioteca.setId(rs.getLong("BIBLIOTECA_MASTER_ID"));
					biblioteca.setIdDeclaracion(rs.getLong("BIBLIOTECA_ID"));
					biblioteca.setNumeroRNB(Util.getString(rs.getString("NUMERO_RNB")));
					biblioteca.setAnioDeclaracion(Util.getString(rs.getString("ANIO_DECLARACION")));
					biblioteca.setNombre(Util.getString(rs.getString("NOMBRE")));
					biblioteca.getTipoBiblioteca().setNombrePadre(rs.getString("TIPO_BIBLIOTECA"));
					biblioteca.getTipoBiblioteca().setNombre(rs.getString("SUB_TIPO_BIBLIOTECA"));
					biblioteca.getUbigeo().setCentroPoblado(Util.getString(rs.getString("CENTRO_POBLADO")));
					biblioteca.getUbigeo().setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));
					biblioteca.getUbigeo().setDireccion(Util.getString(rs.getString("DIRECCION")));
					biblioteca.getUbigeo().setDistrito(Util.getString(rs.getString("DISTRITO")));
					biblioteca.getUbigeo().setProvincia(Util.getString(rs.getString("PROVINCIA")));
					biblioteca.getEntidad().setRazonSocial(Util.getString(rs.getString("RAZON_SOCIAL")));
					biblioteca.getEntidad().setRuc(Util.getString(rs.getString("RUC")));
				}	
				this.closeResultSet(rs);
				
				//atencion servicio
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ATENSERV");
				while (rs.next()) {
					biblioteca.getAtencionServicio().setId(rs.getLong("ATENCION_SERVICIO_ID"));
					biblioteca.getAtencionServicio().getTipoAccesoPublico().setId(Util.getLongToString(rs.getLong("TIPO_ACCESO_PUBLICO_ID")));
					biblioteca.getAtencionServicio().getTipoAccesoPublico().setNombre(Util.getString(rs.getString("TIPO_ACCESO_PUBLICO")));
					biblioteca.getAtencionServicio().setIdEstado(Util.getString(rs.getString("ESTADO")));
				}	
				this.closeResultSet(rs);
				
				//atencion servicio mes
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ATENSERVMES");
				while (rs.next()) {
					AtencionServicioMes mes = new AtencionServicioMes();
					mes.setId(rs.getLong("ATENCION_MES_ID"));
					mes.getMes().setId(Util.getLongToString(rs.getLong("TIPO_MES_ID")));
					mes.getMes().setNombre(Util.getString(rs.getString("TIPO_MES")));
					mes.setValor(Util.getString(rs.getString("VALOR")));
					mes.setIdEstado(Util.getString(rs.getString("ESTADO")));
					biblioteca.getAtencionServicio().getMeses().add(mes);
				}	
				this.closeResultSet(rs);				

				//atencion servicio turnos
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ATENSERVTURNO");
				while (rs.next()) {
					AtencionServicioTurno dia = new AtencionServicioTurno();
					dia.setId(rs.getLong("ATENCION_TURNO_ID"));
					dia.setDia(Util.getString(rs.getString("DIA")));
					dia.setValor(Util.getString(rs.getString("VALOR")));
					dia.setIdEstado(Util.getString(rs.getString("ESTADO")));
					biblioteca.getAtencionServicio().getDias().add(dia);
				}	
				this.closeResultSet(rs);				
				
				//atencion servicio dia
				List<AtencionServicioTurnoDia> lista = new ArrayList<>();
				rs = (ResultSet) cstm.getObject("S_C_CURSOR_ATENSERVTURNODIA");
				while (rs.next()) {
					AtencionServicioTurnoDia dia = new AtencionServicioTurnoDia();
					dia.setId(rs.getLong("TURNO_DIA_ID"));
					dia.setIdAtencionTurno(rs.getLong("ATENCION_TURNO_ID"));
					dia.getTipoTurno().setId(Util.getString(rs.getString("TIPO_TURNO_ID")));
					dia.getTipoTurno().setNombre(Util.getString(rs.getString("TIPO_TURNO")));
					dia.setHoraInicio(Util.getString(rs.getString("HORA_INICIO")));
					dia.setHoraFin(Util.getString(rs.getString("HORA_FIN")));
					dia.setIdEstado(Util.getString(rs.getString("ESTADO")));
					lista.add(dia);
				}	
				biblioteca.getAtencionServicio().asignarTurnosYHorarios(lista);
				this.closeResultSet(rs);
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return biblioteca;	

	}

	@Override
	public ListaEstadistica obtenerEstadisticas(String anio, String tipo) {
		ListaEstadistica response = new ListaEstadistica();
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_ESTADISTICAS.SP_GET_ESTADISTICA_X_TIPO(?,?,?,?) }";
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_ANIO", Util.getString(anio));
			cstm.setString("E_TIPO", Util.getString(tipo));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR", OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.getResponse().setCodigoRespuesta(codigoResultado);
			if (codigoResultado.equals("0000")) {
				response.getResponse().setRespuesta("Transacción exitosa");
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					Estadistica obj= new Estadistica();
					obj.setAnio(Util.getString(rs.getString("ANIO")));
					obj.setTipo(Util.getString(rs.getString("TIPO")));
					obj.setValor(Util.getString(rs.getString("CANTIDAD")));
					response.getLista().add(obj);
				}					
			}else {
				if (codigoResultado.equals("0001")){
					response.getResponse().setRespuesta("Tipo de estadística no válida.");
				}else {
					response.getResponse().setRespuesta("Error al momento de obtener la información de la base de datos.");
				}
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTx subsanarBiblioteca(Biblioteca entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_CORREGIR_DECLARACION("
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?,"
        		+ "?,?,?,?,?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", entity.getId());
			cstm.setString("E_NOMBRE", Util.getStringFormatted(entity.getNombre()));
			cstm.setLong("E_TIPO_BIBLIOTECA_ID", entity.getTipoBiblioteca().getIdTipoBibliotecaPadre());
			cstm.setLong("E_SUB_TIPO_BIBLIOTECA_ID", entity.getTipoBiblioteca().getIdTipoBiblioteca());
			cstm.setLong("E_TIPO_FUNCIONAMIENTO_ID", Util.getStringToLong(entity.getTipoFuncionamiento().getId()));
			cstm.setLong("E_TIPO_AMBITO_ID", Util.getStringToLong(entity.getAmbitoTerritorial().getId()));
			cstm.setString("E_CODDEPARTAMENTO", Util.getString(entity.getUbigeo().getCodDepartamento()));
			cstm.setString("E_CODPROVINCIA", Util.getString(entity.getUbigeo().getCodProvincia()));
			cstm.setString("E_CODDISTRITO", Util.getString(entity.getUbigeo().getCodDistrito()));
			cstm.setString("E_CENTROPOBLADO", Util.getStringFormatted(entity.getUbigeo().getCentroPoblado()));
			cstm.setString("E_DIRECCION", Util.getStringFormatted(entity.getUbigeo().getDireccion()));
			//cstm.setString("E_CORREO", Util.getStringFormatted(entity.getCorreoElectronico()));
			cstm.setString("E_CORREO", Util.cleanValue(entity.getCorreoElectronico()));
			cstm.setString("E_TELEFONO_FIJO", Util.getString(entity.getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL", Util.getString(entity.getTelefonoMovil()));
			cstm.setInt("E_TOTAL_BIBLIOTECOLOGOS", entity.getNumeroBibliotecologos());
			cstm.setInt("E_TOTAL_PERSONAL", entity.getNumeroTotalPersonal());
			cstm.setString("E_USU_TOTAL_ANIO_ANTERIOR", Util.getString(entity.getLectorTotalAnioAnterior()));
			cstm.setString("E_USU_TOTAL", Util.getString(entity.getLectorTotal()));
			cstm.setString("E_USU_TOTAL_CONSULTAS", Util.getString(entity.getLectorTotalConsultas()));
			cstm.setString("E_GESBIB_PLAN_TRABAJO_ANUAL", Util.getString(entity.getFlagGestBibPlanTrabActual()));
			cstm.setString("E_GESBIB_REGLAMENTOS", Util.getString(entity.getFlagGestBibReglamentos()));
			cstm.setString("E_GESBIB_ESTADISTICAS", Util.getString(entity.getFlagGestBibEstadisticas()));
			cstm.setString("E_GESBIB_ESPECIFICAR", Util.getString(entity.getGestionBibEspecificar()));
			//entidad
			//cstm.setString("E_CORREO_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getCorreo()));
			cstm.setString("E_CORREO_ENTIDAD", Util.cleanValue(entity.getEntidad().getCorreo()));
			cstm.setString("E_DIRECCION_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getDireccion()));
			cstm.setString("E_TELEFONO_FIJO_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL_ENTIDAD", Util.getStringFormatted(entity.getEntidad().getTelefonoMovil()));
			cstm.setLong("E_TIPO_GESTION_ID_ENTIDAD", Util.getStringToLong(entity.getEntidad().getTipoGestion().getId()));
			
			//responsable
			//actualizar responsable ini
			cstm.setString("E_NOMBRE_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getNombres()));
			cstm.setString("E_APE_PATERNO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getApellidoPaterno()));
			cstm.setString("E_APE_MATERNO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getApellidoMaterno()));
			cstm.setString("E_TIPO_DOCUMENTO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getTipoDocumentoIdentidad().getNombre()));
			cstm.setString("E_NUMERO_DOCUMENTO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getNumeroDocumentoIdentidad()));
			cstm.setString("E_GENERO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getPersona().getGenero()));
			//System.out.println("fecha nacimiento corregido format:"+  Util.getSQLDate(entity.getResponsable().getPersona().getFechaNacimiento()));
			cstm.setDate("E_FEC_NAC_RESPONSABLE", Util.getSQLDate(entity.getResponsable().getPersona().getFechaNacimiento()));
			cstm.setString("E_ESTADO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getIdEstado()));
			//actualizar responsable fin
			
			cstm.setLong("E_TIPO_GRADO_INSTRUCCION_ID", Util.getStringToLong(entity.getResponsable().getGradoInstruccion().getId()));
			cstm.setString("E_PROFESION_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getProfesion()));
			cstm.setLong("E_TIPO_CONDICION_LABORAL_ID", Util.getStringToLong(entity.getResponsable().getCondicionLaboral().getId()));
			cstm.setString("E_DIRECCION_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getDireccion()));
			cstm.setString("E_TELEFONO_FIJO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getTelefonoFijo()));
			cstm.setString("E_TELEFONO_MOVIL_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getTelefonoMovil()));
			//cstm.setString("E_CORREO_RESPONSABLE", Util.getStringFormatted(entity.getResponsable().getCorreo()));
			cstm.setString("E_CORREO_RESPONSABLE", Util.cleanValue(entity.getResponsable().getCorreo()));

			cstm.setLong("E_TIPO_LOCAL_ID", Util.getStringToLong(entity.getInfraestructura().getTipoLocal().getId()));
			cstm.setString("E_AREA_M2", Util.getString(entity.getInfraestructura().getAreaM2()));
			cstm.setString("E_NUMERO_AMBIENTES", Util.getString(entity.getInfraestructura().getNumeroAmbientes()));
			
			cstm.setLong("E_USUARIO_ID_MODIFICACION", Util.getStringToLong(entity.getIdUsuarioActualizacion()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				response.setId(Util.getLongToString(entity.getId()));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;		
	}

	@Override
	public ResponseTx corregirBiblioteca(BibliotecaId biblioteca) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_SUBSANAR_BIBLIOTECA("
        		+ "?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", Util.getStringToLong(biblioteca.getId()));
			cstm.setLong("E_VERIFICADOR_ID", Util.getStringToLong(biblioteca.getIdUsuario()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				response.setRespuesta("Error: No se pudo realizar la solicitud ");
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ObservacionesBibliotecaDto obtenerObservacionesBiblioteca(Long idDeclaracion) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_LIST_OBS_BIBLIOTECA("
        		+ "?,?,?) }";
        ObservacionesBibliotecaDto response = new ObservacionesBibliotecaDto();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_BIBLIOTECA_ID", idDeclaracion);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					ObservacionBiblioteca observacion = new ObservacionBiblioteca();
					observacion.setEstadoActualBiblioteca(Util.getString(rs.getString("ESTADO_ACTUAL_BIB")));
					observacion.setEstadoObservacion(Util.getString(rs.getString("ESTADO_OBSERVACION")));
					observacion.setEstadoPrevioBiblioteca(Util.getString(rs.getString("ESTADO_PREVIO_BIB")));
					observacion.setFechaObservacion(Util.getString(rs.getString("FECHA_OBSERVACION")));
					observacion.setIdDeclaracion(Util.getString(rs.getString("BIBLIOTECA_ID")));
					observacion.setObservacion(Util.getString(rs.getString("OBSERVACION")));
					response.getObservaciones().add(observacion);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

}
