package pe.gob.bnp.services.rest.renabi.evento.application.dto;

public class EventoEstadoDto {
	private String idEvento;
	private String idEstado;
	
	public String getIdEvento() {
		return idEvento;
	}
	public void setIdEvento(String idEvento) {
		this.idEvento = idEvento;
	}
	public String getIdEstado() {
		return idEstado;
	}
	public void setIdEstado(String idEstado) {
		this.idEstado = idEstado;
	}
	
	
}
