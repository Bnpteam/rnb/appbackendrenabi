package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class UbigeoDepartamentoDto {
	private String idDepartamento;
	private String departamento;
	
	
	public UbigeoDepartamentoDto() {
		super();
		this.idDepartamento = Constants.EMPTY_STRING;
		this.departamento = Constants.EMPTY_STRING;
	}
	public String getIdDepartamento() {
		return idDepartamento;
	}
	public void setIdDepartamento(String idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	
}
