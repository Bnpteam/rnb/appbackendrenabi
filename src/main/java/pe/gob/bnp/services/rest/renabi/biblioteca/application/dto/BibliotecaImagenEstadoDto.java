package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

public class BibliotecaImagenEstadoDto {

	private String idBibliotecaImagen;
	
	private String idBibliotecaImagenEstado;
	
	public String getIdBibliotecaImagen() {
		return idBibliotecaImagen;
	}
	public void setIdBibliotecaImagen(String idBibliotecaImagen) {
		this.idBibliotecaImagen = idBibliotecaImagen;
	}
	public String getIdBibliotecaImagenEstado() {
		return idBibliotecaImagenEstado;
	}
	public void setIdBibliotecaImagenEstado(String idBibliotecaImagenEstado) {
		this.idBibliotecaImagenEstado = idBibliotecaImagenEstado;
	}
}
