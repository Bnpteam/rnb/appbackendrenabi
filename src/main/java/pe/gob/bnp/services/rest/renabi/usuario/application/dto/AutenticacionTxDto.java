package pe.gob.bnp.services.rest.renabi.usuario.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="Respuesta de Autenticación", description="Respuesta")
public class AutenticacionTxDto {
	@ApiModelProperty(notes = "id de la persona",required=true,value="232")
	private String id;
	
	@ApiModelProperty(notes = "nombre completo de la persona",required=true,value="Juan Perez Cardenas")
	private String nombres;
	
	@ApiModelProperty(notes = "código de respuesta",required=true,value="0000")
	private String codigoRespuesta;
	
	@ApiModelProperty(notes = "descripción de la respuesta",required=true,value="Transacción realizada de forma exitosa.")
	private String respuesta;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getCodigoRespuesta() {
		return codigoRespuesta;
	}

	public void setCodigoRespuesta(String codigoRespuesta) {
		this.codigoRespuesta = codigoRespuesta;
	}

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	
}
