package pe.gob.bnp.services.rest.renabi.registrador.infraestructure.jdbc.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaCabeceraDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.FiltroBibliotecaVerificacionDto;
import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.domain.entity.Credencial;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseJDBCOperation;
import pe.gob.bnp.services.rest.renabi.persona.domain.entity.Registrador;
import pe.gob.bnp.services.rest.renabi.registrador.domain.entity.Credenciales;
import pe.gob.bnp.services.rest.renabi.registrador.domain.repository.RegistradorRepository;
import pe.gob.bnp.services.rest.renabi.usuario.application.dto.AutenticacionTxDto;

@Repository
public class RegistradorJBDCRepository extends BaseJDBCOperation implements RegistradorRepository{

	@Override
	public ResponseTx persist(Registrador entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_PERSONA.SP_CREAR_REGISTRADOR(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_NOMBRES", Util.getStringFormatted(entity.getPersona().getNombres()));
			cstm.setString("E_APELLIDO_PATERNO", Util.getStringFormatted(entity.getPersona().getApellidoPaterno()));
			cstm.setString("E_APELLIDO_MATERNO", Util.getStringFormatted(entity.getPersona().getApellidoMaterno()));
			cstm.setString("E_TIPO_DOCUMENTO", Util.getString(entity.getPersona().getTipoDocumentoIdentidad().getNombre()));
			cstm.setString("E_NUMERO_DOCUMENTO", Util.getStringFormatted(entity.getPersona().getNumeroDocumentoIdentidad()));
			cstm.setString("E_GENERO", Util.getString(entity.getPersona().getGenero()));
			cstm.setDate("E_FECHA_NACIMIENTO", Util.getSQLDate(entity.getPersona().getFechaNacimiento()));
			cstm.setString("E_DIRECCION", Util.getStringFormatted(entity.getDireccion()));
			cstm.setString("E_USUARIO", Util.getString(entity.getPersona().getNumeroDocumentoIdentidad()));
			cstm.setString("E_PASSWORD", Util.getString(entity.getPassword()));
			cstm.setString("E_ESTADO", Util.getStringFormatted(entity.getIdEstado()));
			cstm.setString("E_TELEFONO_MOVIL", Util.getStringFormatted(entity.getTelefonoMovil()));
			cstm.setString("E_TELEFONO_FIJO", Util.getStringFormatted(entity.getTelefonoFijo()));
			cstm.setString("E_CORREO", Util.getStringFormatted(entity.getCorreo()));
			cstm.setLong("E_ID_CREACION_AUDITORIA", Util.getStringToLong(entity.getIdUsuarioRegistro())>0L?Util.getStringToLong(entity.getIdUsuarioRegistro()):Constants.ID_AUDITORIA_NO_USUARIO);
			cstm.registerOutParameter("S_PERSONA_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_REGISTRADOR_ID",OracleTypes.NUMBER);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				cn.commit();
				Long idRegistrador =  Util.getLongFromObject(cstm.getObject("S_REGISTRADOR_ID"));
				response.setId(String.valueOf(idRegistrador));
				response.setRespuesta("Transacción exitosa.");
			}else {
				cn.rollback();
				if (response.getCodigoRespuesta().equals("0001")) {
					response.setRespuesta("Error: el documento de identidad ya se encuentra registrado.");
				}if (response.getCodigoRespuesta().equals("0002")) {
					response.setRespuesta("Error: el correo electrónica ya se encuentra registrado.");
				}else {
					response.setRespuesta("Error: no se pudo ejecutar la transacción.");
				}
				
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;		
	}

	@Override
	public Registrador read(Registrador entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Registrador> list(Registrador entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx update(Registrador entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx delete(Registrador entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AutenticacionTxDto autenticarUsuario(Credencial credencial) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_PERSONA.SP_AUTENTICAR_REGISTRADOR(?,?,?,?,?) }";
        AutenticacionTxDto response = new AutenticacionTxDto();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_USUARIO", Util.getString(credencial.getUsuario()));
			cstm.setString("E_PASSWORD", Util.getString(credencial.getPassword()));
			cstm.registerOutParameter("S_REGISTRADOR_ID", OracleTypes.NUMBER);
			cstm.registerOutParameter("S_NOMBRE_COMPLETO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (response.getCodigoRespuesta().equals("0000")) {
				Long id=  Util.getLongFromObject(cstm.getObject("S_REGISTRADOR_ID"));
				String nombres = Util.getString(cstm.getString("S_NOMBRE_COMPLETO"));
				response.setNombres(nombres);
				response.setId(Util.getLongToString(id));
				response.setRespuesta("Credenciales correctas.");
			}else {
				if (response.getCodigoRespuesta().equals("0001")) {
					response.setRespuesta("Error: El usuario/contraseña son incorrectos.");
				}else {
					response.setRespuesta("Error: no se pudo ejecutar la solicitud.");
				} 
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));			
			response.setRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				response.setCodigoRespuesta(String.valueOf(e1.getErrorCode()));			
				response.setRespuesta(e1.getMessage());
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public List<BibliotecaCabeceraDto> listarBibliotecas(Long id) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_LIST_DECL_REGISTRADOR("
        		+ "?,?,?) }";
        List<BibliotecaCabeceraDto> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_REGISTRADOR_ID", id);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					BibliotecaCabeceraDto biblioteca = new BibliotecaCabeceraDto();
					biblioteca.setId(Util.getString(rs.getString("BIBLIOTECA_ID")));
					biblioteca.setFechaInscripcion(Util.sqlDateToString(rs.getDate("FECHA_INSCRIPCION")));
					biblioteca.setNombre(Util.getString(rs.getString("NOMBRE_BIBLIOTECA")));
					biblioteca.setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));
					biblioteca.setProvincia(Util.getString(rs.getString("DEPARTAMENTO")));
					biblioteca.setDistrito(Util.getString(rs.getString("DISTRITO")));
					biblioteca.setNumeroRNB(Util.getString(rs.getString("NUMERO_RNB")));
					biblioteca.setAnioDeclaracion(Util.getString(rs.getString("ANIO_DECLARACION")));
					biblioteca.setEstado(Util.getString(rs.getString("TIPO_ESTADO_BIBLIOTECA")));
					biblioteca.setRazonSocialEntidad(Util.getString(rs.getString("RAZON_SOCIAL_ENTIDAD")));
					biblioteca.setRucEntidad(Util.getString(rs.getString("RUC_ENTIDAD")));
					biblioteca.setNombreResponsable(Util.getString(rs.getString("NOMBRE_RESPONSABLE")));
					biblioteca.setDocIdentidadResponsable(Util.getString(rs.getString("DOC_IDENT_RESPONSABLE")));
					biblioteca.setNombreRegistrador(Util.getString(rs.getString("NOMBRE_REGISTRADOR")));
					biblioteca.setDocIdentidadRegistrador(Util.getString(rs.getString("DOC_IDENT_REGISTRADOR")));
					biblioteca.setDireccion(Util.getString(rs.getString("DIRECCION")));
					biblioteca.setCentroPoblado(Util.getString(rs.getString("CENTRO_POBLADO")));
					response.add(biblioteca);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public List<BibliotecaCabeceraDto> listarBibliotecasVerificadas(FiltroBibliotecaVerificacionDto entity) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_BIBLIOTECA.SP_LIST_BIB_VERIFICADAS("
        		+ "?,?,?,?,?,?,?,?,?,?,?) }";
        List<BibliotecaCabeceraDto> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_CODDEPARTAMENTO", Util.getString(entity.getCodDepartamento()));
			cstm.setString("E_CODPROVINCIA", Util.getString(entity.getCodProvincia()));
			cstm.setString("E_CODDISTRITO", Util.getString(entity.getCodDistrito()));
			cstm.setString("E_NOMBRE", Util.getStringFormatted(entity.getNombre()));
			cstm.setString("E_NUMERO_RNB", Util.getStringFormatted(entity.getNumeroRNB()));
			cstm.setDate("E_FECHA_INSCR_DESDE", Util.stringToSQLDate(Util.getString(entity.getFechaInscripcionDesde())));
			cstm.setDate("E_FECHA_INSCR_HASTA", Util.stringToSQLDate(Util.getString(entity.getFechaInscripcionHasta())));
			cstm.setString("E_DIRECCION", Util.getStringFormatted(entity.getDireccion()));
			cstm.setString("E_RUC", Util.getString(entity.getRuc()));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					BibliotecaCabeceraDto biblioteca = new BibliotecaCabeceraDto();
					biblioteca.setId(Util.getString(rs.getString("BIBLIOTECA_ID")));
					biblioteca.setFechaInscripcion(Util.sqlDateToString(rs.getDate("FECHA_INSCRIPCION")));
					biblioteca.setNombre(Util.getString(rs.getString("NOMBRE_BIBLIOTECA")));
					biblioteca.setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));
					biblioteca.setProvincia(Util.getString(rs.getString("DEPARTAMENTO")));
					biblioteca.setDistrito(Util.getString(rs.getString("DISTRITO")));
					biblioteca.setNumeroRNB(Util.getString(rs.getString("NUMERO_RNB")));
					biblioteca.setAnioDeclaracion(Util.getString(rs.getString("ANIO_DECLARACION")));
					biblioteca.setEstado(Util.getString(rs.getString("TIPO_ESTADO_BIBLIOTECA")));
					biblioteca.setRazonSocialEntidad(Util.getString(rs.getString("RAZON_SOCIAL_ENTIDAD")));
					biblioteca.setRucEntidad(Util.getString(rs.getString("RUC_ENTIDAD")));
					biblioteca.setNombreResponsable(Util.getString(rs.getString("NOMBRE_RESPONSABLE")));
					biblioteca.setDocIdentidadResponsable(Util.getString(rs.getString("DOC_IDENT_RESPONSABLE")));
					biblioteca.setNombreRegistrador(Util.getString(rs.getString("NOMBRE_REGISTRADOR")));
					biblioteca.setDocIdentidadRegistrador(Util.getString(rs.getString("DOC_IDENT_REGISTRADOR")));
					biblioteca.setDireccion(Util.getString(rs.getString("DIRECCION")));
					biblioteca.setCentroPoblado(Util.getString(rs.getString("CENTRO_POBLADO")));
					response.add(biblioteca);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public Credenciales obtenerCredencialRegistrador(String correo) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_PERSONA.SP_VALIDAR_CORREO_REGISTRADOR("
        		+ "?,?,?) }";
        Credenciales response = new Credenciales();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_CORREO", Util.getString(correo));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(codigoResultado);
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				if(rs.next()) {
					response.setPassword(rs.getString("PASSWORD"));
					response.setUsuario(rs.getString("USUARIO"));
				}	
			}else {
				response.setPassword("");
				response.setUsuario("");
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta(String.valueOf(e.getErrorCode()));
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;
	}

	@Override
	public ResponseTx persistM(Registrador entity) {
		// TODO Auto-generated method stub
		return null;
	}
 



}
