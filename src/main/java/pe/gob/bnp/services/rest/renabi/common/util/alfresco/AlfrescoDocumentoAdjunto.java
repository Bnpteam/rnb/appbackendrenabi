package pe.gob.bnp.services.rest.renabi.common.util.alfresco;

import org.apache.log4j.Logger;

import ws.alfresco.servir.gob.pe.ArchivoType;
import ws.alfresco.servir.gob.pe.MetadataType;

public class AlfrescoDocumentoAdjunto extends AlfrescoBase {
	//@Value("${alfresco.repositorio.carpeta}")
	private String rutaAlfresco;
	
	public AlfrescoDocumentoAdjunto(String rutaAlfresco) {
		super();
		this.rutaAlfresco = rutaAlfresco;
	}
	
	
	public AlfrescoDocumentoAdjunto(String rutaAlfresco,String alfrescoWsdl, String alfrescoEndpoint, String alfrescoWsUri, String alfrescoService,
			String alfrescoRepositorioCarpeta, String alfrescoCodigo, String alfrescoUsuario, String alfrescoPassword,
			String alfrescoCantidadGeneral) {

		super(alfrescoWsdl, alfrescoEndpoint, alfrescoWsUri, alfrescoService, alfrescoRepositorioCarpeta, alfrescoCodigo, alfrescoUsuario, alfrescoPassword, alfrescoCantidadGeneral);
		this.rutaAlfresco = rutaAlfresco;
/*		this.setAlfrescoCantidadGeneral(alfrescoCantidadGeneral);
		this.setAlfrescoCodigo(alfrescoCodigo);
		this.setAlfrescoEndpoint(alfrescoEndpoint);
		this.setAlfrescoPassword(alfrescoPassword);
		this.setAlfrescoRepositorioCarpeta(alfrescoRepositorioCarpeta);
		this.setAlfrescoService(alfrescoService);
		this.setAlfrescoUsuario(alfrescoUsuario);
		this.setAlfrescoWsdl(alfrescoWsdl);
		this.setAlfrescoWsUri(alfrescoWsUri);*/
	}



	private static final Logger logger = Logger.getLogger(AlfrescoDocumentoAdjunto.class);
	public ResultAlfresco subirArchivo(FileAlfresco fileAlfresco, String idBiblioteca ){
		ResultAlfresco	resultAlfresco = new ResultAlfresco();	
		try{
			String			nombreArchivoAlfresco;
			
			super.setToken(super.getResponseAutenticacion().getToken());
			
	        MetadataType metadata = new MetadataType();
	        metadata.setParameter1("cmis:document");
	        
	        nombreArchivoAlfresco = fileAlfresco.getFileName(); //VO.convertNameFile(fileAlfresco.getFileName());
	        
	        metadata.setParameter2(nombreArchivoAlfresco);
	
			ArchivoType archivoType = new ArchivoType();
			archivoType.setRutaRepositorio(this.getRutaRepositorio(idBiblioteca));
	
			archivoType.setDocumento(fileAlfresco.getRawFile());
								
	        archivoType.setFormato(fileAlfresco.getExtension());
	        
	        super.setResponseRegistrarContenidoType(super.getPort().registrarContenido(super.getToken(), super.getSeguridad(), super.getAuditoria(), metadata, archivoType));
	        
	        resultAlfresco.setUiid(super.getResponseRegistrarContenidoType().getArchivo().getUiidRecurso());
	        resultAlfresco.setCodigo(super.getResponseRegistrarContenidoType().getMensaje().getCodigoMensaje());
	        resultAlfresco.setDescripcion(super.getResponseRegistrarContenidoType().getMensaje().getDescripMensaje());
	        if (resultAlfresco.getCodigo().equals("00000")){
	        	logger.info("[Resultado: EXITO-> Registrado en ALFRESCO exitosamente. uuid ="+resultAlfresco.getUiid()+"]");
	        }else{
	        	logger.error("[Resultado: ERROR -> Error en ALFRESCO: "+resultAlfresco.getCodigo()+"-"+resultAlfresco.getDescripcion());
	        }
		}catch(Exception e){
			logger.error( "Error", e );
		}
		return resultAlfresco;
	}

	public ResultAlfresco actualizarArchivo(FileAlfresco fileAlfresco){

		ResultAlfresco	resultAlfresco = new ResultAlfresco();	
		
		super.setToken(super.getResponseAutenticacion().getToken());
        MetadataType metadata = new MetadataType();
        metadata.setParameter1("cmis:document");
        metadata.setParameter2(null);
        
        ArchivoType archivoType = new ArchivoType();
        archivoType.setUiidRecurso(fileAlfresco.getUiid());
        archivoType.setDocumento(fileAlfresco.getRawFile());
        archivoType.setFormato(fileAlfresco.getExtension());
        
        super.setResponseModificarContenidoType(super.getPort().modificarContenido(super.getToken(), super.getSeguridad(), super.getAuditoria(), metadata, archivoType));
        
        resultAlfresco.setUiid(super.getResponseModificarContenidoType().getArchivo().getUiidRecurso());
        resultAlfresco.setCodigo(super.getResponseModificarContenidoType().getMensaje().getCodigoMensaje());
        resultAlfresco.setDescripcion(super.getResponseModificarContenidoType().getMensaje().getDescripMensaje());
        
		return resultAlfresco;
	}
	
	
	public String getRutaRepositorio(String id){
		StringBuilder pathAlfrescoFull = new StringBuilder();	
		pathAlfrescoFull.append(rutaAlfresco);
		pathAlfrescoFull.append("/"+id);
		return pathAlfrescoFull.toString();

	}

	public String getRutaAlfresco() {
		return rutaAlfresco;
	}

	public void setRutaAlfresco(String rutaAlfresco) {
		this.rutaAlfresco = rutaAlfresco;
	}

	

}
