package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurnoDia;
import pe.gob.bnp.services.rest.renabi.common.Constants;

public class AtencionServicioTurnoDiaDto{
	private String idTurno;
	private String nombreTurno;
	private String horaInicio;
	private String horaFin;
	
	public AtencionServicioTurnoDiaDto() {
		super();
		this.idTurno = Constants.EMPTY_STRING;
		this.nombreTurno = Constants.EMPTY_STRING;
		this.horaInicio = Constants.EMPTY_STRING;
		this.horaFin = Constants.EMPTY_STRING;
	}

	public AtencionServicioTurnoDiaDto(AtencionServicioTurnoDia objeto) {
		super();
		this.idTurno = objeto.getTipoTurno().getId();
		this.nombreTurno = objeto.getTipoTurno().getNombre();
		this.horaInicio = objeto.getHoraInicio();
		this.horaFin = objeto.getHoraFin();
	}
	
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getIdTurno() {
		return idTurno;
	}
	public void setIdTurno(String idTurno) {
		this.idTurno = idTurno;
	}
	public String getNombreTurno() {
		return nombreTurno;
	}
	public void setNombreTurno(String nombreTurno) {
		this.nombreTurno = nombreTurno;
	}
	
	
	
}
