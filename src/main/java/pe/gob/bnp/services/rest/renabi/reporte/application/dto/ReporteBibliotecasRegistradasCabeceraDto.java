package pe.gob.bnp.services.rest.renabi.reporte.application.dto;

public class ReporteBibliotecasRegistradasCabeceraDto {
	
	private String rowBiblioteca;	
	private String departamento;
	private String provincia;
	private String distrito;
	private String centroPoblado;
	private String tipoBiblioteca;
	private String fechaRegistro;
	private String nroLibros;
	private String porValidar;
	private String enProceso;
	private String verificado;
	private String observado;
	private String total;
	
	
	public String getRowBiblioteca() {
		return rowBiblioteca;
	}
	public void setRowBiblioteca(String rowBiblioteca) {
		this.rowBiblioteca = rowBiblioteca;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getCentroPoblado() {
		return centroPoblado;
	}
	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}
	public String getTipoBiblioteca() {
		return tipoBiblioteca;
	}
	public void setTipoBiblioteca(String tipoBiblioteca) {
		this.tipoBiblioteca = tipoBiblioteca;
	}
	public String getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	public String getNroLibros() {
		return nroLibros;
	}
	public void setNroLibros(String nroLibros) {
		this.nroLibros = nroLibros;
	}
	public String getPorValidar() {
		return porValidar;
	}
	public void setPorValidar(String porValidar) {
		this.porValidar = porValidar;
	}
	public String getEnProceso() {
		return enProceso;
	}
	public void setEnProceso(String enProceso) {
		this.enProceso = enProceso;
	}
	public String getVerificado() {
		return verificado;
	}
	public void setVerificado(String verificado) {
		this.verificado = verificado;
	}
	public String getObservado() {
		return observado;
	}
	public void setObservado(String observado) {
		this.observado = observado;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}

}
