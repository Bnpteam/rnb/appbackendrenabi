package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.common.application.dto.NuevoTipoValorDto;
import pe.gob.bnp.services.rest.renabi.entidad.application.dto.VerEntidadDto;

@ApiModel(value="Ver Biblioteca", description="Representa los datos de una bibioteca ya registrada")
public class VerBibliotecaDto {
	//Datos generales
	@ApiModelProperty(notes = "Id de la biblioteca",required=true,value="Ej: 324")
	private String id;

	@ApiModelProperty(notes = "Año de la declaracion",required=true,value="Ej: 2019")
	private String anioDeclaracion;
	
	@ApiModelProperty(notes = "Nombre de la biblioteca",required=true,value="Ej: Biblioteca de Madrid")
	private String nombre;

	@ApiModelProperty(notes = "Correo electrónico de la biblioteca",required=true,value="Ej: correo@gmail.com")
	private String correoElectronico;

	@ApiModelProperty(notes = "Teléfono fijo",required=true,value="Ej: 06578711")
	private String telefonoFijo;

	@ApiModelProperty(notes = "Teléfono móvil",required=true,value="Ej: 95601112")
	private String telefonoMovil;

	@ApiModelProperty(notes = "Número de bibliotecólogos",required=true,value="Ej: 34")
	private String numeroBibliotecologos;

	@ApiModelProperty(notes = "Número total de personal",required=true,value="Ej: 45")
	private String numeroTotalPersonal;

	@ApiModelProperty(notes = "Id del estado",required=true,value="Ej: 1")
	private String idEstadoBiblioteca;

	@ApiModelProperty(notes = "Estado",required=true,value="Ej: POR VALIDAR")
	private String estadoBiblioteca;

	//Ambito Territorial
	@ApiModelProperty(notes = "Id del ambito territorial",required=true,value="Ej: 24")
	private String idAmbitoTerritorial;
	@ApiModelProperty(notes = "Ámbito territorial",required=true,value="Ej: URBANO")
	private String ambitoTerritorial;

	//Tipo de Funcionamiento
	@ApiModelProperty(notes = "Id del tipo de funcionamiento",required=true,value="Ej: 4")
	private String idTipoFuncionamiento;

	@ApiModelProperty(notes = "Tipo de funcionamiento",required=true,value="Ej: ACTIVA")
	private String tipoFuncionamiento;

	//Registrador
	@ApiModelProperty(notes = "Id del Registrador",required=true,value="Ej: 90")
	private String idRegistrador;

	@ApiModelProperty(notes = "Nombre completo del Registrador",required=true,value="Ej: Juan Pérez Cárdenas")
	private String nombreRegistrador;

	@ApiModelProperty(notes = "Documento de identidad del Registrador",required=true,value="Ej: DNI-12013457")
	private String docIdentidadRegistrador;
	
	@ApiModelProperty(notes = "Usuarios en el año anterior",required=true,value="Ej: 949")
	private String totalLectoresAnioAnterior;

	@ApiModelProperty(notes = "Número de usuarios/asistentes",required=true,value="Ej: 456")
	private String totalLectores;

	@ApiModelProperty(notes = "Número de consultas",required=true,value="Ej: 98")
	private String totalConsultas;

	@ApiModelProperty(notes = "Gestión de la biblioteca - Plan de trabajo anual",required=true,value="Ej: SI")
	private String planTrabActual;

	@ApiModelProperty(notes = "Gestión de la biblioteca - Reglamentos",required=true,value="Ej: SI")
	private String reglamentos;

	@ApiModelProperty(notes = "Gestión de la biblioteca - Estadísticas",required=true,value="Ej: NO")
	private String estadisticas;

	@ApiModelProperty(notes = "Gestión de la biblioteca - Especificar",required=true,value="Ej: ............")
	private String especificar;	
	
	//Entidad
	@ApiModelProperty(notes = "Entidad",required=true,value="Ej: clase Entidad")
	private VerEntidadDto entidad;
	
	//Responsable
	@ApiModelProperty(notes = "Responsable",required=true,value="Ej: clase Responsable")
	private VerResponsableDto responsable;

	//Tipo biblioteca
	@ApiModelProperty(notes = "Id del tipo de biblitoeca",required=true,value="Ej: 45")
	private	String idTipoBibliotecaPadre;

	@ApiModelProperty(notes = "Tipo de biblitoeca",required=true,value="Ej: BIBLIOTECAS DE INSTITUCIONES EDUCATIVAS")
	private	String  tipoBibliotecaPadre;

	@ApiModelProperty(notes = "Id del subtipo de biblitoeca",required=true,value="Ej: 56")
	private String idTipoBibliotecaHijo;

	@ApiModelProperty(notes = "Sub Tipo de biblitoeca",required=true,value="Ej: IE INICIAL")
	private	String  tipoBibliotecaHijo;
	
	
	//ubigeo
	@ApiModelProperty(notes = "Código de departamento",required=true,value="Ej: 01")
	private String codDepartamento;
	@ApiModelProperty(notes = "Código de provincia",required=true,value="Ej: 02")
	private String codProvincia;
	@ApiModelProperty(notes = "Código de distrito",required=true,value="Ej: 03")
	private String codDistrito;
	@ApiModelProperty(notes = "Departamento",required=true,value="Ej: LIMA")
	private String departamento;
	@ApiModelProperty(notes = "Provincia",required=true,value="Ej: LIMA")
	private String provincia;
	@ApiModelProperty(notes = "Distrito",required=true,value="Ej: SAN BORJA")
	private String distrito;
	
	@ApiModelProperty(notes = "Año de la declaracion",required=true,value="Ej: 2019")
	private String centroPoblado;
	@ApiModelProperty(notes = "Dirección de la biblioteca",required=true,value="Ej: Av. Arenales 3435")
	private String direccion;
	
	//Infraestructura
	@ApiModelProperty(notes = "Infraestructura",required=true,value="Ej: clase Infraestructura")
	private VerInfraestructuraDto infraestructura;	
	
	@ApiModelProperty(notes = "Colecciones",required=true,value="Ej: Lista")
	private List<NuevoTipoValorDto> colecciones;
	
	@ApiModelProperty(notes = "Servicios",required=true,value="Ej: Lista")
	private List<NuevoTipoValorDto> servicios;
	
	//Atencion del servicio
	@ApiModelProperty(notes = "Atención del servicio",required=true,value="Ej: Lista")
	private AtencionServicioDto atencionServicio;
	
	public VerBibliotecaDto(){
		this.infraestructura = new  VerInfraestructuraDto();
		this.entidad = new VerEntidadDto();
		this.responsable = new VerResponsableDto();
		this.infraestructura = new VerInfraestructuraDto();
		this.colecciones = new ArrayList<>();
		this.servicios = new ArrayList<>();
		this.atencionServicio = new  AtencionServicioDto();
	}
	
	//fllc completar
	/*
	public VerBibliotecaDto(Biblioteca objeto) {
		super();
		this.anioDeclaracion = objeto.getAnioDeclaracion();
		this.nombre = objeto.getNombre();
		this.entidad = new VerEntidadDto(objeto.getEntidad());
		this.idAmbitoTerritorial = objeto.getAmbitoTerritorial().getId();
		this.idTipoFuncionamiento = objeto.getTipoFuncionamiento().getId();
		this.direccion = objeto.getDireccion();
		this.correoElectronico = objeto.getCorreoElectronico();
		this.numeroBibliotecologos = Util.getIntToString(objeto.getNumeroBibliotecologos());
		this.numeroTotalPersonal = Util.getIntToString(objeto.getNumeroTotalPersonal());
		this.telefonoFijo = objeto.getTelefonoFijo();
		this.telefonoMovil = objeto.getTelefonoMovil();
		
		this.idTipoBibliotecaPadre = Util.getIntToString(objeto.getTipoBiblioteca().getIdTipoBibliotecaPadre());
		this.idTipoBibliotecaHijo = Util.getIntToString(objeto.getTipoBiblioteca().getIdTipoBiblioteca());
		this.idEstadoBiblioteca = objeto.getEstadoBiblioteca().getId();
		this.responsable = new VerResponsableDto();
		this.infraestructura = new  VerInfraestructuraDto();
		
		//this.codDepartamento = obs fllc
	}*/

	public String getAnioDeclaracion() {
		return anioDeclaracion;
	}

	public void setAnioDeclaracion(String anioDeclaracion) {
		this.anioDeclaracion = anioDeclaracion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getIdAmbitoTerritorial() {
		return idAmbitoTerritorial;
	}

	public void setIdAmbitoTerritorial(String idAmbitoTerritorial) {
		this.idAmbitoTerritorial = idAmbitoTerritorial;
	}

	public String getIdTipoFuncionamiento() {
		return idTipoFuncionamiento;
	}

	public void setIdTipoFuncionamiento(String idTipoFuncionamiento) {
		this.idTipoFuncionamiento = idTipoFuncionamiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getNumeroBibliotecologos() {
		return numeroBibliotecologos;
	}

	public void setNumeroBibliotecologos(String numeroBibliotecologos) {
		this.numeroBibliotecologos = numeroBibliotecologos;
	}

	public String getNumeroTotalPersonal() {
		return numeroTotalPersonal;
	}

	public void setNumeroTotalPersonal(String numeroTotalPersonal) {
		this.numeroTotalPersonal = numeroTotalPersonal;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getIdRegistrador() {
		return idRegistrador;
	}

	public void setIdRegistrador(String idRegistrador) {
		this.idRegistrador = idRegistrador;
	}

	public String getCentroPoblado() {
		return centroPoblado;
	}

	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}

	public String getIdTipoBibliotecaPadre() {
		return idTipoBibliotecaPadre;
	}

	public void setIdTipoBibliotecaPadre(String idTipoBibliotecaPadre) {
		this.idTipoBibliotecaPadre = idTipoBibliotecaPadre;
	}

	public String getIdTipoBibliotecaHijo() {
		return idTipoBibliotecaHijo;
	}

	public void setIdTipoBibliotecaHijo(String idTipoBibliotecaHijo) {
		this.idTipoBibliotecaHijo = idTipoBibliotecaHijo;
	}

	public String getIdEstadoBiblioteca() {
		return idEstadoBiblioteca;
	}

	public void setIdEstadoBiblioteca(String idEstadoBiblioteca) {
		this.idEstadoBiblioteca = idEstadoBiblioteca;
	}

	public String getCodDepartamento() {
		return codDepartamento;
	}

	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}

	public String getCodProvincia() {
		return codProvincia;
	}

	public void setCodProvincia(String codProvincia) {
		this.codProvincia = codProvincia;
	}

	public String getCodDistrito() {
		return codDistrito;
	}

	public void setCodDistrito(String codDistrito) {
		this.codDistrito = codDistrito;
	}

	public List<NuevoTipoValorDto> getColecciones() {
		return colecciones;
	}


	public void setColecciones(List<NuevoTipoValorDto> colecciones) {
		this.colecciones = colecciones;
	}


	public List<NuevoTipoValorDto> getServicios() {
		return servicios;
	}


	public void setServicios(List<NuevoTipoValorDto> servicios) {
		this.servicios = servicios;
	}


	public AtencionServicioDto getAtencionServicio() {
		return atencionServicio;
	}


	public void setAtencionServicio(AtencionServicioDto atencionServicio) {
		this.atencionServicio = atencionServicio;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEstadoBiblioteca() {
		return estadoBiblioteca;
	}

	public void setEstadoBiblioteca(String estadoBiblioteca) {
		this.estadoBiblioteca = estadoBiblioteca;
	}

	public String getAmbitoTerritorial() {
		return ambitoTerritorial;
	}

	public void setAmbitoTerritorial(String ambitoTerritorial) {
		this.ambitoTerritorial = ambitoTerritorial;
	}

	public String getTipoFuncionamiento() {
		return tipoFuncionamiento;
	}

	public void setTipoFuncionamiento(String tipoFuncionamiento) {
		this.tipoFuncionamiento = tipoFuncionamiento;
	}

	public String getNombreRegistrador() {
		return nombreRegistrador;
	}

	public void setNombreRegistrador(String nombreRegistrador) {
		this.nombreRegistrador = nombreRegistrador;
	}

	public String getDocIdentidadRegistrador() {
		return docIdentidadRegistrador;
	}

	public void setDocIdentidadRegistrador(String docIdentidadRegistrador) {
		this.docIdentidadRegistrador = docIdentidadRegistrador;
	}

	public VerEntidadDto getEntidad() {
		return entidad;
	}

	public void setEntidad(VerEntidadDto entidad) {
		this.entidad = entidad;
	}

	public VerResponsableDto getResponsable() {
		return responsable;
	}

	public void setResponsable(VerResponsableDto responsable) {
		this.responsable = responsable;
	}

	public String getTipoBibliotecaPadre() {
		return tipoBibliotecaPadre;
	}

	public void setTipoBibliotecaPadre(String tipoBibliotecaPadre) {
		this.tipoBibliotecaPadre = tipoBibliotecaPadre;
	}

	public String getTipoBibliotecaHijo() {
		return tipoBibliotecaHijo;
	}

	public void setTipoBibliotecaHijo(String tipoBibliotecaHijo) {
		this.tipoBibliotecaHijo = tipoBibliotecaHijo;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getDistrito() {
		return distrito;
	}

	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}

	public VerInfraestructuraDto getInfraestructura() {
		return infraestructura;
	}

	public void setInfraestructura(VerInfraestructuraDto infraestructura) {
		this.infraestructura = infraestructura;
	}

	public String getTotalLectoresAnioAnterior() {
		return totalLectoresAnioAnterior;
	}

	public void setTotalLectoresAnioAnterior(String totalLectoresAnioAnterior) {
		this.totalLectoresAnioAnterior = totalLectoresAnioAnterior;
	}

	public String getTotalLectores() {
		return totalLectores;
	}

	public void setTotalLectores(String totalLectores) {
		this.totalLectores = totalLectores;
	}

	public String getTotalConsultas() {
		return totalConsultas;
	}

	public void setTotalConsultas(String totalConsultas) {
		this.totalConsultas = totalConsultas;
	}

	public String getPlanTrabActual() {
		return planTrabActual;
	}

	public void setPlanTrabActual(String planTrabActual) {
		this.planTrabActual = planTrabActual;
	}

	public String getReglamentos() {
		return reglamentos;
	}

	public void setReglamentos(String reglamentos) {
		this.reglamentos = reglamentos;
	}

	public String getEstadisticas() {
		return estadisticas;
	}

	public void setEstadisticas(String estadisticas) {
		this.estadisticas = estadisticas;
	}

	public String getEspecificar() {
		return especificar;
	}

	public void setEspecificar(String especificar) {
		this.especificar = especificar;
	}

	
	
}
