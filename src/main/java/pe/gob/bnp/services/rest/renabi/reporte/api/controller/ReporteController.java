package pe.gob.bnp.services.rest.renabi.reporte.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.VerRptBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.common.api.controller.ResponseHandler;
import pe.gob.bnp.services.rest.renabi.common.application.EntityNotFoundResultException;
import pe.gob.bnp.services.rest.renabi.reporte.application.ReporteApplicationService;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasRegistradasCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasVerificadasCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasxEstadoCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteRegistradoresCabeceraDto;

@RestController
@RequestMapping("/api/reporte")
@Api(value="/api/reporte", description="Servicio REST para Biblioteca - Desarrollado por OTIE/BNP")
@CrossOrigin(origins = "*")
public class ReporteController {	
	
	@Autowired
	ResponseHandler responseHandler;
	
	@Autowired
	ReporteApplicationService reporteApplicationService;
		
	@RequestMapping(method = RequestMethod.GET, path="/registradores",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Reporte Usuarios Registradores para uso interno", response= ReporteRegistradoresCabeceraDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> buscarRegistradores(
			@RequestParam(value = "fecha_desde", defaultValue = "", required=false) String  fechaDesde,
			@RequestParam(value = "fecha_hasta", defaultValue = "", required=false) String  fechaHasta
			){
		try {
			List<ReporteRegistradoresCabeceraDto> response = reporteApplicationService.buscarRegistradores(fechaDesde, fechaHasta);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	
	
	@RequestMapping(method = RequestMethod.GET, path="/bibliotecasxestado",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Reporte Bibliotecas Por Estados para uso interno", response= ReporteBibliotecasxEstadoCabeceraDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> buscarBibliotecasxEstado(
			@RequestParam(value = "fecha_desde", defaultValue = "", required=false) String  fechaDesde,
			@RequestParam(value = "fecha_hasta", defaultValue = "", required=false) String  fechaHasta
			){
		try {
			List<ReporteBibliotecasxEstadoCabeceraDto> response = reporteApplicationService.buscarBibliotecasxEstado(fechaDesde, fechaHasta);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/bibliotecasverificadas",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Reporte Bibliotecas Verificadas para uso interno", response= ReporteBibliotecasVerificadasCabeceraDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> buscarBibliotecasVerificadas(
			@RequestParam(value = "fecha_desde", defaultValue = "", required=false) String  fechaDesde,
			@RequestParam(value = "fecha_hasta", defaultValue = "", required=false) String  fechaHasta
			){
		try {
			List<ReporteBibliotecasVerificadasCabeceraDto> response = reporteApplicationService.buscarBibliotecasVerificadas(fechaDesde, fechaHasta);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.GET, path="/bibliotecasregistradas",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Reporte Bibliotecas Registradas para uso interno", response= ReporteBibliotecasRegistradasCabeceraDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> buscarBibliotecasRegistradas(
			@RequestParam(value = "fecha_desde", defaultValue = "", required=false) String  fechaDesde,
			@RequestParam(value = "fecha_hasta", defaultValue = "", required=false) String  fechaHasta
			){
		try {
			List<ReporteBibliotecasRegistradasCabeceraDto> response = reporteApplicationService.buscarBibliotecasRegistradas(fechaDesde, fechaHasta);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	

	@RequestMapping(method = RequestMethod.GET, path="/bibliotecasxid",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Reporte Bibliotecas x Id para uso interno", response= VerRptBibliotecaDto.class, responseContainer = "Set",httpMethod = "GET")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})	
	public ResponseEntity<Object> buscarBibliotecasxId(
			@RequestParam(value = "idBiblioteca", defaultValue = "", required=true) String  idBiblioteca
			){
		try {
			VerRptBibliotecaDto response = reporteApplicationService.buscarBibliotecasxId(idBiblioteca);
			if (response == null) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}

}
