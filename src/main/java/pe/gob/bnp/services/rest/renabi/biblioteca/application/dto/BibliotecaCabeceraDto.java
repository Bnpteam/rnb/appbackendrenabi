package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

public class BibliotecaCabeceraDto {
	private String id;
	private String nombre;
	private String numeroRNB;
	private String fechaInscripcion;
	private String anioDeclaracion;
	private String estado;
	private String nombreResponsable;
	private String docIdentidadResponsable;
	private String nombreRegistrador;
	private String docIdentidadRegistrador;
	private String razonSocialEntidad;
	private String rucEntidad;
	private String departamento;
	private String provincia;
	private String distrito;
	private String centroPoblado;
	private String direccion;
	private String telefonoMovilResponsable;
	private String telefonoFijoResponsable;
	private String correoResponsable;


	
	public String getTelefonoMovilResponsable() {
		return telefonoMovilResponsable;
	}
	public void setTelefonoMovilResponsable(String telefonoMovilResponsable) {
		this.telefonoMovilResponsable = telefonoMovilResponsable;
	}
	public String getTelefonoFijoResponsable() {
		return telefonoFijoResponsable;
	}
	public void setTelefonoFijoResponsable(String telefonoFijoResponsable) {
		this.telefonoFijoResponsable = telefonoFijoResponsable;
	}
	public String getCorreoResponsable() {
		return correoResponsable;
	}
	public void setCorreoResponsable(String correoResponsable) {
		this.correoResponsable = correoResponsable;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNumeroRNB() {
		return numeroRNB;
	}
	public void setNumeroRNB(String numeroRNB) {
		this.numeroRNB = numeroRNB;
	}
	public String getFechaInscripcion() {
		return fechaInscripcion;
	}
	public void setFechaInscripcion(String fechaInscripcion) {
		this.fechaInscripcion = fechaInscripcion;
	}
	public String getAnioDeclaracion() {
		return anioDeclaracion;
	}
	public void setAnioDeclaracion(String anioDeclaracion) {
		this.anioDeclaracion = anioDeclaracion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getNombreResponsable() {
		return nombreResponsable;
	}
	public void setNombreResponsable(String nombreResponsable) {
		this.nombreResponsable = nombreResponsable;
	}
	public String getNombreRegistrador() {
		return nombreRegistrador;
	}
	public void setNombreRegistrador(String nombreRegistrador) {
		this.nombreRegistrador = nombreRegistrador;
	}
	public String getDocIdentidadRegistrador() {
		return docIdentidadRegistrador;
	}
	public void setDocIdentidadRegistrador(String docIdentidadRegistrador) {
		this.docIdentidadRegistrador = docIdentidadRegistrador;
	}
	public String getRazonSocialEntidad() {
		return razonSocialEntidad;
	}
	public void setRazonSocialEntidad(String razonSocialEntidad) {
		this.razonSocialEntidad = razonSocialEntidad;
	}
	public String getRucEntidad() {
		return rucEntidad;
	}
	public void setRucEntidad(String rucEntidad) {
		this.rucEntidad = rucEntidad;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getCentroPoblado() {
		return centroPoblado;
	}
	public void setCentroPoblado(String centroPoblado) {
		this.centroPoblado = centroPoblado;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getDocIdentidadResponsable() {
		return docIdentidadResponsable;
	}
	public void setDocIdentidadResponsable(String docIdentidadResponsable) {
		this.docIdentidadResponsable = docIdentidadResponsable;
	}
	
}
