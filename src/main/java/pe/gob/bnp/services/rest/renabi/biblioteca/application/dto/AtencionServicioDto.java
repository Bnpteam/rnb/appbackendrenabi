package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import java.util.ArrayList;
import java.util.List;

import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicio;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioMes;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.AtencionServicioTurno;
import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.common.Util;

public class AtencionServicioDto  {
	private String idAccesoPublico;
	private String nombreAccesoPublico;	
	private List<AtencionServicioMesDto> meses;
	private List<AtencionServicioTurnoDto> dias;
	
	public AtencionServicioDto() {
		super();
		this.idAccesoPublico = Constants.EMPTY_STRING;
		this.nombreAccesoPublico = Constants.EMPTY_STRING;
		this.meses = new ArrayList<>();
		this.dias = new ArrayList<>();
	}
	
	public AtencionServicioDto(AtencionServicio objeto) {
		super();
		this.idAccesoPublico = Util.getLongToString(objeto.getId());
		this.nombreAccesoPublico = objeto.getTipoAccesoPublico().getNombre();
		this.meses = new ArrayList<>();
		for (AtencionServicioMes mes: objeto.getMeses()) {
			this.getMeses().add(new AtencionServicioMesDto(mes));
		}		
		this.dias = new ArrayList<>();
		for (AtencionServicioTurno dia: objeto.getDias()) {
			this.getDias().add(new AtencionServicioTurnoDto(dia));
		}		
	}	
	
	public List<AtencionServicioMesDto> getMeses() {
		return meses;
	}

	public void setMeses(List<AtencionServicioMesDto> meses) {
		this.meses = meses;
	}

	public List<AtencionServicioTurnoDto> getDias() {
		return dias;
	}

	public void setDias(List<AtencionServicioTurnoDto> dias) {
		this.dias = dias;
	}

	public String getIdAccesoPublico() {
		return idAccesoPublico;
	}

	public void setIdAccesoPublico(String idAccesoPublico) {
		this.idAccesoPublico = idAccesoPublico;
	}

	public String getNombreAccesoPublico() {
		return nombreAccesoPublico;
	}

	public void setNombreAccesoPublico(String nombreAccesoPublico) {
		this.nombreAccesoPublico = nombreAccesoPublico;
	}


	
}
