package pe.gob.bnp.services.rest.renabi.biblioteca.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class UbigeoProvinciaDto {
	private String idProvincia;
	private String provincia;
	private String idDepartamento;
	
	public UbigeoProvinciaDto() {
		super();
		this.idProvincia = Constants.EMPTY_STRING;
		this.provincia = Constants.EMPTY_STRING;
		this.idDepartamento = Constants.EMPTY_STRING;
	}

	public String getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(String idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getProvincia() {
		return provincia;
	}

	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}

	public String getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(String idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	
	
}
