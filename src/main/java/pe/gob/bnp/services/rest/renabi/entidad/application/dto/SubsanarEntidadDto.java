package pe.gob.bnp.services.rest.renabi.entidad.application.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import pe.gob.bnp.services.rest.renabi.common.Constants;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;

@ApiModel(value="Entidad", description="Representa los datos de una entidad")
public class SubsanarEntidadDto {
	@ApiModelProperty(notes = "Correo de la entidad",required=true,value="Ej: correo@muniarequipa.gob.pe")
	private String correo;
	
	@ApiModelProperty(notes = "Dirección de la entidad",required=true,value="Ej: Av. Mario Vargas LLosa 134")
	private String direccion;
	
	@ApiModelProperty(notes = "Teléfono fijo de la entidad",required=false,value="Ej: 044870115")
	private String telefonoFijo;
	
	@ApiModelProperty(notes = "Teléfono móvil de la entidad",required=false,value="Ej: 987740113")
	private String telefonoMovil;
	
	@ApiModelProperty(notes = "Id del Tipo de gestión de la entidad",required=true,value="Ej: 56")
	private String idTipoGestion;
	
	public SubsanarEntidadDto() {
		super();
		this.correo 				= Constants.EMPTY_STRING;
		this.direccion 				= Constants.EMPTY_STRING;
		this.telefonoFijo 			= Constants.EMPTY_STRING;
		this.telefonoMovil 			= Constants.EMPTY_STRING;
		this.idTipoGestion 			= Constants.EMPTY_STRING;
	}
	
	public SubsanarEntidadDto(Entidad objeto) {
		super();
		this.correo = objeto.getCorreo();
		this.direccion = objeto.getDireccion();
		this.telefonoFijo = objeto.getTelefonoFijo();
		this.telefonoMovil = objeto.getTelefonoMovil();
		this.idTipoGestion = objeto.getTipoGestion().getId();
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefonoFijo() {
		return telefonoFijo;
	}

	public void setTelefonoFijo(String telefonoFijo) {
		this.telefonoFijo = telefonoFijo;
	}

	public String getTelefonoMovil() {
		return telefonoMovil;
	}

	public void setTelefonoMovil(String telefonoMovil) {
		this.telefonoMovil = telefonoMovil;
	}

	public String getIdTipoGestion() {
		return idTipoGestion;
	}

	public void setIdTipoGestion(String idTipoGestion) {
		this.idTipoGestion = idTipoGestion;
	}

}
