package pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity;

public class ImagenAnexa {
	private String id;
	private String uuid;
	private String fileName;
	private String fileExtension;
	
	public ImagenAnexa() {
		super();
		this.id = "";
		this.uuid = "";
		this.fileName = "";
		this.fileExtension = "";
	}
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileExtension() {
		return fileExtension;
	}
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	
}
