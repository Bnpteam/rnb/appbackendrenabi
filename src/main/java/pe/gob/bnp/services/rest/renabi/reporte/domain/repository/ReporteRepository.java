package pe.gob.bnp.services.rest.renabi.reporte.domain.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.BibliotecaCabeceraDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.application.dto.VerRptBibliotecaDto;
import pe.gob.bnp.services.rest.renabi.biblioteca.domain.entity.Biblioteca;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseRepository;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteRegistradoresCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasRegistradasCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasVerificadasCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteBibliotecasxEstadoCabeceraDto;
import pe.gob.bnp.services.rest.renabi.reporte.application.dto.ReporteGenericoFiltroDto;

@Repository
public interface ReporteRepository extends BaseRepository<Biblioteca>{
	public List<ReporteRegistradoresCabeceraDto> listarRegistradores(ReporteGenericoFiltroDto filtro);
	public List<ReporteBibliotecasxEstadoCabeceraDto> listarBibliotecasxEstado(ReporteGenericoFiltroDto filtro);
	public List<ReporteBibliotecasVerificadasCabeceraDto> listarBibliotecasVerificadas(ReporteGenericoFiltroDto filtro);
	public List<ReporteBibliotecasRegistradasCabeceraDto> listarBibliotecasRegistradas(ReporteGenericoFiltroDto filtro);
	public VerRptBibliotecaDto listarBibliotecasxId(String bibliotecaId);	

}
