package pe.gob.bnp.services.rest.renabi.entidad.domain.repository;

import org.springframework.stereotype.Repository;

import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseRepository;
import pe.gob.bnp.services.rest.renabi.entidad.domain.entity.Entidad;

@Repository
public interface EntidadRepository extends BaseRepository<Entidad> {

}
