package pe.gob.bnp.services.rest.renabi.common.application;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class MailContentBuilder {
	private TemplateEngine templateEngine;
	
	@Autowired
	public MailContentBuilder(TemplateEngine templateEngine) {
		this.templateEngine = templateEngine;
	}
	
	public String buildTemplateCreacionCuenta(String usuario, String password) {
		Context context = new Context();
		context.setVariable("dniRegistrador", usuario);
		context.setVariable("passwordRegistrador", password);
		return templateEngine.process("creacionCuentaRegistrador", context);
	}
	
	public String buildTemplateReenvioPassword(String usuario, String password) {
		Context context = new Context();
		context.setVariable("dniRegistrador", usuario);
		context.setVariable("passwordRegistrador", password);
		return templateEngine.process("reenvioPasswordRegistrador", context);
	}
	
	public String buildTemplateBibliotecaAprobada(String nombreBiblioteca, String anioDeclaracion) {
		Context context = new Context();
		context.setVariable("nombreBiblioteca", nombreBiblioteca);
		context.setVariable("anioDeclaracion", anioDeclaracion);
		return templateEngine.process("bibliotecaAprobada", context);
	}	
	
	public String buildTemplateBibliotecaObservada(String nombreBiblioteca, String anioDeclaracion) {
		Context context = new Context();
		context.setVariable("nombreBiblioteca", nombreBiblioteca);
		context.setVariable("anioDeclaracion", anioDeclaracion);
		return templateEngine.process("bibliotecaRechazada", context);
	}		
}
