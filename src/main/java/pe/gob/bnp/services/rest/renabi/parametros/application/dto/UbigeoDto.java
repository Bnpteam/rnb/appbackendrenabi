package pe.gob.bnp.services.rest.renabi.parametros.application.dto;

import pe.gob.bnp.services.rest.renabi.common.Constants;

public class UbigeoDto {
	private String codDepartamento;
	private String codProvincia;
	private String codDistrito;
	private String nombre;
	
	public UbigeoDto() {
		super();
		this.codDepartamento = Constants.EMPTY_STRING;
		this.nombre = Constants.EMPTY_STRING;
		this.codProvincia = Constants.EMPTY_STRING;
		this.codDistrito = Constants.EMPTY_STRING;
	}

	public String getCodDepartamento() {
		return codDepartamento;
	}

	public void setCodDepartamento(String codDepartamento) {
		this.codDepartamento = codDepartamento;
	}

	public String getCodProvincia() {
		return codProvincia;
	}

	public void setCodProvincia(String codProvincia) {
		this.codProvincia = codProvincia;
	}

	public String getCodDistrito() {
		return codDistrito;
	}

	public void setCodDistrito(String codDistrito) {
		this.codDistrito = codDistrito;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
}
