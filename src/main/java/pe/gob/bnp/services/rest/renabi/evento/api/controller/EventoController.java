package pe.gob.bnp.services.rest.renabi.evento.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import pe.gob.bnp.services.rest.renabi.common.api.controller.ResponseHandler;
import pe.gob.bnp.services.rest.renabi.common.application.EntityNotFoundResultException;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.evento.application.EventoApplicationService;
import pe.gob.bnp.services.rest.renabi.evento.application.dto.EventoDto;
import pe.gob.bnp.services.rest.renabi.evento.application.dto.EventoEstadoDto;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.EventoDepartamento;

@RestController
@RequestMapping("api/evento/")
@Api(value = "/api/evento/",description="Servicio REST para Eventos de una biblioteca")
@CrossOrigin(origins = "*")
public class EventoController {
	@Autowired
	EventoApplicationService  eventoApplicationService;	
	
	@Autowired
	ResponseHandler responseHandler;	
	
	@RequestMapping(method = RequestMethod.GET, path="/vigentes",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Obtener lista de eventos vigentes", response= List.class, httpMethod = "GET")
	public ResponseEntity<Object> obtenerNoticiasVigentes(
			@RequestParam(value = "estado", defaultValue = "ALL", required=true) String  estado){
		try {
			List<EventoDto> response = eventoApplicationService.obtenerNoticiasVigentes(estado);
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}
	
	@RequestMapping(method = RequestMethod.POST, path="/estado", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Cambiar de estado a evento", response= ResponseTx.class, responseContainer = "Set",httpMethod = "POST")
	@ApiResponses({
        @ApiResponse(code = 200, message = "Transacción finalizó con éxito."),
        @ApiResponse(code = 404, message = "Solicitud contiene errores."),
        @ApiResponse(code = 500, message = "Error general al momento de ejecutar la solicitud"),
	})		
	public ResponseEntity<Object> cambiarEstadoEvento(
			@ApiParam(value = "Datos para actualizar evento", required = true)
			@RequestBody EventoEstadoDto dto) throws Exception {
		try {
			ResponseTx response = eventoApplicationService.cambiarEstado(dto);
			return this.responseHandler.getOkResponseTx(response);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppMessageResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}
	}	
	
	@RequestMapping(method = RequestMethod.GET, path="/totalesDepartamento",produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Obtener la cantidad de eventos vigentes por departamento", response= List.class, httpMethod = "GET")
	public ResponseEntity<Object> obtenerEventosxDepartamento(){
		try {
			List<EventoDepartamento> response = eventoApplicationService.obtenerEventosxDepartamento();
			if (response == null || response.size()==0 ) {
				return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.");
			}
			return this.responseHandler.getOkObjectResponse(response);
		} catch (EntityNotFoundResultException ex) {
			return this.responseHandler.getNotFoundObjectResponse("No se encontraron registros.", ex);
		} catch (IllegalArgumentException ex) {
			return this.responseHandler.getAppCustomErrorResponse(ex.getMessage());
		} catch (Throwable ex) {
			return this.responseHandler.getAppExceptionResponse(ex);
		}		
		
	}	

}
