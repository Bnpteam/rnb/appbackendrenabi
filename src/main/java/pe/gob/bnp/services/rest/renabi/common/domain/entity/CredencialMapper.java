package pe.gob.bnp.services.rest.renabi.common.domain.entity;

import org.springframework.stereotype.Component;

import pe.gob.bnp.services.rest.renabi.registrador.application.dto.CredencialRegistradorDto;

@Component
public class CredencialMapper {
	
	public CredencialRegistradorDto mapperRegistrador(Credencial objeto) {
		return new CredencialRegistradorDto(objeto);
	}
	
	public Credencial reverseMapperRegistrador(CredencialRegistradorDto dto) {
		Credencial objeto = new Credencial();
		objeto.setPassword(dto.getPassword());
		objeto.setUsuario(dto.getUsuario());
		return objeto;
	}	
}
