package pe.gob.bnp.services.rest.renabi.common.application.dto;

import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;

public class ResponseTxDto {
	private int httpStatus;
	private ResponseTx response;
	
	public int getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(int httpStatus) {
		this.httpStatus = httpStatus;
	}

	public ResponseTx getResponse() {
		return response;
	}

	public void setResponse(ResponseTx response) {
		this.response = response;
	}
       

}
