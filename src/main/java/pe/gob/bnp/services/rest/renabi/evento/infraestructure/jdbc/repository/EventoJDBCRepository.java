package pe.gob.bnp.services.rest.renabi.evento.infraestructure.jdbc.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.gob.bnp.services.rest.renabi.common.Util;
import pe.gob.bnp.services.rest.renabi.common.application.ResponseTx;
import pe.gob.bnp.services.rest.renabi.common.infraestructure.repository.BaseJDBCOperation;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.Evento;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.EventoDepartamento;
import pe.gob.bnp.services.rest.renabi.evento.domain.entity.EventoResumen;
import pe.gob.bnp.services.rest.renabi.evento.domain.repository.EventoRepository;

@Repository
public class EventoJDBCRepository extends BaseJDBCOperation implements EventoRepository {

	@Override
	public ResponseTx persist(EventoResumen entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public EventoResumen read(EventoResumen entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<EventoResumen> list(EventoResumen entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx update(EventoResumen entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseTx delete(EventoResumen entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Evento> listarEventosVigentes(String estado) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_EVENTOS.SP_LISTAR_NOTICIAS_VIGENTES("
        		+ "?,?,?) }";
        List<Evento> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setString("E_ESTADO", Util.getString(estado));
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					Evento evento = new Evento();
					evento.setId(rs.getLong("ID_NOTICIA"));
					evento.setDetalleEvento(Util.getString(rs.getString("DETALLE_EVENTO")));
					evento.setTipoEvento(Util.getString(rs.getString("TIPO_EVENTO")));
					evento.setNombreBiblioteca(Util.getString(rs.getString("NOMBRE_BIBLIOTECA")));
					evento.setNombreResponsable(Util.getString(rs.getString("RESPONSABLE")));
					evento.setFechaInicio(Util.sqlDateToString(rs.getDate("FECHA_INICIO")));
					evento.setFechaFin(Util.sqlDateToString(rs.getDate("FECHA_FIN")));
					evento.setIdEstado(Util.getString(rs.getString("ESTADO")));
					evento.setHoraInicio(Util.getString(rs.getString("HORA_INICIO")));
					evento.setHoraFin(Util.getString(rs.getString("HORA_FIN")));
					evento.setCosto(Util.getString(rs.getString("COSTO")));
					evento.setCantidad(Util.getString(rs.getString("CANTIDAD_MAXIMO")));
					evento.setCorreo(Util.getString(rs.getString("CORREO_EVENTO")));
					evento.setTelefono(Util.getString(rs.getString("TELEFONO")));
					evento.setDireccion(Util.getString(rs.getString("DIRECCION")));
					evento.setReferencia(Util.getString(rs.getString("REFERENCIA")));
					evento.setInscripcion(Util.getString(rs.getString("INSCRIPCION")).equals("")?"0":Util.getString(rs.getString("INSCRIPCION")));
					evento.setPaginaWeb(Util.getString(rs.getString("PAGINA_WEB")));
					evento.setDepartamento(Util.getString(rs.getString("DEPARTAMENTO")));
					evento.setProvincia(Util.getString(rs.getString("PROVINCIA")));
					evento.setDistrito(Util.getString(rs.getString("DISTRITO")));
					evento.setTipoBiblioteca(Util.getString(rs.getString("TIPO_BIBLIOTECA")));
					evento.setSubTipoBiblioteca(Util.getString(rs.getString("SUB_TIPO_BIBLIOTECA")));
					response.add(evento);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTx cambiarEstado(Long id, String idEstado) {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_EVENTOS.SP_UPDATE_ESTADO("
        		+ "?,?,?) }";
        ResponseTx response = new ResponseTx();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.setLong("E_ID_NOTICIA", id);
			cstm.setString("E_ESTADO", idEstado);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			response.setCodigoRespuesta(Util.getString(codigoResultado));
			if (response.getCodigoRespuesta().equals("0000")) {
				response.setRespuesta("Actualización exitosa.");
			}else {
				response.setRespuesta("Error al momento de actualizar estado de evento.");
			}
		}catch(SQLException e) {
			e.printStackTrace();
			response.setCodigoRespuesta("9998");
			response.setCodigoRespuesta(e.getMessage());
			try {
				cn.rollback();
			} catch (SQLException e1) {
				response.setCodigoRespuesta("9999");
				response.setCodigoRespuesta(e1.getMessage());
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public List<EventoDepartamento> listarEventosxDepartamento() {
		Connection cn = null;
        String dBTransaction = "{ Call PKG_API_EVENTOS.SP_GET_DPTOS_EVENTO("
        		+ "?,?) }";
        List<EventoDepartamento> response = new ArrayList<>();
		CallableStatement cstm = null;
		ResultSet rs = null;
		try {
			cn = this.getConnection();
			cn.setAutoCommit(false);
			cstm = cn.prepareCall(dBTransaction);
			cstm.registerOutParameter("S_CODIGO_RESULTADO", OracleTypes.VARCHAR);
			cstm.registerOutParameter("S_C_CURSOR",OracleTypes.CURSOR);
			cstm.execute();
			
			String codigoResultado = (String) cstm.getObject("S_CODIGO_RESULTADO");
			if (Util.getString(codigoResultado).equals("0000")) {
				rs = (ResultSet) cstm.getObject("S_C_CURSOR");
				while (rs.next()) {
					EventoDepartamento evento = new EventoDepartamento();
					evento.setIdDepartamento(rs.getString("CODDEP"));
					evento.setDepartamento(Util.getString(rs.getString("DESCRIPCION")));
					evento.setCantidadEventos(Util.getString(rs.getString("EVENTOS")));
					response.add(evento);
				}	
			}
		}catch(SQLException e) {
			e.printStackTrace();
			try {
				cn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}finally {
			this.closeSqlConnections(rs, cstm);
			this.closeConnection(cn);	
		}
		return response;	
	}

	@Override
	public ResponseTx persistM(EventoResumen entity) {
		// TODO Auto-generated method stub
		return null;
	}

}
