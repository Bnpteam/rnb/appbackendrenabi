## SERVICIOS 1 REGISTROS NACIONAL DE BIBLIOTECAS
### Descripción
- Backend de servicios crud el registro nacional de bibliotecas.
- Desde la página principal del RNB http://rnb.bnp.gob.pe/#/home-page  se puede visualizar consultas de bibliotecas a trravés de 2 tipos de usuarios. 
    Usuario Registrador: Usuario encargado de registrar bibliotecas, puede crear su cuenta, inciar session, crear, actualizar(observadas) y listar bibliotecas.
    Usuario Administrador o Peronsal BNP: Usuario encargado de cambiar el estado actual de las bibliotecas registradas ya sea observada, validando verificada etc. Acceso
                                          Módulo de reportes, Módulo director para firmar bibliotecas.


### Configuración

- Para configurar en el ambientes de TEST:

 * Dentro del Proyecto appBackEndRenabi cambiar parametro spring.profiles.active a "development" en el archivo application.properties.
 * Desactivar ssl archivo principal RenabiApplication y parametros server.ssl.
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA: http://200.123.29.183:8085/serviciosrenabi/swagger-ui.html#/
 
- Para configurar en el ambiente de PROD:

 * Dentro del Proyecto appBackEndRenabi cambiar parametro spring.profiles.active a "production" en el archivo application.properties.
 * Activar ssl archivo principal RenabiApplication y parametros server.ssl. (cambio de puerto)
 * IDE - STS 4.0.
 * TOMCAT 9.0 EMBEBIDO.
 * JDK 1.8.0.261
 * RUTA SIN SSL:  http://rnb.bnp.gob.pe:8085/serviciosrenabi/swagger-ui.html#/
 * RUTA CON SSL: https://apirenabi.bnp.gob.pe:9882/serviciosrenabi/swagger-ui.html#/ 

NOTA: actualmente se utiliza una version con SSL para la relación con el sistema Portal SNB

### Despliegue Microservicio

- Utilizar el archivo renabi-be.service como base para desplegar el microservicio
- Pasos de despliegue:
    1. Generar jar del aplicativo appBakEnRenabi (mave clean build install).
    2. Copiar el jar dentro del servidor test o prod que desee en la ruta indicada por el archivo renabi-be.service. (dar permisos escritura y lectura al jar)
    3. Generar en caso no exista un servicio en linux con el nombre del archivo renabi-be.service.
        * Para crear servicio en linux, acceder a la ruta /etc/systemd validar los servicios existentes y copiar con el comando "cp" servicio existente actualizar       las rutas para que apunten al jar creado. 
    4. Una vez creado o actualizado el archivo .service reiniciarlo de preferencia utilizar los comando:
        systemctl status renbai-be.service (ver el estado actual)
        systemctl stop renabi-be.service (detener servicio)
        systemctl start renabi-be.service (inciar servicio)
    
    5. Finalmente validar los servicios desde la ruta generado por el swagger.

 
#### version: v1.0.2